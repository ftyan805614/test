package com.sports.wallpaper.utils

import android.annotation.SuppressLint
import android.view.View
import com.sports.commonn.mvvm.ui.widget.CommonTitleBar
import com.sports.commonn.mvvm.ui.widget.CustomToolbar
import com.sports.wallpaper.R

/**
 *Create By jane on 2020-03-02
 */
object TitleBarUtil {

    /**
     * 标题栏
     * 左边带有返回键图标
     */
    @SuppressLint("ResourceAsColor")
    fun initTitle(customerTitlebar: CustomToolbar?, title: String?, clickListener: View.OnClickListener? = null): CustomToolbar? {
        customerTitlebar?.setLeftType(CommonTitleBar.TYPE_LEFT_TEXTVIEW)
        customerTitlebar?.setMainTitle(title)
        customerTitlebar?.setLeftTextSize(18f)
        customerTitlebar?.setLeftTextColor(R.color.skin_3F5075_FBFBFB)
        customerTitlebar?.setLeftOnClick(clickListener)
        customerTitlebar?.setPaddingTopTitile()
        return customerTitlebar
    }


    /**
     * 标题栏
     * 右边文字
     */
    fun initTitleByRightTxt(customerTitlebar: CustomToolbar?, title: String?, clickListener: View.OnClickListener? = null): CustomToolbar? {
        initTitle(customerTitlebar, title, clickListener)
        //创建右边文字按钮
        customerTitlebar?.setRightType(CommonTitleBar.TYPE_RIGHT_TEXTVIEW)
        return customerTitlebar
    }


    /**
     * 标题栏
     * 右边图片按钮
     */
    fun initTitleByRightImage(customerTitlebar: CustomToolbar?, title: String?, clickListener: View.OnClickListener? = null): CustomToolbar? {
        initTitle(customerTitlebar, title, clickListener)
        //创建右边图片按钮
        customerTitlebar?.setRightType(CommonTitleBar.TYPE_RIGHT_IMAGEBUTTON)
        return customerTitlebar
    }


    /**
     * 标题栏
     * 右边自定义View
     */
    fun initTitleByRightView(customerTitlebar: CustomToolbar?, title: String?, clickListener: View.OnClickListener? = null): CustomToolbar? {
        initTitle(customerTitlebar, title, clickListener)
        //创建右边view视图
        customerTitlebar?.setRightType(CommonTitleBar.TYPE_RIGHT_CUSTOM_VIEW)
        return customerTitlebar
    }


    /**
     * 标题栏
     * 中间自定义View
     */
    fun initTitleByCenterView(customerTitlebar: CustomToolbar?, title: String?, clickListener: View.OnClickListener? = null): CustomToolbar? {
        initTitle(customerTitlebar, title, clickListener)
        //创建右边view视图
        customerTitlebar?.setCenterType(CommonTitleBar.TYPE_CENTER_TEXTVIEW)
        return customerTitlebar
    }

    /**
     * 标题栏
     * 右边图片按钮
     */
    fun initTitleByRightImage2(customerTitlebar: CustomToolbar?, title: String?, clickListener: View.OnClickListener? = null): CustomToolbar? {
        //创建右边图片按钮
        customerTitlebar?.setRightType(CommonTitleBar.TYPE_RIGHT_IMAGEBUTTON_2)
        initTitle(customerTitlebar, title, clickListener)
        customerTitlebar?.setCenterType(CommonTitleBar.TYPE_CENTER_TEXTVIEW)
        customerTitlebar?.setMainTitle(title)
        customerTitlebar?.setMainTitleSize(14f)
        customerTitlebar?.setMainTitleColor(R.color.skin_3F5075_FBFBFB)
        return customerTitlebar
    }


}
