package com.sports.wallpaper.utils

import android.app.Dialog
import android.content.Context
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import com.sports.commonn.utils.ResUtils
import com.sports.wallpaper.R

/**
 * author:Averson
 * createTime:2021/4/3
 * descriptions:
 *
 **/
object DialogUtils {
    interface StringArrayDialogCallback {
        fun onItemClick(text: String?, tag: Int)
    }

    fun showStringArrayDialog(
        context: Context?,
        array: Array<Int?>,
        callback: StringArrayDialogCallback?
    ) {
        val dialog = Dialog(context!!, R.style.dialog)
        dialog.setContentView(R.layout.dialog_string_array)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val window = dialog.window
        window!!.setWindowAnimations(R.style.bottomToTopAnim)
        val params = window.attributes
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        params.height = WindowManager.LayoutParams.WRAP_CONTENT
        params.gravity = Gravity.BOTTOM
        window.attributes = params
        val container = dialog.findViewById<View>(R.id.container) as LinearLayout
        val itemListener = View.OnClickListener { v: View ->
            val textView = v as TextView
            if (callback != null) {
                callback.onItemClick(textView.text.toString(), v.getTag() as Int)
            }
            dialog.dismiss()
        }
        var i = 0
        val length = array.size
        while (i < length) {
            val textView = TextView(context)
            textView.layoutParams =
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ResUtils.dp2px(50f))
            textView.setTextColor(-0xcdcdce)
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
            textView.gravity = Gravity.CENTER
            textView.setText(array[i]!!)
            textView.tag = array[i]
            textView.setOnClickListener(itemListener)
            container.addView(textView)
            if (i != length - 1) {
                val v = View(context)
                v.layoutParams =
                    LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ResUtils.dp2px(1f))
                v.setBackgroundColor(-0xa0a0b)
                container.addView(v)
            }
            i++
        }
        dialog.findViewById<View>(R.id.btn_cancel)
            .setOnClickListener { v: View? -> dialog.dismiss() }
        dialog.show()
    }

}