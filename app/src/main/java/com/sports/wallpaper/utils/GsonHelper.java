package com.sports.wallpaper.utils;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * 创建时间：2019/8/28
 * 编写人：Averson
 * 功能描述：
 */


public class GsonHelper {

    private Gson gson;

    private GsonHelper() {
        getGson();
    }

    public Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static GsonHelper getInstance() {
        return GsonHelperHolder.gsonHelper;
    }

    private static class GsonHelperHolder {
        private static GsonHelper gsonHelper = new GsonHelper();
    }

    public <T> T getEntity(String text, Class<T> classType) {
        T t = null;
        try {
            t = getGson().fromJson(text, classType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public <T> T getEntity(String text, Type typeOfT) {
        T t = null;
        try {
            t = getGson().fromJson(text, typeOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public String toJson(Object obj) {
        return getGson().toJson(obj);
    }
}
