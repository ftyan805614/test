package com.sports.wallpaper

import android.app.Application
import com.lzy.imagepicker.ImagePicker
import com.lzy.imagepicker.loader.GlideImageLoader
import com.lzy.imagepicker.view.CropImageView
import com.sports.commonn.YaboLib
import com.sports.wallpaper.ui.DefaultLoadStatusFactory
import org.litepal.LitePal


/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        LitePal.initialize(this)

        YaboLib.init(this, BuildConfig.DEBUG, BuildConfig.APPLICATION_ID)
//            .setCurrentVerCode(versionCode)
            .setStatusFactory(DefaultLoadStatusFactory()) //设置全局空数据加载
            .setTheme(R.style.AppTheme)

        initImagePicker()
    }

    private fun initImagePicker(){
        val imagePicker = ImagePicker.getInstance()
        imagePicker.imageLoader = GlideImageLoader()   //设置图片加载器
        imagePicker.isShowCamera = true  //显示拍照按钮
        imagePicker.isMultiMode = false
        imagePicker.isCrop = false        //允许裁剪（单选才有效）
        imagePicker.isSaveRectangle = true //是否按矩形区域保存
        imagePicker.style = CropImageView.Style.CIRCLE  //裁剪框的形状
        imagePicker.focusWidth = 800   //裁剪框的宽度。单位像素（圆形自动取宽高最小值）
        imagePicker.focusHeight = 800  //裁剪框的高度。单位像素（圆形自动取宽高最小值）
        imagePicker.outPutX = 800//保存文件的宽度。单位像素
        imagePicker.outPutY = 800//保存文件的高度。单位像素
    }
}