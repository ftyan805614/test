package com.sports.wallpaper.manger


/**
 * 描述：页面跳转
 * 日期：2020-03-13
 */
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager
import com.sports.commonn.utils.ToastUtil
import com.sports.wallpaper.ui.MainActivity

object IntentManager {


    val ISLOGIN_KEY = "isLogin"


    fun goLoginActivity(
        isRegister: Boolean? = false,
        message: String? = "",
        options: Bundle? = null
    ) {
        try {
            if (!TextUtils.isEmpty(message)) {
                ToastUtil.show(message)
            }
            var bundle: Bundle? = null
            if (null != options) {
                bundle = options
            }
            if (null == bundle) {
                bundle = Bundle()
            }
            bundle.putBoolean(ISLOGIN_KEY, true) //启动页到登录
            if (isRegister == true) {
                bundle.putBoolean("register", isRegister == true)
            }

            val intent =
                Intent(TopLifecyclerOwnerManager.instance.curActivity, MainActivity::class.java)
            intent.putExtras(bundle)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            TopLifecyclerOwnerManager.instance.curActivity?.startActivity(intent)
            TopLifecyclerOwnerManager.instance.curActivity?.finish()
        } catch (e: Exception) {
            LogUtils.e(e)
        }

    }
}