package com.sports.wallpaper.widget.dialog

import android.os.Handler
import android.os.Looper
import androidx.core.os.bundleOf
import androidx.navigation.NavDestination
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.ui.PopupController

/**
 *Create By Albert on 2021/3/2
 * 一次只显示一个dialog
 */
object DialogTask {

    const val AragmentsKey = "DialogTask_AragmentsKey"

    private val tasksShowQueen = LinkedHashMap<String, Any>()

    private val handlder = Handler(Looper.getMainLooper())


    fun addTask(eqKey: String?, builder: DialogBuilder?) {
        if (eqKey.isNullOrEmpty() || builder == null) return
        if (tasksShowQueen.size == 0) {
            builder.showDialog()
        }
        tasksShowQueen[eqKey] = builder
    }

    fun addTask(eqKey: String?, dialogDestId: Int?) {
        if (eqKey.isNullOrEmpty() || dialogDestId == null) return
        if (tasksShowQueen.size == 0) {
            findNavController()?.navigate(dialogDestId, bundleOf(AragmentsKey to eqKey))
        }
        tasksShowQueen[eqKey] = dialogDestId
    }

    fun addTask(eqKey: String?, controller: PopupController?) {
        if (eqKey.isNullOrEmpty() || controller == null) return
        if (tasksShowQueen.size == 0) {
            controller.show()
        }
        tasksShowQueen[eqKey] = controller
    }

    fun checkTask(){
        if (tasksShowQueen.size == 0) return
        handlder.post {
            findNavController()?.currentDestination?.let { showNext(it) }
        }
    }

    fun moveToNext(currentEqKey: String?) {
        if (currentEqKey == null) return
        if (!tasksShowQueen.containsKey(currentEqKey)) return
        tasksShowQueen.remove(currentEqKey)
        handlder.post {
            findNavController()?.currentDestination?.let { showNext(it) }
        }
    }

    private fun showNext(target: NavDestination) {
        if (tasksShowQueen.size == 0) return
//        if (target.id == R.id.bobSportFragment) {
//            val key = tasksShowQueen.keys.firstOrNull()
//            when (val value = tasksShowQueen[key]) {
//                is DialogBuilder -> {
//                    value.showDialog()
//                }
//                is Int -> {
//                    findNavController()?.navigateSF(value, bundleOf(AragmentsKey to key))
//                }
//                is PopupController -> {
//                    value.show()
//                }
//            }
//        }
    }
}