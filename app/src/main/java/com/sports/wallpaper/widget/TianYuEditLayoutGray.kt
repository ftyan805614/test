package com.sports.wallpaper.widget

import android.animation.Animator
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import com.sports.commonn.memory.DataManager
import com.sports.commonn.utils.EditTextUtils
import com.sports.commonn.utils.ResUtils
import com.sports.wallpaper.R


/**
 * created by Albert
 */
class TianYuEditLayoutGray : RelativeLayout {

    internal var context: Context
    var editText: EditText? = null
        private set
    private var mWidth: Int = 0
    private var gradualView: EditTextLine? = null
    private var linView: LinViewGray? = null
    private var deleteWith = 0
    private var deleteView: ImageView? = null
    private var eyeView: ImageView? = null
    internal var lineColor: Int = 0
    private var showDelete = true
    private var showEye = false
    private var isPassword = true //是否时密码模式
    private var isInit = false
    internal var show_line = true
    private var eyeCloseIco = R.mipmap.com_icon_eyeoff
    private var eyeOpenIco = R.mipmap.com_icon_eyeon
    private val imageView: ImageView
        get() {
            val imageView = ImageView(context)
            val layoutParamsGradualDelete = LayoutParams(deleteWith, deleteWith)
            layoutParamsGradualDelete.addRule(CENTER_VERTICAL)
            layoutParamsGradualDelete.addRule(ALIGN_PARENT_RIGHT)
            layoutParamsGradualDelete.setMargins(0, 0, ResUtils.dp2px(5f), 0)
            imageView.layoutParams = layoutParamsGradualDelete
            addView(imageView)
            return imageView
        }

    constructor(context: Context) : super(context) {
        this.context = context
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.context = context
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.TianYuEditLayoutGray)
        init(typedArray)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.context = context
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.TianYuEditLayoutGray)
        init(typedArray)

    }

    fun getLineColor(): Int {
        return lineColor
    }

    fun setLineColor(lineColor: Int) {
        this.lineColor = lineColor
        if (linView != null) {
            linView?.setMlinecolor(lineColor)
        }

    }

    private fun init(typedArray: TypedArray?) {
        deleteWith = dp2px(16f)
        deleteWith = dp2px(16f)
        if (typedArray != null) {
            for (i in 0 until typedArray.indexCount) {
                when (val attr = typedArray.getIndex(i)) {
                    R.styleable.TianYuEditLayoutGray_show_delete -> {
                        showDelete = typedArray.getBoolean(attr, true)
                    }
                    R.styleable.TianYuEditLayoutGray_show_eye -> {
                        showEye = typedArray.getBoolean(attr, false)
                    }
                    R.styleable.TianYuEditLayoutGray_show_line -> {
                        show_line = typedArray.getBoolean(attr, true)
                    }
                    R.styleable.TianYuEditLayoutGray_eye_close_icon -> {
                        eyeCloseIco = typedArray.getResourceId(attr, R.mipmap.com_icon_eyeoff)
                    }
                    R.styleable.TianYuEditLayoutGray_eye_open_icon -> {
                        eyeOpenIco = typedArray.getResourceId(attr, R.mipmap.com_icon_eyeon)
                    }
                }
            }
            typedArray.recycle()
        }


        DataManager.appIsNightTheme
        if (null != DataManager.appIsNightTheme.value) {
            val isnight = DataManager.appIsNightTheme.value
            if (isnight == true) {
                setLineColor(ResUtils.getColor(R.color.skin_3398A3B1))
            } else {
                setLineColor(ResUtils.getColor(R.color.skin_3398A3B1))
            }
        } else {
            setLineColor(ResUtils.getColor(R.color.skin_3398A3B1))
        }

        if (show_line) {
            linView = LinViewGray(context, getLineColor())
            val layoutParamsLinView =
                LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ResUtils.dp2px(0.5f))
            layoutParamsLinView.addRule(ALIGN_PARENT_BOTTOM)
            layoutParamsLinView.setMargins(0, 0, 0, 0)
            linView?.layoutParams = layoutParamsLinView
            addView(linView)

            gradualView = EditTextLine(context)
            val layoutParamsGradualView =
                LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ResUtils.dp2px(0.5f))
            layoutParamsGradualView.addRule(ALIGN_PARENT_BOTTOM)
            layoutParamsGradualView.setMargins(0, 0, 0, 0)
            gradualView?.layoutParams = layoutParamsGradualView
            addView(gradualView)
            gradualView?.visibility = View.GONE
        }


        deleteView = imageView
        deleteView?.setImageDrawable(ResUtils.getDrawable(R.mipmap.bet_close))
        deleteView?.setOnClickListener {
            if (editText != null) {
                editText?.setText("")
            }
        }
        deleteView?.visibility = View.GONE
        if (showEye) {
            eyeView = imageView
            eyeView?.setImageDrawable(ResUtils.getDrawable(eyeCloseIco))
            eyeView?.setOnClickListener {
                if (editText != null) {
                    isPassword = !isPassword
                    if (isPassword) {
                        eyeView?.setImageDrawable(ResUtils.getDrawable(eyeCloseIco))
                    } else {
                        eyeView?.setImageDrawable(ResUtils.getDrawable(eyeOpenIco))
                    }
                    EditTextUtils.passwordEdit(editText, isPassword)
                }
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }


    @SuppressLint("ObjectAnimatorBinding")
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (!isInit) {
            isInit = true
            val childCount = childCount
            for (i in 0 until childCount) {
                val childAt = getChildAt(i)
                mWidth = width
                if (childAt is EditText) {
                    if (childAt.getMeasuredWidth() == width) {
                        val marginLayoutParams =
                            childAt.getLayoutParams() as MarginLayoutParams
                        marginLayoutParams.setMargins(0, 0, deleteWith, 0)
                        childAt.setLayoutParams(marginLayoutParams)
                        requestLayout()
                    }
                    editText = childAt
                    editText?.background = null
                    if (showDelete) {
                        childAt.addTextChangedListener(object : TextWatcher {
                            override fun beforeTextChanged(
                                s: CharSequence,
                                start: Int,
                                count: Int,
                                after: Int
                            ) {

                            }

                            override fun onTextChanged(
                                s: CharSequence,
                                start: Int,
                                before: Int,
                                count: Int
                            ) {

                            }

                            override fun afterTextChanged(s: Editable) {
                                if (s.isEmpty()) {
                                    deleteView?.visibility = View.GONE
                                } else {
                                    deleteView?.visibility = View.VISIBLE
                                }
                            }
                        })
                    }
                    childAt.setOnFocusChangeListener { _, hasFocus ->
                        if (hasFocus) {
                            if (showDelete && editText?.text?.length ?: 0 > 0) {
                                deleteView?.visibility = View.VISIBLE
                            }
                            if (show_line) {
                                val animator1 = ObjectAnimator.ofFloat(
                                    gradualView,
                                    "TranslationX",
                                    -width.toFloat(),
                                    0f
                                )
                                animator1.duration = 1000
                                animator1.addListener(object : Animator.AnimatorListener {
                                    override fun onAnimationStart(animation: Animator) {
                                        gradualView?.visibility = View.VISIBLE
                                    }

                                    override fun onAnimationEnd(animation: Animator) {

                                    }

                                    override fun onAnimationCancel(animation: Animator) {

                                    }

                                    override fun onAnimationRepeat(animation: Animator) {

                                    }
                                })
                                animator1.start()
                            }

                        } else {
                            if (show_line) {
                                deleteView?.visibility = View.GONE
                                val animator1 = ObjectAnimator.ofFloat(
                                    gradualView,
                                    "TranslationX",
                                    0f,
                                    -width.toFloat()
                                )
                                animator1.duration = 1000
                                animator1.addListener(object : Animator.AnimatorListener {
                                    override fun onAnimationStart(animation: Animator) {

                                    }

                                    override fun onAnimationEnd(animation: Animator) {
                                        gradualView?.visibility = View.GONE
                                    }

                                    override fun onAnimationCancel(animation: Animator) {

                                    }

                                    override fun onAnimationRepeat(animation: Animator) {

                                    }
                                })
                                animator1.start()
                            }
                        }
                    }

                    if (layoutParams.height == LayoutParams.WRAP_CONTENT) {
                        layoutParams.height = childAt.getMeasuredHeight()
                        requestLayout()
                    }
                }
            }
        }

    }

    private fun dp2px(dpValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }
}
