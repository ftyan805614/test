package com.sports.wallpaper.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.sports.commonn.utils.ResUtils;
import com.sports.wallpaper.R;

/**
 * created by Albert
 */
public class LinViewGray extends View {

    private Paint paint;
    private int mlinecolor;

    public LinViewGray(Context context, int linecolor) {
        super(context);
        mlinecolor=linecolor;
        init();
    }

    public LinViewGray(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LinViewGray(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();//"#6b6b6b"
        if (mlinecolor!=0){
            paint.setColor(mlinecolor);
        }else {
            paint.setColor(ResUtils.getColor(R.color.skin_3398A3B1));
        }
        paint.setStrokeWidth(10);
    }

    public void setMlinecolor(int mlinecolor) {
        this.mlinecolor = mlinecolor;
        if (mlinecolor!=0){
            paint.setColor(mlinecolor);
        }else {
            paint.setColor(ResUtils.getColor(R.color.skin_3398A3B1));
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int with = getWidth();
        int height = getHeight();
        RectF rectF = new RectF(0, 0, with, height);
        canvas.drawRect(rectF, paint);
    }
}
