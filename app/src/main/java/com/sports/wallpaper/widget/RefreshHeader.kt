package com.sports.wallpaper.widget

import android.content.Context
import android.util.AttributeSet
import com.scwang.smartrefresh.header.MaterialHeader
import com.scwang.smartrefresh.header.internal.MaterialProgressDrawable
import com.sports.commonn.utils.ResUtils
import com.sports.wallpaper.R

/**
 *Create By Albert on 2019-11-04
 */
class RefreshHeader(context: Context?, attrs: AttributeSet?) : MaterialHeader(context, attrs) {
    init {
        val thisGroup = this
        mProgress = MaterialProgressDrawable(this)
        mProgress.setBackgroundColor(ResUtils.getColor(R.color.skin_main_color))
        mProgress.alpha = 255
        mProgress.setColorSchemeColors(ResUtils.getColor(R.color.skin_FFFFFF))
        mCircleView = com.scwang.smartrefresh.header.material.CircleImageView(context, ResUtils.getColor(R.color.skin_main_color))
        mCircleView.setImageDrawable(mProgress)
        thisGroup.addView(mCircleView)
    }

}