package com.sports.wallpaper.widget.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.text.method.ScrollingMovementMethod
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.jeremyliao.liveeventbus.LiveEventBus
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.popBackStackDialog
import com.sports.commonn.mvvm.ui.widget.CustomDialog
import com.sports.commonn.utils.ResUtils
import com.sports.commonn.utils.ScreenUtil
import com.sports.commonn.utils.TextUtil
import com.sports.wallpaper.R
import com.sports.wallpaper.databinding.CoreBaseDialogBinding

/**
 *Create By Albert on 2020/4/21
 */
class AppCustomDialog : CustomDialog<CoreBaseDialogBinding>() {

    var dialogBuilder: DialogBuilder? = null

    companion object {
        const val argument = "AppCustomDialog_argument"
    }

    override fun contentViewLayoutId(): Int = R.layout.core_base_dialog

    override fun initViewModel(bind: CoreBaseDialogBinding) {
        dialogBuilder = arguments?.getParcelable(argument) as? DialogBuilder
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setWindowAnimations(R.style.picker_view_scale_anim)
        dialog?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        bind.root.setOnClickListener {
            closeDialog()
        }
    }

    override fun initObserve() {
        super.initObserve()
        LiveEventBus.get(argument, DialogBuilder.Info::class.java).observeSticky(this, {
            if (it != null) {
                initContent(it)
            }
        })
    }

    private fun initContent(info: DialogBuilder.Info) {
        val width = ScreenUtil.screenWidth
        val height = ScreenUtil.screenHeight
        val smallHeight = if (width > height) {
            height
        } else {
            width
        }
        if (info.contentLayout != null) {
            bind.root.findViewById<FrameLayout>(R.id.fl_container).removeAllViews()
            bind.root.findViewById<FrameLayout>(R.id.fl_container)
                .addView(info.contentLayout)
            return
        }
        val dialogHeight = smallHeight - ResUtils.dp2px(60f)
        if (info.customBankSelectCallBack != null) {
//            addClickListener(R.id.et_bank_name, info.customBankSelectCallBack!!)
        }
        //标题
        val tvBaseDialogTitle = bind.root.findViewById<TextView>(R.id.tv_base_dialog_title)
        //右上角的关闭icon
//        bind.root.findViewById<View>(R.id.iv_close)?.setOnClickListener {
//            closeDialog()
//        }

        if (!TextUtils.isEmpty(dialogBuilder?.title) && tvBaseDialogTitle != null) {
            tvBaseDialogTitle.text = dialogBuilder?.title
            tvBaseDialogTitle.visibility = View.VISIBLE
        } else {
            tvBaseDialogTitle.visibility = View.GONE
        }

        //内容
        if (null != info.customContentView) {
            val container = bind.root.findViewById<ViewGroup>(R.id.llayContentLayout)
            val layoutParams = container.layoutParams
            layoutParams?.width = LinearLayout.LayoutParams.MATCH_PARENT
            layoutParams?.height = LinearLayout.LayoutParams.WRAP_CONTENT
            container?.layoutParams = layoutParams
            container?.removeAllViews()
            container?.addView(info.customContentView)
        } else {
            val tvDialogContent = bind.root.findViewById<TextView>(R.id.tv_dialog_content)
            tvDialogContent?.gravity = dialogBuilder?.contentGravity ?: Gravity.CENTER
            if (TextUtils.isEmpty(dialogBuilder?.content)) {
                tvDialogContent?.visibility = View.GONE
            } else {
                tvDialogContent?.movementMethod = ScrollingMovementMethod.getInstance()
                val contentWidth = TextUtil.getTextWidth(context, dialogBuilder?.content, 16)
                if (contentWidth > dialogHeight) {   //超过屏幕宽度，则认为是多行显示
                    val llayContentLayout =
                        bind.root.findViewById<ViewGroup>(R.id.llayContentLayout)
                    val layoutParams = llayContentLayout.layoutParams as ViewGroup.LayoutParams
                    layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT
                    layoutParams.height =
                        LinearLayout.LayoutParams.WRAP_CONTENT//ResUtils.dp2px(160f);
                    llayContentLayout.layoutParams = layoutParams
                } else {
                    tvDialogContent?.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
                }
                tvDialogContent?.visibility = View.VISIBLE
                tvDialogContent?.text = dialogBuilder?.content
            }
        }

        //背景
        val base_dialog_container = bind.root.findViewById<LinearLayout>(R.id.base_dialog_container)
        if (0 != dialogBuilder?.backgroundColor) {
            base_dialog_container?.setBackgroundColor(dialogBuilder?.backgroundColor!!)
        }

        if (null != info.backgroundDrawable) {
            base_dialog_container?.background = info.backgroundDrawable
        }

        //取消按钮
        val tvBaseDialogCancel = bind.root.findViewById<TextView>(R.id.tv_base_dialog_cancel)
        val llCancelBtn = bind.root.findViewById<View>(R.id.ll_btn_cancel)
        val llConfirmBtn = bind.root.findViewById<View>(R.id.ll_btn_confirm)
        if (dialogBuilder?.leftButtonVisible == true) {
            llCancelBtn?.visibility = View.VISIBLE
        } else {
            llCancelBtn?.visibility = View.GONE
        }
        //确定按钮
        val tvBaseDialogConfirm = bind.root.findViewById<TextView>(R.id.tv_base_dialog_confirm)
        if (!TextUtils.isEmpty(dialogBuilder?.leftButtonText)) {
            tvBaseDialogCancel?.text = dialogBuilder?.leftButtonText
            tvBaseDialogCancel?.setOnClickListener {
                closeDialog()
                if (null != info.leftButtonCallback) {
                    info.leftButtonCallback?.onClick(this, it)
                }
            }
        } else {
            if (null != tvBaseDialogConfirm) {
                llCancelBtn?.visibility = View.GONE
                val layoutParams = tvBaseDialogConfirm.layoutParams as? LinearLayout.LayoutParams
                if (layoutParams != null) {
                    layoutParams.leftMargin = 0
                    tvBaseDialogConfirm.layoutParams = layoutParams
                }
            }
        }

        if (!TextUtils.isEmpty(dialogBuilder?.rightButtonText)) {
            tvBaseDialogConfirm?.text = dialogBuilder?.rightButtonText
            tvBaseDialogConfirm?.setOnClickListener {
                closeDialog()
                if (null != info.rightButtonCallback) {
                    info.rightButtonCallback?.onClick(this, it)
                }
            }
        } else {
            llConfirmBtn?.visibility = View.GONE
        }
        if (dialogBuilder != null && dialogBuilder?.leftButtonColor != null && dialogBuilder?.leftButtonColor != 0) {
            tvBaseDialogCancel?.setTextColor(ResUtils.getColor(dialogBuilder?.leftButtonColor!!))
        }
        if (dialogBuilder?.rightButtonColor != 0 && dialogBuilder?.rightButtonColor != null && dialogBuilder?.rightButtonColor != 0) {
            tvBaseDialogConfirm?.setTextColor(ResUtils.getColor(dialogBuilder?.rightButtonColor!!))
        }
        if (info.leftDrawable != null) {
            tvBaseDialogCancel?.background = info.leftDrawable
        }
        if (info.rightDrawable != null) {
            tvBaseDialogConfirm?.background = info.rightDrawable
        }
    }


    private fun closeDialog() {
        findNavController().popBackStackDialog()
    }

    override fun dismiss() {
        super.dismiss()
        DialogTask.moveToNext(dialogBuilder?.eqKey)
    }
}