package com.sports.wallpaper.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sports.commonn.mvvm.ui.widget.LoadingView;


/**
 * 加载效果
 * Created by jane on 2018/11/30.
 */

public class LoadingViewLayout extends LinearLayout {
    private LoadingView loadingView;

    public LoadingViewLayout(Context context) {
        super(context);
        addView(createView(context));
    }

    public LoadingViewLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        addView(createView(context));
    }

    public LoadingViewLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addView(createView(context));
    }


    public View createView(Context context) {
        loadingView = new LoadingView();
        View view = loadingView.createView(context,false);
        loadingView.cancelBg();
        return view;
    }

    public void cancelBg(){
        if(null != loadingView){
            loadingView.cancelBg();
        }
    }


    public void setLayoutBgColor(int color) {
        if(null != loadingView){
            loadingView.setLayoutBgColor(color);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }


    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == VISIBLE) {
            super.setVisibility(visibility);
            start();
        } else {
            stop();
        }

    }

    public void stop() {
        if (null != loadingView) {
            loadingView.stop();

        }
    }

    public void start() {
        if (null != loadingView) {
            loadingView.start();
        }
    }

    public void setMessage(String message){
        if(null != loadingView){
            loadingView.setMessage(message);
        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stop();
    }

}
