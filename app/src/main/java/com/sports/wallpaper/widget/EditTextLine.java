package com.sports.wallpaper.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.sports.wallpaper.R;


/**
 * created by Albert
 */

public class EditTextLine extends ViewGroup {

    private int mViewWidth = 0;//view的宽度
    private int mViewHeight = 0;//view的高度
    private int leftAndRightMargin = 0;//左右两边的间距,可以用户自定义
    private int mLevelBarHeight = 30;//等级条的高度
    private float position = 0.5f;
    //创建一个画笔
    private Paint mPaint = new Paint();
    private LinearGradient linearGradient;

    //初始化画笔
    private void initPaint() {
        mPaint.setStyle(Paint.Style.FILL);  //设置画笔模式为填充
        mPaint.setStrokeWidth(10f);
    }

    public EditTextLine(Context context) {
        this(context, null);
    }

    public EditTextLine(Context context, AttributeSet attrs) {
        super(context, attrs);

        setBackgroundDrawable(new BitmapDrawable());
        initPaint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        //默认测量模式为EXACTLY，否则请使用上面的方法并指定默认的宽度和高度
        mViewWidth = MeasureSpec.getSize(widthMeasureSpec);
        mViewHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(mViewWidth, mViewHeight);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        //设置滑块在父布局的位置
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawRiskLevelBar(canvas, position);//画条形bar
    }

    /**
     * 画风险等级条
     *
     * @param canvas
     */
    private void drawRiskLevelBar(Canvas canvas, float postiont) {
        linearGradient = new LinearGradient(0, 0, getMeasuredWidth(), 0,
                new int[]{getResources().getColor(R.color.skin_main_color), getResources().getColor(R.color.skin_main_color), getResources().getColor(R.color.skin_main_color)},
                new float[]{0, position, 1}, LinearGradient.TileMode.CLAMP);
        //渐变色
        mPaint.setShader(linearGradient);
        //矩形左上角和右下角的点的坐标
        RectF rectF = new RectF(leftAndRightMargin, mViewHeight / 2 - mLevelBarHeight / 2, mViewWidth - leftAndRightMargin, mViewHeight / 2 + mLevelBarHeight / 2);
        canvas.drawRoundRect(rectF, 4, 4, mPaint);
    }
}