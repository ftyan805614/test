package com.sports.wallpaper.widget.dialog

import android.graphics.drawable.Drawable
import android.os.Parcelable
import android.view.Gravity
import android.view.View
import androidx.core.os.bundleOf
import com.apkfuns.logutils.LogUtils
import com.jeremyliao.liveeventbus.LiveEventBus
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.navigateSingleDialog
import com.sports.commonn.mvvm.ui.widget.CustomDialog
import com.sports.wallpaper.R
import kotlinx.android.parcel.Parcelize


@Parcelize
class DialogBuilder(
    var title: String? = null,
    var content: String? = null,
    var leftButtonVisible: Boolean = true,
    var leftButtonText: String = "取消",
    var rightButtonText: String = "确定",
    var leftButtonColor: Int = 0,
    var rightButtonColor: Int = 0,
    var backgroundColor: Int = 0,
    var contentGravity: Int = Gravity.CENTER,
    var eqKey: String? = null,
) : Parcelable {

    var info = Info()

    constructor(builder: Builder) : this() {
        this.title = builder.title
        this.title = builder.title
        this.content = builder.content
        this.leftButtonVisible = builder.leftButtonVisible
        this.leftButtonText = builder.leftButtonText
        this.rightButtonText = builder.rightButtonText
        this.leftButtonColor = builder.leftButtonColor
        this.rightButtonColor = builder.rightButtonColor
        this.backgroundColor = builder.backgroundColor
        this.contentGravity = builder.contentGravity
        this.eqKey = builder.eqKey

        info.leftDrawable = builder.leftDrawable
        info.rightDrawable = builder.rightDrawable
        info.backgroundDrawable = builder.backgroundDrawable
        info.customContentView = builder.customContentView
        info.leftButtonCallback = builder.leftButtonCallback
        info.rightButtonCallback = builder.rightButtonCallback
        info.customBankSelectCallBack = builder.customBankSelectCallBack
        info.contentLayout = builder.contentLayout
    }


    companion object {
        inline fun build(block: Builder.() -> Unit): DialogBuilder {
            val builder = Builder().apply(block)
            return builder.build()
        }
    }

    fun showDialog() {
        try {
            LiveEventBus.get(AppCustomDialog.argument).post(info)
            findNavController()?.navigate(R.id.appCustomDialog, bundleOf(AppCustomDialog.argument to this))
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }

    fun showSingeDialog() {
        try {
            LiveEventBus.get(AppCustomDialog.argument).post(info)
            findNavController()?.navigateSingleDialog(R.id.appCustomDialog, bundleOf(AppCustomDialog.argument to this))
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }


    class Builder {

        var title: String? = null   //对话框标题
        var content: String? = null //对话框内容
        var leftButtonVisible: Boolean = true //
        var leftButtonText: String = "取消" //默认左侧取消，右侧确定
        var rightButtonText: String = "确定"
        var leftButtonColor: Int = 0      //左侧按钮字体颜色
        var rightButtonColor: Int = 0     //右侧按钮字体颜色
        var leftDrawable: Drawable? = null
        var rightDrawable: Drawable? = null
        var backgroundColor: Int = 0
        var backgroundDrawable: Drawable? = null
        var customContentView: View? = null   //对话框中间布局
        var leftButtonCallback: CustomDialog.OnDialogListener? = null   //左侧按钮回调事件
        var rightButtonCallback: CustomDialog.OnDialogListener? = null  //右侧按钮回调事件
        var ticketNumber: String? = null
        var customButtonCallback: CustomDialog.OnDialogListener? = null
        var isShowIcon: Boolean = false
        var customBankSelectCallBack: CustomDialog.OnDialogListener? = null
        var contentLayout: View? = null

        var contentGravity: Int = Gravity.CENTER
        var eqKey: String? = null

        fun build(): DialogBuilder {
            return DialogBuilder(this)
        }

    }

    class Info(
        var leftDrawable: Drawable? = null,
        var rightDrawable: Drawable? = null,
        var backgroundDrawable: Drawable? = null,
        var customContentView: View? = null,
        var leftButtonCallback: CustomDialog.OnDialogListener? = null,
        var rightButtonCallback: CustomDialog.OnDialogListener? = null,
        var customBankSelectCallBack: CustomDialog.OnDialogListener? = null,
        var contentLayout: View? = null
    )
}

fun generateKey(): String {
    return (100..100000).random().toString()
}