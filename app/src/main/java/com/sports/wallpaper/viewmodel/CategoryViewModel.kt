package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import com.sports.commonn.Constant
import com.sports.commonn.YaboLib
import com.sports.commonn.api.Api
import com.sports.commonn.entity.ImgInfo
import com.sports.commonn.entity.SubjectCategory
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.navigateWithAnim
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.widget.reclcerview.BaseMulteItemEntity
import com.sports.commonn.mvvm.ui.widget.reclcerview.ItemLayout
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.GridLayoutManagerWrapper
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.LinearLayoutManagerWrapper
import com.sports.commonn.utils.ResUtils
import com.sports.commonn.utils.ScreenUtil
import com.sports.commonn.utils.ToastUtil
import com.sports.commonn.utils.getSF
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.ui.daily.WallPaperBannerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class CategoryViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    //下拉刷新状态
    val bindRefreshState = SafeMutableLiveData<Boolean>()

    //下拉刷新的监听
    val bindRefreshListenner = OnRefreshListener {
        getSubjectCategorys()
    }

    val bindShowEmpty = MutableLiveData(View.GONE)

    var currentCategory: SubjectCategory? = null

    var bindSubjectCategoryData = MutableLiveData<ArrayList<SubjectCategoryItemViewModel>>()

    val bindSubjectLayoutManager = MutableLiveData<LinearLayoutManagerWrapper>().apply {
        value = LinearLayoutManagerWrapper(
            YaboLib.app.applicationContext,
            LinearLayoutManager.HORIZONTAL, false
        )
    }

    val bindSubjectItemChildClickListener =
        BaseQuickAdapter.OnItemChildClickListener { adapter, view, position ->
            if (view != null) {
                val item = adapter.data.getSF(position)
                if (item is SubjectCategoryItemViewModel) {
                    currentCategory = item.subjectCategory
                    getWallPapersByCategoryId(
                        item.subjectCategory?.id ?: 0,
                        item.subjectCategory?.category_name,
                        true
                    )
                }
            }
        }

    val bindSubjectItemType: ArrayList<ItemLayout>
        get() = arrayListOf(
            ItemLayout(0, R.layout.category_item_subject)
        )

    var bindWallPaperData = MutableLiveData<ArrayList<CategoryWallPaperItemViewModel>>()

    val bindWallPaperLayoutManager = MutableLiveData<GridLayoutManagerWrapper>().apply {
        value = GridLayoutManagerWrapper(
            YaboLib.app.applicationContext,
            2
        )
    }

    val bindWallPaperItemChildClickListener =
        BaseQuickAdapter.OnItemChildClickListener { adapter, view, position ->
            if (view != null) {
                val item = adapter.getItem(position)
                if (item is CategoryWallPaperItemViewModel) {
                    findNavController()?.navigateWithAnim(
                        R.id.wallPaperBannerFragment,
                        bundleOf(
                            WallPaperBannerFragment.argmentKey to item.imgsData,
                            WallPaperBannerFragment.argmentTitleKey to item.title,
                            WallPaperBannerFragment.argmentPosKey to item.position
                        )
                    )
                }
            }
        }

    val bindWallPaperItemType: ArrayList<ItemLayout>
        get() = arrayListOf(
            ItemLayout(0, R.layout.category_item_wallpaper)
        )

    @SuppressLint("CheckResult")
    fun getSubjectCategorys(showLoading: Boolean? = false) {
        showOrDimissLoadinng(showLoading)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .getSubjectCategoryList("0")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                bindRefreshState.value = false
                if (baseEntity.code == Constant.SUCCESS_CODE) {
                    val datas = ArrayList<SubjectCategoryItemViewModel>()
                    baseEntity.data?.forEach {
                        val item = SubjectCategoryItemViewModel()
                        item.subjectCategory = it
                        datas.add(item)
                    }
                    when {
                        currentCategory != null -> {
                            getWallPapersByCategoryId(
                                currentCategory?.id ?: 0,
                                currentCategory?.category_name,
                                false
                            )
                        }
                        datas.size > 0 -> {
                            val category = baseEntity.data?.getSF(0)
                            currentCategory = category
                            getWallPapersByCategoryId(
                                category?.id ?: 0,
                                category?.category_name,
                                false
                            )
                        }
                        else -> {
                            showOrDimissLoadinng(false)
                        }
                    }
                    bindSubjectCategoryData.value = datas
                } else {
                    showOrDimissLoadinng(false)
                    ToastUtil.show(baseEntity.message)
                }
            }, {
                bindRefreshState.value = false
                showOrDimissLoadinng(false)
                ToastUtil.show("获取专题失败")
            })
    }

    @SuppressLint("CheckResult")
    private fun getWallPapersByCategoryId(
        categoryId: Int,
        catetoryName: String?,
        showLoading: Boolean? = false
    ) {
        showOrDimissLoadinng(showLoading)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .getCategoryWallPaperList(categoryId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                if (baseEntity.code == Constant.SUCCESS_CODE) {
                    val datas = ArrayList<CategoryWallPaperItemViewModel>()
                    baseEntity.data?.data?.forEach {
                        val item = CategoryWallPaperItemViewModel()
                        item.imgInfo = it
                        item.title = catetoryName
                        item.imgsData = baseEntity.data?.data
                        datas.add(item)
                    }
                    bindWallPaperData.value = datas
                    bindShowEmpty.value = if (datas.size > 0) View.GONE else View.VISIBLE
                } else {
                    ToastUtil.show(baseEntity.message)
                    bindShowEmpty.value = View.VISIBLE
                    bindWallPaperData.value = null
                }
            }, {
                showOrDimissLoadinng(false)
                bindWallPaperData.value = null
                bindShowEmpty.value = View.VISIBLE
                ToastUtil.show("获取对应壁纸失败")
            })
    }
}

class SubjectCategoryItemViewModel : BaseMulteItemEntity() {
    var subjectCategory: SubjectCategory? = null

    override fun getClickViewIds(): ArrayList<Int> {
        return arrayListOf(R.id.iv_subject)
    }
}

class CategoryWallPaperItemViewModel : BaseMulteItemEntity() {
    var imgInfo: ImgInfo? = null

    var title: String? = null

    var imgsData: ArrayList<ImgInfo>? = null

    fun bindHeight(): Int {
        val screenW = ScreenUtil.screenWidth
        val space = ResUtils.dp2px(10f)
        return (screenW - space * 3) * 270 / 187 / 2 + space
    }

    fun bindwidth(): Int {
        return ScreenUtil.screenWidth / 2
    }

    fun bindLeftSpaceShow(): Int {
        if (position % 2 == 0) {
            return View.VISIBLE
        }
        return View.GONE
    }

    override fun getClickViewIds(): ArrayList<Int>? {
        return arrayListOf(R.id.iv_img)
    }
}

