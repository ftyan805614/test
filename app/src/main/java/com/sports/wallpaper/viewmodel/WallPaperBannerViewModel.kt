package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.sports.commonn.api.Api
import com.sports.commonn.entity.CollectRecord
import com.sports.commonn.entity.DownloadRecord
import com.sports.commonn.entity.ImgInfo
import com.sports.commonn.memory.DataManager
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.utils.FileUtil
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.litepal.LitePal
import java.lang.ref.WeakReference


/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class WallPaperBannerViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    var currentImgInfo: ImgInfo? = null

    val bindIsDownload = MutableLiveData(false)

    val bindIsCollected = MutableLiveData(false)

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.iv_share -> {

            }
            R.id.iv_download -> {
                if (isDownload(currentImgInfo?.id)) {
                    ToastUtil.show("已下载，请到下载内容中查看")
                } else {
                    canDownImage(currentImgInfo?.id)
                }
            }
            R.id.iv_collect -> {
                doCollect(currentImgInfo?.id)
            }
        }
    }

    private fun getDownloadRecordById(imgId: Int?): List<DownloadRecord> {
        return LitePal.where("imgId = ?", imgId?.toString())
            .find(DownloadRecord::class.java)
    }

    private fun getCollectRecordById(imgId: Int?): List<CollectRecord> {
        return LitePal.where("imgId = ?", imgId?.toString())
            .find(CollectRecord::class.java)
    }

    fun setCurrentItem(item:ImgInfo?){
        currentImgInfo = item
        bindIsDownload.value = isDownload(item?.id)
        bindIsCollected.value = isCollected(item?.id)
    }

    fun isCollected(imgId: Int?): Boolean {
//        val records = getCollectRecordById(imgId)
//        if (records.isNotEmpty()) {
//            return records[0].status == 1
//        }
        return DataManager.collectionList.contains(imgId)
    }

    fun isDownload(imgId: Int?): Boolean {
//        val records = getDownloadRecordById(imgId)
//        if (records.isNotEmpty()) {
//            return records[0].status == 1
//        }
        return DataManager.downloadList.contains(imgId)
    }

    fun getDownloadStatus(imgId: Int?): Int {
        val records = getDownloadRecordById(imgId)
        if (records.isNotEmpty()) {
            return records[0].status
        }
        return -1
    }

    fun getCollectStatus(imgId: Int?): Int {
        val records = getCollectRecordById(imgId)
        if (records.isNotEmpty()) {
            return records[0].status
        }
        return 0
    }

    @SuppressLint("CheckResult")
    fun canDownImage(imgId: Int?) {
        if (imgId == null) {
            ToastUtil.show("图片id不能为空")
            return
        }
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .canDownImage(imgId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { baseEntity ->
                if (baseEntity.isSuc()) {
                    val context = fragment?.get()?.context
                    if (context != null) {
                        //开始下载
                        ToastUtil.show("壁纸下载中...")
                        Glide.with(context).asBitmap().load(currentImgInfo?.fullUrl)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: Transition<in Bitmap>?
                                ) {
                                    val destFile = FileUtil.createSaveFile(
                                        context,
                                        false,
                                        "${System.currentTimeMillis()}.jpg",
                                        "sport_wallpaper"
                                    )
                                    FileUtil.saveBitmap2SelfDirectroy(
                                        context,
                                        resource,
                                        destFile
                                    )
                                    bindIsDownload.value = isDownload(currentImgInfo?.id)
                                    ToastUtil.show("壁纸下载成功")
                                    showOrDimissLoadinng(false)
                                }

                                override fun onLoadFailed(errorDrawable: Drawable?) {
                                    super.onLoadFailed(errorDrawable)
                                    showOrDimissLoadinng(false)
                                    ToastUtil.show("壁纸下载失败，请重新下载")
                                }

                                override fun onStop() {
                                    super.onStop()
                                    showOrDimissLoadinng(false)
                                }
                            })
                    }
                } else {
                    showOrDimissLoadinng(false)
                    ToastUtil.show(baseEntity.message)
                }
            }
    }

    @SuppressLint("CheckResult")
    fun doCollect(imgId: Int?) {
        if (imgId == null) {
            ToastUtil.show("收藏图片id不能为空")
            return
        }
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .doCollect(imgId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { baseEntity ->
                showOrDimissLoadinng()
                if (baseEntity.isSuc()) {
                    if (isCollected(imgId)) {
                        DataManager.collectionList.remove(imgId)
                        ToastUtil.show("取消收藏成功")
                    } else {
                        DataManager.collectionList.add(imgId)
                        ToastUtil.show("收藏成功")
                    }
                    bindIsCollected.value = isCollected(currentImgInfo?.id)
                } else {
                    showOrDimissLoadinng()
                    ToastUtil.show(baseEntity.message)
                }
            }
    }
}