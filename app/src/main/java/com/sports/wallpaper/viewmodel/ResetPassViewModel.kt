package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextUtils
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.sports.commonn.api.Api
import com.sports.commonn.entity.request.RegisterReq
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.timer.DownTimerHelper
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class ResetPassViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    val bindMobile by lazy { SafeMutableLiveData("17708437315") }
    val bindResetPwd by lazy { SafeMutableLiveData("123456") }
    val bindResetClickable by lazy { SafeMutableLiveData(true) }
    val bindVerifyCode = SafeMutableLiveData<String>()
    val bindSendVerCodeEnable = SafeMutableLiveData(false)
    val bindCountDownText = SafeMutableLiveData("发送验证码")
    val bindCountDownEnable = SafeMutableLiveData(true)
    private var countDownTimer: DownTimerHelper? = null

    val goBack = MutableLiveData(false)

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tv_sendverification -> {
                sendSms()
            }
            R.id.tv_reset_pass -> {
                resetPassword()
            }
            R.id.tv_login -> {
                goBack.value = true
//                findNavController()?.navigate(R.id.loginFragement)
            }
        }
    }

    override fun afterTextChanged(ed: Editable) {
        if (bindCountDownEnable.value == false) {
            bindSendVerCodeEnable.value = false
        } else {
            bindSendVerCodeEnable.value = !bindMobile.value.isNullOrEmpty()
        }
        bindResetClickable.value =
            !bindMobile.value.isNullOrEmpty() && !bindResetPwd.value.isNullOrEmpty() && !bindVerifyCode.value.isNullOrEmpty()
    }

    @SuppressLint("CheckResult")
    private fun resetPassword() {
        if (bindMobile.value.isNullOrEmpty()) {
            return ToastUtil.show("请输入手机号")
        }

        if (bindResetPwd.value.isNullOrEmpty()) {
            return ToastUtil.show("请输入密码")
        }

        if (bindVerifyCode.value.isNullOrEmpty()) {
            return ToastUtil.show("请输入验证码")
        }

        checkSmsCode()
    }

    @SuppressLint("CheckResult")
    fun sendSms() {
        if (bindMobile.value.isNullOrEmpty()) {
            return ToastUtil.show("请输入手机号")
        }
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .sendSms(bindMobile.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                if (baseEntity.isSuc()) {
                    if (null != countDownTimer) {
                        countDownTimer?.cancel()
                    }
                    countDownTimer = object : DownTimerHelper((60 * 1000).toLong(), 1000) {

                        override fun onTick(untilFinished: Long) {
                            bindCountDownEnable.value = false
                            bindSendVerCodeEnable.value = false
                            bindCountDownText.value = (untilFinished.toString() + "s后重试")
                        }

                        override fun onFinish() {
                            bindCountDownEnable.value = true
                            bindSendVerCodeEnable.value = true
                            bindCountDownText.value = "发送验证码"
                        }
                    }
                    countDownTimer?.start(activity?.get())
                    ToastUtil.showToast("验证码已发送，请注意查收")
                } else {
                    if (TextUtils.isEmpty(baseEntity.message)) {
                        ToastUtil.show("获取验证码失败")
                    } else {
                        ToastUtil.show(baseEntity.message)
                    }
                }
            }, {
                showOrDimissLoadinng(false)
                ToastUtil.show("获取验证码失败")
            }

            )
    }

    @SuppressLint("CheckResult")
    private fun checkSmsCode() {
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .checkSms(bindMobile.value, bindVerifyCode.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                if (baseEntity.isSuc()) {
                    resetPass()
                } else {
                    showOrDimissLoadinng(false)
                    ToastUtil.show(baseEntity.message)
                }
            }, {
                showOrDimissLoadinng(false)
                ToastUtil.show("验证码验证失败，请重试")
            })
    }

    @SuppressLint("CheckResult")
    private fun resetPass() {
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .resetPassword(bindMobile.value, bindResetPwd.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                if (baseEntity.isSuc()) {
                    ToastUtil.show("密码重置成功")
                    goBack.value = true
                } else {
                    ToastUtil.show(baseEntity.message)
                }
            }, {
                showOrDimissLoadinng(false)
                ToastUtil.show("密码重置成功，请重试")
            })
    }
}