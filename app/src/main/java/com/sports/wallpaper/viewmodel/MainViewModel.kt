package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import com.sports.commonn.api.Api
import com.sports.commonn.entity.UserInfo
import com.sports.commonn.memory.DataManager
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.utils.GsonHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class MainViewModel (lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {


    @SuppressLint("CheckResult")
    fun initCollectionList() {
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .getCollectionlist()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { baseEntity ->
                if (baseEntity.isSuc()) {
                    DataManager.collectionList.clear()
                    baseEntity.data?.data?.forEach {
                        it.img_id?.let { id ->
                            DataManager.collectionList.add(id)
                        }
                    }
                }
            }
    }

    @SuppressLint("CheckResult")
    fun initDownloadList() {
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .getDownlist()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { baseEntity ->
                if (baseEntity.isSuc()) {
                    DataManager.downloadList.clear()
                    baseEntity.data?.data?.forEach {
                        it.img_id?.let { id ->
                            DataManager.downloadList.add(id)
                        }
                    }
                }
            }
    }
}
