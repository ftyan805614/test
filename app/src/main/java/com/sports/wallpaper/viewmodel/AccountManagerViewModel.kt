package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.sports.commonn.api.Api
import com.sports.commonn.entity.UserInfo
import com.sports.commonn.memory.DataManager
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.navigateWithAnim
import com.sports.commonn.mvvm.popBackStackDialog
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.widget.CustomDialog
import com.sports.commonn.utils.TextUtil
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.manger.IntentManager
import com.sports.wallpaper.ui.mine.RecordResultFragment
import com.sports.wallpaper.utils.GsonHelper
import com.sports.wallpaper.widget.dialog.DialogBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class AccountManagerViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    val bindUsername = MutableLiveData(DataManager.user?.username)

    val bindUserMobile = MutableLiveData(DataManager.user?.phone)


    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.ll_username -> {
                showEditUsernameDialog()
            }
            R.id.ll_mobile -> {

            }
            R.id.ll_change_pass -> {
                findNavController()?.navigateWithAnim(
                    R.id.resetPassFragment
                )
            }
            R.id.tv_logout -> {
                logout()
            }
        }
    }

    private fun logout() {
        DialogBuilder.build {
            content = "你是否确定退出该账户?"
            rightButtonCallback = object : CustomDialog.OnDialogListener {
                override fun onClick(dialog: CustomDialog<*>, view: View) {
                    findNavController().popBackStackDialog()
                    doLogout()
                }
            }
        }.showSingeDialog()
    }

    @SuppressLint("CheckResult")
    private fun doLogout() {
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .logout()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                if (baseEntity.isSuc()) {
                    DataManager.token = ""
                    DataManager.user = null
                    IntentManager.goLoginActivity(false)
                } else {
                    ToastUtil.show(baseEntity.message)
                }
            }, {
                showOrDimissLoadinng(false)
                DataManager.token = ""
                DataManager.user = null
                IntentManager.goLoginActivity(false)
            }

            )
    }

    private fun showEditUsernameDialog() {
        val contentView =
            LayoutInflater.from(activity?.get()).inflate(R.layout.dialog_edit_username, null)
        contentView.findViewById<View>(R.id.fl_container)
            .setOnClickListener { findNavController().popBackStackDialog() }

        val edit = contentView.findViewById<EditText>(R.id.et_username)
        contentView.findViewById<View>(R.id.ll_btn_confirm).setOnClickListener {

            val username = edit.text.toString()
            if (TextUtils.isEmpty(username)) {
                ToastUtil.show("用户名不能为空")
                return@setOnClickListener
            }
            changeUsername(username)
            findNavController().popBackStackDialog()
        }
        val builder = DialogBuilder.build {
            contentLayout = contentView
        }
        builder.showSingeDialog()
    }

    @SuppressLint("CheckResult")
    private fun changeUsername(username: String?) {
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .updateUsername(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                ToastUtil.show(baseEntity.message)
                if (baseEntity.isSuc()) {
                    bindUsername.value = username
                    DataManager.user?.username = username
                }
            }, {
                showOrDimissLoadinng(false)
                ToastUtil.show("修改用户名失败")
            }
            )
    }
}