package com.sports.wallpaper.viewmodel

import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.wallpaper.R
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class StartViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
        }
    }
}