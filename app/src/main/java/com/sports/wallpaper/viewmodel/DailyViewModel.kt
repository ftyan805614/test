package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import com.sports.commonn.YaboLib
import com.sports.commonn.api.Api
import com.sports.commonn.entity.DailyImgInfos
import com.sports.commonn.entity.ImgInfo
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.navigateWithAnim
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.widget.reclcerview.BaseMulteItemEntity
import com.sports.commonn.mvvm.ui.widget.reclcerview.ItemLayout
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.LinearLayoutManagerWrapper
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.ui.daily.WallPaperBannerFragment
import com.sports.wallpaper.utils.showLoadingExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class DailyViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    val bindWallPaperData = SafeMutableLiveData<ArrayList<DailyItemViewModel>>(ArrayList())

    val bindItemType: ArrayList<ItemLayout>
        get() = arrayListOf(
            ItemLayout(0, R.layout.daily_item_wallpaper)
        )

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
        }
    }

    @SuppressLint("CheckResult")
    fun getDailyWallpapers(showLoading: Boolean? = false) {
        showOrDimissLoadinng(showLoading)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .getDailyWallPaper()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                bindRefreshState.value = false
                showOrDimissLoadinng(false)
                if (baseEntity.isSuc()) {
                    val data = ArrayList<DailyItemViewModel>()
                    baseEntity.data?.data?.forEach {
                        val item = DailyItemViewModel()
                        item.categoryWallPaperInfo = it
                        val imgList = ArrayList<DailyImgItemViewModel>()
                        it.img?.forEach { imgInfo ->
                            val imgItem = DailyImgItemViewModel()
                            imgItem.imgInfo = imgInfo
                            imgItem.title = it.created_at
                            imgList.add(imgItem)
                        }
                        item.bindImgsData.value = imgList
                        item.imgsData = it.img
                        data.add(item)
                    }
                    bindWallPaperData.value = data
                } else {
                    ToastUtil.show(baseEntity.message)
                }
            }, {
                showOrDimissLoadinng(false)
                bindRefreshState.value = false
                ToastUtil.show("获取每日数据失败")
            }
            )
    }

    //下拉刷新状态
    val bindRefreshState = SafeMutableLiveData<Boolean>()

    //下拉刷新的监听
    val bindRefreshListenner = OnRefreshListener {
        getDailyWallpapers()
    }

}

class DailyItemViewModel : BaseMulteItemEntity() {

    val bindLayoutManager = MutableLiveData<LinearLayoutManagerWrapper>().apply {
        value = LinearLayoutManagerWrapper(
            YaboLib.app.applicationContext,
            LinearLayoutManager.HORIZONTAL, false
        )
    }

    var categoryWallPaperInfo: DailyImgInfos.CategoryWallPaperInfo? = null

    var bindImgsData = MutableLiveData<ArrayList<DailyImgItemViewModel>>()

    var imgsData: ArrayList<ImgInfo>? = null

    val bindImgItemChildClickListener =
        BaseQuickAdapter.OnItemChildClickListener { adapter, view, position ->
            if (view != null) {
                val item = adapter.getItem(position)
                if (item is DailyImgItemViewModel) {
                    findNavController()?.navigateWithAnim(
                        R.id.wallPaperBannerFragment,
                        bundleOf(
                            WallPaperBannerFragment.argmentKey to imgsData,
                            WallPaperBannerFragment.argmentTitleKey to item.title,
                            WallPaperBannerFragment.argmentPosKey to item.position
                        )
                    )
                }
            }
        }

    val bindImgItemType: ArrayList<ItemLayout>
        get() = arrayListOf(
            ItemLayout(0, R.layout.daily_item_img)
        )
}

class DailyImgItemViewModel : BaseMulteItemEntity() {
    var imgInfo: ImgInfo? = null

    var title: String? = null

    override fun getClickViewIds(): ArrayList<Int>? {
        return arrayListOf(R.id.iv_daily_wallpaper)
    }
}