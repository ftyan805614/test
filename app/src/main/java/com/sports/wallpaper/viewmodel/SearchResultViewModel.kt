package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.text.TextUtils
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.chad.library.adapter.base.BaseQuickAdapter
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import com.sports.commonn.YaboLib
import com.sports.commonn.api.Api
import com.sports.commonn.entity.ImgInfo
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.navigateWithAnim
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.widget.reclcerview.BaseMulteItemEntity
import com.sports.commonn.mvvm.ui.widget.reclcerview.ItemLayout
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.GridLayoutManagerWrapper
import com.sports.commonn.utils.ResUtils
import com.sports.commonn.utils.ScreenUtil
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.ui.daily.WallPaperBannerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class SearchResultViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    var searchWord: String? = null


    //下拉刷新状态
    val bindRefreshState = SafeMutableLiveData<Boolean>()

    //下拉刷新的监听
    val bindRefreshListenner = OnRefreshListener {
        getSearchResultByWord(searchWord)
    }

    var bindSearchResultData = MutableLiveData<ArrayList<SearchResultItemViewModel>>()

    val bindSearchResultLayoutManager = MutableLiveData<GridLayoutManagerWrapper>().apply {
        value = GridLayoutManagerWrapper(
            YaboLib.app.applicationContext,
            3
        )
    }

    val bindSearchResultItemChildClickListener =
        BaseQuickAdapter.OnItemChildClickListener { adapter, view, position ->
            if (view != null) {
                val item = adapter.getItem(position)
                if (item is SearchResultItemViewModel) {
                    findNavController()?.navigateWithAnim(
                        R.id.wallPaperBannerFragment,
                        bundleOf(
                            WallPaperBannerFragment.argmentKey to item.imgsData,
                            WallPaperBannerFragment.argmentTitleKey to searchWord,
                            WallPaperBannerFragment.argmentPosKey to item.position
                        )
                    )
                }
            }
        }

    val bindSearchResultItemType: ArrayList<ItemLayout>
        get() = arrayListOf(
            ItemLayout(0, R.layout.search_item_wallpaper)
        )

    val bindShowEmpty = MutableLiveData(View.GONE)

    @SuppressLint("CheckResult")
    fun getSearchResultByWord(word: String?) {
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .getSearchResultByWord(word!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                bindRefreshState.value = false
                if (baseEntity.isSuc()) {
                    val hotwordItemss = ArrayList<SearchResultItemViewModel>()
                    baseEntity.data?.data?.forEach {
                        val item = SearchResultItemViewModel()
                        item.imgInfo = it
                        item.imgsData = baseEntity.data?.data
                        hotwordItemss.add(item)
                    }
                    bindSearchResultData.value = hotwordItemss
                    bindShowEmpty.value = if (hotwordItemss.size > 0) View.GONE else View.VISIBLE
                } else {
                    bindSearchResultData.value = null
                    bindShowEmpty.value = View.VISIBLE
                    if (!TextUtils.isEmpty(baseEntity.message)) {
                        ToastUtil.show(baseEntity.message)
                    }
                }
            }, {
                bindRefreshState.value = false
                showOrDimissLoadinng(false)
                bindSearchResultData.value = null
                bindShowEmpty.value = View.VISIBLE
            })
    }
}

class SearchResultItemViewModel : BaseMulteItemEntity() {
    var imgInfo: ImgInfo? = null

    var imgsData: ArrayList<ImgInfo>? = null

    fun bindHeight(): Int {
        val screenW = ScreenUtil.screenWidth
        val space = ResUtils.dp2px(10f)
        return (screenW - space * 4) * 270 / 187 / 3 + space
    }

    fun bindwidth(): Int {
        return ScreenUtil.screenWidth / 3
    }

    fun bindLeftSpaceShow(): Int {
        if (position % 3 == 0) {
            return View.VISIBLE
        }
        return View.GONE
    }

    override fun getClickViewIds(): ArrayList<Int>? {
        return arrayListOf(R.id.iv_img)
    }
}