package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.chad.library.adapter.base.BaseQuickAdapter
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import com.sports.commonn.YaboLib
import com.sports.commonn.api.Api
import com.sports.commonn.entity.HotWord
import com.sports.commonn.entity.SearchRecord
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.navigateWithAnim
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.widget.reclcerview.BaseMulteItemEntity
import com.sports.commonn.mvvm.ui.widget.reclcerview.ItemLayout
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.GridLayoutManagerWrapper
import com.sports.commonn.utils.ToastUtil
import com.sports.commonn.utils.getSF
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.ui.daily.WallPaperBannerFragment
import com.sports.wallpaper.ui.search.SearchResultFragmennt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.litepal.LitePal
import java.lang.ref.WeakReference
import kotlin.collections.ArrayList


/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class SearchViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    //下拉刷新状态
    val bindRefreshState = SafeMutableLiveData<Boolean>()

    //下拉刷新的监听
    val bindRefreshListenner = OnRefreshListener {
        getHotSearchs()
    }

    val bindSearchTxt = MutableLiveData("")

    val bindHistorySearch = MutableLiveData<List<String>>()

    var bindHotSearchData = MutableLiveData<ArrayList<HotWordItemViewModel>>()

    var bindShowSearchHistoryTitle = MutableLiveData(View.GONE)

    val bindHotSearchLayoutManager = MutableLiveData<GridLayoutManagerWrapper>().apply {
        value = GridLayoutManagerWrapper(
            YaboLib.app.applicationContext,
            2
        )
    }

    val bindHotSearchItemChildClickListener =
        BaseQuickAdapter.OnItemChildClickListener { adapter, view, position ->
            if (view != null) {
                val item = adapter.data.getSF(position) as? HotWordItemViewModel
                gotoSearchResult(item?.hotword?.search)
            }
        }

    fun gotoSearchResult(search: String?) {
        findNavController()?.navigateWithAnim(
            R.id.searchResultFragment,
            bundleOf(SearchResultFragmennt.argmentKey to search)
        )
    }

    val bindHotSearchItemType: ArrayList<ItemLayout>
        get() = arrayListOf(
            ItemLayout(0, R.layout.search_item_hotword)
        )

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.iv_select_search -> {
                doSearch(bindSearchTxt.value)
            }
            R.id.iv_del_all -> {
                delAllSearchRecord()
            }
        }
    }

    fun getHistorySearch() {
        val records = getAllSearchRecord()
        if (records.isNotEmpty()) {
            val list = ArrayList<String>()
            records.forEach { word ->
                word.searchName?.let { list.add(it) }
            }
            bindHistorySearch.value = list
            bindShowSearchHistoryTitle.value = if (list.isEmpty()) View.GONE else View.VISIBLE
        }
    }


    fun doSearch(searchWord: String?) {
        if (TextUtils.isEmpty(searchWord)) {
            ToastUtil.show("请输入搜索内容")
            return
        }
        saveSearchWord(searchWord)
        getHistorySearch()
        gotoSearchResult(searchWord)
    }

    private fun saveSearchWord(searchWord: String?) {
        val records = getSearchRecordByWord(searchWord)
        if (records.isNotEmpty()) {
            val record = records.getSF(0)
            record?.updateTime = System.currentTimeMillis()
            record?.times = record?.times ?: 0 + 1
            record?.saveOrUpdate()
        } else {
            val record = SearchRecord()
            record.createTime = System.currentTimeMillis()
            record.updateTime = System.currentTimeMillis()
            record.searchName = searchWord
            record.times = 1
            record.save()
        }
    }

    private fun getSearchRecordByWord(searchWord: String?): List<SearchRecord> {
        return LitePal.where("searchName = ?", searchWord)
            .find(SearchRecord::class.java)
    }


    private fun getAllSearchRecord(): List<SearchRecord> {
        return LitePal.findAll(SearchRecord::class.java)
    }

    private fun delAllSearchRecord() {
        LitePal.deleteAll(SearchRecord::class.java)
        (bindHistorySearch.value as? ArrayList<String>)?.clear()
        bindHistorySearch.value = bindHistorySearch.value
    }

    @SuppressLint("CheckResult")
    fun getHotSearchs() {
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .getHotSearchs()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                bindRefreshState.value = false
                showOrDimissLoadinng(false)
                if (baseEntity.isSuc()) {
                    val hotwordItemss = ArrayList<HotWordItemViewModel>()
                    baseEntity.data?.hotSearchs?.forEach {
                        val item = HotWordItemViewModel()
                        item.hotword = it
                        hotwordItemss.add(item)
                    }
                    bindHotSearchData.value = hotwordItemss
                } else {
                    if (!TextUtils.isEmpty(baseEntity.message)) {
                        ToastUtil.show(baseEntity.message)
                    }
                }
            }, { showOrDimissLoadinng(false)
                bindRefreshState.value = false})
    }
}

class HotWordItemViewModel : BaseMulteItemEntity() {
    var hotword: HotWord? = null

    val bindShowHot = MutableLiveData(View.GONE)

    override fun setPosition(position: Int, totalCount: Int) {
        super.setPosition(position, totalCount)
        if(position<3){
            bindShowHot.value = View.VISIBLE
        }else{
            bindShowHot.value = View.GONE
        }
    }

    override fun getClickViewIds(): ArrayList<Int>? {
        return arrayListOf(R.id.tv_hotword)
    }
}