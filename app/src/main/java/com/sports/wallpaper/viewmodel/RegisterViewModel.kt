package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.text.TextUtils
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.sports.commonn.api.Api
import com.sports.commonn.entity.request.RegisterReq
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.utils.ToastUtil
import com.sports.commonn.utils.uneq
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class RegisterViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    val bindRegisterMobile by lazy { SafeMutableLiveData("17712341234") }
    val bindRegisterUserName by lazy { SafeMutableLiveData("aversonn") }
    val bindRegisterPwd by lazy { SafeMutableLiveData("123456") }
    val bindRegisterPwdConfirm by lazy { SafeMutableLiveData("123456") }
    val bindRegisterClickable by lazy { SafeMutableLiveData(true) }

    val goBack = MutableLiveData(false)

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tv_register -> {
                go2Register()
            }
            R.id.tv_login -> {
                goBack.value = true
//                findNavController()?.navigate(R.id.loginFragement)
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun go2Register() {
        if (bindRegisterMobile.value.isNullOrEmpty()) {
            return ToastUtil.show("请输入手机号")
        }

        if (bindRegisterUserName.value.isNullOrEmpty()) {
            return ToastUtil.show("请输入用户名")
        }
        if (bindRegisterPwd.value.isNullOrEmpty()) {
            return ToastUtil.show("请输入注册密码")
        }

        if (bindRegisterPwd.value uneq bindRegisterPwdConfirm.value) {
            return ToastUtil.show("二次输入密码不一致")
        }
        showOrDimissLoadinng(true)
        val registerReq = RegisterReq()
        registerReq.mobile = bindRegisterMobile.value
        registerReq.user_name = bindRegisterUserName.value
        registerReq.password = bindRegisterPwd.value
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .register(registerReq)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                if (baseEntity.isSuc()) {
                    ToastUtil.show("注册成功，请登录")
                    goBack.value = true
                } else {
                    if (TextUtils.isEmpty(baseEntity.message)) {
                        ToastUtil.show("注册不成功")
                    } else {
                        ToastUtil.show(baseEntity.message)
                    }
                }
            }, {
                showOrDimissLoadinng(false)
                ToastUtil.show("注册不成功")
            }

            )
    }


}