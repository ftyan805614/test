package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.apkfuns.logutils.LogUtils
import com.lzy.imagepicker.ImagePicker
import com.lzy.imagepicker.bean.ImageItem
import com.lzy.imagepicker.ui.ImageGridActivity
import com.sports.commonn.api.Api
import com.sports.commonn.memory.DataManager
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.navigateWithAnim
import com.sports.commonn.mvvm.popBackStackDialog
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.widget.CustomDialog
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.ui.mine.RecordResultFragment
import com.sports.wallpaper.utils.DialogUtils
import com.sports.wallpaper.widget.dialog.DialogBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
open class MineViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    val bindUsername = MutableLiveData("").apply {
        value = userName
    }

    val userName: String?
        get() = if (DataManager.user == null) "未登录" else DataManager.user?.username

    val bindIdentity = MutableLiveData("普通会员")

    //用户头像的网络url,绑定到imageview 上
    val bindHeadUrl by lazy { SafeMutableLiveData<String>() }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.ll_account -> {
                findNavController()?.navigateWithAnim(
                    R.id.accountManagerFragmennt
                )
            }
            R.id.iv_user_head -> {
                DialogUtils.showStringArrayDialog(fragment?.get()?.context, arrayOf(
                    R.string.alumb, R.string.camera
                ), object : DialogUtils.StringArrayDialogCallback {

                    override fun onItemClick(text: String?, tag: Int) {
                        if (tag == R.string.camera) {
                            tackPhoto(true)
                        } else {
                            tackPhoto(false)
                        }
                    }
                })

            }
            R.id.ll_downlist -> {
                findNavController()?.navigateWithAnim(
                    R.id.recordResultFragment,
                    bundleOf(RecordResultFragment.argmentKey to RecordResultFragment.categoryDownload)
                )
            }
            R.id.ll_collect_list -> {
                findNavController()?.navigateWithAnim(
                    R.id.recordResultFragment,
                    bundleOf(RecordResultFragment.argmentKey to RecordResultFragment.categoryCollect)
                )
            }
            R.id.ll_clear_cache -> {
                clearCache()
            }
            R.id.ll_suggestion -> {
                sendSuggestion()
            }
            R.id.ll_protocol -> {
                findNavController()?.navigateWithAnim(
                    R.id.protocolFragment
                )
            }
        }
    }

    private fun clearCache() {
        DialogBuilder.build {
            content = "确定清除所有缓存?"
            rightButtonCallback = object : CustomDialog.OnDialogListener {
                override fun onClick(dialog: CustomDialog<*>, view: View) {
                    findNavController().popBackStackDialog()
                    //doLogout()
                }
            }
        }.showSingeDialog()
    }

    private fun sendSuggestion() {
        DialogBuilder.build {
            title = "发送邮件至"
            content = "sportswallpaper@gmail.com"
            rightButtonCallback = object : CustomDialog.OnDialogListener {
                override fun onClick(dialog: CustomDialog<*>, view: View) {
                    findNavController().popBackStackDialog()
                    //doLogout()
                }
            }
        }.showSingeDialog()
    }

    private var images = ArrayList<ImageItem>()

    private fun tackPhoto(useCamera: Boolean? = false) {
        val intent = Intent(TopLifecyclerOwnerManager.curActivity, ImageGridActivity::class.java)
        intent.putExtra(ImageGridActivity.EXTRAS_IMAGES, images)
        intent.putExtra(ImageGridActivity.EXTRAS_TAKE_PICKERS, useCamera)
        fragment?.get()?.let {
            it.startActivityForResult(intent, 100)
        }
    }

    @SuppressLint("CheckResult")
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            if (data != null && requestCode == 100) {
                try {
                    images =
                        data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS) as ArrayList<ImageItem>
                    updalodAvatar(images[0].path)
                } catch (e: Exception) {
                    LogUtils.e(e)
                }
            } else {
                ToastUtil.show("没有数据")
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun updalodAvatar(path: String) {
        showOrDimissLoadinng(true)
        val file = File(path)
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .uploadAvatar(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    showOrDimissLoadinng(false)
                    ToastUtil.show("上传成功")
                    bindHeadUrl.value = DataManager.imgServerHost + it.path
                }, {
                    showOrDimissLoadinng(false)
                    ToastUtil.show("上传失败")
                }
            )
    }
}