package com.sports.wallpaper.viewmodel

import android.annotation.SuppressLint
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.sports.commonn.api.Api
import com.sports.commonn.entity.UserInfo
import com.sports.commonn.entity.request.LoginReq
import com.sports.commonn.memory.DataManager
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.utils.ToastUtil
import com.sports.net.RetroCommonFactory
import com.sports.wallpaper.R
import com.sports.wallpaper.utils.GsonHelper
import com.sports.wallpaper.utils.dismissLoadingExt
import com.sports.wallpaper.utils.showLoadingExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class LoginViewModel(lifecycleOwner: WeakReference<LifecycleOwner>) :
    BaseViewModel(lifecycleOwner = lifecycleOwner) {

    val bindLoginUserName = MutableLiveData("17712341234")

    val bindLoginPwd = MutableLiveData("123456")

    val bindIsRemember by lazy { SafeMutableLiveData(true) }

    val bindLoginClickable by lazy { SafeMutableLiveData(true) }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tv_forget -> {
                findNavController()?.navigate(R.id.resetPassFragment)
            }
            R.id.tv_login -> {
                login(bindLoginUserName.value, bindLoginPwd.value)
            }
            R.id.tv_register -> {
                findNavController()?.navigate(R.id.registerFragment)
            }
        }
    }

    @SuppressLint("CheckResult")
    fun login(mobile: String?, password: String?) {
        if (TextUtils.isEmpty(mobile)) {
            ToastUtil.show("请输入手机号")
            return
        }
        if (TextUtils.isEmpty(password)) {
            ToastUtil.show("请输入密码")
            return
        }
        val loginReq = LoginReq()
        loginReq.mobile = mobile
        loginReq.password = password
        showOrDimissLoadinng(true)
        RetroCommonFactory.getInstance()
            .create(Api::class.java)
            .login(loginReq)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ baseEntity ->
                showOrDimissLoadinng(false)
                if (baseEntity.isSuc()) {
                    val jsonStr = GsonHelper.getInstance().toJson(baseEntity.data)
                    val user = GsonHelper.getInstance().getEntity(jsonStr, UserInfo::class.java)
                    user.phone = mobile
                    DataManager.token = user?.token
                    DataManager.user = user
                    Log.d("w_www", "token=${DataManager.token}")
                    findNavController()?.navigate(R.id.mainFragment)
                } else {
                    ToastUtil.show(baseEntity.message)
                }
            }, { throwable ->
                run {
                    ToastUtil.show("登录异常")
                    showOrDimissLoadinng(false)
                }
            }

            )
    }
}