package com.sports.wallpaper.base

import android.view.View
import androidx.databinding.ViewDataBinding
import com.sports.commonn.mvvm.ui.BaseNavigationFragment
import com.sports.commonn.mvvm.ui.widget.CustomToolbar
import com.sports.wallpaper.R
import com.sports.wallpaper.utils.TitleBarUtil

abstract class AppBaseMvvmFragment<T : ViewDataBinding> : BaseNavigationFragment<T>(){


    var customerTitlebar: CustomToolbar? = null


    /**
     * 标题栏效果
     * 左边图片
     */
    fun initTitle(
        title: String? = "",
        leftClickListener: View.OnClickListener? = null
    ): CustomToolbar? {
        customerTitlebar = findViewById(R.id.toolbar)
        TitleBarUtil.initTitle(customerTitlebar, title, leftClickListener ?: View.OnClickListener {
            exitActivity()
        })
        return customerTitlebar
    }


    /**
     * 标题栏效果
     * 右边文字
     */
    fun initTitleByRightTxt(
        title: String?,
        leftClickListener: View.OnClickListener? = null
    ): CustomToolbar? {
        customerTitlebar = findViewById(R.id.toolbar)
        TitleBarUtil.initTitleByRightTxt(
            customerTitlebar,
            title,
            leftClickListener ?: View.OnClickListener {
                exitActivity()
            })
        return customerTitlebar
    }


    /**
     * 标题栏效果
     * 右边图片
     */
    fun initTitleByRightImage(
        title: String? = "",
        leftClickListener: View.OnClickListener? = null
    ): CustomToolbar? {
        customerTitlebar = findViewById(R.id.toolbar)
        TitleBarUtil.initTitleByRightImage(
            customerTitlebar,
            title,
            leftClickListener ?: View.OnClickListener {
                exitActivity()
            })
        return customerTitlebar
    }


    /**
     * 标题栏效果
     * 右边自定义view
     */
    fun initTitleByRightView(
        title: String?,
        leftClickListener: View.OnClickListener? = null
    ): CustomToolbar? {
        customerTitlebar = findViewById(R.id.toolbar)
        TitleBarUtil.initTitleByRightView(
            customerTitlebar,
            title,
            leftClickListener ?: View.OnClickListener {
                exitActivity()
            })
        return customerTitlebar
    }

    /**
     * 标题栏效果
     * 右边自定义view
     */
    fun initTitleByCenterView(
        title: String? = "",
        leftClickListener: View.OnClickListener? = null
    ): CustomToolbar? {
        customerTitlebar = findViewById(R.id.toolbar)
        TitleBarUtil.initTitleByCenterView(
            customerTitlebar,
            title,
            leftClickListener ?: View.OnClickListener {
                exitActivity()
            })
        return customerTitlebar
    }

    /**
     * 标题栏效果
     * 右边图片
     */
    fun initTitleByRightImage2(
        title: String? = "",
        leftClickListener: View.OnClickListener? = null
    ): CustomToolbar? {
        customerTitlebar = findViewById(R.id.toolbar)
        TitleBarUtil.initTitleByRightImage2(
            customerTitlebar,
            title,
            leftClickListener ?: View.OnClickListener {
                exitActivity()
            })
        return customerTitlebar
    }


    fun finishActivityNoAnim() {
        finish()
    }

    override fun onBackPressedSupport(): Boolean {
        if (isVisible) {
            exitActivity()
        }
        return true
    }
}