package com.sports.wallpaper.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.BaseNavigationMvvmDialogFragment
import com.sports.wallpaper.R

abstract class AppBaseDialogFragment<T : ViewDataBinding> : BaseNavigationMvvmDialogFragment<T>() {

    override fun onStart() {
        super.onStart()
        dialog?.setCanceledOnTouchOutside(true)
        onDialogAnim()
    }

    open fun onDialogAnim() {
        val window = dialog?.window
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setWindowAnimations(R.style.picker_view_scale_anim)
    }

}