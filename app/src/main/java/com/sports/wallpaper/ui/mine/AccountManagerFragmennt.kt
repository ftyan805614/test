package com.sports.wallpaper.ui.mine

import com.sports.commonn.mvvm.ui.widget.CommonTitleBar
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentAccountManagerBinding
import com.sports.wallpaper.databinding.FragmentMineBinding
import com.sports.wallpaper.viewmodel.AccountManagerViewModel
import com.sports.wallpaper.viewmodel.MineViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class AccountManagerFragmennt : AppBaseMvvmFragment<FragmentAccountManagerBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_account_manager
    }

    override fun initViewModel(bind: FragmentAccountManagerBinding) {
        bind.viewModel = ofScope(requireActivity(), this, AccountManagerViewModel::class.java)
    }

    override fun initView() {
        super.initView()
        initTitleByCenterView()?.setMainTitle("账号管理")
            ?.setMainTitleSize(16f)
            ?.setMainTitleColor(R.color.skin_3F5075_FBFBFB)
            ?.setLeftText("返回")
            ?.setLeftType(CommonTitleBar.TYPE_LEFT_IMAGEBUTTON)
            ?.setLeftTxtImg(R.mipmap.com_icon_navback, 0)
            ?.setLeftOnClick {
                exitActivity()
            }
    }
}