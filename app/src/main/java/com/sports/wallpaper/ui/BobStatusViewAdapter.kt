package com.sports.wallpaper.ui

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.sports.commonn.mvvm.ui.widget.LoadStatus
import com.sports.commonn.mvvm.ui.widget.StatusAdapter
import com.sports.commonn.utils.NetworkUtils
import com.sports.commonn.utils.ResUtils
import com.sports.commonn.utils.ScreenUtil
import com.sports.wallpaper.R
import com.sports.wallpaper.widget.LoadingViewLayout
import java.util.*

/**
 * Desc: 页面加载、空view显示
 */
class BobStatusViewAdapter : StatusAdapter() {
    private var mStatusLoaddingPb: ProgressBar? = null
    private var mEmptyLoadingTv: TextView? = null
    private var mHintImg: ImageView? = null
    private var mEmptyMessageTv: TextView? = null
    private var mEmptyRetryTv: TextView? = null
    private var mEmptyGoTv: TextView? = null
    private var depositMoenyBtn: View? = null
    private var mMoreTv: TextView? = null
    private var errorType = 0
    private var errorMessage: String? = null
    private var mStatusContentCl: ConstraintLayout? = null
    private var rootView: View? = null
    private var mStatusContentLl: FrameLayout? = null
    private var mStatusBgIv: ImageView? = null
    private var mNoDataState: TextView? = null
    private var resId = -1
    private var llayLoadingLayout: LinearLayout? = null
    private var loadingViewLayout: LoadingViewLayout? = null
    private val otherVies = ArrayList<View>()
    override fun getView(context: Context): View {
        val view = LayoutInflater.from(context).inflate(R.layout.core_default_status, null)
        view.setOnClickListener(null)
        initView(view)
        view.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                val parent = view?.parent as ViewGroup
                val childCount = parent.childCount
                for (i in 0 until childCount) {
                    if (parent.getChildAt(i) !== view) {
                        otherVies.add(parent.getChildAt(i))
                    }
                }
            }
        })
        return view
    }

    private fun initView(itemView: View) {
        rootView = itemView
        llayLoadingLayout = itemView.findViewById(R.id.llayLoadingLayout)
        mStatusLoaddingPb = itemView.findViewById<View>(R.id.pb_status_loadding) as? ProgressBar
        mEmptyLoadingTv = itemView.findViewById<View>(R.id.tv_empty_loading) as? TextView
        mHintImg = itemView.findViewById<View>(R.id.img_hint) as? ImageView
        mEmptyMessageTv = itemView.findViewById<View>(R.id.tv_empty_message) as? TextView
        mEmptyRetryTv = itemView.findViewById<View>(R.id.tv_empty_retry) as? TextView
        mEmptyGoTv = itemView.findViewById<View>(R.id.tv_empty_go) as? TextView
        mMoreTv = itemView.findViewById<View>(R.id.tv_more) as? TextView
        setOnClickListener(mEmptyRetryTv)
        mStatusContentCl = itemView.findViewById<View>(R.id.cl_status_content) as? ConstraintLayout
        mStatusContentLl = itemView.findViewById(R.id.ll_status_content)
        mStatusBgIv = itemView.findViewById<View>(R.id.iv_status_bg) as? ImageView
        loadingViewLayout = itemView.findViewById(R.id.loadingViewLayout)
        mNoDataState = itemView.findViewById(R.id.tv_no_data_wrap)
        depositMoenyBtn = itemView.findViewById(R.id.bt_bank)
        setOnClickListener(depositMoenyBtn)
        if (resId != -1) mStatusBgIv?.setImageResource(resId)
        mEmptyLoadingTv?.visibility = View.GONE
        mStatusContentCl?.viewTreeObserver?.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                mStatusContentCl?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                val itemHeight = itemView.height
                (itemHeight - ResUtils.dp2px(50f)) / 2 - ResUtils.dp2px(75f)
                changeImageMarginTopByStatus()
            }
        })
    }

    override fun showError(i: Int, s: String?) {
        errorType = i
        errorMessage = s
        showLoadFailUi()
        if (!TextUtils.isEmpty(s)) {
            mEmptyMessageTv?.text = s
            mEmptyMessageTv?.setTextColor(ResUtils.getColor(R.color.skin_B1BCCC))
        }
        mHintImg?.visibility = View.VISIBLE
        depositMoenyBtn?.visibility = View.GONE
        if (checkNetWork()) return
        when (i) {
            LoadStatus.EMPTY_DATA_MESSAGE,
            LoadStatus.EMPTY_DATA,
            LoadStatus.EMPTY_DATA_RECORD,
            MESSAGE_NO_DATA -> {
                mHintImg?.setBackgroundResource(R.mipmap.ill_norecord)
                mEmptyRetryTv?.visibility = View.GONE
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            EMPTY_TRANSATION_DEPOSIT_RECORD -> {
                hideOtherView()
                mHintImg?.setBackgroundResource(R.mipmap.ill_norecord)
                mEmptyRetryTv?.visibility = View.GONE
                depositMoenyBtn?.visibility = View.VISIBLE
                setOnClickListener(mEmptyMessageTv)

            }
            BET_RECORD_EMPTY -> {
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                mEmptyRetryTv?.visibility = View.GONE
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            NOT_OPEN -> {
                mEmptyMessageTv?.text = "暂未开放 敬请期待"
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                mEmptyRetryTv?.visibility = View.GONE
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            NETWORK_ERROR -> {
                mEmptyMessageTv?.text = ResUtils.getString(R.string.network_error)
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            TRANSACTION -> {
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                mEmptyRetryTv?.visibility = View.GONE
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            NOT_BIND_PHONE -> {
                mEmptyGoTv?.visibility = View.VISIBLE
                mEmptyGoTv?.text = "完善个人资料"
//                mHintImg?.setImageResource(R.mipmap.pic_no_phone)
                setOnClickListener(mEmptyGoTv)
                hideOtherView()
            }
            REQUEST_ERROR_CODE -> {
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            RESPONSE_HANDLER_ERROR -> {
                mEmptyMessageTv?.text = "服务器异常"
//                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.img_ip_limit))
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            NOT_SPORTS_DATA -> {
                mEmptyMessageTv?.text = if (TextUtils.isEmpty(s)) "暂无赛事" else s
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                mEmptyRetryTv?.visibility = View.GONE
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            NOT_SPORTS_DATA_AND_MORE -> {
                mEmptyMessageTv?.text = if (TextUtils.isEmpty(s)) "暂无赛事" else s
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                mEmptyRetryTv?.visibility = View.GONE
                mMoreTv?.visibility = View.VISIBLE
                setOnClickListener(mEmptyMessageTv)
                setOnClickListener(mMoreTv)
                hideOtherView()
            }
            SHABA_LIMIT_DATA -> {
                mEmptyRetryTv?.visibility = View.GONE
                mHintImg?.visibility = View.GONE
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }
            COMMISSION_DATA -> {
                mHintImg?.setImageDrawable(ResUtils.getDrawable(R.mipmap.ill_norecord))
                mEmptyRetryTv?.visibility = View.GONE
                setOnClickListener(mEmptyMessageTv)
                hideOtherView()
            }

            MATCH_ANALYSIS_NO_DATA -> {
                mStatusContentCl?.visibility = View.GONE
                mNoDataState?.visibility = View.VISIBLE
                mNoDataState?.text = "暂无数据"
                hideOtherView()
            }
        }
    }

    /**
     * 检查网路
     *
     * @return
     */
    private fun checkNetWork(): Boolean {
        if (!NetworkUtils.isAvailable()) {
            mEmptyMessageTv?.visibility = View.VISIBLE
            mEmptyMessageTv?.text = ResUtils.getString(R.string.network_error)
            mHintImg?.setImageResource(R.mipmap.ill_norecord)
            mEmptyGoTv?.visibility = View.GONE
            mEmptyRetryTv?.visibility = View.GONE
            return true
        }
        return false
    }

    override fun showLoadding(s: String?) {
        showLoaddingUi(s)
    }

    private fun hideOtherView() {
        otherVies.forEach {
            it.visibility = View.GONE
        }
    }

    private fun showOtherView() {
        otherVies.forEach {
            it.visibility = View.VISIBLE
        }
    }

    fun changeImageMarginTopByStatus() {
        if (mStatusContentCl == null) return
        val contentHeight = ResUtils.dp2px(308f)
        val height = realHeight
        if (height < 0) return
        val newMarginTop = (height - contentHeight) / 3
        val params = mStatusContentCl?.layoutParams as ViewGroup.MarginLayoutParams
        params.topMargin = newMarginTop
        mStatusContentCl?.layoutParams = params
    }

    private val realHeight: Int
        get() {
            if (mStatusContentLl != null) {
                val location = IntArray(2)
                if (null != mStatusContentLl) {
                    mStatusContentLl?.getLocationOnScreen(location)
                }
                return ScreenUtil.screenHeight - location[1]
            }
            return -1
        }

    private fun showLoadFailUi() {
        mStatusContentCl?.visibility = View.VISIBLE
        llayLoadingLayout?.visibility = View.GONE
        mHintImg?.visibility = View.VISIBLE
        mEmptyMessageTv?.visibility = View.VISIBLE
        mEmptyGoTv?.visibility = View.GONE
        mMoreTv?.visibility = View.GONE
        mEmptyRetryTv?.visibility = View.GONE
    }

    private fun showLoaddingUi(msg: String?) {
        mStatusContentCl?.visibility = View.GONE
        llayLoadingLayout?.visibility = View.VISIBLE
        mHintImg?.visibility = View.GONE
        mEmptyMessageTv?.visibility = View.GONE
        mEmptyRetryTv?.visibility = View.GONE
        mEmptyGoTv?.visibility = View.GONE
        mMoreTv?.visibility = View.GONE
        if (!TextUtils.isEmpty(msg)) {
            mEmptyLoadingTv?.text = msg
        } else {
            mEmptyLoadingTv?.text = ""
        }
    }

    override fun success() {
        super.success()
        llayLoadingLayout?.visibility = View.GONE
        showOtherView()
    }

    fun setBackground(@DrawableRes resId: Int?) {
        this.resId = resId ?: 0
        if (mStatusBgIv != null) mStatusBgIv?.setImageResource(resId ?: 0)
    }

    companion object {

        @JvmField
        val NETWORK_ERROR = 11001 //网络异常

        @JvmField
        val BET_RECORD_EMPTY = -10 //暂无投注记录  投注列表页空数据时也使用此页

        @JvmField
        val NOT_OPEN = -11 //暂未开放,敬请期待 优惠活动空数据时也使用这个


        @JvmField
        val TRANSACTION = -12 //暂无交易记录  站内消息空数据时也使用此类型

        @JvmField
        val MESSAGE_NO_DATA = -13 //消息

        @JvmField
        val NOT_BIND_PHONE = -14 //暂未绑定手机号码

        @JvmField
        val NOT_SPORTS_DATA = -15 //暂无比赛数据

        @JvmField
        val SHABA_LIMIT_DATA = -16 //沙巴限制

        @JvmField
        val COMMISSION_DATA = -17 //佣金

        @JvmField
        val REQUEST_ERROR_CODE = 11005 //请求异常

        @JvmField
        val RESPONSE_HANDLER_ERROR =11007 //json解析失败等

        @JvmField
        val MATCH_ANALYSIS_NO_DATA = -19 //赛事分析暂无数据


        @JvmField
        val NOT_SPORTS_DATA_AND_MORE = -20 //暂无比赛数据和更多按钮（按钮4。0通用）

        @JvmField
        val EMPTY_TRANSATION_DEPOSIT_RECORD = -21
    }
}