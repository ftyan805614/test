package com.sports.wallpaper.ui.login

import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentLoginBinding
import com.sports.wallpaper.viewmodel.LoginViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class LoginFragmennt : AppBaseMvvmFragment<FragmentLoginBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun initViewModel(bind: FragmentLoginBinding) {
        bind.viewModel = ofScope(requireActivity(), this, LoginViewModel::class.java)
    }

    override fun onBackPressedSupport(): Boolean {
        exitApp()
        return true
    }
}