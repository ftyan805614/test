package com.sports.wallpaper.ui.mine

import androidx.databinding.ViewDataBinding
import com.sports.commonn.mvvm.findNavController
import com.sports.commonn.mvvm.ui.widget.CommonTitleBar
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentStartBinding
import com.sports.wallpaper.viewmodel.StartViewModel
import kotlinx.android.synthetic.main.fragment_mine_protocol.*
import kotlinx.android.synthetic.main.fragment_start.*

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class ProtocolFragmennt : AppBaseMvvmFragment<ViewDataBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_mine_protocol
    }

    override fun initView() {
        super.initView()
        initTitleByCenterView()?.setMainTitle("用户条款")
            ?.setMainTitleSize(16f)
            ?.setMainTitleColor(R.color.skin_3F5075_FBFBFB)
            ?.setLeftText("返回")
            ?.setLeftType(CommonTitleBar.TYPE_LEFT_IMAGEBUTTON)
            ?.setLeftTxtImg(R.mipmap.com_icon_navback, 0)
            ?.setLeftOnClick {
                exitActivity()
            }
        webview.loadUrl("file:///android_asset/protocol.html");
    }

    override fun initViewModel(bind: ViewDataBinding) {
    }

}