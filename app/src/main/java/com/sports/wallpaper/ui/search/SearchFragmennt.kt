package com.sports.wallpaper.ui.search

import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.sports.commonn.utils.getSF
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentSearchBinding
import com.sports.wallpaper.viewmodel.SearchViewModel
import com.zhy.view.flowlayout.FlowLayout
import com.zhy.view.flowlayout.TagAdapter
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class SearchFragmennt : AppBaseMvvmFragment<FragmentSearchBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_search
    }

    override fun initViewModel(bind: FragmentSearchBinding) {
        bind.viewModel = ofScope(requireActivity(), this, SearchViewModel::class.java)
        bind.viewModel?.getHistorySearch()
        bind.viewModel?.getHotSearchs()
        bind.viewModel?.bindHistorySearch?.observe(this, {
            flowlayout.adapter = object : TagAdapter<String?>(it) {
                override fun getView(parent: FlowLayout, position: Int, s: String?): View {
                    val view = LayoutInflater.from(context).inflate(
                        R.layout.search_item_word,
                        flowlayout, false
                    )
                    val tv = view.findViewById<TextView>(R.id.tv_title)
                    tv.text = s
                    return view
                }
            }
            flowlayout.setOnTagClickListener { _, position, _ ->
                bind.viewModel?.gotoSearchResult(it.getSF(position))
                true
            }
        })
    }

    override fun initView() {
        super.initView()
        et_search.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                bind.viewModel?.doSearch(et_search.text.toString())
                return@OnKeyListener true
            }
            false
        })
    }
}