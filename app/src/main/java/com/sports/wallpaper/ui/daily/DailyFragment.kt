package com.sports.wallpaper.ui.daily

import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentDailyBinding
import com.sports.wallpaper.viewmodel.DailyViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class DailyFragment : AppBaseMvvmFragment<FragmentDailyBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_daily
    }

    override fun initViewModel(bind: FragmentDailyBinding) {
        bind.viewModel = ofScope(requireActivity(), this, DailyViewModel::class.java)
        bind.viewModel?.getDailyWallpapers(true)
    }

    override fun initView() {
        super.initView()
        initTitleByCenterView()?.setMainTitle("每日壁纸")
            ?.setMainTitleSize(16f)
            ?.setMainTitleColor(R.color.skin_3F5075_FBFBFB)
    }
}