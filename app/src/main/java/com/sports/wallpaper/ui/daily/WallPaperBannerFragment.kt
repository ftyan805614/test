package com.sports.wallpaper.ui.daily

import com.sports.commonn.entity.ImgInfo
import com.sports.commonn.utils.getSF
import com.sports.wallpaper.R
import com.sports.wallpaper.adapter.ImageNetAdapter
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentDailyWallpaperBannerBinding
import com.sports.wallpaper.viewmodel.WallPaperBannerViewModel
import com.youth.banner.listener.OnPageChangeListener
import kotlinx.android.synthetic.main.fragment_daily_wallpaper_banner.*

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class WallPaperBannerFragment : AppBaseMvvmFragment<FragmentDailyWallpaperBannerBinding>() {

    companion object {
        const val argmentKey = "argmentKey_data"
        const val argmentTitleKey = "argmentKey_title"
        const val argmentPosKey = "argmentKey_pos"
    }

    var data: ArrayList<ImgInfo>? = null

    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_daily_wallpaper_banner
    }

    override fun initViewModel(bind: FragmentDailyWallpaperBannerBinding) {
        bind.viewModel = ofScope(requireActivity(), this, WallPaperBannerViewModel::class.java)
    }

    override fun initView() {
        super.initView()
        initTitleByCenterView()?.setMainTitle(arguments?.getString(argmentTitleKey))
            ?.setMainTitleSize(16f)
            ?.setMainTitleColor(R.color.skin_3F5075_FBFBFB)

        val pos = arguments?.getInt(argmentPosKey) ?: 0
        /**
         * 画廊效果
         */
        data = arguments?.get(argmentKey) as? ArrayList<ImgInfo>
        bind.viewModel?.setCurrentItem(data?.getSF(pos))

        val adapter = ImageNetAdapter(data)
        banner1.adapter = adapter
        banner1.setIndicator(indicator_out, false)
        //添加画廊效果
        banner1.setBannerGalleryEffect(18, 10)

        banner1.addOnPageChangeListener(listener)
        banner1.currentItem = pos + 1

    }

    val listener = object : OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            val item = data?.getSF(position)
            bind.viewModel?.setCurrentItem(item)
        }

        override fun onPageScrollStateChanged(state: Int) {
        }

    }
}