package com.sports.wallpaper.ui;


import com.sports.commonn.mvvm.ui.widget.StatusAdapter;
import com.sports.commonn.utils.StatusViewHelper;
import com.sports.wallpaper.ui.BobStatusViewAdapter;

/**
 * Desc:
 * Created by Jeff on 2018/9/26
 **/
public class DefaultLoadStatusFactory implements StatusViewHelper.Factory {
    @Override
    public StatusAdapter adapter() {
        return new BobStatusViewAdapter();
    }
}
