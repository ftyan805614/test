package com.sports.wallpaper.ui.mine

import com.sports.commonn.mvvm.ui.widget.CommonTitleBar
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentMineResultBinding
import com.sports.wallpaper.viewmodel.RecordResultViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class RecordResultFragment : AppBaseMvvmFragment<FragmentMineResultBinding>() {

    companion object {
        const val argmentKey = "argmentKey_category"
        const val categoryDownload = "category_download"
        const val categoryCollect = "category_collect"
    }

    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_mine_result
    }

    override fun initViewModel(bind: FragmentMineResultBinding) {
        bind.viewModel = ofScope(requireActivity(), this, RecordResultViewModel::class.java)
    }

    override fun initView() {
        super.initView()
        val category = arguments?.getString(argmentKey)
        bind.viewModel?.category = category

        val title = getTitle(category)
        bind.viewModel?.title = title
        initTitleByCenterView()?.setMainTitle(title)
            ?.setMainTitleSize(16f)
            ?.setMainTitleColor(R.color.skin_3F5075_FBFBFB)
            ?.setLeftText("返回")
            ?.setLeftType(CommonTitleBar.TYPE_LEFT_IMAGEBUTTON)
            ?.setLeftTxtImg(R.mipmap.com_icon_navback, 0)
            ?.setLeftOnClick {
                exitActivity()
            }
        bind.viewModel?.getRecordResult(true)
    }

    private fun getTitle(category: String?): String {
        return when (category) {
            categoryDownload -> "下载记录"
            else -> "我的收藏"
        }
    }
}