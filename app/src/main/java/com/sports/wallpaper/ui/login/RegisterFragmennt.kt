package com.sports.wallpaper.ui.login

import androidx.lifecycle.Observer
import com.sports.commonn.mvvm.ui.widget.CommonTitleBar
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentLoginBinding
import com.sports.wallpaper.databinding.FragmentRegisterBinding
import com.sports.wallpaper.viewmodel.LoginViewModel
import com.sports.wallpaper.viewmodel.RegisterViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class RegisterFragmennt : AppBaseMvvmFragment<FragmentRegisterBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_register
    }

    override fun initViewModel(bind: FragmentRegisterBinding) {
        bind.viewModel = ofScope(requireActivity(), this, RegisterViewModel::class.java)
        bind.viewModel?.goBack?.observe(this, Observer {
            if (it) {
                onBackPressedSupport()
            }
        })
    }

    override fun initView() {
        super.initView()
        initTitleByCenterView()?.setMainTitle("")
            ?.setLeftText("返回")
            ?.setLeftType(CommonTitleBar.TYPE_LEFT_IMAGEBUTTON)
            ?.setLeftTxtImg(R.mipmap.com_icon_navback, 0)
            ?.setLeftOnClick {
                exitActivity()
            }
    }

}