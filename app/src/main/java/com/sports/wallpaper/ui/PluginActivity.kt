package com.sports.wallpaper.ui

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import androidx.fragment.app.Fragment
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.YaboLib
import com.sports.commonn.memory.DataManager
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager
import com.sports.commonn.mvvm.ui.CommonActivity
import com.sports.commonn.utils.StatusBarUtil
import com.sports.wallpaper.R

/**
 * 描述：fragment加载类
 * 日期：2020-03-08
 */
open class PluginActivity : CommonActivity() {

    var event: MotionEvent? = null

    var isTouch = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBarNight()
    }

    open fun setBarNight() {
        if (DataManager.appIsNightTheme.value != null && DataManager.appIsNightTheme.value == true) {
            StatusBarUtil.setAndroidNativeLightStatusBar(this, false)
        } else {
            StatusBarUtil.setAndroidNativeLightStatusBar(this, true)
        }
    }

//    override fun applySkin() {
//        setBarNight()
//    }

    override fun contentViewLayoutID(): Int {
        return R.layout.activity_fragment_plugin
    }

    override fun initViewsAndEvents(paramBundle: Bundle?) {
        try {
            val intent = intent
            val bundle = intent.getBundleExtra(KEY_PARAMS)
            val stringClass = intent.getStringExtra(KEY_FRAGMENT)
            if (!stringClass.isNullOrEmpty()) {
                val fClass = Class.forName(stringClass)
                val fragment = fClass.newInstance() as Fragment
                LogUtils.i("bundle %s", if (bundle == null) "is  null" else " is not null")
                if (bundle != null) {
                    fragment.arguments = bundle
                }
                loadRootFragment(R.id.fl_fragment_plugin_content, fragment)
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }


    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_CANCEL) {
            isTouch = false
        }
        if (ev.action == MotionEvent.ACTION_DOWN || ev.action == MotionEvent.ACTION_MOVE) {
            isTouch = true
        }
        this.event = ev
        return super.dispatchTouchEvent(ev)
    }


    companion object {
        const val KEY_PARAMS = "params"
        const val KEY_FRAGMENT = "fragment"
        const val isNewTask = "isNewTask"

        @JvmStatic
        fun go(toClass: Class<out Fragment>, options: Bundle? = null) {
            val intent = Intent(YaboLib.applicationContext, PluginActivity::class.java)
            intent.putExtra(KEY_FRAGMENT, toClass.canonicalName)
            if (options != null) {
                intent.putExtra(KEY_PARAMS, options)
                val isNewTask = options.getBoolean(isNewTask, false)
                if (isNewTask) {
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
            } else {
                val options = Bundle()
                intent.putExtra(KEY_PARAMS, options)
            }
            if (TopLifecyclerOwnerManager.instance.curActivity != null) {
                TopLifecyclerOwnerManager.instance.curActivity?.startActivity(intent)
            }
        }


    }


    /**
     * 描述：Android系统 6.0以下AudioManager内存泄漏问题  播放视频内存泄漏
     * 日期：2019-07-07
     * 参数：
     * 返回值：
     */
    override fun attachBaseContext(newBase: Context) {
        try {
            super.attachBaseContext(object : ContextWrapper(newBase) {
                override fun getSystemService(name: String): Any? {
                    // 解决 VideoView 中 AudioManager 造成的内存泄漏
                    return if (Context.AUDIO_SERVICE == name && null != applicationContext) {
                        applicationContext.getSystemService(name)
                    } else super.getSystemService(name)
                }
            })
        } catch (e: Exception) {
            super.attachBaseContext(newBase)
        }
    }


}
