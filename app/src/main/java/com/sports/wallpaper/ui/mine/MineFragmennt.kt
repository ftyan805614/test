package com.sports.wallpaper.ui.mine

import android.content.Intent
import com.sports.commonn.memory.DataManager
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentMineBinding
import com.sports.wallpaper.viewmodel.MineViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class MineFragmennt : AppBaseMvvmFragment<FragmentMineBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_mine
    }

    override fun initViewModel(bind: FragmentMineBinding) {
        bind.viewModel = ofScope(requireActivity(), this, MineViewModel::class.java)
        bind.viewModel?.bindHeadUrl?.value = DataManager.imgServerHost + DataManager.user?.avatar
    }

    override fun onResume() {
        super.onResume()
        bind.viewModel?.bindUsername?.value = bind.viewModel?.userName
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        bind.viewModel?.onActivityResult(requestCode, resultCode, data)
    }
}