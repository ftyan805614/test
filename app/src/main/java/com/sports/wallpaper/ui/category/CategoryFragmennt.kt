package com.sports.wallpaper.ui.category

import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentCategoryBinding
import com.sports.wallpaper.databinding.FragmentLoginBinding
import com.sports.wallpaper.databinding.FragmentStartBinding
import com.sports.wallpaper.viewmodel.CategoryViewModel
import com.sports.wallpaper.viewmodel.LoginViewModel
import com.sports.wallpaper.viewmodel.StartViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class CategoryFragmennt : AppBaseMvvmFragment<FragmentCategoryBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_category
    }

    override fun initViewModel(bind: FragmentCategoryBinding) {
        bind.viewModel = ofScope(requireActivity(), this, CategoryViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        if (bind.viewModel?.bindSubjectCategoryData?.value?.size ?: 0 == 0) {
            bind.viewModel?.getSubjectCategorys(true)
        }
    }
}