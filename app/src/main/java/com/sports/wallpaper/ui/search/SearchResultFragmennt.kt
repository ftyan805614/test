package com.sports.wallpaper.ui.search

import com.sports.commonn.mvvm.ui.widget.CommonTitleBar
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentSearchResultBinding
import com.sports.wallpaper.viewmodel.SearchResultViewModel

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class SearchResultFragmennt : AppBaseMvvmFragment<FragmentSearchResultBinding>() {

    companion object {
        const val argmentKey = "argmentKey_search"
    }

    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_search_result
    }

    override fun initViewModel(bind: FragmentSearchResultBinding) {
        bind.viewModel = ofScope(requireActivity(), this, SearchResultViewModel::class.java)
    }

    override fun initView() {
        super.initView()
        val search = arguments?.getString(argmentKey)
        bind.viewModel?.searchWord = search
        initTitleByCenterView()?.setMainTitle(search)
            ?.setMainTitleSize(16f)
            ?.setMainTitleColor(R.color.skin_3F5075_FBFBFB)
            ?.setLeftText("返回")
            ?.setLeftType(CommonTitleBar.TYPE_LEFT_IMAGEBUTTON)
            ?.setLeftTxtImg(R.mipmap.com_icon_navback, 0)
            ?.setLeftOnClick {
                exitActivity()
            }
        bind.viewModel?.getSearchResultByWord(search)
    }
}