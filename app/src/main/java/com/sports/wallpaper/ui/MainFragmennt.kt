package com.sports.wallpaper.ui

import android.Manifest
import android.view.View
import androidx.core.app.ActivityCompat
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager
import com.sports.commonn.utils.PermissionUtil
import com.sports.commonn.utils.getSF
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentMainBinding
import com.sports.wallpaper.ui.category.CategoryFragmennt
import com.sports.wallpaper.ui.daily.DailyFragment
import com.sports.wallpaper.ui.mine.MineFragmennt
import com.sports.wallpaper.ui.search.SearchFragmennt
import com.sports.wallpaper.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_main.*

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class MainFragmennt : AppBaseMvvmFragment<FragmentMainBinding>() {

    private var mCurrTv: View? = null
    private var mClickView: View? = null
    private var currentIndex = 0
    private var pageFragment =
        arrayListOf(DailyFragment(), CategoryFragmennt(), SearchFragmennt(), MineFragmennt())


    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_main
    }

    override fun initViewModel(bind: FragmentMainBinding) {
        bind.viewModel = ofScope(requireActivity(), this, MainViewModel::class.java)
        bind.viewModel?.initCollectionList()
        bind.viewModel?.initDownloadList()
    }

    override fun initView() {
        super.initView()
        loadMultipleRootFragment(
            R.id.fragment_container,
            currentIndex,
            pageFragment,
            currentIndex == 0
        )
        onSelectView(currentIndex, false)
        ll_daily?.setOnClickListener(this)
        ll_category?.setOnClickListener(this)
        ll_search?.setOnClickListener(this)
        ll_mine?.setOnClickListener(this)

        if (PermissionUtil.checkPermissionAllGranted(
                activity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
            )
        ) {
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                100
            )
        }

        if (!PermissionUtil.checkPermissionAllGranted(
                TopLifecyclerOwnerManager.instance.curActivity,
                PermissionUtil.PERMISSION_CAMERA_WRITE
            )
        ) {
            PermissionUtil.isPermissionsApply(TopLifecyclerOwnerManager.instance.curActivity, PermissionUtil.PERMISSION_CAMERA_WRITE)
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let { setSelectId(it) }
    }


    private fun setSelectId(view: View) {
        //重复点击当前fragment并不进行操作
        if (mClickView == view) return
        mClickView = view
        when (view.id) {
            R.id.ll_search -> {
                onSelectView(2)
            }
            R.id.ll_daily -> {
                onSelectView(0)
            }
            R.id.ll_category -> {
                onSelectView(1)
            }
            R.id.ll_mine -> {
                onSelectView(3)
            }
        }
    }

    private fun onSelectView(
        positin: Int,
        isNeedSwithcFragment: Boolean = true
    ) {
        mCurrTv?.isSelected = false
        when (positin) {
            0 -> {
                mCurrTv = tv_daily
                currentIndex = 0
                mCurrTv?.isSelected = true
            }
            1 -> {
                mCurrTv = tv_category
                currentIndex = 1
                mCurrTv?.isSelected = true
            }
            2 -> {
                mCurrTv = tv_search
                currentIndex = 2
                mCurrTv?.isSelected = true
            }
            3 -> {
                mCurrTv = tv_mine
                currentIndex = 3
                mCurrTv?.isSelected = true
            }
        }
        if (isNeedSwithcFragment) {
            showHideFragment(fragments?.getSF(positin))
        }
    }

    override fun onBackPressedSupport(): Boolean {
        exitApp(2000)
        return true
    }
}