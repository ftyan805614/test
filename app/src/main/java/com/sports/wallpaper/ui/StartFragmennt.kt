package com.sports.wallpaper.ui

import com.sports.commonn.mvvm.findNavController
import com.sports.wallpaper.R
import com.sports.wallpaper.base.AppBaseMvvmFragment
import com.sports.wallpaper.databinding.FragmentStartBinding
import com.sports.wallpaper.viewmodel.StartViewModel
import kotlinx.android.synthetic.main.fragment_start.*

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class StartFragmennt : AppBaseMvvmFragment<FragmentStartBinding>() {
    override fun contentViewLayoutId(): Int {
        return R.layout.fragment_start
    }

    override fun initViewModel(bind: FragmentStartBinding) {
        bind.viewModel = ofScope(requireActivity(), this, StartViewModel::class.java)
    }

    override fun initView() {
        super.initView()
        cl_root.postDelayed(delayGoRun, 2000)
    }

    private val delayGoRun = Runnable {
        findNavController()?.navigate(R.id.loginFragement)
    }

    override fun onBackPressedSupport(): Boolean {
        cl_root.removeCallbacks(delayGoRun)
        exitApp()
        return true
    }
}