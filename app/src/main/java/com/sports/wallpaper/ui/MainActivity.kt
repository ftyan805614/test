package com.sports.wallpaper.ui

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.sports.wallpaper.R
import com.sports.wallpaper.manger.IntentManager
import kotlinx.android.synthetic.main.activity_start.*

class MainActivity : PluginActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        val navHostFragment = nav_fragment as NavHostFragment
        val inflater = navHostFragment.navController.navInflater
        val graph = inflater.inflate(R.navigation.home_grah)
        val isLogin = intent?.getBooleanExtra(IntentManager.ISLOGIN_KEY, false) ?: false
        if (isLogin) { //到登录页面
            graph.startDestination = R.id.loginFragement
        } else {  //到启动页
            graph.startDestination = R.id.startFragment
        }
        navHostFragment.navController.graph = graph
    }
}