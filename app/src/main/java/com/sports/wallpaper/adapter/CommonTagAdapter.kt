package com.sports.wallpaper.adapter

import com.zhy.view.flowlayout.TagAdapter

/**
 * author:Averson
 * createTime:2021/3/31
 * descriptions:
 *
 **/
abstract class CommonTagAdapter(data: List<String>) : TagAdapter<String>(data) {

}