package com.sports.net;

import android.util.Log;

import com.sports.commonn.memory.DataManager;
import com.sports.net.interceptor.LogInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.fastjson.FastJsonConverterFactory;

/**
 * 创建时间：2019/8/28
 * 编写人：Averson
 * 功能描述：
 */

public class RetroCommonFactory {
    //1开发环境 2.测试环境 3预发布环境
    private static final int ENVIRONMENT_TYPE = 2;

    private static final String TAG = RetroCommonFactory.class.getName();
    public static String BASE_HOST_URL = "http://api.tybzbz.com"; //sklee //业务dev地址
//    public static final String BASE_HOST_URL = "http://161.117.195.63"; //测试环境
//    public static final String BASE_HOST_URL = "http://47.75.122.104"; //预发布环境


    //图片加载路径
    public static final String IMAGE_HOST_URL = "http://161.117.194.247:7788";
    public static final String IMAGE_EXTRA = "/fastdfs/download/image?filePath=";

    private static final long TIMEOUT = 30;

    static {
        switch (ENVIRONMENT_TYPE) {
            case 1:
                //开发环境
                BASE_HOST_URL = "http://api.tybzbz.com";
                break;
            case 2:
                //测试环境
                BASE_HOST_URL = "http://api.tybzbz.com";
                break;
            case 3:
                //预发布环境
                BASE_HOST_URL = "http://api.tybzbz.com";
                break;
        }
    }

    /**
     * 构建OkHttpClient
     */
    public static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new HeadTokenInterceptor())
                .addInterceptor(new HttpLoggingInterceptor(message -> Log.e(TAG, "请求参数:" + message))
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(new LogInterceptor(false))
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS);
        HttpSSLSetting.SSLParams sslParams1 = HttpSSLSetting.getSslSocketFactory();
        builder.sslSocketFactory(sslParams1.sSLSocketFactory, sslParams1.trustManager);
        return builder.build();
    }

    private static Retrofit mRetrofit = new Retrofit.Builder()
            .baseUrl(BASE_HOST_URL)
            //.addConverterFactory(GsonConverterFactory.create(buildGson()))
            .addConverterFactory(FastJsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(getOkHttpClient())
            .build();


    public static Retrofit getInstance() {
        return mRetrofit;
    }



/*    private static Gson buildGson() {
        return new GsonBuilder()
                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                // 此处可以添加Gson 自定义TypeAdapter
//                .registerTypeAdapter(UserInfo.class, new UserInfoTypeAdapter())
                .create();
    }*/

    /**
     * 请求头拦截器，添加公共参数
     */
    private static class HeadTokenInterceptor implements Interceptor {

        HeadTokenInterceptor() {

        }

        @Override
        public Response intercept(Chain chain) {
            try {

                Request oldRequest = chain.request();
                // 添加新的参数
                String token = DataManager.getToken();
                HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                        .newBuilder()
                        .scheme(oldRequest.url().scheme())
                        .host(oldRequest.url().host())
                        .addQueryParameter("token", token);

                // 新的请求
                Request newRequest = oldRequest.newBuilder()
                        .method(oldRequest.method(), oldRequest.body())
                        .url(authorizedUrlBuilder.build())
                        .header("Content-Type", "application/json")
                        .header("deviceType", String.valueOf(3))
                        .header("deviceImei", "GlobalConstants.S_ANDROID_DEVICE_ID")
                        .build();
                return chain.proceed(newRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
