package com.sports.commonn

import android.app.Application
import android.text.TextUtils
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager.instance
import com.sports.commonn.mvvm.ui.widget.Config
import com.sports.commonn.mvvm.ui.widget.DefaultStatusFactory

/**
 * Desc:
 * Created by Jeff on 2018/9/9
 */
object YaboLib : Application() {

    override fun onCreate() {
        super.onCreate()
        init(this)
    }

    @JvmStatic
    lateinit var app: Application


    @JvmField
    var isDebug = false

    @JvmStatic
    var config: Config? = null
        private set

    @JvmField
    var realPackageName: String? = null


    fun init(context: Application, pakgeName: String?): Config? {
        return init(context, false, pakgeName)
    }

    fun init(context: Application, isDebug: Boolean): Config? {
        return init(context, isDebug, null)
    }

    fun init(context: Application, debug: Boolean = false, pakgeName: String? = null): Config {
        if (config == null) {
            app = context
            isDebug = debug
            packageName = if (!TextUtils.isEmpty(pakgeName)) {
                pakgeName
            } else {
                context.packageName
            }
            initLog()
            instance.init(app!!)
            config = Config()
                    .setStatusFactory(DefaultStatusFactory())
        }
        return config!!
    }

    private fun initLog() {
        LogUtils.getLogConfig()
                .configAllowLog(isDebug)
                .configShowBorders(false)
    }

    fun setPackageName(packageName: String?) {
        realPackageName = packageName
    }

    fun setApplication(application: Application) {
        this.app = application
        init(application, isDebug)
    }
}