package com.sports.commonn.mvvm

import android.animation.*
import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.Editable
import android.text.Html
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.LinearInterpolator
import android.webkit.WebView
import android.widget.*
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.animation.doOnEnd
import androidx.core.view.doOnLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.apkfuns.logutils.LogUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.SingleRequest
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.appbar.AppBarLayout
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import com.sports.commonn.R
import com.sports.commonn.utils.RecyclerViewUtils
import com.sports.commonn.utils.ResUtils
import java.lang.ref.WeakReference
import java.lang.reflect.Field
import java.util.*


/**
 * created by Albert
 */
object CommonBind {

    var isLiveFleid: Field? = null

    init {
        try {
            isLiveFleid = ViewTreeObserver::class.java.getDeclaredField("mAlive")
            isLiveFleid?.isAccessible = true
        } catch (e: Exception) {

        }

    }

    @JvmStatic
    @BindingAdapter("bindRecyclerItemDecoration")
    fun setItemDecoration(view: RecyclerView, newVal: ItemDecoration?) {
        if (newVal != null) {
            if (view.itemDecorationCount != 0) {
                view.removeItemDecorationAt(view.itemDecorationCount - 1)
                view.addItemDecoration(newVal)
            } else {
                view.addItemDecoration(newVal)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("bindRecyclerHasfixedsize")
    fun setItemDecoration(view: RecyclerView, newVal: Boolean?) {
        if (newVal != null) {
            view.setHasFixedSize(newVal)
        }
    }

    @JvmStatic
    @BindingAdapter(
        value = [
            "bindViewExpand", "bindViewExpandFromUser", "bindExpandDuration"
        ], requireAll = false
    )
    fun bindExpand(
        view: View,
        expand: MutableLiveData<Boolean>?,
        isFromUser: Boolean?,
        time: Long? = 300
    ) {
        if (isFromUser == false) {
            view.setTag(R.id.view_expanded, expand?.value)
        }
        if (expand != null && expand.value != null) {
            if (isFromUser == true) {
                Log.i("bindExpand", "bindExpand expand: ${expand?.value} isFromUser :${isFromUser}")
                if (expand.value == true) {
                    expand(view, time)
                } else {
                    collapse(view, time)
                }
            } else {
                if (expand.value == true) {
                    view.measure(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    val viewHeight = view.measuredHeight
                    view.layoutParams.height = viewHeight
                    view.requestLayout()
                    view.visibility = View.VISIBLE
                } else {

                    view.visibility = View.GONE
                }
            }
        } else {
            view.visibility = View.GONE
        }

    }


    private fun expand(view: View, time: Long?) {
        view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val viewHeight = view.measuredHeight
        view.layoutParams.height = 0
        view.visibility = View.VISIBLE
        val animation = ObjectAnimator.ofInt(0, viewHeight)
        animation.doOnEnd {
            view.layoutParams.height = viewHeight

        }
        animation.addUpdateListener {
            view.layoutParams.height = it.animatedValue as Int
            view.requestLayout()
        }
        animation.duration = time ?: 300
        animation.interpolator = FastOutLinearInInterpolator()
        animation.start()
    }

    private fun collapse(view: View, time: Long?) {
        view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val viewHeight = view.measuredHeight
        val animation = ObjectAnimator.ofInt(viewHeight, 0)
        animation.addUpdateListener {
            view.layoutParams.height = it.animatedValue as Int
            view.requestLayout()
        }
        animation.doOnEnd {
            view.visibility = View.GONE
        }
        animation.duration = time ?: 300
        animation.interpolator = FastOutLinearInInterpolator()
        animation.start()
    }

    @JvmStatic
    @BindingAdapter(value = ["bindTextHtml"], requireAll = false)
    fun bindTextHtml(textView: TextView, text: String?) {
        if (!text.isNullOrEmpty()) {
            textView.text = Html.fromHtml(text)
        }
    }

    @BindingAdapter(
        value = [
            "bindLoadMoreEnable",
            "bindRefreshEnable",
            "bindRefreshState",
            "bindLoadMoreState"], requireAll = false
    )
    @JvmStatic
    fun bindSmartListener(
        smartRefreshLayout: SmartRefreshLayout,
        bindLoadMoreEnable: MutableLiveData<Boolean>?,
        bindRefreshEnable: MutableLiveData<Boolean>?,
        bindRefreshSate: MutableLiveData<Boolean>?,
        bindLoadMoreState: MutableLiveData<Boolean>?
    ) {

        if (bindLoadMoreEnable != null) {
            smartRefreshLayout.setEnableLoadMore(bindLoadMoreEnable.value ?: false)
        }

        if (bindRefreshEnable != null) {
            smartRefreshLayout.setEnableRefresh(bindRefreshEnable.value ?: false)
        }

        if (bindRefreshSate != null && bindRefreshSate.value != null) {
            if (bindRefreshSate.value == false) {
                smartRefreshLayout.finishRefresh()
            } else {
                smartRefreshLayout.autoRefresh()
            }
            bindRefreshSate.value = null
        }
        if (bindLoadMoreState != null && bindLoadMoreState.value != null) {
            if (bindLoadMoreState.value == false) {
                smartRefreshLayout.finishLoadMore()
            } else {
                smartRefreshLayout.autoLoadMore()
            }
            bindLoadMoreState.value = null
        }
    }


    @JvmStatic
    @BindingAdapter(
        value = [
            "bindRefreshListener",
            "bindLoadMoreListener",
            "bindRefreshLoadMoreListener"
        ], requireAll = false
    )
    fun bindDoRefresh(
        refreshLayout: SmartRefreshLayout, listener: OnRefreshListener?,
        loadMoreListener: OnLoadMoreListener?, loadMoreRefreshListener: OnRefreshLoadMoreListener?
    ) {
        if (listener != null) {
            refreshLayout.setOnRefreshListener(listener)
        }
        if (loadMoreListener != null) {
            refreshLayout.setOnLoadMoreListener(loadMoreListener)
        }
        if (loadMoreRefreshListener != null) {
            refreshLayout.setOnRefreshLoadMoreListener(loadMoreRefreshListener)
        }
    }


    @JvmStatic
    @BindingAdapter(value = ["focusChangeListener"], requireAll = false)
    fun focusChanged(editText: EditText, listener: View.OnFocusChangeListener?) {
        editText.onFocusChangeListener = listener
    }


    @JvmStatic
    @BindingAdapter(value = ["textSize"], requireAll = false)
    fun bindTextSize(text: TextView, textSize: Float) {
        text.textSize = textSize
    }


    @JvmStatic
    @BindingAdapter("bindLayoutHeight")
    fun bindLayoutHeight(view: View, height: Int) {
        val layoutParams = view.layoutParams
        if (layoutParams != null && layoutParams.height != height) {
            layoutParams.height = height
            view.layoutParams = layoutParams
            view.requestLayout()
        }
    }

    @JvmStatic
    @BindingAdapter("bindLayoutWidth")
    fun bindLayoutWidth(view: View, width: Int) {
        val layoutParams = view.layoutParams
        if (layoutParams != null && layoutParams.width != width) {
            layoutParams.width = width
            view.layoutParams = layoutParams
            view.requestLayout()
        }

    }


    @SuppressLint("CheckResult")
    @JvmStatic
    @BindingAdapter(
        value = ["bindGlidUrl",
            "bindLocalIcon",
            "bindGlideUri",
            "bindGlidePlaceholder",
            "android:src",
            "bindImageCorner",
            "bindGlideCompressionRatio", "bindGlideWithLifecyclerOwner", "bindGlideOnImageLoadReady"],
        requireAll = false
    )
    fun bindGlidUrl(
        imageView: ImageView,
        url: String?,
        localIcon: Int?,
        uri: Uri?,
        placeholder: Int?,
        src: Int?,
        imageCorner: Int?,
        compressionRatio: Float? = 0.5f,
        lifecycleOwner: LifecycleOwner?,
        readyListener: OnImageLoadReady?
    ) {
        try {
            if ((src == null || src == 0) && url.isNullOrEmpty() && uri == null && (localIcon == null && localIcon == 0)) {
                imageView.setImageDrawable(null)
            } else if (src != null && src != 0) {
                imageView.setImageDrawable(ResUtils.getDrawable(src))
            } else if (imageView.tag is SingleRequest<*>) {
                val request = (imageView.tag as? SingleRequest<*>)
                request?.clear()
            }
        } catch (e: Exception) {

        }
        val wight = (imageView.getTag(R.id.bind_view_wight) as? Int) ?: 0
        val height = (imageView.getTag(R.id.bind_view_height) as? Int) ?: 0

        if (wight == 0 || height == 0) {
            imageView.doOnLayout {
                it.setTag(R.id.bind_view_wight, imageView.measuredWidth)
                it.setTag(R.id.bind_view_height, imageView.measuredHeight)
                if (imageView.measuredWidth > 0 && imageView.measuredHeight > 0) {
                    loadImage(
                        imageView,
                        localIcon,
                        url,
                        placeholder,
                        uri,
                        imageView.measuredWidth,
                        imageView.measuredHeight,
                        imageCorner,
                        compressionRatio,
                        lifecycleOwner,
                        readyListener
                    )
                } else {
                    loadImage(
                        imageView,
                        localIcon,
                        url,
                        placeholder,
                        uri,
                        0,
                        0,
                        imageCorner,
                        compressionRatio,
                        lifecycleOwner,
                        readyListener
                    )
                }
            }
        } else {
            loadImage(
                imageView,
                localIcon,
                url,
                placeholder,
                uri,
                wight,
                height,
                imageCorner,
                compressionRatio,
                lifecycleOwner,
                readyListener
            )
        }
    }


    @SuppressLint("CheckResult")
    private fun loadImage(
        imageView: ImageView,
        localIcon: Int?,
        url: String?,
        placeholder: Int?,
        uri: Uri?,
        width: Int,
        height: Int,
        imageCorner: Int?,
        compressionRatio: Float?,
        lifecycleOwner: LifecycleOwner?,
        readyListener: OnImageLoadReady?
    ) {
        try {
            val requestOptions = RequestOptions()
                .error(ResUtils.getDrawable(placeholder ?: 0))
                .placeholder(ResUtils.getDrawable(placeholder ?: 0))
            if (imageCorner ?: 0 > 0) {
                val roundedCorners = RoundedCorners(ResUtils.dp2px((imageCorner ?: 0).toFloat()))
                requestOptions.transform(roundedCorners)
            }
            val comratio = compressionRatio ?: 0.5f
            val context = if (lifecycleOwner is FragmentActivity) {
                lifecycleOwner
            } else if (imageView.context is Activity) {
                val activity = imageView.context as Activity
                if (activity.isFinishing || activity.isDestroyed) {
                    TopLifecyclerOwnerManager.curActivity
                } else {
                    imageView.context
                }
            } else {
                imageView.context
            }
            if (context != null) {
                imageView.setTag(R.id.imageloader_uri, url)
                val requestManager =
                    if (lifecycleOwner is Fragment) Glide.with(lifecycleOwner) else Glide.with(
                        context
                    )
                var requestBuilder: RequestBuilder<Drawable>? = null
                if (width > 0 && height > 0 && (url != null || placeholder != null)) {
                    requestBuilder = requestManager.asDrawable()
                        .apply(requestOptions)
                        .override((width * comratio).toInt(), (height * comratio).toInt())
                        .load(url)
                }
                if (localIcon != null && localIcon != 0 && url.isNullOrEmpty() && uri == null) {
                    requestBuilder =
                        requestManager.asDrawable().load(localIcon).apply(requestOptions)
                } else if (!url.isNullOrEmpty()) {
                    requestBuilder = requestManager.asDrawable().load(url).apply(requestOptions)
                } else if (uri != null) {
                    requestBuilder = requestManager.asDrawable().load(uri).apply(requestOptions)
                }
                if (readyListener == null) {
                    requestBuilder?.into(imageView)
                } else {
                    val imageWeak = WeakReference(imageView)
                    val listenerWeak = WeakReference(readyListener)
                    requestBuilder?.into(object : CustomTarget<Drawable>() {
                        override fun onResourceReady(
                            resource: Drawable,
                            transition: Transition<in Drawable>?
                        ) {
                            imageWeak.get()?.setImageDrawable(resource)
                            listenerWeak.get()?.onReady()
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }

                    })
                }

            }

        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }


    @JvmStatic
    @BindingAdapter(
        value = ["bindLoopAnimation", "bindLoopIsReverse", "bindLoopAnimationAlpha"],
        requireAll = false
    )
    fun bindLoopAnimation(
        view: View,
        isStart: Boolean?,
        isReverse: Boolean? = false,
        isAlpha: Boolean? = false
    ) {
        val tag = view.getTag(R.id.Animation_isStart) as? AnimatorSet
        if (tag == null && isStart != null && isStart) {
            val translationX = if (isReverse == true) ObjectAnimator.ofFloat(
                view,
                "TranslationX",
                0f,
                ResUtils.dp2px(20f).toFloat()
            )
            else ObjectAnimator.ofFloat(view, "TranslationX", 0f, -ResUtils.dp2px(20f).toFloat())
            translationX.repeatMode = ValueAnimator.RESTART
            translationX.repeatCount = -1
            val alpha = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f, 0f)
            alpha.repeatMode = ValueAnimator.RESTART
            alpha.repeatCount = -1
            val animatorSet = AnimatorSet()
            if (isAlpha == true) {
                animatorSet.play(alpha)
            } else {
                animatorSet.play(translationX).with(alpha)
            }

            animatorSet.duration = 1500
            animatorSet.start()
            view.setTag(R.id.Animation_isStart, animatorSet)
        }
        if (isStart != null && isStart) {
            view.visibility = View.VISIBLE
        }
        if (isStart != null && !isStart) {
            view.visibility = View.GONE
        }
        if (tag != null && isStart != null && isStart && tag.isPaused) {
            tag.resume()
            view.visibility = View.VISIBLE
        }
        if (tag != null && isStart != null && !isStart && tag.isRunning) {
            tag.pause()
            view.visibility = View.GONE
        }

    }


    @JvmStatic
    @BindingAdapter(
        value = ["bindLoopRotion", "bindLoopRotionDuration"],
        requireAll = false
    )
    fun bindLoopRotion(view: View, isRotionStart: Boolean?, duration: Long?) {
        if (isRotionStart == null) return
        val rotatitonAnimation = view.getTag(R.id.Animation_Rotation) as? ObjectAnimator
        if (rotatitonAnimation != null) {
            if (isRotionStart == true) {
                rotatitonAnimation.start()
            } else {
                rotatitonAnimation.cancel()
                view.rotation = 0f
                view.setTag(R.id.Animation_Rotation, null)
            }
        } else if (isRotionStart) {
            val rotate = ObjectAnimator.ofFloat(view, "rotation", 0f, 360f);
            rotate.duration = duration ?: 1000 //时间
            rotate.repeatMode = ValueAnimator.RESTART
            rotate.repeatCount = -1
            rotate.start()//开始
            view.setTag(R.id.Animation_Rotation, rotate)
        } else if (!isRotionStart) {
            view.rotation = 0f
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["bindAlphaDuration", "bindAlphaAnims"], requireAll = false)
    fun bindAlphaAnimators(view: View, duration: Long?, targetAlpha: FloatArray?) {
        if (view.getTag(R.id.bindAlphaAnim) != null) {
            val tag = view.getTag(R.id.bindAlphaAnim) as ValueAnimator
            tag.cancel()
        }
        if (targetAlpha != null) {
            val valueAnimator = ObjectAnimator.ofFloat(*targetAlpha)
            valueAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.alpha = value
            }
            valueAnimator.interpolator = LinearInterpolator()
            if (duration != null) {
                valueAnimator.duration = duration
            } else {
                valueAnimator.duration = 400
            }
            view.setTag(R.id.bindAlphaAnim, valueAnimator)
            valueAnimator.start()
        }

    }

    @JvmStatic
    @BindingAdapter("android:textStyle")
    fun setTextStyle(textView: TextView, type: Int?) {
        if (type != null) {
            textView.typeface = Typeface.defaultFromStyle(type)
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["bindGroupSelect", "bindGroupEnable"], requireAll = false)
    fun bindClearAllSelect(radioGroup: RadioGroup, value: String?, isEnable: Boolean?) {
        radioGroup.clearCheck()
        val childCount = radioGroup.childCount
        for (i in 0 until childCount) {
            val childAt = radioGroup.getChildAt(i)
            if (childAt is RadioButton) {
                if (TextUtils.equals(childAt.text.toString(), value)) {
                    radioGroup.check(childAt.getId())
                    radioGroup.isEnabled = isEnable ?: false
                }
            }
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["bindTouchListener", "bindViewSelect"], requireAll = false)
    fun bindTouchListener(view: View, listener: View.OnTouchListener?, isSelect: Boolean?) {
        view.setOnTouchListener(listener)
        if (isSelect != null) {
            view.isSelected = isSelect
        }
    }


    @JvmStatic
    @BindingAdapter(value = ["bindHint"])//requireAll表示所有参数都必须有
    fun setCustomHint(view: TextView, text: String?) {
        view.hint = text
    }


    @JvmStatic
    @BindingAdapter(
        value = ["bindKeepTow", "bindEdSelection", "bindRequestFocus"],
        requireAll = false
    )
    fun bindKeepTow(editText: EditText, isKeepTow: Boolean?, position: Int?, isRequest: Boolean?) {
        if (isRequest == true) {
            editText.requestFocus()
        } else {
            editText.clearFocus()
        }
        try {
            if (isKeepTow == true || position != null) {
                if (editText.getTag(R.id.Editext_Bind_TextWatcher) != null) {
                    val commonBindTextWather =
                        editText.getTag(R.id.Editext_Bind_TextWatcher) as? CommonBindTextWather
                    commonBindTextWather?.position = position ?: -1
                    commonBindTextWather?.isKeepTow = isKeepTow
                    return
                }
                val textWatcher = CommonBindTextWather(editText, isKeepTow)
                textWatcher.position = position ?: -1
                editText.setTag(R.id.Editext_Bind_TextWatcher, textWatcher)
                editText.addTextChangedListener(textWatcher)
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }

    }

    @JvmStatic
    @BindingAdapter(
        value = ["bindMotionToState", "bindMotionTransitionListener", "bindMotionDuration"],
        requireAll = false
    )
    fun bindMotionLayout(
        motionLayout: MotionLayout,
        state: Int?,
        listener: MotionLayout.TransitionListener?,
        duration: Int?
    ) {
        if (state != null) {
            if (duration != null) {
                motionLayout.setTransitionDuration(duration)
                motionLayout.updateState()

            }
            try {
                motionLayout.transitionToState(state!!)
            } catch (e: java.lang.Exception) {

            }
        }
        if (listener != null) {
            motionLayout.setTransitionListener(listener)
        }

    }

    @JvmStatic
    @BindingAdapter(
        value = ["bindAppBarCollApsed"],
        requireAll = false
    )
    fun bindCollApsed(appBarLayout: AppBarLayout, isColled: MutableLiveData<Boolean>?) {
        if (isColled != null && isColled.value != null && !appBarLayout.isPressed) {
            if (isColled.value == true) {
                appBarLayout.setExpanded(false, true)
            } else {
                appBarLayout.setExpanded(true, true)
            }
        }
    }


    @InverseBindingAdapter(
        attribute = "bindAppBarCollApsed"
    )
    @JvmStatic
    fun bindCollApsedAttribute(appBarLayout: AppBarLayout): Boolean {
        return (appBarLayout.getTag(R.id.appBarLayout_bindCollApsed) as? Boolean) ?: false
    }

    //region RecyclerView
    @JvmStatic
    @BindingAdapter(
        value = ["bindRecyclerToLast"],
        requireAll = false
    )
    fun bindRecyclerViewListennerLast(
        recyclerView: RecyclerView,
        isGotoLast: MutableLiveData<Boolean>?
    ) {
        if (isGotoLast != null && isGotoLast.value == true) {
            if (recyclerView.adapter != null && (recyclerView.adapter?.itemCount
                    ?: 0) > 0 && !RecyclerViewUtils.isVisBottom(recyclerView)
            ) {
                val linearLayoutManager = recyclerView.layoutManager
                val adapter = recyclerView.adapter
                if (linearLayoutManager is LinearLayoutManager && adapter != null) {
                    linearLayoutManager.scrollToPositionWithOffset(
                        adapter.itemCount - 1,
                        Integer.MIN_VALUE
                    )
                } else {
                    recyclerView.scrollToPosition((recyclerView.adapter?.itemCount ?: 0) - 1)
                }
            }
        }
    }

    @JvmStatic
    @BindingAdapter(
        value = ["bindRecyclerViewThemeChange"],
        requireAll = false
    )
    fun bindRecyclerViewThemeChange(
        recyclerView: RecyclerView,
        change: Boolean?
    ) {
        if (change != null) {
            if (recyclerView.adapter != null && (recyclerView.adapter?.itemCount ?: 0) > 0) {
                recyclerView.adapter?.notifyDataSetChanged()
            }
        }
    }

    @JvmStatic
    @BindingAdapter(
        value = ["bindRecyclerViewPool"],
        requireAll = false
    )
    fun bindRecyclerViewPool(
        recyclerView: RecyclerView,
        pool: RecyclerView.RecycledViewPool?
    ) {
        if (pool != null) {
            recyclerView.setRecycledViewPool(pool)
        }
    }


    @JvmStatic
    @BindingAdapter(
        value = ["bindRecyclerToFirst"],
        requireAll = false
    )
    fun bindRecyclerViewListennerFirst(
        recyclerView: RecyclerView,
        isGotoFirst: MutableLiveData<Boolean>?
    ) {

        if (isGotoFirst != null && isGotoFirst.value == true) {
            if (recyclerView.adapter != null && (recyclerView.adapter?.itemCount
                    ?: 0) > 0 && !RecyclerViewUtils.isVisTop(recyclerView)
            ) {
                recyclerView.scrollToPosition(0)
            }
        }
    }

    @JvmStatic
    @BindingAdapter(
        value = ["bindRecyclerViewScrollListener"],
        requireAll = false
    )
    fun bindRecyclerViewScrollListener(
        recyclerView: RecyclerView,
        li: RecyclerView.OnScrollListener?
    ) {
        if (li != null) {
            recyclerView.addOnScrollListener(li)
        }
    }


    @InverseBindingAdapter(
        attribute = "bindRecyclerToLast"
    )
    @JvmStatic
    fun getbindRecyclerViewStateLast(recyclerView: RecyclerView): Boolean {
        return (recyclerView.getTag(R.id.recycler_view_scolle_state_last) as? Boolean) ?: false
    }

    @InverseBindingAdapter(
        attribute = "bindRecyclerToFirst"
    )
    @JvmStatic
    fun getbindRecyclerViewStateFrist(recyclerView: RecyclerView): Boolean {
        return (recyclerView.getTag(R.id.recycler_view_scolle_state_first) as? Boolean) ?: false
    }

    @JvmStatic
    @BindingAdapter(
        value = ["bindRecyclerToLastAttrChanged", "bindRecyclerToFirstAttrChanged"],
        requireAll = false
    )
    fun bindRecyclerViewListennerLast(
        recyclerView: RecyclerView,
        bindingListenerLast: InverseBindingListener?, bindingListenerFirst: InverseBindingListener?
    ) {
        val last = WeakReference(bindingListenerLast)
        val frist = WeakReference(bindingListenerFirst)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (RecyclerViewUtils.isVisCompleteBottom(recyclerView)) {
                        recyclerView.setTag(R.id.recycler_view_scolle_state_last, true)
                    } else {
                        recyclerView.setTag(R.id.recycler_view_scolle_state_last, false)
                    }

                    if (RecyclerViewUtils.isVisCompleteTop(recyclerView)) {
                        recyclerView.setTag(R.id.recycler_view_scolle_state_first, true)
                    } else {
                        recyclerView.setTag(R.id.recycler_view_scolle_state_first, false)
                    }
                    last.get()?.onChange()
                    frist.get()?.onChange()
                }

            }
        })

    }
    //endregion


    @JvmStatic
    @BindingAdapter(value = ["bindHomeId", "bindAwayId"])
    fun bindPopularMatchIcon(view: View, homeId: String?, awayId: String?) {
        /* val homeImageView = view.findViewById<ImageView>(R.id.img_home)
         val awaymageView = view.findViewById<ImageView>(R.id.img_away)
         ImUiUtils.showTeamLogo(homeImageView, awaymageView, homeId, awayId)
  */
    }


    @JvmStatic
    @BindingAdapter(value = ["bindAlphaAnim", "bindAlphaDuration"], requireAll = false)
    fun bindAlphaAnimator(view: View, targetAlpha: Float, duration: Long?) {
        val alpha = view.alpha
        if (view.getTag(R.id.bindAlphaAnim) != null) {
            val tag = view.getTag(R.id.bindAlphaAnim) as ValueAnimator
            tag.cancel()
        }
        val valueAnimator = ObjectAnimator.ofFloat(alpha, targetAlpha)
        valueAnimator.addUpdateListener { animation ->
            val value = animation.animatedValue as Float
            view.alpha = value
        }
        valueAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                if (targetAlpha > 0) {
                    view.visibility = View.VISIBLE
                } else {
                    view.visibility = View.GONE
                }
            }

            override fun onAnimationStart(animation: Animator) {
                super.onAnimationStart(animation)
                if (targetAlpha > 0) {
                    view.visibility = View.VISIBLE
                } else {
                    view.visibility = View.GONE
                }

            }
        })
        valueAnimator.interpolator = LinearInterpolator()
        if (duration != null) {
            valueAnimator.duration = duration
        } else {
            valueAnimator.duration = 200
        }
        view.setTag(R.id.bindAlphaAnim, valueAnimator)
        valueAnimator.start()
    }


    @JvmStatic
    @BindingAdapter(
        value = ["bindScale", "bindFromScale", "bindScaleDuration"],
        requireAll = false
    )
    fun bindScaleAnimator(view: View, targetXY: Float, bindFromScale: Float, duration: Long?) {
        val scaleX = view.scaleX
        val scaleY = view.scaleY
        if (scaleX == bindFromScale || scaleY == bindFromScale) return
        val tag = view.getTag(R.id.bind_scale)
        if (tag is ValueAnimator) {
            tag.cancel()
        }
        var valueAnimator = ObjectAnimator.ofFloat(scaleX, targetXY)
        view.setTag(R.id.bind_scale, valueAnimator)
        if (bindFromScale != 0f) {
            valueAnimator = ObjectAnimator.ofFloat(bindFromScale, targetXY)
        }
        valueAnimator.addUpdateListener { animation ->
            view.scaleY = animation.animatedValue as Float
            view.scaleX = animation.animatedValue as Float
        }
        valueAnimator.interpolator = LinearInterpolator()
        if (duration != null) {
            valueAnimator.duration = duration
        } else {
            valueAnimator.duration = 300
        }
        valueAnimator.start()
    }

    @JvmStatic
    @BindingAdapter(
        value = ["layout_marginEnd", "layout_marginStart", "layout_marginBottom", "layout_marginTop"],
        requireAll = false
    )
    fun bindmarginRight(view: View, right: Int?, left: Int?, bottom: Int?, top: Int?) {
        val layoutParams = view.layoutParams as? ViewGroup.MarginLayoutParams
        if (layoutParams != null) {
            if (right != null) {
                layoutParams.rightMargin = ResUtils.dp2px(right.toFloat())
            }
            if (left != null) {
                layoutParams.leftMargin = ResUtils.dp2px(left.toFloat())
            }
            if (bottom != null) {
                layoutParams.bottomMargin = ResUtils.dp2px(bottom.toFloat())
            }
            if (top != null) {
                layoutParams.topMargin = ResUtils.dp2px(top.toFloat())
            }
            view.layoutParams = layoutParams
            view.requestLayout()
        }
    }

    class CommonBindTextWather(var editText: EditText, var isKeepTow: Boolean?) : TextWatcher {

        var position: Int = -1
            set(value) {
                field = value
                setSelectionPositon(editText.text.toString(), position)
                field = -1
            }
        private val digits = 2

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (isKeepTow == true) {
                var s = s
                //删除“.”后面超过2位后的数据
                if (s.toString().contains(".")) {
                    if (s.length - 1 - s.toString().indexOf(".") > digits) {
                        s = s.toString().subSequence(
                            0,
                            s.toString().indexOf(".") + digits + 1
                        )
                        editText.setText(s)
                        editText.setSelection(s.length) //光标移到最后
                    }
                }
                //如果"."在起始位置,则起始位置自动补0
                if (s.toString().trim { it <= ' ' }.substring(0) == ".") {
                    s = "0$s"
                    editText.setText(s)
                    editText.setSelection(2)
                }

                //如果起始位置为0,且第二位跟的不是".",则无法后续输入
                if (s.toString().startsWith("0") && s.toString().trim { it <= ' ' }.length > 1) {
                    if (s.toString().substring(1, 2) != ".") {
                        editText.setText(s.subSequence(0, 1))
                        editText.setSelection(1)
                        return
                    }
                }
            }
        }

        override fun afterTextChanged(s: Editable) {
            setSelectionPositon(s.toString(), position)
            position = -1

        }

        private fun setSelectionPositon(s: String, fposition: Int) {
            if (s.length >= fposition) {
                try {
                    editText.setSelection(fposition)
                } catch (e: Exception) {

                }

            }
        }
    }


    @JvmStatic
    @BindingAdapter(value = ["bindLoadWebviewUrl"])
    fun bindWebvieew(webView: WebView, url: String?) {
        if (url != null) {
            webView.loadUrl(url)
        }
    }

    @JvmStatic
    @BindingAdapter("background")
    fun setBackgroundRes(view: View, background: Int?) {
        if (background != null) {
            view.background = ResUtils.getDrawable(background)
        }
    }

    @JvmStatic
    @BindingAdapter("textColor")
    fun setTextViewColor(view: TextView, textColor: Int?) {
        if (textColor != null) {
            view.setTextColor(ResUtils.getColor(textColor))
        }
    }

    @JvmStatic
    @BindingAdapter("drawableTop")
    fun setTextViewDrawableTop(view: TextView, drawable: Int?) {
        if (view != null && null != drawable) {
            val drawable = ResUtils.getDrawable(drawable)
            drawable?.setBounds(
                0,
                0,
                drawable.getMinimumWidth(),
                drawable.getMinimumHeight()
            ) //必须设置图片大小，否则不显示

            if (null != drawable) {
                view.setCompoundDrawables(null, drawable, null, null)
            }

        }
    }

    @JvmStatic
    @BindingAdapter("imageSrc")
    fun setImageSrc(imageView: ImageView, imageSrc: MutableLiveData<Int>?) {
        if (null != imageSrc?.value && imageSrc.value != -1 && imageSrc.value != 0) {
            imageView?.setImageDrawable(ResUtils.getDrawable(imageSrc.value ?: 0))
        }
    }


    @JvmStatic
    @BindingAdapter(value = ["bindSkinTextColor"], requireAll = false)
    fun bindSkinTextColor(textView: TextView, color: Int?) {
        if (color != null && color != 0) {
            textView.setTextColor(ResUtils.getColor(color))
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["bindSkinBackgroundColor"], requireAll = false)
    fun bindSkinBackgroundColor(view: View, resId: Int?) {
        try {
            if (resId != null && resId != 0) {
                view.setBackgroundColor(ResUtils.getColor(resId))
            }
        } catch (e: java.lang.Exception) {

        }
    }

    @JvmStatic
    @BindingAdapter(value = ["bindSkinBackgroundDrawable"], requireAll = false)
    fun bindSkinBackgroundDrawable(view: View, resId: Int?) {
        try {
            if (resId != null && resId != 0) {
                view.background = (ResUtils.getDrawable(resId))
            }
        } catch (e: java.lang.Exception) {

        }
    }

    @JvmStatic
    @BindingAdapter(value = ["bindSkinSrc"], requireAll = false)
    fun bindSkinSrc(view: ImageView, resId: Int?) {
        try {
            if (resId != null && resId != 0) {
                view.setImageDrawable(ResUtils.getDrawable(resId))
            }
        } catch (e: java.lang.Exception) {

        }
    }


    @JvmStatic
    @BindingAdapter(value = ["android:layout_weight"], requireAll = false)
    fun bindLayoutWeight(view: View, weight: Float?) {
        try {
            if (weight != null && view.layoutParams != null) {
                (view.layoutParams as? LinearLayout.LayoutParams)?.weight = weight
                view.requestLayout()
            }
        } catch (e: java.lang.Exception) {

        }
    }


    @JvmStatic
    @BindingAdapter(
        value = ["bindTextDrawGlideUrl", "bindTextDrawGravity", "bindTextDrawWidth",
            "bindTextDrawHeight", "bindTextDrawClearText", "bindTextDrawPlaceholder"],
        requireAll = false
    )
    fun bindGlideDrawUrl(
        textView: TextView,
        teamUrl: String?,
        gravity: Int,
        withd: Int?,
        height: Int?,
        isclear: Boolean = false,
        placeholder: Int?
    ) {
        val w = ResUtils.dp2px((withd ?: 30).toFloat())
        val h = ResUtils.dp2px((height ?: withd ?: 30).toFloat())
        if (!TextUtils.isEmpty(teamUrl)) {
            try {
                val requestOptions = RequestOptions()
                    .error(ResUtils.getDrawable(placeholder ?: 0))
                    .placeholder(ResUtils.getDrawable(placeholder ?: 0))
                Glide.with(textView)
                    .asBitmap()
                    .override(w, h)
                    .load(teamUrl)
                    .apply(requestOptions)
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            val drawable = BitmapDrawable(resource)
                            monResourceReady(textView, isclear, drawable, w, h, gravity)
                        }
                    })
            } catch (e: Exception) {
                LogUtils.e(e)
            }
        } else if (placeholder != null) {
            val drawable = ResUtils.getDrawable(placeholder)
            if (drawable != null) {
                monResourceReady(textView, isclear, drawable, w, h, gravity)
            }
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["app:loadBitmap", "app:placeholder"], requireAll = false)
    fun loadBitmap(
        imageView: ImageView,
        loadBitmap: MutableLiveData<Bitmap>?,
        placeholder: Int? = 0
    ) {
        if (null != loadBitmap?.value) {
            val requestOptions = Glide.with(imageView.context)
                .load(loadBitmap.value)
            if (null != placeholder) {
                requestOptions.placeholder(placeholder)
            }
            requestOptions.into(imageView)
        }
    }


    @JvmStatic
    @BindingAdapter("app:layout_weight")
    fun setViewWeight(view: View, float: Float) {
        (view.layoutParams as LinearLayout.LayoutParams)?.let {
            it.weight = float
        }
        view.requestLayout()
    }


    @JvmStatic
    @BindingAdapter("app:bindAlpha")
    fun bindViewAlpha(view: View, alpha: Float?) {
        if (alpha != null && alpha >= 0) {
            view.alpha = alpha
        }
    }

    @JvmStatic
    @BindingAdapter("app:bindAppBarLayoutFix", requireAll = false)
    fun bindAppBarLayoutExp(view: AppBarLayout, isfix: Boolean?) {
        if (isfix != null && view.childCount > 0) {
            val fisrtView = view.getChildAt(0)
            val params = fisrtView?.layoutParams as? AppBarLayout.LayoutParams
            if (params != null) {
                params.scrollFlags =
                    if (isfix) 0x00 else AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
                fisrtView.layoutParams = params
                view.requestLayout()
            }
        }
    }


    private fun monResourceReady(
        textView: TextView,
        isclear: Boolean = false,
        drawable: Drawable,
        w: Int,
        h: Int,
        gravity: Int
    ) {
        if (isclear) {
            textView.text = ""
        }
        drawable.setBounds(0, 0, w, h)
        when (gravity) {
            Gravity.START, Gravity.LEFT -> {
                textView.setCompoundDrawables(drawable, null, null, null)
            }
            Gravity.TOP -> {
                textView.setCompoundDrawables(null, drawable, null, null)
            }
            Gravity.END, Gravity.RIGHT -> {
                textView.setCompoundDrawables(null, null, drawable, null)
            }
            Gravity.BOTTOM -> {
                textView.setCompoundDrawables(null, null, null, drawable)
            }
        }
    }

    interface OnImageLoadReady {
        fun onReady()
    }
}

