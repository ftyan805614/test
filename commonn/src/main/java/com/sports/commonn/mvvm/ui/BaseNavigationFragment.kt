package com.sports.commonn.mvvm.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.apkfuns.logutils.LogUtils

/**
 *Create By Albert on 2020/8/13
 */
abstract class BaseNavigationFragment<T : ViewDataBinding> : BaseMvvmFragment<T>() {

    protected var lastView: View? = null//记录上次创建的view

    override fun callOnCreateView(
        paramLayoutInflater: LayoutInflater,
        paramViewGroup: ViewGroup?,
        paramBundle: Bundle?
    ): View? {
        if (lastView == null) {
            lastView = super.callOnCreateView(paramLayoutInflater, paramViewGroup, paramBundle)
        }
        (lastView?.parent as? ViewGroup)?.removeView(lastView)
        val parent = lastView?.parent as? ViewGroup
        if (parent != null) {
            lastView = super.callOnCreateView(paramLayoutInflater, paramViewGroup, paramBundle)
            isViewInit = false
            isDataInit = false
            if (lastView?.parent != null) throw RuntimeException("fragment content already has a parent")
        } else {
            LogUtils.i("lastView?.parent is ${lastView?.parent}")
        }
        return lastView
    }


    override fun onBackPressedSupport(): Boolean {
        exitActivity()
        return true
    }

    open fun finishFragment() {
        exitActivity()
    }

    open fun remove() {
        parentFragmentManager.beginTransaction().remove(this).commitAllowingStateLoss()
    }


    override fun finish() {
        exitActivity()
    }

    override fun finishActivity() {
        exitActivity()
    }

    override fun exitActivity() {
        try {
            findNavController().popBackStack()
            val cf = findNavHostFragment(this)
            val cfm = cf?.childFragmentManager
            val lastF = cfm?.fragments?.last()
            if ((cfm?.fragments?.size) == 1) {
                view?.postDelayed({
                    lastF?.let {
                        cfm.beginTransaction().remove(it).commitAllowingStateLoss()
                    }
                }, 200)
            }
        } catch (e: Exception) {
            if (e.message?.contains("does not have a NavController set", false) == true) {
                activity?.finish()
            }
        }
    }


    private fun findNavHostFragment(fragment: Fragment): NavHostFragment? {
        val parentFragment = fragment.parentFragment
        if (parentFragment != null) {
            return if (parentFragment is NavHostFragment) {
                parentFragment
            } else {
                findNavHostFragment(parentFragment)
            }
        }
        return null
    }


    fun navigate(id: Int) {
        findNavController().navigate(id)
    }

    fun navigate(des: NavDirections) {
        findNavController().navigate(des)
    }

    fun pop(id: Int) {
        findNavController().popBackStack(id, true)
    }


}
 
