package com.sports.commonn.mvvm.ui.widget.toast;


import androidx.annotation.LayoutRes;

/**
 * Desc:
 * Created by Jeff on 2018/10/28
 **/
public abstract class ToastAdapter {

    /**
     * The ToastAdapter's tag.
     */
    private ViewHolder mTag = null;

    @LayoutRes
    public abstract int getLayoutId();

    public abstract void onBindViewHolder(ViewHolder holder, Object obj);

    /**
     * desc: 为了重复利用holder,toast适配器可以设置成静态的,需要时传进来
     *@author Jeff created on 2018/10/28 17:08
     */
    public ViewHolder getTag() {
        return mTag;
    }

    public void setTag(ViewHolder tag) {
        mTag = tag;
    }
}
