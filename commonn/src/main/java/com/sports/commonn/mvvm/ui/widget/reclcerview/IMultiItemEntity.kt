package com.sports.commonn.mvvm.ui.widget.reclcerview

import com.chad.library.adapter.base.entity.MultiItemEntity


interface IMultiItemEntity : MultiItemEntity {
    
    fun setPosition(position: Int,totalCount: Int)

    fun getClickViewIds(): ArrayList<Int>?

}