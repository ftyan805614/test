package com.sports.commonn.mvvm.ui.widget;

/**
 * Desc:
 * Created by Jeff on 2018/9/26
 **/
public interface LoadStatus {
    int EMPTY_DATA = 1;
    int LOAD_FAILD = 2;
    int EMPTY_DATA_MESSAGE = 3;
    int EMPTY_DATA_RECORD = 4;
    int COMMISSION_DATA=5;
    int EMPTY_DATA_DETAIL=6;
    int NOT_SPORTS_DATA = -15;//暂无比赛数据
    int EMPTY_DATA_NOTICE_MESSAGE = 7;

}
