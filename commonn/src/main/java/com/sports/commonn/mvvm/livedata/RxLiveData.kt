package com.sports.commonn.mvvm.livedata

import android.app.Activity
import android.app.Fragment
import androidx.lifecycle.*
import android.util.Log
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.disposables.Disposable

/**
 *create by Albert
 */
class RxLiveData<T> : MutableLiveData<T>() {


    private val emitters = HashMap<Int, ArrayList<FlowableEmitter<T>>>()
    private val ownerName = HashSet<Int>()
    fun observe(owner: LifecycleOwner, requestDefaultValue: Boolean = false): Flowable<T> {
        val rx = initRx(owner)
        if (!ownerName.contains(owner.hashCode())) {
            ownerName.add(owner.hashCode())
            owner.lifecycle.addObserver(LifecycleMonitor(emitters, ownerName, owner.hashCode(), owner))
            super.observe(owner, RxObserver(emitters, owner.hashCode(), requestDefaultValue))
        }

        return rx
    }

    private fun initRx(owner: LifecycleOwner): Flowable<T> {
        return Flowable.create({
            var emitterList = emitters[owner.hashCode()]
            if (emitterList == null) {
                emitterList = ArrayList()
                emitters[owner.hashCode()] = emitterList
            }
            emitterList.add(it)
        }, BackpressureStrategy.LATEST)
    }

    override fun onInactive() {
        super.onInactive()
    }




    class RxObserver<T>(private var emitters: HashMap<Int, ArrayList<FlowableEmitter<T>>>, private var hashCode: Int?, var requestDefaultValue: Boolean) : Observer<T> {
        private var isFirst = true
        override fun onChanged(t: T?) {
            if (isFirst) {
                isFirst = false
                if (!requestDefaultValue) return
            }
            if (t == null) return
            emitters[hashCode]?.forEach {
                it.onNext(t)
            }
        }
    }

    class LifecycleMonitor<T>(var emitters: HashMap<Int, ArrayList<FlowableEmitter<T>>>, var ownerName: HashSet<Int>, var hashCode: Int,
                              var lifecycleOwner: LifecycleOwner) : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            if (lifecycleOwner is Activity) {
                if ((lifecycleOwner as? Activity)?.isFinishing==true) {
                    emitters.remove(hashCode)
                    ownerName.remove(hashCode)
                }
            }
            if (lifecycleOwner is Fragment) {
                if ((lifecycleOwner as Fragment).activity.isFinishing) {
                    emitters.remove(hashCode)
                    ownerName.remove(hashCode)
                }
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            emitters.remove(hashCode)
            ownerName.remove(hashCode)
        }
    }


}

// 订阅时请用此订阅 自动解绑
fun <T> Flowable<T>.subscribeLiveData(owner: LifecycleOwner, onNext: (T) -> Unit): Disposable {
    val nextLiveData = SafeMutableLiveData<T>()
    nextLiveData.observe(owner, Observer {
        if (it != null) {
            Log.i("RxLiveData", "2")
            onNext(it)
        }

    })
    return `as`(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(owner.lifecycle, Lifecycle.Event.ON_DESTROY)))
            .subscribe {
                Log.i("RxLiveData", "1")
                nextLiveData.postValue(it)
            }
}

// 订阅时请用此订阅 自动解绑
fun <T> Flowable<T>.subscribeLiveData(owner: LifecycleOwner, onNext: (T) -> Unit, onError: (Throwable) -> Unit): Disposable {
    val nextLiveData = SafeMutableLiveData<T>()
    val errorLiveData = SafeMutableLiveData<Throwable>()
    nextLiveData.observe(owner, Observer {
        if (it != null) onNext(it)
    })
    errorLiveData.observe(owner, Observer {
        if (it != null) onError(it)
    })
    return `as`(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(owner.lifecycle, Lifecycle.Event.ON_DESTROY)))
            .subscribe({
                nextLiveData.postValue(it)
            }, {
                errorLiveData.postValue(it)
            })
}

