package com.sports.commonn.mvvm.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import com.sports.commonn.R;
import com.sports.commonn.utils.StatusViewHelper;


/**
 * Desc:
 * Created by Jeff on 2018/12/7
 **/
public class StatusLayout extends FrameLayout implements IStatusViewHelper {
    public static final int ERROR = 1;
    public static final int SUCCESS = 3;
    public static final int LOADING = 2;
    private StatusViewHelper mStatusViewHelper;
    private int showStataus;
    private int errorType;
    private String message;

    public StatusLayout(@NonNull Context context) {
        super(context);
    }

    public StatusLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mStatusViewHelper = new StatusViewHelper(this);
        initCustomAttrs(context, attrs);
        switch (showStataus) {
            case SUCCESS:
                mStatusViewHelper.success();
                break;
            case ERROR:
                mStatusViewHelper.showError(errorType, message);
                break;
            case LOADING:
                mStatusViewHelper.showLoadding(message);
                break;
        }
    }

    private void initCustomAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StatusLayout);
        final int N = typedArray.getIndexCount();
        for (int i = 0; i < N; i++) {
            initCustomAttr(typedArray.getIndex(i), typedArray);
        }
        typedArray.recycle();
    }

    private void initCustomAttr(int attr, TypedArray typedArray) {
        if (attr == R.styleable.StatusLayout_error_type) {
            errorType = typedArray.getInteger(attr, 2);
        } else if (attr == R.styleable.StatusLayout_status) {
            showStataus = typedArray.getInt(attr, SUCCESS);
        } else if (attr == R.styleable.StatusLayout_message) {
            message = typedArray.getString(R.styleable.StatusLayout_message);
        }
    }

    @Override
    public void showError(int type, String message) {
        mStatusViewHelper.showError(type, message);
    }

    @Override
    public void showLoadding(String message) {
        mStatusViewHelper.showLoadding(message);
    }

    @Override
    public void success() {
        mStatusViewHelper.success();
    }

    @Override
    public void setOnChildClickListener(OnStatusClickListener listener) {
        mStatusViewHelper.setOnChildClickListener(listener);
    }

    @Override
    public void setFactory(StatusViewHelper.Factory factory) {
        mStatusViewHelper.setFactory(factory);
    }

    public StatusViewHelper getStatusViewHelper() {
        return mStatusViewHelper;
    }

    @BindingAdapter(value = {"bindStateLayoutState", "bindStateLayoutErrorType", "bindStateLayoutErrorMessage"}, requireAll = false)
    public static void bindStateLayout(StatusLayout statusLayout, Integer state, Integer errorType, String error) {
        if (state == null) return;
        switch (state) {
            case 1:
                if (errorType!=null){
                    statusLayout.showError(errorType, error);
                }
                break;
            case 2:
                statusLayout.showLoadding("");
                break;
            case 3:
                statusLayout.success();
                break;
        }
    }
}
