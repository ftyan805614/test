package com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper

import android.content.Context
import android.util.AttributeSet

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.apkfuns.logutils.LogUtils

/**
 * created by Albert
 */
class SafeGridLayoutManager : GridLayoutManager {

    var recyclerView: RecyclerView? = null
        private set

    constructor(context: Context?, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {}

    constructor(context: Context?, spanCount: Int) : super(context, spanCount) {}

    constructor(context: Context?, spanCount: Int, orientation: Int, reverseLayout: Boolean) : super(context, spanCount, orientation, reverseLayout) {}

    override fun onAttachedToWindow(view: RecyclerView?) {
        super.onAttachedToWindow(view)
        this.recyclerView = view
    }

    override fun supportsPredictiveItemAnimations(): Boolean {
        return super.supportsPredictiveItemAnimations()
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: Exception) {
            LogUtils.e(e)
        }

    }
}
