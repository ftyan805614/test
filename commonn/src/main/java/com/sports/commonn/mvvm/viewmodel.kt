package com.sports.commonn.mvvm

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.sports.commonn.mvvm.activityshare.ViewModelShareProvider
import com.sports.commonn.mvvm.ui.BaseShareViewModel
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.ViewModelFactory
import java.lang.ref.WeakReference

/**
 *Create By Albert on 2021/3/10
 */


fun <V : ViewModel?> ofScopeFragment(t: Class<V>): V? {
    return ofScope(TopLifecyclerOwnerManager.curFragment, TopLifecyclerOwnerManager.curFragment, t)
}


fun <V : ViewModel?> ofScopeActivity(t: Class<V>): V? {
    return ofScope(TopLifecyclerOwnerManager.actvitiy() as? FragmentActivity, TopLifecyclerOwnerManager.actvitiy() as? FragmentActivity, t)
}

fun <V : ViewModel?> ofScope(container: Any?, lifecycleOwner: LifecycleOwner?, t: Class<V>): V? {
    val viewModel = when (container) {
        is FragmentActivity -> {
            ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t) as V
        }
        is Fragment -> {
            ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t)
        }
        else -> {
            return null
        }
    }
    if (viewModel is BaseViewModel && lifecycleOwner != null) {
        lifecycleOwner.lifecycle.addObserver(viewModel as BaseViewModel)
        (viewModel as BaseViewModel).lifecycleOwner = WeakReference(lifecycleOwner)
    }
    return viewModel
}

fun <V : BaseShareViewModel?> ofScopeShareActivity(t: Class<V>): V? {
    return ofScopeShare(TopLifecyclerOwnerManager.actvitiy() as? FragmentActivity, TopLifecyclerOwnerManager.actvitiy() as? FragmentActivity, t)
}

fun <V : BaseShareViewModel?> ofScopeShareFragment(t: Class<V>): V? {
    return ofScopeShare(TopLifecyclerOwnerManager.curFragment, TopLifecyclerOwnerManager.curFragment, t)
}

fun <V : BaseShareViewModel?> ofScopeShare(container: LifecycleOwner?, lifecycleOwner: LifecycleOwner?, t: Class<V>): V? {
    if (container == null || lifecycleOwner == null) return null
    return ViewModelShareProvider.ofGet(container, t, lifecycleOwner)
}
