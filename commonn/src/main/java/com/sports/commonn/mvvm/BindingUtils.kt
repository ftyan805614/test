package com.sports.commonn.mvvm

import androidx.databinding.ViewDataBinding
import com.apkfuns.logutils.LogUtils
import java.lang.reflect.Field

/**
 *Create By Albert on 2021/1/3
 */
object BindingUtils {

    private var bindLifecycleOwnerField: Field? = null

    @JvmStatic
    fun relaseBindLifecycerowner(bind: ViewDataBinding?) {
        try {
            if (bind == null) return
            if (bindLifecycleOwnerField == null) {
                bindLifecycleOwnerField =
                    ViewDataBinding::class.java.getDeclaredField("mLifecycleOwner")
            }
            bindLifecycleOwnerField?.set(bind, null)
        } catch (e: Exception) {
            LogUtils.e(e)
        }

    }
}