package com.sports.commonn.mvvm.ui.widget;

import android.content.Context;
import android.view.View;

/**
 * Desc:
 * Created by Jeff on 2018/9/14
 **/
public abstract class StatusAdapter {

    private OnStatusClickListener mEmptyClickListener;

    public abstract View getView(Context context);

    public abstract void showError(int type,String errorMsg);

    public abstract void showLoadding(String loaddingMsg);

    public void success(){}

    public void setOnChildClickListener(OnStatusClickListener listener){
        mEmptyClickListener = listener;
    }

    public void setOnClickListener(View view){
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmptyClickListener != null){
                    mEmptyClickListener.onClick(v);
                }
            }
        });

    }
}
