package com.sports.commonn.mvvm.ui.widget.reclcerview

import androidx.annotation.CallSuper

open class BaseMulteItemEntity : IMultiItemEntity {
    var itemViewType = 0
    var position = 0
    var totalCount = 0

    @CallSuper
    override fun setPosition(position: Int, totalCount: Int) {
        this.position = position
        this.totalCount = totalCount
    }

    override fun getClickViewIds(): ArrayList<Int>? = null

    override fun getItemType(): Int = itemViewType

    val bindIsLastItem
        get() = (position + 1) == totalCount


    @CallSuper
    open fun resDestroy() {

    }
}