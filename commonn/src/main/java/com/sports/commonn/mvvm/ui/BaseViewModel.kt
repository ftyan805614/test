package com.sports.commonn.mvvm.ui

import android.app.Activity
import android.text.Editable
import android.view.View
import androidx.annotation.CallSuper
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.activityshare.ViewModelShareProvider
import com.sports.commonn.mvvm.anim.DownAlphaAnim
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.ui.widget.reclcerview.IMultiItemEntity
import com.sports.commonn.utils.EventBusClickUtil
import com.sports.wallpaper.utils.dismissLoadingExt
import com.sports.wallpaper.utils.showLoadingExt
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import java.lang.ref.WeakReference

/**
 *created by Albert
 */
open class BaseViewModel(open var lifecycleOwner: WeakReference<LifecycleOwner>?) : ViewModel(),
    LifecycleObserver, IMultiItemEntity {

    var itemViewType = 0

    var isResDestory = false


    private val clickIds = ArrayList<Int>()

    private val defaultKey = "androidx.lifecycle.ViewModelProvider.DefaultKey"


    val fragment: WeakReference<Fragment>?
        get() {
            return when (val owner = lifecycleOwner?.get()) {
                is Fragment -> WeakReference(owner)
                is DialogFragment -> WeakReference(owner)
                else -> null
            }
        }

    val activity: WeakReference<FragmentActivity>?
        get() {
            return when (val owner = lifecycleOwner?.get()) {
                is FragmentActivity -> WeakReference(owner)
                is Fragment -> WeakReference(owner.activity)
                is BaseMvvmPopuWindow<*> -> WeakReference(owner.mActivity)
                else -> null
            }
        }

    var onattouchedLifecycleOwner = ArrayList<WeakReference<LifecycleOwner>>()

    var action = SafeMutableLiveData<Action>()

    private var compositeDisposable = CompositeDisposable()

    open fun attouchLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        val iterator = onattouchedLifecycleOwner.iterator()
        iterator.forEach {
            if (it.get() == null) {
                iterator.remove()
            }
        }
        val mLifecycleOwner = onattouchedLifecycleOwner.firstOrNull { it.get() == lifecycleOwner }
        if (mLifecycleOwner == null) {
            this.lifecycleOwner = WeakReference(lifecycleOwner)
            onattouchedLifecycleOwner.add(this.lifecycleOwner!!)
        } else {
            this.lifecycleOwner = mLifecycleOwner
        }
    }

    //添加一个dipsose
    protected fun addDisposable(disposable: Disposable?) {
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    /**
     * 得到 fragment的 CoroutineScope
     */
    fun fgtCoroutineScope(): CoroutineScope? {
        return fragment?.get() as? CoroutineScope
    }

    open fun onClick(v: View) {
        //点击动作发送
        EventBusClickUtil.click(v.id)
        if (!clickIds.contains(v.id)) {
            clickIds.add(v.id)
            DownAlphaAnim(v).setOnTouchEvent(null)
        }
    }

    open fun afterTextChanged(editable: Editable) {

    }

    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    open fun onCreate(lifecycleOwner: LifecycleOwner) {

    }


    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume(lifecycleOwner: LifecycleOwner) {
        attouchLifecycleOwner(lifecycleOwner)
    }


    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause(lifecycleOwner: LifecycleOwner) {
        if ((lifecycleOwner as? Activity)?.isFinishing == true) {
            preResDestroy(lifecycleOwner)
        }
    }

    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestory(lifecycleOwner: LifecycleOwner) {
        preResDestroy(lifecycleOwner)
    }


    private fun preResDestroy(lifecycleOwner: LifecycleOwner) {
        val weakLifecycleOwner =
            onattouchedLifecycleOwner.firstOrNull { it.get() == lifecycleOwner }
        if (weakLifecycleOwner != null) {
            onattouchedLifecycleOwner.remove(weakLifecycleOwner)
        }
        if (onattouchedLifecycleOwner.size == 0) {
            resDestroy(onattouchedLifecycleOwner.size == 0)
        }
        this.lifecycleOwner = onattouchedLifecycleOwner.lastOrNull()
    }


    fun <V : ViewModel?> ofScopeFragment(t: Class<V>): V? {
        return ofScope(fragment?.get(), fragment?.get(), t)
    }


    fun <V : ViewModel?> ofScopeActivity(t: Class<V>): V? {
        return ofScope(activity?.get(), activity?.get(), t)
    }

    fun <V : ViewModel?> ofScope(
        container: Any?,
        lifecycleOwner: LifecycleOwner?,
        t: Class<V>
    ): V? {
        val viewModel = when (container) {
            is FragmentActivity -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t) as V
            }
            is Fragment -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t)
            }
            else -> {
                return null
            }
        }
        if (viewModel is BaseViewModel && lifecycleOwner != null) {
            lifecycleOwner.lifecycle.addObserver(viewModel as BaseViewModel)
            (viewModel as BaseViewModel).lifecycleOwner = WeakReference(lifecycleOwner)
        }
        return viewModel
    }

    fun <V : BaseShareViewModel?> ofScopeShareActivity(t: Class<V>): V? {
        return ofScopeShare(activity?.get(), activity?.get(), t)
    }

    fun <V : BaseShareViewModel?> ofScopeShareFragment(t: Class<V>): V? {
        return ofScopeShare(fragment?.get(), fragment?.get(), t)
    }

    fun <V : BaseShareViewModel?> ofScopeShare(
        container: LifecycleOwner?,
        lifecycleOwner: LifecycleOwner?,
        t: Class<V>
    ): V? {
        if (container == null || lifecycleOwner == null) return null
        return ViewModelShareProvider.ofGet(container, t, lifecycleOwner)
    }

    fun <T> ofViewModelCallBack(viewModelCallBack: ViewModelCallback<T>): ViewModelCallbackWapper<T>? {
        return ViewModelCallBackProvider.ofGet(lifecycleOwner?.get(), viewModelCallBack)
    }

    /**
     * 手动释放 资源
     * 比如：rx dispose
     */
    @CallSuper
    open fun resDestroy() {
        if (isResDestory) return
        isResDestory = true
    }

    open fun finishActivity() {
        if (lifecycleOwner?.get() is BaseNavigationFragment<*>) {
            (lifecycleOwner?.get() as BaseNavigationFragment<*>).finishFragment()
        } else if (lifecycleOwner?.get() is Activity) {
            (lifecycleOwner?.get() as? Activity)?.finish()
        }
    }

    private fun resDestroy(isNeedClearViewModel: Boolean) {
        if (isNeedClearViewModel) {
            clearViewModel()
        }
        if (isResDestory) return
        resDestroy()
        if (onattouchedLifecycleOwner.size == 0) {
            clickIds.clear()
            onattouchedLifecycleOwner.clear()
        }
    }

    private fun clearViewModel() {
        try {
            val key = defaultKey + ":" + this::class.java.canonicalName
            val field = ViewModelStore::class.java.getDeclaredField("mMap")
            field.isAccessible = true
            val store = activity?.get()?.viewModelStore
            if (store != null) {
                val f = field.get(store) as? HashMap<String, ViewModel>
                if (f?.get(key) == this) {
                    f.remove(key)
                }
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }

    fun getFragment(): Fragment? {
        return (lifecycleOwner?.get() as? Fragment)
    }


    public override fun onCleared() {
        super.onCleared()
        if (!isResDestory) {
            resDestroy(false)
        }

    }

    fun <T> weak(block: () -> T): WeakReference<T> {
        return WeakReference(block.invoke())
    }

    override fun setPosition(position: Int, totalCount: Int) {

    }

    override fun getClickViewIds(): ArrayList<Int>? = null

    override fun getItemType(): Int = itemViewType

    fun showOrDimissLoadinng(isShow: Boolean? = false) {
        if (isShow == true) {
            fragment?.get()?.showLoadingExt()
        } else {
            fragment?.get()?.dismissLoadingExt()
        }
    }
}