package com.sports.commonn.mvvm.ui.widget;

import com.sports.commonn.utils.StatusViewHelper;

/**
 *
 * Desc:
 * Created by Jeff on 2018/12/7
 **/
public interface IStatusViewHelper {

    /**
     * 显示错误界面
     *
     * @param type    错误状态码 也可以是请求状态码
     * @param message 错误信息
     */
    void showError(int type, String message);

    /**
     * 正在加载中
     * @param message
     */
    void showLoadding(String message);

    /**
     * 请求成功
     */
    void success();

    /**
     * 设置监听
     */
    void setOnChildClickListener(OnStatusClickListener listener);

    /**
     * 设置适配器的工程类
     * @param factory {@link DefaultStatusFactory}
     */
    void setFactory(StatusViewHelper.Factory factory);
}
