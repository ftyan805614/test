package com.sports.commonn.mvvm.ui.widget.reclcerview

import android.os.Handler
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.LayoutManagerWrapper
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.LinearLayoutManagerWrapper
import com.sports.commonn.utils.RecyclerViewUtils
import com.sports.commonn.utils.getSF
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.ref.WeakReference

class ExpandableAdapter2(
    dataList: ArrayList<IExpandCollapseViewModel>,
    var lifecycleOwner: WeakReference<LifecycleOwner?>?, layouts: List<ItemLayout>,
    var recyclerView: RecyclerView?
) : ExpandableAdapter(dataList, lifecycleOwner, layouts), LifecycleObserver {

    private val handler = Handler()

    companion object {

        @JvmStatic
        @BindingAdapter(value = ["bindExp2LayoutManager"], requireAll = false)
        fun <V : LayoutManagerWrapper> bindExpLayoutManager(
            recyclerView: RecyclerView,
            layoutManager: MutableLiveData<V>?
        ) {
            if (layoutManager == null || layoutManager.value == null) {
                val layoutManagers = LinearLayoutManagerWrapper(recyclerView.context)
                layoutManagers.attachToRecyclerView(recyclerView)
            } else {
                layoutManager.value?.attachToRecyclerView(recyclerView)
            }
        }

        @JvmStatic
        @BindingAdapter(value = ["bindExp2AllItem"], requireAll = false)
        fun bindAllExpande(recyclerView: RecyclerView, expall: MutableLiveData<Boolean>?) {
            val adapter = recyclerView.adapter as? ExpandableAdapter2
            if (adapter != null && expall?.value != null) {
                val item = adapter.data?.getSF(0)
                adapter.expAllChildOnlySet(0, expall.value ?: true, true)
                item?.setExpand(expall.value ?: true)
            }

        }

        @JvmStatic
        @BindingAdapter(value = ["bindExp2ItemTypeChange"], requireAll = false)
        fun <V : LayoutManagerWrapper> bindExpItemsChange(
            recyclerView: RecyclerView,
            items: ArrayList<ItemLayout>?
        ) {
            val adapter = recyclerView.adapter
            if (items != null && items.size > 0 && adapter is ExpandableAdapter2) {
                adapter.setNewItems(items)
                recyclerView.swapAdapter(adapter, true)
                recyclerView.recycledViewPool.clear()
                recyclerView.layoutManager?.removeAllViews()
                recyclerView.removeAllViews()
                adapter.notifyDataSetChanged()
            }
        }

        @BindingAdapter(
            value = ["bindExp2Data", "bindExp2ItemType", "bindExp2LifecycleOwner",
                "bindExp2ItemListener", "bindExp2ChildClickListener"], requireAll = false
        )
        @JvmStatic
        fun <T : IExpandCollapseViewModel, V : RecyclerView.LayoutManager> bindAdapter(
            recyclerView: RecyclerView,
            data: MutableLiveData<ArrayList<T>>?,
            items: ArrayList<ItemLayout>?,
            lifecycleOwner: LifecycleOwner?,
            itemClickListener: OnItemClickListener?,
            childClickListener: OnItemChildClickListener?
        ) {
            if (recyclerView.adapter == null && data != null && data.value != null && items != null) {
                val dataList = ArrayList<IExpandCollapseViewModel>()
                dataList.addAll(data.value!!)
                val expandableAdapter = ExpandableAdapter2(dataList, WeakReference(lifecycleOwner), items, recyclerView)
                expandableAdapter.let { lifecycleOwner?.lifecycle?.addObserver(it) }
                if (recyclerView.layoutManager == null) {
                    LinearLayoutManagerWrapper(recyclerView.context).attachToRecyclerView(recyclerView)
                }
                recyclerView.adapter = expandableAdapter
                if (childClickListener != null) {
                    expandableAdapter.setOnItemChildClickListener(childClickListener)
                }
                if (itemClickListener != null) {
                    expandableAdapter.setOnItemClickListener(itemClickListener)
                }
                recyclerView.itemAnimator = expandableAdapter.itemAnimator
                expandableAdapter.updateData(data.value as? ArrayList<IExpandCollapseViewModel>)
            } else {
                val adapter = (recyclerView.adapter as? ExpandableAdapter2)
                if (adapter?.mLifecycleOwner?.get() != lifecycleOwner) {
                    adapter?.let { lifecycleOwner?.lifecycle?.addObserver(it) }
                    adapter?.mLifecycleOwner = WeakReference(lifecycleOwner)
                }
                try {
                    adapter?.updateData(data?.value as? ArrayList<IExpandCollapseViewModel>?)
                } catch (e: Exception) {
                    LogUtils.e(e)
                }
            }
        }

    }


    private var cacheNewOrginData: ArrayList<IExpandCollapseViewModel>? = null

    @Volatile
    private var isInAnimatoring = false

    val itemAnimator = DefaultItemAnimator()

    private val animatorFinishedlistener = RecyclerView.ItemAnimator.ItemAnimatorFinishedListener {
        if (isInAnimatoring) {
            isInAnimatoring = false
            if (cacheNewOrginData != null) {
                updateData(cacheNewOrginData)
                cacheNewOrginData = null
            }
        }
    }

    override fun onParentListItemCollapsed(position: Int) {
        expOrCollapsed(position, false)
    }

    override fun onParentListItemExpanded(position: Int) {
        expOrCollapsed(position, true)
    }

    private fun expOrCollapsed(position: Int, isExpandAllChildren: Boolean) {
        notifyExpDiff(position, isExpandAllChildren)
    }


    private fun notifyExpDiff(position: Int, isExpandAllChildren: Boolean) {
        try {
            val o = data?.getSF(position)
            if (o != null) {
                o.setExpand(isExpandAllChildren)
                if (data != null) {
                    GlobalScope.launch {
                        try {
                            isInAnimatoring = true
                            val targets = ArrayList<IExpandCollapseViewModel>()
                            getNodeList2(orginDataList, targets)
                            val diffResult = DiffUtil.calculateDiff(DiffExpUtils(data!!, targets), true)
                            withContext(Dispatchers.Main) {
                                try {
                                    handler.postDelayed({
                                        itemAnimator.isRunning(animatorFinishedlistener)
                                    }, 100)
                                    diffResult.dispatchUpdatesTo(ExpListUpdateCallback(adapter, recyclerView))
                                    data = targets
                                } catch (e: Exception) {
                                    LogUtils.e(e)
                                }
                            }
                        } catch (e: Exception) {
                            LogUtils.e(e)
                            isInAnimatoring = false
                        }
                    }

                }
            }
        } catch (e: IndexOutOfBoundsException) {
            LogUtils.e(e)
        }

    }

    override fun updateData(newData: ArrayList<IExpandCollapseViewModel>?) {
        if (!isInAnimatoring) {
            if (newData != null) {
                orginDataList.clear()
                orginDataList.addAll(newData)
                setNewData(getNodeList(newData))
            }
        } else {
            cacheNewOrginData = newData
        }
    }

    override fun setNewData(newData: ArrayList<IExpandCollapseViewModel>?) {
        if (data.isNullOrEmpty() || newData.isNullOrEmpty()) {
            this.data = newData
            notifyDataSetChanged()
        } else {
            GlobalScope.launch {
                try {
                    val diffResult = DiffUtil.calculateDiff(DiffExpUtils(data!!, newData), true)
                    withContext(Dispatchers.Main) {
                        try {
                            diffResult.dispatchUpdatesTo(ExpListUpdateCallback(adapter, recyclerView))
                            data = newData
                        } catch (e: Exception) {
                            LogUtils.e(e)
                        }
                    }
                } catch (e: Exception) {
                    LogUtils.e(e)
                }
            }
        }
    }


    override fun onBindViewHolder(holder: ExpandableAdapterViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            val data = payloads[0] as? IExpandCollapseViewModel
            if (data != null) {
                bindMeBindViewHolder(holder, position, data)
            }
        }
    }


    class DiffExpUtils<T : IExpandCollapseViewModel>(var oldDatas: ArrayList<T>, var newDatas: ArrayList<T>) :
        DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldDatas.size
        }

        override fun getNewListSize(): Int {
            return newDatas.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val newData = newDatas.getOrNull(newItemPosition)
            val oldData = oldDatas.getOrNull(oldItemPosition)
            if (oldData is Diff) {
                return oldData.areItemsTheSame(newData)
            }
            return oldDatas.getOrNull(oldItemPosition)?.hashCode() == newDatas.getOrNull(newItemPosition).hashCode()
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldDatas.getOrNull(oldItemPosition)
            val newItem = newDatas.getOrNull(newItemPosition)
            if (oldItem is Diff) {
                return oldItem.areContentsTheSame(newItem)
            }
            return oldItem == newItem
        }

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            val oldItem = oldDatas.getOrNull(oldItemPosition)
            val newItem = newDatas.getOrNull(newItemPosition)
            if (oldItem is Diff) {
                return oldItem.getChangePayload(newItem)
            }
            return newDatas.getOrNull(newItemPosition)
        }

    }

    override fun dispose(lifecycleOwner: LifecycleOwner) {
        super.dispose(lifecycleOwner)
        recyclerView = null
        mLifecycleOwner = null
        handler.removeCallbacksAndMessages(null)
        lifecycleOwner.lifecycle.removeObserver(this)
    }

    class ExpListUpdateCallback(var mAdapter: RecyclerView.Adapter<*>?, var recyclerView: RecyclerView?) : ListUpdateCallback {

        override fun onInserted(position: Int, count: Int) {
            var isTop = false
            if (recyclerView != null) {
                isTop = RecyclerViewUtils.isVisTop(recyclerView!!)
            }
            mAdapter?.notifyItemRangeInserted(position, count)
            if (position == 0 && isTop) {
                recyclerView?.scrollToPosition(0)
            }

        }

        override fun onRemoved(position: Int, count: Int) {
            mAdapter?.notifyItemRangeRemoved(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            mAdapter?.notifyItemMoved(fromPosition, toPosition)
            if (toPosition == 0) {
                recyclerView?.scrollToPosition(0)
            }
        }

        override fun onChanged(position: Int, count: Int, payload: Any?) {
            mAdapter?.notifyItemRangeChanged(position, count, payload)
        }
    }
}

