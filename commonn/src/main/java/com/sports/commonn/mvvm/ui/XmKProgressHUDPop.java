package com.sports.commonn.mvvm.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.sports.commonn.R;

import razerdp.basepopup.BasePopupWindow;


/**
 * 创建时间：2019/9/24
 * 编写人：Averson
 * 功能描述：
 */
public class XmKProgressHUDPop extends BasePopupWindow {

    public XmKProgressHUDPop(Context context) {
        super(context);
    }

    public XmKProgressHUDPop(Fragment context) {
        super(context);
    }

   /* public XmKProgressHUDPop(Context context, boolean delayInit) {
        super(context, delayInit);
    }*/

    public XmKProgressHUDPop(Context context, int width, int height) {
        super(context, width, height);
    }

    @Override
    public View onCreateContentView() {
        setBackgroundColor(Color.TRANSPARENT);
        return createPopupById(R.layout.xm_kprogresshud_hud);
    }
}
