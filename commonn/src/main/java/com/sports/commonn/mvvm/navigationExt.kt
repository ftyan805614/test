package com.sports.commonn.mvvm

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.DialogFragmentNavigator
import androidx.navigation.fragment.NavHostFragment
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.R
import com.sports.commonn.mvvm.ui.BaseShareViewModel
import com.sports.commonn.mvvm.ui.BaseViewModel

/**
 *Create By Albert on 2020/9/25
 */

//view model 扩展
fun BaseViewModel.findNavController(): NavController? {
    try {
        if (fragment?.get() != null && fragment?.get() !is DialogFragment) {
            return NavHostFragment.findNavController(fragment?.get()!!)
        } else if (activity?.get() is FragmentActivity) {
            val fragment = (activity?.get() as FragmentActivity).supportFragmentManager.fragments.lastOrNull {
                it is NavHostFragment
            }
            if (fragment != null) {
                return NavHostFragment.findNavController(fragment)
            }
        } else {
            val fragment = (TopLifecyclerOwnerManager.curActivity as? FragmentActivity)?.supportFragmentManager?.fragments?.lastOrNull {
                it is NavHostFragment
            }
            if (fragment != null) {
                return NavHostFragment.findNavController(fragment)
            }
        }
    } catch (e: Exception) {
        LogUtils.e(e)
    }
    return null
}


//到达 navDestination, 不会崩溃，但是如果目的地不存在就无操作
fun NavController?.navigateSF(id: Int, isNeedAnim: Boolean? = false) {
    try {
        this.navigateSF(id, null, null, isNeedAnim)
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}

//到达 navDestination, 不会崩溃，但是如果目的地不存在就无操作
fun NavController?.navigateSF(id: Int, agrs: Bundle?) {
    try {
        this.navigateSF(id, agrs, null)
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}

//到达 navDestination, 不会崩溃，但是如果目的地不存在就无操作 自带动画
fun NavController?.navigateSF(
    id: Int,
    agrs: Bundle? = null,
    mnavBuilder: NavOptions.Builder? = null,
    isNeedAnim: Boolean? = false
) {

    try {
        if (isNeedAnim == true) {
            var navBuilder = mnavBuilder
            if (navBuilder == null) {
                navBuilder = NavOptions.Builder()
            }
            navBuilder.setEnterAnim(R.anim.fw_slide_in_right).setExitAnim(R.anim.fw_slide_out_left)
                .setPopEnterAnim(R.anim.fw_slide_in_left).setPopExitAnim(R.anim.fw_slide_out_right)
            this?.navigate(id, agrs, navBuilder.build())
        } else {
            this?.navigate(id, agrs, mnavBuilder?.build())
        }
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}


//到达fragment, 伴随着转场动画
fun NavController?.navigateWithAnim(id: Int, agrs: Bundle?, isSingtop: Boolean = true) {
    try {
        val navBuilder = NavOptions.Builder()
        navBuilder.setEnterAnim(R.anim.fw_slide_in_right).setExitAnim(R.anim.fw_slide_out_left)
            .setPopEnterAnim(R.anim.fw_slide_in_left).setPopExitAnim(R.anim.fw_slide_out_right)
            .setLaunchSingleTop(isSingtop)
        this?.navigate(id, agrs, navBuilder.build())
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}

//到达fragment, 伴随着转场动画
fun NavController?.navigateWithAnim(id: Int) {
    try {
        navigateWithAnim(id, null)
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}


//如果存在这个dialog 就直接popback； 不再做后续操作
fun NavController?.navigateOrPopbackDialog(id: Int) {
    navigateOrPopbackDialog(id, null)
}

//如果存在这个dialog 就直接popback 不再做后续操作
fun NavController?.navigateOrPopbackDialog(id: Int, arg: Bundle?) {
    navigateDialog(id, arg, true)
}

//如果存在这个dialog 直接return， 不再做后续操作
fun NavController?.navigateSingleDialog(id: Int) {
    navigateSingleDialog(id, null)
}

//如果存在这个dialog 直接return， 不再做后续操作
fun NavController?.navigateSingleDialog(id: Int, arg: Bundle?) {
    navigateDialog(id, arg, false)
}

//关闭dialog
fun NavController?.popBackStackDialog(
    currentDialogdestinationId: Int? = null,
    destinationId: Int? = null
) {
    try {
        if (this?.currentDestination is DialogFragmentNavigator.Destination) {
            if (this.currentDestination?.id == currentDialogdestinationId) {
                this.popBackStack()
                return
            }
            if ((this.currentDestination is DialogFragmentNavigator.Destination)) {
                if (destinationId != null) {
                    this.popBackStack(destinationId, true)
                } else {
                    this.popBackStack()
                }
            }
        }
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}


fun NavController?.navigateDialog(id: Int, arg: Bundle?, needPopBack: Boolean = false) {
    try {
        val navDestination = this?.graph?.findNode(id)
        if (navDestination is DialogFragmentNavigator.Destination) {
            if (this?.currentDestination is DialogFragmentNavigator.Destination) {
                if ((this.currentDestination as DialogFragmentNavigator.Destination).id == id) {
                    if (needPopBack) {
                        this.popBackStack()
                    }
                    return
                }
            }
        }
        this?.navigate(id, arg)
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}


// 如果前一个是navDestination是dialog，就先popback 前一个dialog ，再显示要到达的dialog
fun NavController?.navigateOnlyDialog(id: Int) {
    navigateOnlyDialog(id, null)
}

// 如果前一个是navDestination是dialog，就先popback 前一个dialog;如果当前存在这个dialog 直接return,再显示要到达的dialog;
fun NavController?.navigateOnlyDialog(id: Int, arg: Bundle?) {
    try {
        val navDestination = this?.graph?.findNode(id)
        if (navDestination is DialogFragmentNavigator.Destination) {
            if (this?.currentDestination is DialogFragmentNavigator.Destination) {
                if ((this.currentDestination as DialogFragmentNavigator.Destination).id == id) {
                    return
                } else {
                    this.popBackStack()
                }
            }
        }
        this?.navigate(id, arg)
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}

//全局通用
fun findNavController(): NavController? {
    try {
        val activity = TopLifecyclerOwnerManager.instance.curActivity
        if (activity is FragmentActivity) {
            val fragment = activity.supportFragmentManager.fragments.lastOrNull {
                it is NavHostFragment
            }
            if (fragment != null) {
                return NavHostFragment.findNavController(fragment)
            }
        }
    } catch (e: Exception) {
        LogUtils.e(e)
    }
    return null
}

fun BaseShareViewModel.findNavController(): NavController? {
    try {
        if (fragment?.get() != null && fragment?.get() !is DialogFragment) {
            return NavHostFragment.findNavController(fragment?.get()!!)
        } else if (activity?.get() is FragmentActivity) {
            val fragment =
                (activity?.get() as FragmentActivity).supportFragmentManager.fragments.lastOrNull {
                    it is NavHostFragment
                }
            if (fragment != null) {
                return NavHostFragment.findNavController(fragment)
            }
        } else {
            val fragment =
                (TopLifecyclerOwnerManager.curActivity as? FragmentActivity)?.supportFragmentManager?.fragments?.lastOrNull {
                    it is NavHostFragment
                }
            if (fragment != null) {
                return NavHostFragment.findNavController(fragment)
            }
        }
        return null
    } catch (e: Exception) {
        LogUtils.e(e)
        return null
    }

}