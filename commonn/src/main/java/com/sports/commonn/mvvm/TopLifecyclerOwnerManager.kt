package com.sports.commonn.mvvm

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import java.lang.ref.WeakReference
import java.util.LinkedHashMap

object TopLifecyclerOwnerManager : Application.ActivityLifecycleCallbacks {


    private val linkedActivityHashMap = LinkedHashMap<Int, WeakReference<Activity>>()

    private val linkedFragmentHashMap = LinkedHashMap<Int, WeakReference<Fragment>>()


    var isAppOnBackground = false

    private var hander: Handler? = null


    val curActivity: Activity?
        get() {
            if (linkedActivityHashMap.size > 0) {
                checkActivityContainer()
            }
            return linkedActivityHashMap[linkedActivityHashMap.keys.lastOrNull()]?.get()
        }

    val curFragment: Fragment?
        get() {
            if (linkedFragmentHashMap.size > 0) {
                checkFragmentContainer()
            }
            return linkedFragmentHashMap[linkedFragmentHashMap.keys.lastOrNull()]?.get()
        }

    @JvmStatic
    fun actvitiy(): Activity? {
        return curActivity
    }


    fun init(app: Application) {
        app.registerActivityLifecycleCallbacks(this)
        hander = Handler(Looper.getMainLooper())
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (linkedActivityHashMap[activity.hashCode()] == null) {
            linkedActivityHashMap[activity.hashCode()] = WeakReference(activity)
        }
        checkActivityContainer()
        (activity as? FragmentActivity)?.supportFragmentManager?.registerFragmentLifecycleCallbacks(fragmentLifecycleListener, true)
    }

    override fun onActivityStarted(activity: Activity) {

    }

    override fun onActivityResumed(activity: Activity) {
        if (linkedActivityHashMap[activity.hashCode()] == null) {
            linkedActivityHashMap[activity.hashCode()] = WeakReference(activity)
        }
        isAppOnBackground = false
        checkActivityContainer()
    }

    override fun onActivityPaused(activity: Activity) {
        isAppOnBackground = true
        if (activity.isFinishing) {
            val weak = linkedActivityHashMap[activity.hashCode()]
            weak?.clear()
            linkedActivityHashMap.remove(activity.hashCode())
            checkActivityContainer()
        }
    }

    override fun onActivityStopped(activity: Activity) {

    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

    }

    override fun onActivityDestroyed(activity: Activity) {
        val ac = linkedActivityHashMap[activity.hashCode()]
        ac?.clear()
        linkedActivityHashMap.remove(activity.hashCode())
        checkActivityContainer()
        hander?.removeCallbacksAndMessages(null)
        (activity as? FragmentActivity)?.supportFragmentManager?.unregisterFragmentLifecycleCallbacks(fragmentLifecycleListener)
    }

    private fun checkActivityContainer() {
        val iterator = linkedActivityHashMap.iterator()
        val removeKey = ArrayList<Int>()
        iterator.forEach {
            if (it.value.get() == null) {
                removeKey.add(it.key)
            }
        }
        removeKey.forEach {
            linkedActivityHashMap.remove(it)
        }
    }


    private val fragmentLifecycleListener = object : FragmentManager.FragmentLifecycleCallbacks() {

        override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
            super.onFragmentResumed(fm, f)
            if (linkedFragmentHashMap[f.hashCode()] == null) {
                linkedFragmentHashMap[f.hashCode()] = WeakReference(f)
            }
            checkActivityContainer()

        }

        override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
            super.onFragmentPaused(fm, f)
            if (f.isRemoving) {
                val weak = linkedFragmentHashMap[f.hashCode()]
                weak?.clear()
                linkedFragmentHashMap.remove(f.hashCode())
                checkFragmentContainer()
            }

        }

        override fun onFragmentDestroyed(fm: FragmentManager, f: Fragment) {
            super.onFragmentDestroyed(fm, f)
            linkedFragmentHashMap.remove(f.hashCode())
            checkFragmentContainer()
        }

    }

    private fun checkFragmentContainer() {
        val iterator = linkedFragmentHashMap.iterator()
        val removeKey = ArrayList<Int>()
        iterator.forEach {
            if (it.value.get() == null) {
                removeKey.add(it.key)
            }
        }
        removeKey.forEach {
            linkedFragmentHashMap.remove(it)
        }
    }


    @JvmStatic
    val instance: TopLifecyclerOwnerManager
        get() = this

    fun finishAllActivity(block: (Activity) -> Boolean) {
        val iterator = linkedActivityHashMap.iterator()
        val removeKey = ArrayList<Int>()
        iterator.forEach {
            if (it.value.get() == null) {
                removeKey.add(it.key)
            } else {
                if (block(it.value.get()!!)) {
                    removeKey.add(it.key)
                }
            }
        }
        removeKey.forEach {
            linkedActivityHashMap.remove(it)
        }
    }


}
