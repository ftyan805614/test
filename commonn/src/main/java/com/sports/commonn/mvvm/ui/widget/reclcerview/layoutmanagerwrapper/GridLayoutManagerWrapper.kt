package com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.DefaultSpanSizeLookup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference

/**
 *Create By Albert on 2020/4/24
 */
class GridLayoutManagerWrapper : LayoutManagerWrapper {

    var spanCount = 1
    var orientation: Int = LinearLayoutManager.VERTICAL
    var reverseLayout: Boolean = false
    var mStackFromEnd = false

    private var mSpanSizeLookup: GridLayoutManager.SpanSizeLookup? = DefaultSpanSizeLookup()

    constructor(context: Context?, spanCount: Int) {
        this.context = WeakReference(context)
        this.spanCount = spanCount
        layoutManager = WeakReference(SafeGridLayoutManager(context, spanCount))
    }

    constructor(context: Context?, spanCount: Int, orientation: Int, reverseLayout: Boolean) {
        this.context = WeakReference(context)
        this.spanCount = spanCount
        this.orientation = orientation
        this.reverseLayout = reverseLayout
        layoutManager = WeakReference(SafeGridLayoutManager(context, spanCount, orientation, reverseLayout))
    }

    fun spanSizeLookup(spanSizeLookup: GridLayoutManager.SpanSizeLookup) {
        mSpanSizeLookup = spanSizeLookup
        (layoutManager?.get() as? GridLayoutManager)?.spanSizeLookup = spanSizeLookup
    }

    override fun getLayoutManager(): RecyclerView.LayoutManager? {
        return layoutManager?.get()
    }

    override fun copyLayoutManager(context: Context?): RecyclerView.LayoutManager {
        this.context = WeakReference(context)
        return SafeGridLayoutManager(context, spanCount, orientation, reverseLayout).apply {
            spanSizeLookup = mSpanSizeLookup
            stackFromEnd = mStackFromEnd
        }
    }

    override fun setStackFromEnd(statck: Boolean) {
        val manager = layoutManager?.get()
        mStackFromEnd = statck
        if (manager is GridLayoutManager) {
            manager.stackFromEnd = statck
        }
    }
}