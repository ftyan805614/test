package com.sports.commonn.mvvm.ui

import android.app.Activity
import android.util.SparseArray
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import java.lang.ref.WeakReference

object ViewModelCallBackProvider{
    private val monitors = SparseArray<LifecycleMonitor>()
    private val viewModelCallBacks = SparseArray<ArrayList<WeakReference<ViewModelCallbackWapper<*>>>>()

    fun <T> ofGet(lifecycleOwner: LifecycleOwner?, viewModelCallback: ViewModelCallback<T>, extra:Any?=null): ViewModelCallbackWapper<T>? {
        if(null != lifecycleOwner && null != viewModelCallback){
            var newCallback = ViewModelCallbackWapper<T>()
            newCallback.callbackRefrence = WeakReference(viewModelCallback)
            viewModelCallback.extra = extra
            var key = lifecycleOwner.hashCode()
            if(0!= key){
                var list = viewModelCallBacks.get(key)
                if (list == null) {
                    list = ArrayList()
                }
                list.add(WeakReference(newCallback))
                viewModelCallBacks.put(key,list)
            }

            val monitor = monitors.get(key)
            if (monitor == null) {
                if (newCallback!=null){
                    monitors.put(key,
                        LifecycleMonitor(
                            lifecycleOwner,
                            viewModelCallBacks,
                            monitors
                        )
                    )
                }
            }
            return newCallback
        }
        return null
    }


    fun <T> ofGet(activity: Activity, fragment: Fragment? = null, viewModelCallback: ViewModelCallback<T>, extra:Any?=null): ViewModelCallbackWapper<T>? {
        var lifecycleObserver:LifecycleOwner? = null
        if (fragment != null) {
            lifecycleObserver = fragment
        } else if(activity != null){
            lifecycleObserver = activity as LifecycleOwner
        }
        return ofGet(lifecycleObserver, viewModelCallback, extra)
    }

    private class LifecycleMonitor(
        var lifecycleOwner: LifecycleOwner,
        var viewModelCallbacks: SparseArray<ArrayList<WeakReference<ViewModelCallbackWapper<*>>>>,
        var monitor: SparseArray<LifecycleMonitor>?) : LifecycleObserver {

        val hashKey = lifecycleOwner.hashCode()
        var isDisposed = false

        init {
            lifecycleOwner.lifecycle.addObserver(this)
        }


        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun onCreate() {
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onStart() {
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume() {
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            if (lifecycleOwner is Activity && (lifecycleOwner as? Activity)?.isFinishing==true) {
                dispose()
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop() {
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            if (!isDisposed) {
                dispose()
            }

        }

        private fun dispose() {
            isDisposed = true
            monitor?.remove(hashKey)
            viewModelCallbacks.remove(hashKey)
            lifecycleOwner.lifecycle.removeObserver(this)
        }
    }
}