package com.sports.commonn.mvvm.ui.widget;

public interface Indeterminate {
    void setAnimationSpeed(float scale);
}