package com.sports.commonn.mvvm.ui.widget.reclcerview

/**
 *created by Albert
 */
interface ConvertListener<T> {

    fun onConvert(mvViewHolder: MVViewHolder, item: T)
}