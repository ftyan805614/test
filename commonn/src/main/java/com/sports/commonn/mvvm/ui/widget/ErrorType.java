package com.sports.commonn.mvvm.ui.widget;

/**
 * Desc:错误的类型,用于显示不同的错误提示,更多类型可以自定义
 * Created by Jeff on 2018/9/14
 **/
public interface ErrorType {
    int EMPTY = 0;
    int TIMEOUT = 1;
    int ERROR = 2;
}
