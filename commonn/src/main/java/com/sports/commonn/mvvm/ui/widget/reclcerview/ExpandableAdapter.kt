package com.sports.commonn.mvvm.ui.widget.reclcerview

import android.content.Context
import android.content.MutableContextWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.LayoutManagerWrapper
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.LinearLayoutManagerWrapper
import com.sports.commonn.utils.getSF
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

open class ExpandableAdapter(
    dataList: ArrayList<IExpandCollapseViewModel>,
    var mLifecycleOwner: WeakReference<LifecycleOwner?>?, layouts: List<ItemLayout>
) : RecyclerView.Adapter<ExpandableAdapter.ExpandableAdapterViewHolder>(), LifecycleObserver {


    companion object {

        @JvmStatic
        @BindingAdapter(value = ["bindExpLayoutManager"], requireAll = false)
        fun <V : LayoutManagerWrapper> bindExpLayoutManager(
            recyclerView: RecyclerView,
            layoutManager: MutableLiveData<V>?
        ) {
            if (layoutManager == null || layoutManager.value == null) {
                val layoutManagers = LinearLayoutManagerWrapper(recyclerView.context)
                layoutManagers.attachToRecyclerView(recyclerView)
            } else {
                layoutManager.value?.attachToRecyclerView(recyclerView)
            }
        }

        @JvmStatic
        @BindingAdapter(value = ["bindExpAllItem"], requireAll = false)
        fun bindAllExpande(recyclerView: RecyclerView, expall: MutableLiveData<Boolean>?) {
            val adapter = recyclerView.adapter as? ExpandableAdapter
            if (adapter != null && expall?.value != null) {
                val item = adapter.data?.getSF(0)
                adapter.expAllChildOnlySet(0, expall.value ?: true, true)
                item?.setExpand(expall.value ?: true)
            }

        }

        @JvmStatic
        @BindingAdapter(value = ["bindExpItemTypeChange"], requireAll = false)
        fun <V : LayoutManagerWrapper> bindExpItemsChange(
            recyclerView: RecyclerView,
            items: ArrayList<ItemLayout>?
        ) {
            val adapter = recyclerView.adapter
            if (items != null && items.size > 0 && adapter is ExpandableAdapter) {
                adapter.setNewItems(items)
                recyclerView.swapAdapter(adapter, true)
                recyclerView.recycledViewPool.clear()
                recyclerView.layoutManager?.removeAllViews()
                recyclerView.removeAllViews()
                adapter.notifyDataSetChanged()
            }
        }

        @BindingAdapter(
            value = ["bindExpData", "bindExpItemType", "bindExpLifecycleOwner",
                "bindExpItemListener", "bindExpChildClickListener"], requireAll = false
        )
        @JvmStatic
        fun <T : IExpandCollapseViewModel, V : RecyclerView.LayoutManager> bindAdapter(
            recyclerView: RecyclerView,
            data: MutableLiveData<ArrayList<T>>?,
            items: ArrayList<ItemLayout>?,
            lifecycleOwner: LifecycleOwner?,
            itemClickListener: OnItemClickListener?,
            childClickListener: OnItemChildClickListener?
        ) {
            if (recyclerView.adapter == null && data != null && data.value != null && items != null) {
                val dataList = ArrayList<IExpandCollapseViewModel>()
                dataList.addAll(data.value!!)
                val expandableAdapter = ExpandableAdapter(dataList, WeakReference(lifecycleOwner), items)
                expandableAdapter.let { lifecycleOwner?.lifecycle?.addObserver(it) }
                if (recyclerView.layoutManager == null) {
                    LinearLayoutManagerWrapper(recyclerView.context).attachToRecyclerView(recyclerView)
                }
                recyclerView.adapter = expandableAdapter
                if (childClickListener != null) {
                    expandableAdapter.setOnItemChildClickListener(childClickListener)
                }
                if (itemClickListener != null) {
                    expandableAdapter.setOnItemClickListener(itemClickListener)
                }
                (recyclerView.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
                expandableAdapter.updateData(data.value as? ArrayList<IExpandCollapseViewModel>)
            } else {
                val adapter = (recyclerView.adapter as? ExpandableAdapter)
                if (adapter?.mLifecycleOwner?.get() != lifecycleOwner) {
                    adapter?.let { lifecycleOwner?.lifecycle?.addObserver(it) }
                    adapter?.mLifecycleOwner = WeakReference(lifecycleOwner)
                }
                try {
                    adapter?.updateData(data?.value as? ArrayList<IExpandCollapseViewModel>?)
                } catch (e: Exception) {
                    LogUtils.e(e)
                }
            }
        }

        fun getNodeList2(source: ArrayList<IExpandCollapseViewModel>?, targert: ArrayList<IExpandCollapseViewModel>) {
            if (source == null) return
            source.forEach {
                if (it.isExpanded()) {
                    targert.add(it)
                    val childs = it.getChildList<IExpandCollapseViewModel>()
                    if (childs != null && childs.size > 0) {
                        getNodeList2(childs, targert)
                    }
                } else {
                    targert.add(it)
                }
            }
        }

        fun getNodeList(dataList: ArrayList<IExpandCollapseViewModel>?): ArrayList<IExpandCollapseViewModel> {
            if (dataList == null) return ArrayList()
            val newList = ArrayList<IExpandCollapseViewModel>()
            var i = 0
            val size = dataList.size
            while (i < size) {
                val item = dataList[i]
                newList.add(item)
                val expandableListItem = dataList[i]
                expandableListItem.flatDataBefor(i)
                if (expandableListItem.isExpanded()) {
                    val childNodeList = getNodeList(expandableListItem.getChildList())
                    newList.addAll(childNodeList)
                }
                i++
            }
            return newList
        }

    }

    var isDispose = false

    @Volatile
    var data: ArrayList<IExpandCollapseViewModel>? = null

    val orginDataList = ArrayList<IExpandCollapseViewModel>()

    private var childClickListener: OnItemChildClickListener? = null
    private var itemClickListener: OnItemClickListener? = null
    private val mLayouts = HashMap<Int, Int>()
    private var disposable: Disposable? = null


    val adapter: ExpandableAdapter
        get() = this

    fun setOnItemClickListener(itemClickListener: OnItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    fun setOnItemChildClickListener(childClickListener: OnItemChildClickListener) {
        this.childClickListener = childClickListener
    }




    init {
        for ((type, layout) in layouts) {
            mLayouts[type] = layout
        }
        data = getNodeList(dataList)
    }





    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpandableAdapterViewHolder {
        val owner = mLifecycleOwner?.get()
        val context = if (owner is Context) {
            owner
        } else {
            parent.context
        }
        val type = mLayouts[viewType] ?: throw RuntimeException("not found itemType $viewType in $mLayouts")
        val viewBinding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(MutableContextWrapper(context)),
            type,
            parent,
            false
        )

        viewBinding.lifecycleOwner = mLifecycleOwner?.get()
        val expandableAdapterViewHolder = ExpandableAdapterViewHolder(viewBinding.root)
        expandableAdapterViewHolder.bind = WeakReference(viewBinding)
        viewBinding.executePendingBindings()
        return expandableAdapterViewHolder
    }

    override fun onBindViewHolder(holder: ExpandableAdapterViewHolder, position: Int) {
        val item = data?.getSF(position)
        if (item != null) {
            bindMeBindViewHolder(holder, position, item)
        }
    }

    fun bindMeBindViewHolder(holder: ExpandableAdapterViewHolder, position: Int, item: IExpandCollapseViewModel) {
        try {
            holder.setOnItemClickListener(itemClickListener)
            holder.setItemChildClickListener(childClickListener)
            holder.viewPosition = position
            holder.setAdapter(this)
            item.getClickViewIds()?.forEach {
                holder.addOnClickListener(it)
            }
            item.setAdapter(this)
            item.setPosition(position, data?.size ?: 0)
            item.setLifecyclerOwner(mLifecycleOwner)
            holder.getBind()?.lifecycleOwner = mLifecycleOwner?.get()
            holder.getBind()?.setVariable(com.sports.commonn.BR.item, item)
            holder.getBind()?.setVariable(com.sports.commonn.BR.viewModel, item)
            holder.getBind()?.executePendingBindings()
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }


    override fun getItemViewType(position: Int): Int {
        val iExpandCollapseEntity = data.getSF(position)
        return iExpandCollapseEntity?.itemType ?: 0
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    fun setNewItems(layouts: ArrayList<ItemLayout>?) {
        if (layouts == null) return
        mLayouts.clear()
        for ((type, layout) in layouts) {
            mLayouts[type] = layout
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume(lifecycleOwner: LifecycleOwner) {
        val weakLifecycleOwner = WeakReference(lifecycleOwner)
        adapter.data?.forEach {
            it.onResume(weakLifecycleOwner)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause(lifecycleOwner: LifecycleOwner) {

        val weakLifecycleOwner = WeakReference(lifecycleOwner)

        adapter.data?.forEach {
            it.onPause(weakLifecycleOwner)
        }
        val activity = when (lifecycleOwner) {
            is FragmentActivity -> {
                lifecycleOwner
            }
            is Fragment -> {
                lifecycleOwner.activity
            }
            else -> null
        }

        if (activity?.isFinishing == true) {
            dispose(lifecycleOwner)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestory(lifecycleOwner: LifecycleOwner) {
        dispose(lifecycleOwner)
    }

    open fun dispose(lifecycleOwner: LifecycleOwner) {
        if (!isDispose) {
            isDispose = true
            mLifecycleOwner = null
            adapter.data?.forEach {
                it.deAttouchLifecyclerOwner()
            }
        }

    }

    open fun updateData(newData: ArrayList<IExpandCollapseViewModel>?) {
        if (newData != null) {
            orginDataList.clear()
            orginDataList.addAll(newData)
            val nodeList = getNodeList(newData)
            if (this.data == null) {
                this.data = ArrayList()
            }
            this.data?.clear()
            this.data?.addAll(nodeList)
            setNewData(this.data)
        }
    }


    open fun onParentListItemCollapsed(position: Int) {
        try {
            val o = data?.getSF(position)
            if (o is IExpandCollapseViewModel) {
                collapseParentListItem(o, position, true)
                setNewData(data)
                o.setExpand(false)
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }

    }

    open fun onParentListItemExpanded(position: Int) {
        onParentListItemExpanded(position, false)
    }

    private fun onParentListItemExpanded(position: Int, isExpandAllChildren: Boolean) {
        try {
            val o = data?.getSF(position)
            if (o != null) {
                expandParentListItem(o, position, true, isExpandAllChildren)
                setNewData(data)
            }
        } catch (e: IndexOutOfBoundsException) {
            LogUtils.e(e)
        }

    }


    open fun setNewData(data: ArrayList<IExpandCollapseViewModel>?) {
        try {
            this.data = data
            notifyDataSetChanged()
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }


    open fun expAllChildOnlySet(position: Int, isExp: Boolean, onlyValue: Boolean): Boolean {
        try {
            val o = data?.getSF(position)
            if (o is IExpandCollapseViewModel) {
                var isAllClose = false
                if (!onlyValue) {
                    isAllClose = true
                    o.getChildList<IExpandCollapseViewModel>()?.forEach {
                        if (it.isExpanded()) {
                            isAllClose = false
                        }
                    }
                }
                if (isAllClose) {
                    expAllChild(o.getChildList(), true)
                } else {
                    expAllChild(o.getChildList(), isExp)
                }

                data = getNodeList(orginDataList)
                notifyDataSetChanged()
                if (isAllClose) {
                    return true
                }
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }

        return isExp
    }

    open fun expAllChild(position: Int, isExp: Boolean): Boolean {
        return expAllChildOnlySet(position, isExp, false)
    }

    private fun expAllChild(exps: ArrayList<IExpandCollapseViewModel>?, isExp: Boolean) {
        if (exps == null) return
        for (entity in exps) {
            entity.setExpand(isExp)
            if (entity.getChildList<IExpandCollapseViewModel>() != null) {
                expAllChild(entity.getChildList(), isExp)
            }
        }
    }

    private fun collapseParentListItem(
        expandableListItem: IExpandCollapseViewModel,
        parentIndex: Int,
        collapseTriggeredByListItemClick: Boolean
    ) {
        if (expandableListItem.isExpanded()) {
            val childItemList = expandableListItem.getChildList<IExpandCollapseViewModel>()
            if (childItemList != null && childItemList.isNotEmpty()) {
                val childListItemCount = childItemList.size
                for (i in childListItemCount - 1 downTo 0) {
                    val index = parentIndex + i + 1
                    val o = data.getSF(index)
                    if (o != null) {
                        val parentListItem: IExpandCollapseViewModel
                        try {
                            parentListItem = o
                            collapseParentListItem(parentListItem, index, false)
                        } catch (e: Exception) {
                            LogUtils.e(e)
                        }

                    }
                    data?.removeAt(index)
                }

                notifyItemRangeRemoved(parentIndex + 1, childListItemCount)
                expandableListItem.setExpand(false)
                notifyItemRangeChanged(parentIndex + 1, (data?.size ?: 0) - parentIndex - 1)
            }

        }
    }


    private fun expandParentListItem(
        expandableListItem: IExpandCollapseViewModel,
        parentIndex: Int,
        expansionTriggeredByListItemClick: Boolean,
        isExpandAllChildren: Boolean
    ) {
        if (!expandableListItem.isExpanded()) {
            val childItemList = expandableListItem.getChildList<IExpandCollapseViewModel>()
            if (childItemList != null && childItemList.isNotEmpty()) {
                expandableListItem.setExpand(true)
                val childListItemCount = childItemList.size
                for (i in 0 until childListItemCount) {
                    val o = childItemList[i]
                    var newIndex = parentIndex + i + 1
                    if (isExpandAllChildren && i > 0) {
                        for (j in 0 until i) {
                            val childBefore = childItemList[j]
                            if (childBefore.getChildList<IExpandCollapseViewModel>() != null) {
                                val size =
                                    childBefore.getChildList<IExpandCollapseViewModel>()?.size
                                        ?: 0
                                newIndex += size
                            }
                        }
                    }
                    data?.add(newIndex, o)
                    notifyItemInserted(newIndex)
                    if (isExpandAllChildren)
                        expandParentListItem(
                            o,
                            newIndex,
                            expansionTriggeredByListItemClick,
                            isExpandAllChildren
                        )
                }
                // notifyItemRangeInserted(parentIndex + 1, childListItemCount);
                val positionStart = parentIndex + childListItemCount
                if (parentIndex != (data?.size ?: 0) - 1) {
                    notifyItemRangeChanged(positionStart, (data?.size ?: 0) - positionStart)
                }
                // notifyItemExpandedOrCollapsed(parentIndex, true);
            }
        }
    }


    private fun getListItem(position: Int): Any? {
        val indexInRange = position >= 0 && position < (data?.size ?: 0)
        return if (indexInRange) {
            data.getSF(position)
        } else {
            null
        }
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }


    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        if (disposable != null && disposable?.isDisposed == false) {
            disposable?.dispose()
        }
    }


    class ExpandableAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var bind: WeakReference<ViewDataBinding>? = null
        private var itemChildClickListener: OnItemChildClickListener? = null
        private var onItemClickListener: OnItemClickListener? = null
        private var adapter: ExpandableAdapter? = null
        var viewPosition = 0

        init {
            itemView.setOnClickListener {
                if (onItemClickListener != null && adapter != null) {
                    onItemClickListener?.onItemClick(adapter!!, itemView, viewPosition)
                }
            }
        }

        fun addOnClickListener(ids: Int) {
            itemView.findViewById<View>(ids)?.setOnClickListener {
                if (adapter != null && adapter != null)
                    itemChildClickListener?.onItemChildClick(adapter!!, itemView, viewPosition)
            }

        }

        fun getBind(): ViewDataBinding? {
            if ((bind == null || bind?.get() == null)) {
                bind = WeakReference(DataBindingUtil.bind(itemView))
            }
            return bind?.get()
        }


        fun setAdapter(adapter: ExpandableAdapter) {
            this.adapter = adapter
        }


        fun setItemChildClickListener(itemChildClickListener: OnItemChildClickListener?) {
            this.itemChildClickListener = itemChildClickListener
        }

        fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
            this.onItemClickListener = onItemClickListener
        }
    }

    interface OnItemChildClickListener {
        fun onItemChildClick(adapter: ExpandableAdapter, view: View?, position: Int)
    }

    interface OnItemClickListener {
        fun onItemClick(adapter: ExpandableAdapter, view: View, position: Int)
    }


}

