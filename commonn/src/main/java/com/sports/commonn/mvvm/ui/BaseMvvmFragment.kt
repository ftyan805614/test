package com.sports.commonn.mvvm.ui

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Process
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.*
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager
import com.sports.commonn.mvvm.activityshare.ViewModelShareProvider
import com.sports.commonn.mvvm.anim.ClickSelector
import com.sports.commonn.mvvm.anim.DownAlphaAnim
import com.sports.commonn.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import org.greenrobot.eventbus.EventBus
import java.lang.reflect.Field
import kotlin.system.exitProcess

/**
 * created by Albert
 */
abstract class BaseMvvmFragment<T : ViewDataBinding> : Fragment(), View.OnClickListener, CoroutineScope by MainScope() {


    lateinit var bind: T
    var isSupportVisible = false
    private var mExitTime: Long = 0
    protected var mStatusViewHelper: StatusViewHelper? = null
    protected open val isLazyEndAnimation = false
    protected open val lazyDataTime = 0L
    protected open val lazyViewTime = 0L
    private var lifecycleOwnerFile: Field? = null

    protected var isDataInit = false//记录是否已经初始化过一次视图
    protected var isViewInit = false//记录是否已经初始化过一次视图

    var fragments: ArrayList<Fragment>? = null
    private var fragmentContanierId = 0


    override fun onCreateView(
        paramLayoutInflater: LayoutInflater,
        paramViewGroup: ViewGroup?,
        paramBundle: Bundle?
    ): View? {
        return callOnCreateView(paramLayoutInflater, paramViewGroup, paramBundle)
    }

    open fun callOnCreateView(
        paramLayoutInflater: LayoutInflater,
        paramViewGroup: ViewGroup?,
        paramBundle: Bundle?
    ): View? {
        val view = getContentView(paramViewGroup)
            ?: LayoutInflater.from(requireActivity()).inflate(
                contentViewLayoutId(),
                paramViewGroup,
                false
            )
        bindRootView(view)
        return view
    }


    open fun getContentView(viewGroup: ViewGroup?): View? = null

    fun bindRootView(view: View) {
        bind = DataBindingUtil.bind(view)!!
        bind.lifecycleOwner = this
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (lazyViewTime <= 0) {
            initByLayzView()
        } else {
            view.postDelayed(lazyViewInit, lazyViewTime)
        }
    }

    private fun initByLayzView() {
        callOnCrateViewed()
        if (!isLazyEndAnimation && lazyDataTime <= 0) {
            callInitData()
        } else if (lazyDataTime > 0) {
            view?.postDelayed(lazyDataInit, lazyDataTime)
        }
    }

    private val lazyViewInit = Runnable {
        initByLayzView()
    }

    private val lazyDataInit = Runnable {
        callInitData()
    }

    open fun callOnCrateViewed() {
        if (!isViewInit) {
            initViewModel(bind)
            bind.executePendingBindings()
            initView()
            isViewInit = true
        }
    }

    open fun callInitData() {
        if (!isDataInit) {
            initObserve()
            initData()
            isDataInit = true
        }

    }


    protected abstract fun contentViewLayoutId(): Int

    protected abstract fun initViewModel(bind: T)


    open fun initView() {

    }

    open fun initObserve() {

    }

    open fun initData() {

    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (nextAnim == 0) {
            if (isLazyEndAnimation && enter) {
                callInitData()
            }
            return null
        }
        val anim = AnimationUtils.loadAnimation(activity, nextAnim)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                //动画开始
            }

            override fun onAnimationEnd(animation: Animation?) {
                //动画结束
                if (enter) {
                    callInitData()
                }
            }

            override fun onAnimationRepeat(animation: Animation?) {

            }

        })
        return anim
    }

    override fun onStart() {
        super.onStart()
        (lifecycle as? LifecycleRegistry)?.handleLifecycleEvent(Lifecycle.Event.ON_START)

    }


    private fun assemblyViewMode(viewModel: BaseViewModel?, lifecycleOwner: LifecycleOwner? = null): BaseViewModel? {
        if (lifecycleOwner != null) {
            viewModel?.attouchLifecycleOwner(lifecycleOwner)
        } else {
            viewModel?.attouchLifecycleOwner(this)
        }
        if (viewModel != null) {
            lifecycleOwner?.lifecycle?.addObserver(viewModel)
        }
        return viewModel
    }

    fun <V : ViewModel?> ofScopeFragment(t: Class<V>): V {
        return ofScope(this, this, t)
    }


    fun <V : ViewModel?> ofScopeActivity(t: Class<V>): V {
        return ofScope(requireActivity(), requireActivity(), t)
    }

    fun <V : ViewModel?> ofScope(container: Any, lifecycleOwner: LifecycleOwner, t: Class<V>): V {
        val v = when (container) {
            is FragmentActivity -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t) as V
            }
            is Fragment -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t)
            }
            else -> {
                ViewModelProviders.of(this, ViewModelFactory(lifecycleOwner)).get(t)
            }
        }
        if (v is BaseViewModel) {
            assemblyViewMode(v, lifecycleOwner)
        }
        return v
    }

    fun <V : BaseShareViewModel?> ofScopeShareActivity(t: Class<V>): V {
        return ofScopeShare(activity as FragmentActivity, activity as FragmentActivity, t)
    }

    fun <V : BaseShareViewModel?> ofScopeShareFragment(t: Class<V>): V {
        return ofScopeShare(this, this, t)
    }

    fun <V : BaseShareViewModel?> ofScopeShare(container: LifecycleOwner, lifecycleOwner: LifecycleOwner, t: Class<V>): V {
        return ViewModelShareProvider.ofGet(container, t, lifecycleOwner)
    }


    protected fun <T : View> findViewById(@IdRes id: Int): T? {
        return view?.findViewById<T>(id)
    }

    open fun exitActivity() {
        ActivityCompat.finishAfterTransition(requireActivity())
    }

    open fun finishActivity() {
        if (isAdded) {
            activity?.finish()
        }
    }

    open fun finish() {
        activity?.finish()
    }

    protected fun setOnClickListener(@IdRes id: Int) {
        setOnClickListener(findViewById<View>(id))
    }

    protected fun setOnClickListener(view: View?) {
        view?.setOnClickListener(this)
        DownAlphaAnim(view).setOnTouchEvent(null)
    }

    protected fun setOnClickListener(@IdRes id: Int, idNormal: Int, idPress: Int) {
        setOnClickListener(
            findViewById(id),
            ResUtils.getDrawable(idNormal),
            ResUtils.getDrawable(idPress)
        )
    }


    override fun onResume() {
        super.onResume()
        //没有hidden的时候才回调onSupportVisible
        if (userVisibleHint && isViewInit && !isHidden) {
            onSupportVisible()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isViewInit) {
            if (isVisibleToUser && isResumed) {
                onSupportVisible()
            } else {
                onSupportInvisible()
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (isViewInit) {
            if (hidden) {
                onSupportInvisible()
            } else {
                onSupportVisible()
            }

        }
    }


    open fun onSupportVisible() {
        isSupportVisible = true
    }


    open fun onSupportInvisible() {
        hideSoftInput()
        isSupportVisible = false
    }

    open fun hideSoftInput() {
        val activity: Activity? = activity
        if (activity != null) {
            val view = activity.window.decorView
            SupportHelper.hideSoftInput(view)
        }
    }

    override fun onClick(v: View?) {
        //点击动作发送
        EventBusClickUtil.click(v?.id ?: 0)
    }

    open fun setStatusView(view: View?): StatusViewHelper? {
        mStatusViewHelper = view?.let { StatusViewHelper(it) }

        return mStatusViewHelper
    }

    protected fun setOnClickListener(view: View?, normal: Drawable, press: Drawable) {
        view?.setOnClickListener(this)
        if (view is ImageView) {
            ClickSelector.addSelectorFromDrawable(view, normal, press)
        } else {
            ClickSelector.addSelectorFromDrawable(view, normal, press)
        }

    }

    override fun onPause() {
        super.onPause()
        if (activity?.isFinishing == true) {
            resDestroy()
        }
        onSupportInvisible()
    }


    @CallSuper
    open fun resDestroy() {
        cancel()
        EventBus.getDefault().unregister(this)
        view?.removeCallbacks(lazyDataInit)
        if (this::bind.isInitialized) {
            bind.unbind()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (activity != null && activity?.isFinishing == false) {
            try {
                resDestroy()
            } catch (e: Exception) {
            }
        }

    }

    private var showPosition = 0


    fun loadMultipleRootFragment(containerId: Int, showPosition: Int, vararg fragments: Fragment) {
        loadMultipleRootFragment(containerId, showPosition, fragments.toList())
    }

    fun loadMultipleRootFragment(containerId: Int, showPosition: Int, fragments: List<Fragment>, isLayz: Boolean = false) {
        this.fragmentContanierId = containerId
        this.fragments = ArrayList(fragments)
        this.showPosition = showPosition
        var ft = childFragmentManager.beginTransaction()
        fragments.withIndex().forEach {
            if (!it.value.isAdded) {
                if (isLayz) {
                    if (it.index == showPosition) {
                        ft.add(containerId, it.value, it.value.javaClass.name)
                        ft.show(it.value)
                    }
                } else {
                    ft.add(containerId, it.value, it.value.javaClass.name)
                    if (it.index == showPosition) {
                        ft.show(it.value)
                    } else {
                        ft.hide(it.value)
                    }

                }
            }
        }
        ft.commitNowAllowingStateLoss()
        ft = childFragmentManager.beginTransaction()
        fragments.withIndex().forEach {
            if (it.value.isAdded) {
                if (it.index == showPosition) {
                    ft.show(it.value)
                } else {
                    ft.hide(it.value)
                }
            }
        }
        ft.commitNowAllowingStateLoss()

    }


    open fun showHideFragment(
        showFragment: Fragment?,
        hideFragment: Fragment? = null,
        fm: FragmentManager? = childFragmentManager
    ) {
        if (showFragment == null) return
        if (showFragment !== hideFragment) {
            val ft = (fm ?: childFragmentManager).beginTransaction()
            if (!showFragment.isAdded) {
                ft.add(fragmentContanierId, showFragment)
            }
            ft.show(showFragment)
            if (hideFragment == null) {
                fragments?.forEach {
                    if (it.isAdded && it !== showFragment) {
                        ft.hide(it)
                    }
                }
            } else {
                if (!hideFragment.isAdded) {
                    ft.add(fragmentContanierId, hideFragment)
                }
                ft.hide(hideFragment)
            }
            ft.commitAllowingStateLoss()
        }
    }


    open fun onBackPressedSupport(): Boolean {
        finishActivity()
        return true
    }

    fun exitApp(doubleClickTime: Long) {
        try {
            when {
                doubleClickTime <= 0L -> {
                    exitApp()
                }
                System.currentTimeMillis() - this.mExitTime > doubleClickTime -> {
                    ToastUtil.show("再按一次退出程序")
                    this.mExitTime = System.currentTimeMillis()
                }
                else -> {
                    exitApp()
                }
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }

    open fun exitApp() {
        this.onExitApp()
        TopLifecyclerOwnerManager.finishAllActivity {
            it.finish()
            true
        }
        val am = activity?.getSystemService(Context.ACTIVITY_SERVICE) as? ActivityManager
        am?.runningAppProcesses?.forEach {
            if (it.pid != Process.myPid()) {
                Process.killProcess(it.pid)
            }
        }
        activity?.finish()
        System.gc()
        exitProcess(0)
    }

    open fun onExitApp() {}

    open fun applySkinTheme() {
        /*if (::bind.isInitialized) {
            if (bind.root is ViewGroup) {
                if (bind.root is SkinCompatSupportable) {
                    (bind.root as SkinCompatSupportable).applySkin()
                }
                SkinUtil.changeThemeChild(bind.root as ViewGroup)
            }
        }*/
    }

}
