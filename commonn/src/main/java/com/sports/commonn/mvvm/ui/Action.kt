package com.sports.commonn.mvvm.ui

/**
 * created by Albert
 */
class Action {

    var key = 0
    var data: Any? = null

    var statusCode = 6000

    constructor() {}
    constructor(key: Int) {
        this.key = key
    }

    constructor(key: Int, data: Any?) {
        this.key = key
        this.data = data
    }

    companion object {

        @JvmStatic
        val None = -1

        @JvmStatic
        val Show_Component = 3

        @JvmStatic
        val Close_Dialog = 5

        @JvmStatic
        val State_View_Loading = 6

        @JvmStatic
        val State_View_Empty = 7

        @JvmStatic
        val State_View_Success = 8

        @JvmStatic
        val Open_Hint_Dialog = 9

        @JvmStatic
        val State_View_Error = 10

        @JvmStatic
        val Clear_Recycler_Data = 11

        @JvmStatic
        val CLOSE_WINDOW = 12

        @JvmStatic
        val NOTIFY_FLAG = 13


    }

    enum class WindowEventFlow {
        ONSHOW, ONDISSMISS
    }
}
