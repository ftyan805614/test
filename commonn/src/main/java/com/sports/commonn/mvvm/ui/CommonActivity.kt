package com.sports.commonn.mvvm.ui

import android.content.Context
import android.content.ContextWrapper
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager
import kotlinx.coroutines.cancel
import java.lang.ref.WeakReference

/**
 * Desc:
 * Created by Jeff on 2018/10/26
 */
abstract class CommonActivity : BobFragmentActivity() {

    private var isCleared = false
    val fragments = ArrayList<WeakReference<Fragment>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
        } catch (e: Exception) {
        }
        //沉侵式状态栏
//        AppUtil.fullStatusBar(window)
    }

    override fun contentViewLayoutID(): Int = 0

    /**
     * 描述：Android系统 6.0以下AudioManager内存泄漏问题  播放视频内存泄漏
     * 日期：2019-07-07
     * 参数：
     * 返回值：
     */
    override fun attachBaseContext(newBase: Context) {
        try {
            super.attachBaseContext(object : ContextWrapper(newBase) {
                override fun getSystemService(name: String): Any? {
                    // 解决 VideoView 中 AudioManager 造成的内存泄漏
                    return if (Context.AUDIO_SERVICE == name) {
                        applicationContext.getSystemService(name)
                    } else super.getSystemService(name)
                }
            })
        } catch (e: Exception) {
            super.attachBaseContext(newBase)
        }
    }

    override fun onResume() {
        super.onResume()
        isOnAppBackground(TopLifecyclerOwnerManager.instance.isAppOnBackground)
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        fragments.add(WeakReference(fragment))
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            resDestroy()
            isCleared = true
        }
        isOnAppBackground(TopLifecyclerOwnerManager.instance.isAppOnBackground)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isCleared) {
            resDestroy()
        }
    }


    open fun resDestroy() {
        cancel()
        fragments.clear()
    }


    override fun finish() {
        try {
            super.finish()
            finshActivityAnim()
        } catch (e: Exception) {
        }
    }




    open fun startActivityAnim() {
        overridePendingTransition(0, 0)
    }

    open fun finshActivityAnim() {
        overridePendingTransition(0,0)
    }



}
