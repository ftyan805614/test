package com.sports.commonn.mvvm.ui.widget;

import androidx.annotation.StyleRes;

import com.sports.commonn.mvvm.ui.widget.toast.DefaultToastAdapter;
import com.sports.commonn.mvvm.ui.widget.toast.ToastAdapter;
import com.sports.commonn.utils.StatusViewHelper;

/**
 * Desc:
 * Created by Jeff on 2018/9/14
 **/
public class Config {

    private String currentVerName = null;
    private Integer currentVerCode = null;
    /**
     * 自定义StatusView
     */
    private static StatusViewHelper.Factory sStatusFactory = new DefaultStatusFactory();
    /**
     * 自定义全局Toast适配器
     */
    private static ToastAdapter sToastAdapter = new DefaultToastAdapter();
    private static @StyleRes
    int themeId;

    public Config() {
    }

    public static StatusViewHelper.Factory getStatusFactory() {
        return sStatusFactory;
    }

    public Config setStatusFactory(StatusViewHelper.Factory statusFactory) {
        sStatusFactory = statusFactory;
        return this;
    }

    public void setToastAdapter(ToastAdapter toastAdapter) {
        sToastAdapter = toastAdapter;
    }

    public static ToastAdapter getToastAdapter() {
        return sToastAdapter;
    }

    public Config setTheme(int themeId) {
        Config.themeId = themeId;
        return this;
    }

    public static int getThemeId() {
        return themeId;
    }

    //设置插件的versionCode
    public Config setCurrentVerCode(Integer currentVerCode) {
        this.currentVerCode = currentVerCode;
        return this;
    }

    //设置插件的versionCode
    public Config setCurrentVerName(String currentVerName) {
        this.currentVerName = currentVerName;
        return this;
    }

    public String getCurrentVerName() {
        return currentVerName;
    }

    public Integer getCurrentVerCode() {
        return currentVerCode;
    }
}
