package com.sports.commonn.mvvm.ui

import android.content.Context

/**
 *Create By Albert on 2020/12/29
 */
interface CacheSupportAble {

    fun attachContext(context: Context)

    fun disAttachContext(context: Context)
}