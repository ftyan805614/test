package com.sports.commonn.mvvm.ui.widget.reclcerview

import android.view.View
import androidx.lifecycle.MutableLiveData


/**
 * Create By jane on 2019-12-06
 */
abstract class ExpandCollapseViewModel : Diff(), IExpandCollapseViewModel {

    @Transient
    var expandableAdapter: ExpandableAdapter? = null

    open var mExpand = true

    @Transient
    var bindExpand = MutableLiveData<Boolean>().apply { postValue(true) }

    var position = 0

    var totalCount = 0

    override fun <V : IExpandCollapseViewModel> getChildList(): ArrayList<V>? {
        return null
    }

    override fun isExpanded(): Boolean = mExpand

    open fun onClick(v: View) {

    }

    override fun setExpand(isExpand: Boolean) {
        mExpand = isExpand
        bindExpand.postValue(isExpand)
    }

    override fun setAdapter(expandableAdapter: ExpandableAdapter) {
        this.expandableAdapter = expandableAdapter
    }

    override fun setPosition(position: Int, totalCount: Int) {
        this.position = position
        this.totalCount = totalCount
    }

    override fun getClickViewIds(): ArrayList<Int>? = null

    override fun areItemsTheSame(newData: Any?): Boolean {
        return hashCode() == newData?.hashCode()
    }

    override fun areContentsTheSame(newData: Any?): Boolean {
        return this == newData
    }

    override fun getChangePayload(newData: Any?): Any? {
        return newData
    }

    val bindIsLastItem
        get() = position + 1 != totalCount
}

abstract class Diff {

    abstract fun areItemsTheSame(newData: Any?): Boolean

    abstract fun areContentsTheSame(newData: Any?): Boolean

    abstract fun getChangePayload(newData: Any?): Any?
}

