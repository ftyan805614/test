package com.sports.commonn.mvvm.ui

import java.lang.ref.WeakReference

open class ViewModelCallbackWapper<T>() {
    var callbackRefrence:WeakReference<ViewModelCallback<T>>? = null

    private fun getCallBack(): ViewModelCallback<T>?{
        return callbackRefrence?.get()
    }

    fun callBack(data:T?){
        getCallBack()?.callBack(data)
    }

    fun setExtra(extra:Any?){
        getCallBack()?.extra = extra
    }

    fun <E> getExtra(): E? {
        return getCallBack()?.extra as? E
    }
}

open class ViewModelCallback<T>{
    open fun callBack(data:T?){

    }

    var extra:Any?=null
}