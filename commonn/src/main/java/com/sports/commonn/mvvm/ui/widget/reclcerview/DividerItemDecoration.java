package com.sports.commonn.mvvm.ui.widget.reclcerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sports.commonn.R;
import com.sports.commonn.utils.ResUtils;

/**
 * Created by Administrator on 2017/12/6.
 * 分割线
 */

public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};
    public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;
    public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;
    private Drawable mDivider;
    private int mOrientation;
    private int dividerLongthdp;
    private boolean isHaveLastDecoration = false;

    public DividerItemDecoration(Context context, int orientation, int dividerLongthdp) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        mDivider = ResUtils.getDrawable(R.color.transparent_frame_work);
        this.dividerLongthdp = dividerLongthdp;
        a.recycle();
        setOrientation(orientation);
    }

    public DividerItemDecoration(Context context, int orientation, int dividerLongthdp, boolean isHaveLastDecoration) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        mDivider = ResUtils.getDrawable(R.color.transparent_frame_work);
        this.dividerLongthdp = dividerLongthdp;
        a.recycle();
        setOrientation(orientation);
        this.isHaveLastDecoration = isHaveLastDecoration;
    }

    public DividerItemDecoration(Context context, int orientation, int dividerLongthdp, boolean isHaveLastDecoration, int drawbleId) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        mDivider = ResUtils.getDrawable(drawbleId);//R.color.transparent
        this.dividerLongthdp = dividerLongthdp;
        a.recycle();
        setOrientation(orientation);
        this.isHaveLastDecoration = isHaveLastDecoration;
    }

    public void setOrientation(int orientation) {
        if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
            throw new IllegalArgumentException("invalid orientation");
        }
        mOrientation = orientation;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent) {

        if (mOrientation == VERTICAL_LIST) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }

    }


    private void drawVertical(Canvas c, RecyclerView parent) {
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            final View child = parent.getChildAt(i);
            RecyclerView v = new RecyclerView(parent.getContext());
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    private void drawHorizontal(Canvas c, RecyclerView parent) {
        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, int itemPosition, RecyclerView parent) {
        if (isHaveLastDecoration) {

            if (mOrientation == VERTICAL_LIST) {
                outRect.set(0, 0, 0, ResUtils.dp2px(dividerLongthdp));
            } else {
                outRect.set(0, 0, ResUtils.dp2px(dividerLongthdp), 0);
            }
        } else {
            if (itemPosition < parent.getAdapter().getItemCount() - 1) {

                if (mOrientation == VERTICAL_LIST) {
                    outRect.set(0, 0, 0, ResUtils.dp2px(dividerLongthdp));
                } else {
                    outRect.set(0, 0, ResUtils.dp2px(dividerLongthdp), 0);
                }
            }
        }
    }
}