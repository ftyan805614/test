package com.sports.commonn.mvvm.ui.widget.toast;

import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;

import com.apkfuns.logutils.LogUtils;

import static android.view.View.NO_ID;

/**
 * Desc:
 * Created by Jeff on 2018/10/28
 **/
public class ViewHolder {
    private SparseArray<View> mViews = new SparseArray<>();
    private View mView;

    public ViewHolder(View view) {
        mView = view;
    }

    @Nullable
    public final <T extends View> T findView(@IdRes int id) {
        if (id == NO_ID) {
            return null;
        }

        try {
            View view = mViews.get(id);
            if (view == null){
                view = mView.findViewById(id);
                if (view != null){
                    mViews.put(id,view);
                }
            }
            return (T) view;
        }catch (Exception e){
            LogUtils.e(e);
            return null;
        }
    }

    public void setText(@IdRes int id,String message){
        if (message == null) return;
        TextView tv = findView(id);
        if (tv != null){
            tv.setText(message);
        }
    }

    public void setImageRes(@IdRes int id,@DrawableRes int resId){
        if (id == NO_ID) {
            return;
        }
        ImageView tv = findView(id);
        if (tv != null){
            tv.setImageResource(resId);
        }
    }

    public View getView() {
        return mView;
    }
}
