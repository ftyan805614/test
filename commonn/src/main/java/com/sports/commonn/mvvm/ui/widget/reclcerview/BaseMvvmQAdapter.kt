package com.sports.commonn.mvvm.ui.widget.reclcerview

import android.content.MutableContextWrapper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import java.lang.ref.WeakReference

abstract class BaseMvvmQAdapter<T, K : BaseViewHolder> : BaseQuickAdapter<T, K> {


    constructor(layoutResId: Int, data: List<T>?) : super(layoutResId, data)

    constructor(data: List<T>?) : super(data)

    constructor(layoutResId: Int) : super(layoutResId)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): K {
        return if (viewType != LOADING_VIEW && viewType != HEADER_VIEW && viewType != EMPTY_VIEW && viewType != FOOTER_VIEW) {
            val d = DataBindingUtil.inflate<ViewDataBinding>(
                    LayoutInflater.from(MutableContextWrapper(parent.context)),
                    this.mLayoutResId,
                    null,
                    false
            )
            d.executePendingBindings()
            val mvViewHolder = MVViewHolder(d.root, WeakReference(d))
            bindViewClickListener(mvViewHolder)
            mvViewHolder.setQAdapter(this)
            mvViewHolder as K
        } else {
            super.onCreateViewHolder(parent, viewType)
        }
    }


    private fun bindViewClickListener(baseViewHolder: BaseViewHolder?) {
        if (baseViewHolder == null) {
            return
        }
        val view = baseViewHolder.itemView ?: return
        if (onItemClickListener != null) {
            view.setOnClickListener { v ->
                onItemClickListener.onItemClick(
                        this@BaseMvvmQAdapter,
                        v,
                        baseViewHolder.layoutPosition - headerLayoutCount
                )
            }
        }
        if (onItemLongClickListener != null) {
            view.setOnLongClickListener { v ->
                onItemLongClickListener.onItemLongClick(
                        this@BaseMvvmQAdapter,
                        v,
                        baseViewHolder.layoutPosition - headerLayoutCount
                )
            }
        }
    }


}
