package com.sports.commonn.mvvm.ui.widget

import android.app.Activity
import android.graphics.Rect
import android.os.Bundle
import android.util.SparseArray
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.core.util.keyIterator
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.BaseMvvmDialogFragment
import androidx.fragment.app.DialogFragment

/**
 * Desc:
 * / **
 * 自定义对话框
 * 用例： final CustomDialog dialog=new CustomDialog(getActivity(),R.style.AlertDialogStyle,R.layout.dialog_home_tip);
 *
 * mTipDialog.setText(R.id.tv_check_content,"你有新的版本，要现在更新吗？")
 * .addClickListener(R.id.btn_check_cancel, new CustomDialog.OnDialogListener() {
 * @Override
 * public void onClick(CustomDialog dialog, View v) {
 * dialog.dismiss();
 * }
 * })
 * .addClickListener(R.id.btn_check_confirm, new CustomDialog.OnDialogListener() {
 * @Override
 * public void onClick(CustomDialog dialog, View v) {
 * dialog.dismiss();
 * }
 * });
 * CheckBox cbDont = mTipDialog.find(R.id.cb_check_dont);
 * cbDont.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
 * @Override
 * public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
 * isShow = isChecked;
 * }
 * });
 * dialog.addListener(R.id.btn_dialog_confirm, new CustomDialog.DialogListener() {
 * @Override
 * public void onClick(Dialog dialog, View v) {
 * dialog.dismiss();
 * }
 * });
 * dialog.show();
 * Created by Jeff on 2018/9/4
 */
abstract class CustomDialog<T : ViewDataBinding> : BaseMvvmDialogFragment<T>() {


    private var gravity = 0 //对话框的位置
    private var width = 0 //对话框的宽
    private var height = 0 //对话框的高
    private var mAlpha = 1.0f //透明度
    private val mViews = SparseArray<View?>() //性能优化,使用SparseArray代替HashMap
    private val mIds = SparseArray<OnDialogListener>() //性能优化,使用SparseArray代替HashMap
    private val mTexts = SparseArray<String>() //性能优化,使用SparseArray代替HashMap
    private var isInitDialog = false
    private var isInitView = false
    private var blocks = ArrayList<(dialog: DialogFragment) -> Unit>()


    override fun initView() {
        super.initView()
    }

    override fun onStart() {
        super.onStart()
        initDialog()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isInitView = true
        checkViewClick()
    }

    /**
     * 初始化界面控件
     */
    private fun initDialog() {
//        if (isInitDialog) return
        val window = dialog?.window
        if (window != null) {
            val lp = window.attributes
            if (gravity != 0) {
                window.setGravity(gravity)
            }
            if (width != 0) lp.width = width
            if (height != 0) {
                lp.height = height
            } else {
                lp.height = getContextRect(requireActivity())
            }
            if (mAlpha.toDouble() != 0.0) lp.alpha = mAlpha //透明度
            window.attributes = lp
        }
        isInitDialog = true
        blocks.forEach {
            it.invoke(this)
        }
    }

    //获取内容区域
    fun getContextRect(activity: Activity): Int {
        //应用区域
        val outRect1 = Rect()
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(outRect1)
        return outRect1.height()
    }

    /**
     * 设置对话框显示的最终位置
     *
     * @param gravity 例如：Gravity.RIGHT | Gravity.BOTTOM 在右下方
     */
    fun setGravity(gravity: Int) {
        this.gravity = gravity
    }

    /**
     * 设置对话框的宽高属性
     *
     * @param width  对话框的宽
     * @param height 对话框的高
     * @param alpha  对话框背景的透明度。 0.0f :完全透明   ；1.0f:不透明。
     */
    fun setAttributes(width: Int, height: Int, alpha: Float) {
        this.height = height
        this.width = width
        mAlpha = alpha
    }

    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     *
     * @param viewId 控件的id
     * @return  控件对象
     */
    fun <T : View?> find(viewId: Int): T? {
        var view = mViews[viewId]
        if (view == null) {
            view = rootView?.findViewById(viewId)
            mViews.put(viewId, view)
        }
        return view as T?
    }

    /**
     * 为TextView设置字符串
     *
     * @param viewId TextView 的id
     * @param text  要给TextView设置的内容
     * @return  dialog对象
     */
    fun setText(viewId: Int, text: String?): CustomDialog<T> {
        if (text != null && viewId != 0) {
            val view = find<TextView>(viewId)
            view?.text = text
        }
        return this
    }


    fun addClickListener(view: View, onDialogClickListener: OnDialogListener): CustomDialog<T> {
        view.setOnClickListener { v -> onDialogClickListener.onClick(this@CustomDialog, v) }
        return this
    }

    fun addClickListener(id: Int, onDialogClickListener: OnDialogListener): CustomDialog<T> {
        val view = find<View>(id)
        mIds.put(id, onDialogClickListener)
        view?.let { addClickListener(it, onDialogClickListener) }
        return this
    }

    private fun checkViewClick() {
        mIds.keyIterator().forEach {
            addClickListener(it, mIds.get(it))
        }
    }

    interface OnDialogListener {
        fun onClick(dialog: CustomDialog<*>, view: View)
    }

    fun getDialogWindow(): Window? {
        return dialog?.window
    }

    fun addInitedViewListener(block: (dialog: DialogFragment) -> Unit) {
        blocks.add(block)
    }

    override fun resDestroy() {
        super.resDestroy()
        blocks.clear()
        mIds.clear()
    }
}