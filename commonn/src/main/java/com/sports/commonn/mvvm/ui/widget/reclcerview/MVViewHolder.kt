package com.sports.commonn.mvvm.ui.widget.reclcerview


import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import java.lang.ref.WeakReference

class MVViewHolder(itemView: View, binding: WeakReference<ViewDataBinding>?) : BaseViewHolder(itemView) {

    var dataViewBinding: WeakReference<ViewDataBinding>? = null
        internal set

    init {
        this.dataViewBinding = binding
    }

    fun setQAdapter(adapter: BaseQuickAdapter<*, *>): BaseViewHolder {
        super.setAdapter(adapter)
        return this
    }

    fun getBind(): WeakReference<ViewDataBinding>? {
        if (dataViewBinding?.get() == null) {
            dataViewBinding = WeakReference(DataBindingUtil.bind(itemView))
        }
        return dataViewBinding
    }

}
