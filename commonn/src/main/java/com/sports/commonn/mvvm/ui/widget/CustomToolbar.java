package com.sports.commonn.mvvm.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sports.commonn.R;
import com.sports.commonn.mvvm.ui.CacheSupportAble;
import com.sports.commonn.utils.CacheSupportUtils;
import com.sports.commonn.utils.EventBusClickUtil;
import com.sports.commonn.utils.ResUtils;
import com.sports.commonn.utils.ScreenUtil;

import org.jetbrains.annotations.NotNull;

import static com.sports.commonn.mvvm.ui.widget.CommonTitleBar.TYPE_CENTER_TEXTVIEW;
import static com.sports.commonn.mvvm.ui.widget.CommonTitleBar.TYPE_LEFT_IMAGEBUTTON;


/**
 * Created by Administrator on 2017/12/8.
 */

public class CustomToolbar extends RelativeLayout implements CacheSupportAble {
    private CommonTitleBar titlebar;
    float contentMargin;

    public CustomToolbar(Context context) {
        this(context, null);
    }

    public CustomToolbar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(attrs, 0);
    }

    public CustomToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);

    }


    public void init(AttributeSet attrs, int defStyleAttr) {
        contentMargin = ScreenUtil.INSTANCE.getStatusBarHeight();
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomToolbar);

            for (int i = 0; i < typedArray.getIndexCount(); i++) {
                if (typedArray.getIndex(i) == R.styleable.CustomToolbar_content_margin_top) {
                    contentMargin = typedArray.getDimension(i, ScreenUtil.INSTANCE.getStatusBarHeight());
                }
            }
        }

        if (isInEditMode()) return;
        initView();

    }


    private void initView() {
        LayoutParams marginLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        titlebar = new CommonTitleBar(getContext(), null);
        titlebar.setBackground(new ColorDrawable(Color.TRANSPARENT));
        addView(titlebar, marginLayoutParams);
        Context context;
        if (getContext() instanceof MutableContextWrapper) {
            context = ((MutableContextWrapper) getContext()).getBaseContext();
        } else {
            context = getContext();
        }
        if (context instanceof Activity) {
            if (isStatusBarShown((Activity) context)) {
                marginLayoutParams.setMargins(0, (int) contentMargin, 0, 0);
            }
        }

    }

    //设置titlebar高度
    public void setTitlebarHeight(int height) {
        if (titlebar != null) {
            titlebar.setTitleBarHight(height);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

    }

    @Override
    public void setBackgroundResource(int resid) {
        super.setBackgroundResource(resid);
    }


    public CustomToolbar setBackgroundColors(int color) {
        setBackgroundColor(ResUtils.getColor(color));
        return this;
    }

    public CustomToolbar setLeftType(int leftType) {
        titlebar.setLeftType(leftType);
        return this;
    }

    public CustomToolbar setRightType(int rightType) {
        titlebar.setRightType(rightType);
        return this;
    }

    public CustomToolbar setCenterType(int centerType) {
        titlebar.setCenterType(centerType);
        return this;
    }


    //设置主title的内容
    public CustomToolbar setMainTitle(String text) {
        if (titlebar.getCenterTextView() != null) {
            titlebar.getCenterTextView().setVisibility(View.VISIBLE);
            titlebar.getCenterTextView().setText(text);
        } else {
            titlebar.initMainCenterViews(TYPE_CENTER_TEXTVIEW);
            titlebar.getCenterTextView().setVisibility(View.VISIBLE);
            titlebar.getCenterTextView().setText(text);
        }
        return this;
    }

    //设置主title的内容文字的颜色
    public CustomToolbar setMainTitleColor(int mainTitleColor) {
        if (titlebar.getCenterTextView() != null) {
            titlebar.getCenterTextView().setTag(R.id.titlebar_center_txt_id, mainTitleColor);
            titlebar.getCenterTextView().setTextColor(ResUtils.getColor(mainTitleColor));
        }
        return this;
    }

    //设置主title文字大小
    public CustomToolbar setMainTitleSize(float txtSize) {
        if (null != titlebar.getCenterTextView()) {
            titlebar.getCenterTextView().setTextSize(txtSize);
        }
        return this;
    }

    //设置title图标
    public CustomToolbar setMainTitleDrawable(int res) {
        Drawable dwright = ResUtils.getDrawable(res);
        if (dwright != null) {
            dwright.setBounds(0, 0, dwright.getMinimumWidth(), dwright.getMinimumHeight());
        }
        if (titlebar.getCenterTextView() != null) {
            // titlebar.getCenterTextView().setTag(titlebar.getCenterTextView().hashCode(), res);
            titlebar.getCenterTextView().setCompoundDrawables(null, null, dwright, null);
        }
        return this;
    }

    //设置title图标
    public CustomToolbar setMainTitleDrawablePading(float res) {

        if (titlebar.getCenterTextView() != null) {
            // titlebar.getCenterTextView().setTag(titlebar.getCenterTextView().hashCode(), res);
            titlebar.getCenterTextView().setCompoundDrawablePadding(ResUtils.dp2px(res));
        }
        return this;
    }

    //设置title点击事件
    public CustomToolbar setMainTitleOnClick(OnClickListener clickListener) {
        if (titlebar.getCenterTextView() != null) {
            titlebar.getCenterTextView().setOnClickListener(new ViewClickListener(clickListener));
        }
        return this;
    }


    public View getMainTitle() {
        return titlebar.getCenterTextView();
    }


    //设置左边文字
    public CustomToolbar setLeftText(String text) {
        if (null != titlebar.getLeftTextView()) {
            titlebar.getLeftTextView().setVisibility(View.VISIBLE);
            titlebar.getLeftTextView().setText(text);
        }
        return this;
    }

    //设置左边文字颜色
    public CustomToolbar setLeftTextColor(int resColor) {
        if (null != titlebar.getLeftTextView()) {
            titlebar.getLeftTextView().setTag(R.id.titlebar_left_txt_id, resColor);
            titlebar.getLeftTextView().setTextColor(ResUtils.getColor(resColor));
        }
        return this;
    }

    public void setMainTitleLeftIcon() {
    }

    //设置左边文字颜色
    public CustomToolbar setLeftTextSize(float txtSize) {
        if (null != titlebar.getLeftTextView()) {
            titlebar.getLeftTextView().setTextSize(txtSize);
        }
        return this;
    }

    //设置左边返回键图片
    public CustomToolbar setLeftImg(int res) {
        titlebar.setLeftType(TYPE_LEFT_IMAGEBUTTON);
        if (null != titlebar.getLeftImageButton()) {
            titlebar.getLeftImageButton().setTag(R.id.titlebar_left_image_id, res);
            Drawable drawable = ResUtils.getDrawable(res);
            if (drawable != null) {
                titlebar.getLeftImageButton().setImageDrawable(ResUtils.getDrawable(res));
            } else {
                titlebar.getLeftImageButton().setImageDrawable(null);
            }
        }
        return this;
    }

    //设置左边返回键图片
    private int leftDrawable = 0;

    public CustomToolbar setLeftTxtImg(int leftDrawable, int leftDrawablePadding) {
        if (null != titlebar.getLeftTextView()) {
            this.leftDrawable = leftDrawable;
            titlebar.getLeftTextView().setCompoundDrawablePadding(leftDrawablePadding);
            titlebar.getLeftTextView().setCompoundDrawablesWithIntrinsicBounds(ResUtils.getDrawable(leftDrawable), null, null, null);
        }
        return this;
    }

    //设置title左边点击事件
    public CustomToolbar setLeftOnClick(OnClickListener clickListener) {
        if (null != titlebar.getLeftImageButton()) {
            titlebar.getLeftImageButton().setOnClickListener(new ViewClickListener(clickListener));
        }
        if (null != titlebar.getLeftTextView()) {
            titlebar.getLeftTextView().setOnClickListener(new ViewClickListener(clickListener));
        }
        return this;
    }

    /**
     * 左边点击事件返回
     */
    public CustomToolbar setLeftClickFinish() {
        setLeftOnClick(new ViewClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getContext() instanceof Activity) {
                    ((Activity) getContext()).finish();
                }

            }
        }));
        return this;
    }

    public TextView getLeftTextView() {
        return titlebar.getLeftTextView();
    }

    /**
     * 获取标题栏左边ImageButton，对应leftType = imageButton
     *
     * @return
     */
    public ImageButton getLeftImageButton() {
        return titlebar.getLeftImageButton();
    }

    //设置右边文字
    public CustomToolbar setRightText(String text) {
        setRightType(CommonTitleBar.TYPE_RIGHT_TEXTVIEW);
        if (null != titlebar.getRightTextView()) {
            titlebar.getRightTextView().setVisibility(View.VISIBLE);
            titlebar.getRightTextView().setText(text);
            titlebar.getRightTextView().setTextSize(16);
        }
        return this;
    }

    //设置右边文字左边图片
    public CustomToolbar setRightTextLeftImg(int leftDrawable, int leftDrawablePadding) {
        if (null != titlebar.getRightTextView()) {
            titlebar.getRightTextView().setCompoundDrawablePadding(leftDrawablePadding);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                titlebar.getRightTextView().setCompoundDrawablesRelativeWithIntrinsicBounds(leftDrawable, 0, 0, 0);
            } else {
                titlebar.getRightTextView().setCompoundDrawablesWithIntrinsicBounds(leftDrawable, 0, 0, 0);
            }
        }
        return this;
    }

    //设置右边文字
    public CustomToolbar setRightTextRightImg(int rightDrawable, int rightDrawablePadding) {
        if (null != titlebar.getRightTextView()) {
            titlebar.getLeftTextView().setCompoundDrawablePadding(rightDrawablePadding);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                titlebar.getLeftTextView().setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, rightDrawable, 0);
            } else {
                titlebar.getLeftTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, rightDrawable, 0);
            }
        }
        return this;
    }

    //设置右边文字颜色
    public CustomToolbar setRightTextColor(int resColor) {
        if (null != titlebar.getRightTextView()) {
            titlebar.getRightTextView().setTag(R.id.titlebar_right_txt_id, resColor);
            titlebar.getRightTextView().setTextColor(ResUtils.getColor(resColor));
        }
        return this;
    }

    //设置右边文字颜色
    public CustomToolbar setRightTextSize(float txtSize) {
        if (null != titlebar.getRightTextView()) {
            titlebar.getRightTextView().setTextSize(txtSize);
        }
        return this;
    }

    //设置右边图片
    public CustomToolbar setRightImg(int res) {
        titlebar.setRightType(CommonTitleBar.TYPE_RIGHT_IMAGEBUTTON);
        if (null != titlebar.getRightImageButton()) {
            titlebar.getRightImageButton().setVisibility(View.VISIBLE);
            titlebar.getRightImageButton().setTag(R.id.titlebar_right_image_id, res);
            titlebar.getRightImageButton().setImageDrawable(ResUtils.getDrawable(res));
        }
        return this;
    }

    public CustomToolbar setRightImg1(int res) {
        titlebar.setRightType(CommonTitleBar.TYPE_RIGHT_IMAGEBUTTON_2);
        if (null != titlebar.getRightImageButton1()) {
            titlebar.getRightImageButton1().setVisibility(View.VISIBLE);
            titlebar.getRightImageButton1().setTag(R.id.titlebar_right_image_id_1, res);
            titlebar.getRightImageButton1().setImageDrawable(ResUtils.getDrawable(res));
        }
        return this;
    }

    public CustomToolbar setRightImg2(int res) {
        titlebar.setRightType(CommonTitleBar.TYPE_RIGHT_IMAGEBUTTON_2);
        if (null != titlebar.getRightImageButton2()) {
            titlebar.getRightImageButton2().setVisibility(View.VISIBLE);
            titlebar.getRightImageButton2().setTag(R.id.titlebar_right_image_id_2, res);
            titlebar.getRightImageButton2().setImageDrawable(ResUtils.getDrawable(res));
        }
        return this;
    }

    public CustomToolbar setBarBackgroundColor(int res) {
        if (null != titlebar.getRightImageButton()) {
            setBackgroundColor(ResUtils.getColor(res));
        }
        return this;
    }

    public CustomToolbar setBarBackgroundResource(int res) {
        if (null != titlebar.getRightImageButton()) {
            setBackgroundResource(ResUtils.getColor(res));
        }
        return this;
    }

    //设置右边图片
    public CustomToolbar setRightImg(Drawable res) {
        if (null != titlebar.getRightImageButton()) {
            titlebar.getRightImageButton().setVisibility(View.VISIBLE);
            titlebar.getRightImageButton().setImageDrawable(res);
        }
        return this;
    }

    public CustomToolbar setRightClick(OnClickListener clickListener) {
        if (null != titlebar.getRightImageButton()) {
            titlebar.getRightImageButton().setOnClickListener(new ViewClickListener(clickListener));
        }
        if (null != titlebar.getRightTextView()) {
            titlebar.getRightTextView().setOnClickListener(new ViewClickListener(clickListener));
        }
        return this;
    }

    public CustomToolbar setRightClick1(OnClickListener clickListener) {
        if (null != titlebar.getRightImageButton1()) {
            titlebar.getRightImageButton1().setOnClickListener(new ViewClickListener(clickListener));
        }
        if (null != titlebar.getRightTextView()) {
            titlebar.getRightTextView().setOnClickListener(new ViewClickListener(clickListener));
        }
        return this;
    }

    public CustomToolbar setRightClick2(OnClickListener clickListener) {
        if (null != titlebar.getRightImageButton2()) {
            titlebar.getRightImageButton2().setOnClickListener(new ViewClickListener(clickListener));
        }
        if (null != titlebar.getRightTextView()) {
            titlebar.getRightTextView().setOnClickListener(new ViewClickListener(clickListener));
        }
        return this;
    }

    public TextView getRightView() {
        return titlebar.getRightTextView();
    }


    /**
     * 获取标题栏右边ImageButton，对应rightType = imageButton
     *
     * @return
     */
    public ImageButton getRightImageButton() {
        return titlebar.getRightImageButton();
    }

    /**
     * 右边点击事件finish
     */
    public CustomToolbar setRightClickFinish() {
        setRightClick(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getContext() instanceof Activity) {
                    ((Activity) getContext()).finish();
                }

            }
        });
        return this;
    }

    public void setMainTitleRightColorAppearance(int style) {
        if (null != titlebar.getRightTextView()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                titlebar.getRightTextView().setTextAppearance(style);
            }
        }
    }

    public CustomToolbar setLeftView(View leftView) {
        if (null != titlebar) {
            titlebar.setLeftView(leftView);
        }
        return this;
    }

    public CustomToolbar setRightView(View rightView) {
        if (null != titlebar) {
            titlebar.setRightView(rightView);
        }
        return this;
    }

    public CustomToolbar setPaddingTopTitile() {
        if (null != titlebar) {
            titlebar.setPadding(0, ResUtils.dp2px(25f), 0, 0);
        }
        return this;
    }

    public CustomToolbar setCenterView(View centerView) {
        if (null != titlebar) {
            titlebar.setCenterView(centerView);
        }
        return this;
    }

    public CustomToolbar setBarColor(int color) {
        if (null != titlebar) {
            setBackgroundColor(color);
        }
        return this;
    }


    public CommonTitleBar getTitlebar() {
        return titlebar;
    }

    @Override
    public void attachContext(@NotNull Context context) {
        CacheSupportUtils.changeContextChild(this, true, context);
    }


    @Override
    public void disAttachContext(@NotNull Context context) {
        CacheSupportUtils.changeContextChild(this, false, context);
    }


    class ViewClickListener implements OnClickListener {
        private OnClickListener onClickListener;

        ViewClickListener(OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }

        @Override
        public void onClick(View v) {
            if (null != onClickListener) {
                onClickListener.onClick(v);
            }
            EventBusClickUtil.INSTANCE.click(v.getId());
        }
    }

    public CustomToolbar setCBackgroundDrawableId(Integer background) {
        Drawable drawable = ResUtils.getDrawable(background);
        setBackground(drawable);
        return this;
    }

    //设置title左边点击事件
    public void setMainTitleLeftOnClick(OnClickListener clickListener) {
        setLeftOnClick(clickListener);
    }


    //设置title右边文字
    public CustomToolbar setMainTitleRightText(String text) {
        setRightText(text);
        return this;
    }


    //设置title点击事件
    public void setMainTitleRightOnClick(OnClickListener clickListener) {
        setRightClick(clickListener);
    }

    public boolean isStatusBarShown(Activity context) {
        WindowManager.LayoutParams params = context.getWindow().getAttributes();
        int paramsFlag = params.flags & (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return paramsFlag == params.flags;
    }
}
