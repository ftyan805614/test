package com.sports.commonn.mvvm.ui

import android.app.Activity
import android.content.Context
import android.content.MutableContextWrapper
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.activityshare.ViewModelShareProvider
import com.sports.commonn.utils.ResUtils
import com.sports.commonn.utils.ScreenUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import java.lang.ref.WeakReference


/**
 *Create By Albert on 2019-11-20
 */
abstract class BaseMvvmPopuWindow<T : ViewDataBinding>(val context: Context) : PopupWindow(),
    CoroutineScope by MainScope(), LifecycleOwner, LifecycleObserver {

    lateinit var bind: T

    private var mContextView: View? = null

    private var bgPopupWindow: PopupWindow? = null

    private var onDismissListeners = ArrayList<OnDismissListener>()

    protected var popupHeight = 0
    protected var popupWidth = 0

    var mLifecycleRegistry: LifecycleRegistry? = null
    var mActivity: FragmentActivity? = null

    init {
        mActivity = context as? FragmentActivity
        initLifecycle()
    }

    private fun initLifecycle() {
        (context as? FragmentActivity)?.lifecycle?.addObserver(this)
        mLifecycleRegistry = LifecycleRegistry(this)
        mLifecycleRegistry?.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate(lifecycleOwner: LifecycleOwner) {
        mLifecycleRegistry?.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(lifecycleOwner: LifecycleOwner) {
        mLifecycleRegistry?.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume(lifecycleOwner: LifecycleOwner) {
        mLifecycleRegistry?.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause(lifecycleOwner: LifecycleOwner) {
        mLifecycleRegistry?.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        if (lifecycleOwner is Activity && lifecycleOwner.isFinishing) {
            try {
                lifecycleOwner.lifecycle.removeObserver(this)
                resDestroy()
            } catch (e: Exception) {
                LogUtils.e(e)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestory(lifecycleOwner: LifecycleOwner) {
        lifecycleOwner.lifecycle.removeObserver(this)
        resDestroy()
    }


    private fun initViewModelData(anchor: View?) {
        mLifecycleRegistry?.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
        bind = DataBindingUtil.inflate(
            LayoutInflater.from(MutableContextWrapper(context)),
            getContentViewLayoutId(),
            null,
            false
        )
        mContextView = bind.root
        contentView = mContextView
        initViewModel(bind)
        contentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        popupHeight = contentView.measuredHeight
        popupWidth = contentView.measuredWidth
        super.setOnDismissListener {
            resDestroy()
            onDismissListeners.forEach {
                it.onDismiss()
            }
        }
        isFocusable = true
        width = if (popupWidth == 0) ScreenUtil.screenWidth else popupWidth
        height = ViewGroup.LayoutParams.WRAP_CONTENT
        setBackgroundDrawable(ColorDrawable(0x00000000))//设置透明背景
        isOutsideTouchable = true//设置outside可点击
        initView(anchor)
        initObserve()
        initData()
    }


    override fun setOnDismissListener(onDismissListener: OnDismissListener) {
    }

    fun addOnDismissListener(onDismissListener: OnDismissListener) {
        onDismissListeners.add(onDismissListener)
    }

    @CallSuper
    open fun initView(anchor: View?) {

    }

    @CallSuper
    open fun initObserve() {

    }

    @CallSuper
    open fun initData() {

    }


    @LayoutRes
    abstract fun getContentViewLayoutId(): Int

    abstract fun initViewModel(bind: T)


    fun <V : ViewModel?> ofScopePopup(t: Class<V>): V? {
        val viewModel = ViewModelProviders.of(context as FragmentActivity, ViewModelFactory(this))
            .get(t) as? BaseViewModel
        if (viewModel is LifecycleObserver) {
            lifecycle.addObserver(viewModel as LifecycleObserver)
        }
        return viewModel as? V
    }


    fun <V : ViewModel?> ofScopeActivity(t: Class<V>): V? {
        val viewModel =
            ViewModelProviders.of(context as FragmentActivity, ViewModelFactory(context))
                .get(t) as? BaseViewModel
        if (viewModel is LifecycleObserver) {
            context.run { lifecycle.addObserver(viewModel as LifecycleObserver) }
        }
        return viewModel as? V
    }

    fun <V : ViewModel?> ofScope(container: Any, lifecycleOwner: LifecycleOwner?, t: Class<V>): V? {
        val viewModel = when (container) {
            is FragmentActivity -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t) as V
            }
            is Fragment -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t)
            }
            else -> {
                ViewModelProviders.of(context as FragmentActivity, ViewModelFactory(lifecycleOwner))
                    .get(t)
            }
        }
        if (viewModel is BaseViewModel && lifecycleOwner != null) {
            lifecycleOwner.lifecycle.addObserver(viewModel as BaseViewModel)
            (viewModel as BaseViewModel).lifecycleOwner = WeakReference(lifecycleOwner)
            viewModel.attouchLifecycleOwner(lifecycleOwner)
        }
        return viewModel
    }


    fun <V : BaseShareViewModel?> ofScopeShareFragment(t: Class<V>): V? {
        return (context as? LifecycleOwner)?.let { ViewModelShareProvider.ofGet(it, t) }
    }


    fun <V : BaseShareViewModel?> ofScopeShareActivity(t: Class<V>): V {
        if (context is FragmentActivity) {
            return ViewModelShareProvider.ofGet(context, t)
        }
        return ViewModelShareProvider.ofGet(this, t)
    }


    fun <V : BaseShareViewModel?> ofScopeShare(
        container: LifecycleOwner?,
        lifecycleOwner: LifecycleOwner?,
        t: Class<V>
    ): V? {
        if (container == null || lifecycleOwner == null) return null
        return ViewModelShareProvider.ofGet(container, t, lifecycleOwner)
    }

    open fun resDestroy() {
        mLifecycleRegistry?.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        mActivity = null
        cancel()
    }

    override fun showAsDropDown(anchor: View?, xoff: Int, yoff: Int, gravity: Int) {
        initViewModelData(anchor)
        super.showAsDropDown(anchor, xoff, yoff, gravity)
    }

    override fun showAtLocation(parent: View?, gravity: Int, x: Int, y: Int) {
        initViewModelData(null)
        super.showAtLocation(parent, gravity, x, y)
    }


    //处理android 7.0 兼容问
    override fun showAsDropDown(anchor: View?, xoff: Int, yoff: Int) {
        if (Build.VERSION.SDK_INT == 24) {
            val location = IntArray(2)
            anchor?.getLocationOnScreen(location)
            val x = location[0]
            val y = location[1]
            showBgPopu()?.showAtLocation(anchor, Gravity.NO_GRAVITY, 0, y + (anchor?.height ?: 0))
            super.showAsDropDown(anchor, xoff, yoff - 2)
            return
        }
        showBgPopu()?.showAsDropDown(anchor, xoff, yoff)
        super.showAsDropDown(anchor, xoff, yoff)
    }

    fun showViewUp(view: View) {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        initViewModelData(view)
        showAtLocation(
            view, Gravity.NO_GRAVITY,
            location[0], location[1]
        )
    }

    override fun getLifecycle(): Lifecycle {
        return mLifecycleRegistry ?: LifecycleRegistry(this)
    }

    override fun dismiss() {
        bgPopupWindow?.dismiss()
        super.dismiss()
    }

    private fun showBgPopu(): PopupWindow? {
        bgPopupWindow = PopupWindow()
        val mContentView = LinearLayout(context)
        val view = View(context).apply {
            if (!isTransparent()) setBackgroundColor(Color.parseColor("#80000000"))
        }
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        layoutParams.topMargin = ResUtils.dp2px(20f)
        mContentView.addView(view, layoutParams)
        bgPopupWindow?.contentView = mContentView
        bgPopupWindow?.animationStyle = 0
        bgPopupWindow?.width = ScreenUtil.screenWidth
        bgPopupWindow?.height = ViewGroup.LayoutParams.WRAP_CONTENT
        return bgPopupWindow
    }


    open fun isTransparent(): Boolean {
        return false
    }
}