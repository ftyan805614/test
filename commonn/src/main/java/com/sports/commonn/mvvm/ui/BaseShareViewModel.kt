package com.sports.commonn.mvvm.ui

import android.app.Activity
import android.text.Editable
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager
import com.sports.commonn.mvvm.activityshare.ViewModelShareProvider
import com.sports.commonn.mvvm.anim.DownAlphaAnim
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData
import com.sports.commonn.mvvm.ui.widget.reclcerview.BaseMulteItemEntity
import com.sports.commonn.utils.EventBusClickUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference

/**
 * 多个activity 共享 一个viewModel
 *created by Albert
 */
open class BaseShareViewModel : ViewModelShareProvider.LifecycleShareObserver,
    BaseMulteItemEntity() {


    private val clickIds = ArrayList<Int>()

    private var compositeDisposable = CompositeDisposable()


    var lifecycleOwner: WeakReference<LifecycleOwner>? = null

    var onattouchedLifecycleOwner = ArrayList<WeakReference<LifecycleOwner>>()

    val fragment: WeakReference<Fragment?>?
        get() = when (val owner = lifecycleOwner?.get()) {
            is Fragment -> WeakReference(owner)
            is DialogFragment -> WeakReference(owner)
            else -> null
        }

    val activity: WeakReference<FragmentActivity?>?
        get() = when (val owner = lifecycleOwner?.get()) {
            is FragmentActivity -> WeakReference(owner)
            is Fragment -> WeakReference(owner.activity)
            is BaseMvvmPopuWindow<*> -> WeakReference(owner.mActivity)
            else -> null
        }

    open fun attouchLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        val mLifecycleOwner = onattouchedLifecycleOwner.firstOrNull { it.get() == lifecycleOwner }
        if (mLifecycleOwner == null) {
            this.lifecycleOwner = WeakReference(lifecycleOwner)
            onattouchedLifecycleOwner.add(this.lifecycleOwner!!)
        } else {
            this.lifecycleOwner = mLifecycleOwner
        }
    }


    fun addDisposable(diaspoable: Disposable?) {
        diaspoable?.also { compositeDisposable.add(diaspoable) }
    }

    fun clearDisposable() {
        compositeDisposable.clear()
    }

    var action = SafeMutableLiveData<Action>()

    open fun onClick(v: View) {
        EventBusClickUtil.click(v.id)
        if (!clickIds.contains(v.id)) {
            clickIds.add(v.id)
            DownAlphaAnim(v).setOnTouchEvent(null)
        }
    }

    open fun afterTextChanged(editable: Editable) {

    }

    fun <V : ViewModel?> ofScopeFragment(t: Class<V>): V {
        return ofScope(
            fragment?.get() ?: activity?.get(),
            fragment?.get() ?: (activity?.get() as? LifecycleOwner),
            t
        )
    }


    fun <V : ViewModel> ofScopeActivity(t: Class<V>): V {
        return ofScope(activity?.get(), activity?.get(), t)
    }

    fun <V : ViewModel?> ofScope(container: Any?, lifecycleOwner: LifecycleOwner?, t: Class<V>): V {
        val viewModel = when (container) {
            is FragmentActivity -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t) as V
            }
            is Fragment -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t)
            }
            else -> {
                ViewModelProviders.of(TopLifecyclerOwnerManager.curActivity as FragmentActivity, ViewModelFactory(TopLifecyclerOwnerManager.curActivity as FragmentActivity)).get(t)
            }
        }
        if (viewModel is BaseViewModel && lifecycleOwner != null) {
            lifecycleOwner.lifecycle.addObserver(viewModel as BaseViewModel)
            (viewModel as BaseViewModel).lifecycleOwner = WeakReference(lifecycleOwner)
        }
        return viewModel
    }

    fun <V : BaseShareViewModel?> ofScopeShareActivity(t: Class<V>): V {
        return ofScopeShare(activity?.get(), activity?.get(), t)
    }

    fun <V : BaseShareViewModel?> ofScopeShareFragment(t: Class<V>): V {
        return ofScopeShare(fragment?.get(), fragment?.get(), t)
    }

    fun <V : BaseShareViewModel?> ofScopeShare(
        container: LifecycleOwner?,
        lifecycleOwner: LifecycleOwner?,
        t: Class<V>
    ): V {
        return ViewModelShareProvider.ofGet(container ?: (TopLifecyclerOwnerManager.curActivity as FragmentActivity), t, lifecycleOwner ?: (TopLifecyclerOwnerManager.curActivity as FragmentActivity))
    }

    override fun onCreate(lifecycleOwner: LifecycleOwner) {
    }

    override fun onStart(lifecycleOwner: LifecycleOwner) {

    }

    override fun onResume(lifecycleOwner: LifecycleOwner) {

    }


    override fun onPause(lifecycleOwner: LifecycleOwner) {

    }

    override fun onStop(lifecycleOwner: LifecycleOwner) {

    }

    override fun onDestroy(lifecycleOwner: LifecycleOwner) {

    }

    fun <T> weak(block: () -> T): WeakReference<T> {
        return WeakReference(block.invoke())
    }


    /**
     * 手动释放 资源
     * 比如：rx dispose
     */
    override fun resDestroy() {
        super.resDestroy()

    }

    fun getFragment(): Fragment? {
        return if (lifecycleOwner?.get() is Fragment) lifecycleOwner?.get() as? Fragment else null;
    }

    fun finishActivity() {
        if (lifecycleOwner?.get() is BaseNavigationFragment<*>) {
            (lifecycleOwner?.get() as BaseNavigationFragment<*>).finishFragment()
        } else if (lifecycleOwner?.get() is Activity) {
            (lifecycleOwner?.get() as? Activity)?.finish()
        }
    }


    fun finishAllTouchedActivity() {
        onattouchedLifecycleOwner.forEach {
            if (it.get() is BaseNavigationFragment<*>) {
                (it.get() as BaseNavigationFragment<*>).finishFragment()
            } else if (it.get() is Activity) {
                (it.get() as? Activity)?.finish()
            }
        }
    }

    fun onCleared() {
        clickIds.clear()
        clearDisposable()
        resDestroy()
    }

}