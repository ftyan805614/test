package com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper

import android.content.Context
import android.util.AttributeSet

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.apkfuns.logutils.LogUtils

/**
 * created by Albert
 */
class SafeLinearLayoutManager : LinearLayoutManager {
    
    constructor(context: Context?) : super(context) {}

    constructor(context: Context?, orientation: Int, reverseLayout: Boolean) : super(context, orientation, reverseLayout) {}

    constructor(context: Context?, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {}

    override fun supportsPredictiveItemAnimations(): Boolean {
        return super.supportsPredictiveItemAnimations()
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: Exception) {
            LogUtils.e(e)
        }

    }
}
