package com.sports.commonn.mvvm.ui.widget.reclcerview

import android.app.Activity
import android.view.animation.LayoutAnimationController
import androidx.databinding.BindingAdapter
import androidx.lifecycle.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import com.apkfuns.logutils.LogUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.ref.WeakReference


/**
 * create by Albert
 */
class MultiRecyclerViewAdapter<T : IMultiItemEntity> {

    companion object {
        @JvmStatic
        @BindingAdapter(value = ["bindLayoutManager", "bindPageChangeListener"], requireAll = false)
        fun <V : LayoutManagerWrapper> bindLayoutManager(
            recyclerView: RecyclerView,
            layoutManager: MutableLiveData<V>?,
            listener: PagerGridLayoutManager.PageListener?
        ) {
            if (layoutManager == null || layoutManager.value == null) {
                val layoutManagers = LinearLayoutManagerWrapper(recyclerView.context)
                layoutManagers.attachToRecyclerView(recyclerView)
            } else {
                layoutManager.value?.attachToRecyclerView(recyclerView)
                if (recyclerView.layoutManager is PagerGridLayoutManager && listener != null) {
                    (recyclerView.layoutManager as? PagerGridLayoutManager)?.setPageListener(
                        listener
                    )
                }
                if (layoutManager.value is PagerGridLayoutManagerWapper && recyclerView.onFlingListener == null) {
                    val pageSnapHelper = PagerGridSnapHelper()
                    pageSnapHelper.attachToRecyclerView(recyclerView)
                }
            }
        }

        @JvmStatic
        @BindingAdapter(
            value = ["bindItemType", "bindLifecycleOwner", "bindItemListener", "bindData",
                "bindConvert", "bindChildClickListener", "bindAnimaController",
                "bindItemDecoration", "bindRecyclerStackFromEnd"],
            requireAll = false
        )
        fun <T : IMultiItemEntity> appMultiAdapter(
            recyclerView: RecyclerView,
            items: ArrayList<ItemLayout>?,
            lifecycleOwner: LifecycleOwner?,
            listener: BaseQuickAdapter.OnItemClickListener?,
            data: MutableLiveData<ArrayList<T>>?,
            convertListener: ConvertListener<T>?,
            childClickListener: BaseQuickAdapter.OnItemChildClickListener?,
            animationController: LayoutAnimationController?,
            itemDecoration: RecyclerView.ItemDecoration?,
            stackFromEnd: Boolean?,
        ) {
            bind(
                recyclerView,
                items,
                lifecycleOwner,
                listener,
                data,
                convertListener,
                childClickListener,
                animationController,
                itemDecoration,
                stackFromEnd
            )
        }

        fun <T : IMultiItemEntity> bind(
            recyclerView: RecyclerView,
            items: ArrayList<ItemLayout>?,
            lifecycleOwner: LifecycleOwner?,
            listener: BaseQuickAdapter.OnItemClickListener?,
            data: MutableLiveData<ArrayList<T>>?,
            convertListener: ConvertListener<T>?,
            childClickListener: BaseQuickAdapter.OnItemChildClickListener?,
            animationController: LayoutAnimationController?,
            itemDecoration: RecyclerView.ItemDecoration?,
            stackFromEnd: Boolean?
        ) {
            if (recyclerView.adapter == null) {
                if (items == null) return
                if (data == null) return
                val adapter: MultiRecAdapter<T> = MultiRecAdapter(
                    data.value,
                    items,
                    convertListener,
                    WeakReference(lifecycleOwner)
                )
                if (listener != null) {
                    adapter.onItemClickListener = listener
                }
                if (childClickListener != null) {
                    adapter.onItemChildClickListener = childClickListener
                }
                if (recyclerView.layoutManager == null) {
                    LinearLayoutManagerWrapper(recyclerView.context).apply {
                        attachToRecyclerView(recyclerView)
                        if (stackFromEnd != null) {
                            setStackFromEnd(stackFromEnd)
                        }
                    }
                }
                recyclerView.adapter = adapter
                if (animationController != null) {
                    recyclerView.layoutAnimation = animationController
                    recyclerView.scheduleLayoutAnimation()
                }
                if (data.value != null) {
                    adapter.setNewData(data.value)
                }
                lifecycleOwner?.lifecycle?.let {
                    it.removeObserver(adapter)
                    it.addObserver(adapter)
                }
            } else {
                try {
                    val adapter = (recyclerView.adapter as? MultiRecAdapter<T>)

                    if (adapter?.lifecycleOwner != lifecycleOwner) {
                        if (adapter != null) {
                            lifecycleOwner?.lifecycle?.let {
                                it.removeObserver(adapter)
                                it.addObserver(adapter)
                            }
                        }
                        adapter?.lifecycleOwner = WeakReference(lifecycleOwner)
                        adapter?.notifyDataSetChanged()
                    }
                    if (animationController != null) {
                        recyclerView.layoutAnimation = animationController
                        recyclerView.scheduleLayoutAnimation()
                    }
                    if (data?.value != null) {
                        adapter?.setNewData(data.value)
                    }
                    if (childClickListener != null && adapter?.onItemChildClickListener == null) {
                        adapter?.onItemChildClickListener = childClickListener
                    }
                } catch (e: Exception) {
                    LogUtils.e(e)
                }
            }
            if (itemDecoration != null) {
                recyclerView.addItemDecoration(itemDecoration)
            }
        }

    }


    open class MultiRecAdapter<T : IMultiItemEntity>(
        data: ArrayList<T>?,
        layouts: List<ItemLayout>,
        var convertListener: ConvertListener<T>?,
        var lifecycleOwner: WeakReference<LifecycleOwner?>?
    ) : BaseMvvmMultiItemQuickAdapter<T, MVViewHolder>(data), LifecycleObserver {

        init {
            for ((type, layout) in layouts) {
                addItemType(type, layout)
            }
        }

        fun setNewItems(layouts: List<ItemLayout>) {
            getLayouts().clear()
            for ((type, layout) in layouts) {
                addItemType(type, layout)
            }
        }

        override fun convert(helper: MVViewHolder, item: T) {
            realConvert(helper, item)
        }

        fun realConvert(helper: MVViewHolder, item: T) {
            try {
                item.getClickViewIds()?.forEach {
                    helper.addOnClickListener(it)
                }
                item.setPosition(helper.adapterPosition, data.size)
                helper.getBind()?.get()?.lifecycleOwner = lifecycleOwner?.get()
                helper.getBind()?.get()?.setVariable(com.sports.commonn.BR.item, item)
                helper.getBind()?.get()?.setVariable(com.sports.commonn.BR.viewModel, item)
                helper.getBind()?.get()?.executePendingBindings()
                if (convertListener != null) {
                    convertListener?.onConvert(helper, item)
                }
            } catch (e: Exception) {
                LogUtils.e(e)
            }
        }

        override fun getItemViewType(position: Int): Int {
            val t = data[position]
            t?.setPosition(position, data.size)
            return super.getItemViewType(position)
        }


        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            if ((lifecycleOwner as? Activity)?.isFinishing == true) {
                lifecycleOwner = null
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        open fun onDestory() {
            lifecycleOwner = null
        }
    }

    class DiffMultiRecAdapter<T : IMultiItemEntity>(
        data: ArrayList<T>?,
        layouts: List<ItemLayout>,
        convertListener: ConvertListener<T>?,
        lifecycleOwner: WeakReference<LifecycleOwner?>?
    ) : MultiRecAdapter<T>(data, layouts, convertListener, lifecycleOwner), LifecycleObserver {

        init {
            for ((type, layout) in layouts) {
                addItemType(type, layout)
            }
        }

        override fun onBindViewHolder(
            holder: MVViewHolder,
            position: Int,
            payloads: MutableList<Any>
        ) {
            if (payloads.isEmpty()) {
                super.onBindViewHolder(holder, position)
            } else {
                val t = payloads[0] as? T
                if (t != null) {
                    realConvert(holder, t)
                }
            }
        }

        override fun getItemViewType(position: Int): Int {
            val t = data[position]
            t?.setPosition(position, data.size)
            return super.getItemViewType(position)
        }

        override fun setNewData(newData: MutableList<T>?) {
            if (newData.isNullOrEmpty() || data.isNullOrEmpty()) {
                super.setNewData(newData)
            } else {
                GlobalScope.launch {
                    try {
                        val diffResult = DiffUtil.calculateDiff(
                            DiffMutAdpterUtils<T>(
                                data as ArrayList<T>,
                                newData as ArrayList<T>
                            ), true
                        )
                        withContext(Dispatchers.Main) {
                            try {
                                diffResult.dispatchUpdatesTo(MutAdapterListUpdateCallback(this@DiffMultiRecAdapter))
                                mData = newData
                            } catch (e: Exception) {
                                LogUtils.e(e)
                            }
                        }
                    } catch (e: Exception) {
                        LogUtils.e(e)
                    }
                }
            }

        }

        class DiffMutAdpterUtils<T : IMultiItemEntity>(
            var oldDatas: ArrayList<T>,
            var newDatas: ArrayList<T>
        ) :
            DiffUtil.Callback() {

            override fun getOldListSize(): Int {
                return oldDatas.size
            }

            override fun getNewListSize(): Int {
                return newDatas.size
            }

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val newData = newDatas.getOrNull(newItemPosition)
                val oldData = oldDatas.getOrNull(oldItemPosition)
                if (oldData is Diff) {
                    if (newData is IMultiItemEntity) {
                        newData.setPosition(newItemPosition, newDatas.size)
                    }
                    return oldData.areItemsTheSame(newData)
                }
                return oldDatas.getOrNull(oldItemPosition)?.hashCode() == newDatas.getOrNull(
                    newItemPosition
                ).hashCode()
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldItem = oldDatas.getOrNull(oldItemPosition)
                val newItem = newDatas.getOrNull(newItemPosition)
                if (oldItem is Diff) {
                    return oldItem.areContentsTheSame(newItem)
                }
                return oldItem == newItem
            }

            override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
                val oldItem = oldDatas.getOrNull(oldItemPosition)
                val newItem = newDatas.getOrNull(newItemPosition)
                if (oldItem is Diff) {
                    return oldItem.getChangePayload(newItem)
                }
                return newDatas.getOrNull(newItemPosition)
            }

        }


        class MutAdapterListUpdateCallback(var mAdapter: RecyclerView.Adapter<*>?) :
            ListUpdateCallback {

            override fun onInserted(position: Int, count: Int) {
                mAdapter?.notifyItemRangeInserted(position, count)
            }

            override fun onRemoved(position: Int, count: Int) {
                mAdapter?.notifyItemRangeRemoved(position, count)
            }

            override fun onMoved(fromPosition: Int, toPosition: Int) {
                mAdapter?.notifyItemMoved(fromPosition, toPosition)
            }

            override fun onChanged(position: Int, count: Int, payload: Any?) {
                mAdapter?.notifyItemRangeChanged(position, count, payload)
            }
        }
    }
}