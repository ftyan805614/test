package com.sports.commonn.mvvm.livedata

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.lang.ref.WeakReference

open class SafeMutableLiveData<T> : MutableLiveData<T> {


    constructor() : super()

    constructor(value: T) : super(value)


    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        removeObserver(observer)
        super.observe(owner, observer)
    }


}