package com.sports.commonn.mvvm.ui.widget.reclcerview

import androidx.lifecycle.LifecycleOwner
import java.lang.ref.WeakReference


/**
 * Create By jane on 2019-12-06
 */
interface IExpandCollapseViewModel : IMultiItemEntity {

    fun <V : IExpandCollapseViewModel> getChildList(): ArrayList<V>?

    fun isExpanded(): Boolean

    fun setExpand(isExpand: Boolean)

    fun setAdapter(expandableAdapter: ExpandableAdapter)

    fun flatDataBefor(position: Int) {}

    fun setLifecyclerOwner(lifecycleOwner: WeakReference<LifecycleOwner?>?) {}

    fun deAttouchLifecyclerOwner() {}

    fun onResume(lifecycleOwner: WeakReference<LifecycleOwner?>?) {}

    fun onPause(lifecycleOwner: WeakReference<LifecycleOwner?>?) {}


}


