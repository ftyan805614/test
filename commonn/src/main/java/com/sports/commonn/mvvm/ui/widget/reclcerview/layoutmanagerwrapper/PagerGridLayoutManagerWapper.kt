package com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference

/**
 *Create By Albert on 2020/7/1
 */
class PagerGridLayoutManagerWapper : LayoutManagerWrapper {

    var rowCount = 2
    var columCoumt = 3
    var orientation: Int = LinearLayoutManager.VERTICAL
    var reverseLayout: Boolean = false

    constructor(context: Context?, rowCount: Int, columCount: Int) {
        this.context = WeakReference(context)
        this.rowCount = rowCount
        this.columCoumt = columCount
        layoutManager = WeakReference(PagerGridLayoutManager(rowCount, orientation, orientation))
    }

    constructor(context: Context?, rowCount: Int, columCount: Int, orientation: Int, reverseLayout: Boolean) {
        this.context = WeakReference(context)
        this.rowCount = rowCount
        this.columCoumt = columCount
        this.orientation = orientation
        this.reverseLayout = reverseLayout
        layoutManager = WeakReference(PagerGridLayoutManager(rowCount, columCount, orientation))
    }


    override fun getLayoutManager(): RecyclerView.LayoutManager? {
        return layoutManager?.get()
    }

    override fun copyLayoutManager(context: Context?): RecyclerView.LayoutManager {
        this.context = WeakReference(context)
        return PagerGridLayoutManager(rowCount, orientation, orientation)
    }

    override fun setStackFromEnd(statck: Boolean) {

    }
}