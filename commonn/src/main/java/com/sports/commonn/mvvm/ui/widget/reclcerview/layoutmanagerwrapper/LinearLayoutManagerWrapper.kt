package com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference

/**
 *Create By Albert on 2020/4/24
 */
class LinearLayoutManagerWrapper : LayoutManagerWrapper {

    var orientation: Int = LinearLayoutManager.VERTICAL
    var reverseLayout: Boolean = false
    var mStackFromEnd = false

    constructor(context: Context?) {
        this.context = WeakReference(context)
        layoutManager = WeakReference(SafeLinearLayoutManager(context))
    }

    constructor(context: Context?, orientation: Int, reverseLayout: Boolean) {
        this.context = WeakReference(context)
        this.orientation = orientation
        this.reverseLayout = reverseLayout
        layoutManager = WeakReference(SafeLinearLayoutManager(context, orientation, reverseLayout))
    }


    override fun getLayoutManager(): RecyclerView.LayoutManager? {
        return layoutManager?.get()
    }

    override fun copyLayoutManager(context: Context?): RecyclerView.LayoutManager {
        this.context = WeakReference(context)
        return SafeLinearLayoutManager(context, orientation, reverseLayout).apply {
            stackFromEnd = mStackFromEnd
        }
    }

    override fun setStackFromEnd(statck: Boolean) {
        val manager = layoutManager?.get()
        mStackFromEnd = statck
        if (manager is LinearLayoutManager) {
            manager.stackFromEnd = statck
        }
    }


}