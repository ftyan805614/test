package com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.apkfuns.logutils.LogUtils
import java.lang.ref.WeakReference

/**
 *Create By Albert on 2020/4/24
 */
abstract class LayoutManagerWrapper {

    var layoutManager: WeakReference<RecyclerView.LayoutManager>? = null
    var context: WeakReference<Context?>? = null
    var recyclerView: WeakReference<RecyclerView>? = null

    abstract fun getLayoutManager(): RecyclerView.LayoutManager?

    fun attachToRecyclerView(recyclerView: RecyclerView) {
        try {
            this.recyclerView = WeakReference(recyclerView)
            if (layoutManager?.get() == null) {
                layoutManager = WeakReference(copyLayoutManager(recyclerView.context))
                recyclerView.layoutManager = layoutManager?.get()
                return
            }
            val field = RecyclerView.LayoutManager::class.java.getDeclaredField("mRecyclerView")
            field.isAccessible = true
            val recyrlerView = field[layoutManager?.get()]
            if (recyrlerView != null) {
                layoutManager = WeakReference(copyLayoutManager(recyclerView.context))
                recyclerView.layoutManager = layoutManager?.get()
            } else {
                recyclerView.layoutManager = layoutManager?.get()
            }
        } catch (e: Exception) {
            LogUtils.e(e)
            layoutManager = WeakReference(copyLayoutManager(recyclerView.context))
            recyclerView.layoutManager = layoutManager?.get()
        }
    }

    abstract fun copyLayoutManager(context: Context?): RecyclerView.LayoutManager


    abstract fun setStackFromEnd(statck: Boolean)
}