package com.sports.commonn.mvvm.livedata

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.lang.ref.WeakReference

class EqMutableLiveData<T> : SafeMutableLiveData<T> {


    constructor() : super()

    constructor(value: T) : super(value)


    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        removeObserver(observer)
        super.observe(owner, observer)
    }

    override fun equals(other: Any?): Boolean {
        if (other is LiveData<*>) {
            return value == (other as LiveData<Any?>)?.value
        }
        return value == other
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}