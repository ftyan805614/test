package com.sports.commonn.mvvm.ui

import android.app.Activity
import android.content.Context
import android.view.*
import android.widget.PopupWindow
import java.util.*

open class PopupController {
    //布局id
    private var layoutResId = 0
    private var context: Context?
    var popupWindow: PopupWindow?

    //弹窗布局View
    var mPopupView: View? = null
    private var mView: View? = null
    private var locationView: View? = null
    private var mWindow: Window? = null
    private var dismissListeners = ArrayList<PopupWindow.OnDismissListener>()

    constructor(context: Context?) {
        this.context = context
        popupWindow = PopupWindow()
        popupWindow?.setOnDismissListener {
            setBackGroundLevel(1f)
            dismissListeners.forEach {
                it.onDismiss()
            }
            dismissListeners.clear()
        }
    }

    constructor(context: Context, popupWindow: PopupWindow?) {
        this.context = context
        this.popupWindow = popupWindow
        popupWindow?.setOnDismissListener {
            setBackGroundLevel(1f)
            dismissListeners.forEach {
                it.onDismiss()
            }
            dismissListeners.clear()
        }
    }

    fun setView(layoutResId: Int) {
        mView = null
        this.layoutResId = layoutResId
        installContent()
    }

    fun setView(view: View?) {
        mView = view
        layoutResId = 0
        installContent()
    }

    private fun installContent() {
        if (layoutResId != 0) {
            mPopupView = LayoutInflater.from(context).inflate(layoutResId, null)
        } else if (mView != null) {
            mPopupView = mView
        }
        popupWindow?.contentView = mPopupView
        setWidthAndHeight(0, 0)
    }

    /**
     * 设置宽度
     *
     * @param width  宽
     * @param height 高
     */
    fun setWidthAndHeight(width: Int, height: Int) {
        if (width == 0 || height == 0) {
            //如果没设置宽高，默认是WRAP_CONTENT
            popupWindow?.width = ViewGroup.LayoutParams.WRAP_CONTENT
            popupWindow?.height = ViewGroup.LayoutParams.WRAP_CONTENT
        } else {
            popupWindow?.width = width
            popupWindow?.height = height
        }
    }

    fun showAsDropDown(anchor: View?): PopupController {
        popupWindow?.showAsDropDown(anchor)
        return this
    }

    fun showAsDropDown(anchor: View?, offestX: Int, offestY: Int): PopupController {
        popupWindow?.showAsDropDown(anchor, offestX, offestY)
        return this
    }

    fun showAtLocation(anchor: View?, Gravity: Int, offsetX: Int, offsetY: Int): PopupController {
        popupWindow?.showAtLocation(anchor, Gravity, offsetX, offsetY)
        return this
    }

    open fun dimiss() {
        if (isShowing) {
            popupWindow?.dismiss()
        }
    }

    val isShowing: Boolean
        get() = popupWindow?.isShowing == true

    /**
     * 设置背景灰色程度
     *
     * @param level 0.0f-1.0f
     */
    fun setBackGroundLevel(level: Float) {
        if (context is Activity) {
            mWindow = (context as Activity).window
            val params = mWindow?.attributes
            params?.alpha = level
            mWindow?.attributes = params
        }
    }

    /**
     * 设置动画
     */
    fun setAnimationStyle(animationStyle: Int) {
        popupWindow?.animationStyle = animationStyle
    }

    /**
     * 设置Outside是否可点击
     *
     * @param touchable 是否可点击
     */
    fun setOutsideTouchable(touchable: Boolean) {
        popupWindow?.isOutsideTouchable = touchable //设置outside可点击
        popupWindow?.isFocusable = touchable
    }

    open fun show() {
        if (locationView!=null){
            popupWindow?.showAtLocation(locationView, Gravity.CENTER, 0, 0)
        }
    }

    class PopupParams(var mContext: Context?) {

        //布局id
        var layoutResId = 0
        var mWidth = 0

        //弹窗的宽和高
        var mHeight = 0
        var isShowBg = false
        var isShowAnim = false

        //屏幕背景灰色程度
        var bgLevel = 0f

        //动画Id
        var animationStyle = 0

        var mView: View? = null

        var locationView: View? = null

        var dismissListener: PopupWindow.OnDismissListener? = null

        var isTouchable = true

        fun apply(controller: PopupController) {
            when {
                mView != null -> {
                    controller.setView(mView)
                }
                layoutResId != 0 -> {
                    controller.setView(layoutResId)
                }
                else -> {
                    throw IllegalArgumentException("PopupView's contentView is null")
                }
            }
            controller.setWidthAndHeight(mWidth, mHeight)
            controller.setOutsideTouchable(isTouchable) //设置outside可点击
            if (isShowBg) {
                //设置背景
                controller.setBackGroundLevel(bgLevel)
            }
            if (isShowAnim) {
                controller.setAnimationStyle(animationStyle)
            }
            dismissListener?.let { controller.addDismissListener(it) }
        }
    }

    fun addDismissListener(dismissListener: PopupWindow.OnDismissListener) {
        this.dismissListeners.add(dismissListener)
    }
}