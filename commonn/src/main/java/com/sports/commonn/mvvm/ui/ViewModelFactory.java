package com.sports.commonn.mvvm.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.apkfuns.logutils.LogUtils;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * created by Albert
 */
public class ViewModelFactory implements ViewModelProvider.Factory {

    private LifecycleOwner lifecycleOwner;

    public ViewModelFactory(LifecycleOwner lifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (BaseViewModel.class.isAssignableFrom(modelClass)) {
            try {
                Constructor<T> constructor = modelClass.getConstructor(WeakReference.class);
                WeakReference<LifecycleOwner> weakReference = new WeakReference(lifecycleOwner);
                BaseViewModel viewModel = (BaseViewModel) constructor.newInstance(weakReference);
                viewModel.attouchLifecycleOwner(lifecycleOwner);
                return (T) viewModel;
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                if (e instanceof InvocationTargetException) {
                    LogUtils.e(((InvocationTargetException) e).getTargetException());
                } else {
                    LogUtils.e(e);
                }

            }
        }
        try {
            return modelClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LogUtils.e(e);
        }
        return null;
    }
}
