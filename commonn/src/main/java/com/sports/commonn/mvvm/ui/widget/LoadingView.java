package com.sports.commonn.mvvm.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.sports.commonn.R;
import com.sports.commonn.utils.ResUtils;


/**
 * 加载效果
 * Created by jane on 2018/11/30.
 */

public class LoadingView {
    private LottieAnimationView ivLoading;
    private View layoutBg = null;
    private TextView tvMsg = null;

    public View createView(Context context) {
        return this.createView(context, true);
    }

    public View createView(Context context, boolean hasBg) {
        View view = LayoutInflater.from(context).inflate(hasBg ? R.layout.loading_dialog : R.layout.loading_dialog_no_bg, (ViewGroup) null);
        ivLoading = view.findViewById(R.id.ivLoading);
        ivLoading.setAnimation("animation_sport.json");
        layoutBg = view.findViewById(R.id.dialog_bg);
        tvMsg = view.findViewById(R.id.tvMsg);
        return view;
    }

    public void cancelBg() {
        if (layoutBg != null) layoutBg.setBackground(null);
    }

    public void setLayoutBgColor(int color) {
        if (layoutBg != null && color > 0) layoutBg.setBackgroundColor(color);
    }

    public void setLayoutBgDrawable(Drawable drawable) {
        if (layoutBg != null && null != drawable) layoutBg.setBackgroundDrawable(drawable);
    }

    public void stop() {
        if (null != layoutBg) {
            layoutBg.setVisibility(View.GONE);
        }
        if (null != ivLoading) {
            ivLoading.cancelAnimation();
            ivLoading.clearAnimation();
        }
    }

    public void start() {
        if (null != layoutBg) {
            layoutBg.setVisibility(View.VISIBLE);
        }
        if (null != ivLoading && !ivLoading.isAnimating()) {
            ivLoading.playAnimation();
        }
    }

    public void setMessage(String message) {
        if (null != tvMsg) {
            tvMsg.setText(message);
        }
    }

    public void showBlackTheme() {
        if (null != layoutBg) {
            layoutBg.setBackgroundResource(R.drawable.skin_dialog_bg_framework_night);
        }
        if (null != tvMsg) {
            tvMsg.setTextColor(ResUtils.getColor(R.color.skin_loading_frame_work));
        }
    }


}

