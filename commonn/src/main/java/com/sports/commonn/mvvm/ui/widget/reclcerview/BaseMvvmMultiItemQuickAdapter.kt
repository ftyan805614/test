package com.sports.commonn.mvvm.ui.widget.reclcerview

import android.content.MutableContextWrapper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import java.lang.NullPointerException
import java.lang.ref.WeakReference
import java.util.concurrent.ConcurrentHashMap


/**
 *created by Albert
 */
abstract class BaseMvvmMultiItemQuickAdapter<T : IMultiItemEntity, K : BaseViewHolder>(data: List<T>?) :
    BaseMultiItemQuickAdapter<T, K>(data) {

    private var mLayouts = ConcurrentHashMap<Int, Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): K {
        getLayoutId(viewType) ?: throw NullPointerException("Not find mvvm view type = $viewType mLayouts =$mLayouts")
        return if (viewType != BaseQuickAdapter.LOADING_VIEW && viewType != BaseQuickAdapter.HEADER_VIEW && viewType != BaseQuickAdapter.EMPTY_VIEW && viewType != BaseQuickAdapter.FOOTER_VIEW) {
            val viewTypes = getLayoutId(viewType) ?: throw NullPointerException("Not find mvvm view type = $viewType")
            val d = DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(MutableContextWrapper(parent.context)),
                viewTypes,
                parent,
                false
            ) ?: throw NullPointerException("Not add <layout>...</layout> arrebute at root view type = $viewType  mLayouts =$mLayouts")
            d.executePendingBindings()
            val mvViewHolder = MVViewHolder(d.root, WeakReference(d))
            bindViewClickListener(mvViewHolder)
            mvViewHolder.setQAdapter(this)
            mvViewHolder as K
        } else {
            super.onCreateViewHolder(parent, viewType)
        }
    }

    private fun getLayoutId(viewType: Int): Int? {
        return mLayouts[viewType]
    }


    override fun addItemType(type: Int, layoutResId: Int) {
        mLayouts[type] = layoutResId
        // super.addItemType(type, layoutResId)
    }


    fun getLayouts(): ConcurrentHashMap<Int, Int> {
        return mLayouts
    }


    private fun bindViewClickListener(baseViewHolder: BaseViewHolder?) {
        if (baseViewHolder == null) {
            return
        }
        val view = baseViewHolder.itemView

        onItemClickListener?.apply {
            view.setOnClickListener { v ->
                onItemClickListener.onItemClick(
                    this@BaseMvvmMultiItemQuickAdapter,
                    v,
                    baseViewHolder.layoutPosition - headerLayoutCount
                )
            }
        }
        onItemLongClickListener?.apply {
            view.setOnLongClickListener { v ->
                onItemLongClickListener.onItemLongClick(
                    this@BaseMvvmMultiItemQuickAdapter,
                    v,
                    baseViewHolder.layoutPosition - headerLayoutCount
                )
            }
        }
    }

}