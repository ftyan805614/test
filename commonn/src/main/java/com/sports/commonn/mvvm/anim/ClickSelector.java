package com.sports.commonn.mvvm.anim;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.ImageView;

import com.sports.commonn.utils.ResUtils;


/**
 * Desc:动态设置 点击事件 selector 的工具类
 * Created by Jeff on 2018/9/27
 **/
public class ClickSelector {

    /**
     * 从 drawable 获取图片 id 给 Imageview 添加 selector
     *
     * @param idNormal 默认图片的 id
     * @param idPress  点击图片的 id
     * @param iv   点击的 view
     */
    public static void addSelectorFromDrawable(ImageView iv,int idNormal, int idPress){
        Drawable normal = ResUtils.getDrawable(idNormal);
        Drawable press = ResUtils.getDrawable(idPress);
        addSelectorFromDrawable(iv,normal,press);
    }

    /**
     * 从 drawable 获取图片 id 给 Imageview 添加 selector
     *
     * @param idNormal 默认图片的 id
     * @param idPress  点击图片的 id
     * @param iv   点击的 view
     */
    public static void addSelectorFromDrawable(View iv,int idNormal, int idPress){
        Drawable normal = ResUtils.getDrawable(idNormal);
        Drawable press = ResUtils.getDrawable(idPress);
        addSelectorFromDrawable(iv,normal,press);
    }

    /**
     * 从 drawable 获取图片 id 给 Imageview 添加 selector
     *
     * @param normal 默认图片的 id
     * @param press  点击图片的 id
     * @param iv   点击的 view
     */
    public static void addSelectorFromDrawable(ImageView iv,Drawable normal, Drawable press){

        StateListDrawable drawable = new StateListDrawable();
        drawable.addState(new int[]{android.R.attr.state_pressed},press);
        drawable.addState(new int[]{-android.R.attr.state_pressed},normal);
        if (iv.getBackground() != null){
            iv.setBackground(drawable);
        }else {
            iv.setImageDrawable(drawable);
        }
    }

    /**
     * 从 drawable 获取图片 id 给 Imageview 添加 selector
     *
     * @param normal 默认图片的 id
     * @param press  点击图片的 id
     * @param iv   点击的 view
     */
    public static void addSelectorFromDrawable(View iv,Drawable normal, Drawable press){
        StateListDrawable drawable = new StateListDrawable();
        drawable.addState(new int[]{android.R.attr.state_pressed},press);
        drawable.addState(new int[]{-android.R.attr.state_pressed},normal);
        iv.setBackground(drawable);
    }

}
