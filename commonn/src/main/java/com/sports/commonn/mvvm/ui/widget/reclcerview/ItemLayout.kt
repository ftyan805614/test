package com.sports.commonn.mvvm.ui.widget.reclcerview

import androidx.annotation.LayoutRes


/**
 *created by Albert
 */
data class ItemLayout(var type: Int=0, @LayoutRes var layout: Int)