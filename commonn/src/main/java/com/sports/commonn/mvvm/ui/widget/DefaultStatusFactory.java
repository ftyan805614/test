package com.sports.commonn.mvvm.ui.widget;

import com.sports.commonn.utils.StatusViewHelper;

/**
 * Desc:
 * Created by Jeff on 2018/9/14
 **/
public class DefaultStatusFactory implements StatusViewHelper.Factory {
    @Override
    public StatusAdapter adapter() {
        return new DefaultStatusAdapter();
    }
}
