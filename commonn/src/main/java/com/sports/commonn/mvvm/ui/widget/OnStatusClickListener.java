package com.sports.commonn.mvvm.ui.widget;

import android.view.View;

/**
 * Desc:
 * Created by Jeff on 2018/10/22
 **/
public interface OnStatusClickListener{
    void onClick(View v);
}
