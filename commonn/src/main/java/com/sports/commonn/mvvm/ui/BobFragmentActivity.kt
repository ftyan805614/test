package com.sports.commonn.mvvm.ui

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Process
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import androidx.annotation.IdRes
import androidx.fragment.app.*
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleRegistry
import androidx.navigation.fragment.NavHostFragment
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.anim.ClickSelector
import com.sports.commonn.mvvm.anim.DownAlphaAnim
import com.sports.commonn.utils.EventBusClickUtil.click
import com.sports.commonn.utils.ResUtils
import com.sports.commonn.utils.StatusViewHelper
import com.sports.commonn.utils.ToastUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope

/**
 * @author jane
 * 创建日期：2019-10-02
 * 版本：v1.0
 * 描述：
 */
abstract class BobFragmentActivity : FragmentActivity(),
    View.OnClickListener, CoroutineScope by MainScope() {
    @JvmField
    protected var mStatusViewHelper: StatusViewHelper? = null
    private var isInitView = false
    private var mExitTime: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (needCreate()) {
            if (contentViewLayoutID() != 0) {
                setContentView(contentViewLayoutID())
            }
            initViewsAndEvents(savedInstanceState)
            isInitView = true
        }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val isIntercept = checkIntercept(supportFragmentManager.fragments, false)
            if (isIntercept) {
                return true
            }
            return super.onKeyDown(keyCode, event)
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun checkIntercept(fragments: List<Fragment>, action: Boolean): Boolean {
        var ac = action
        if (fragments.isNotEmpty()) {
            fragments.forEach {
                when (it) {
                    is NavHostFragment -> {
                        ac = checkIntercept(it.childFragmentManager.fragments, ac)
                    }
                    is BaseNavigationFragment<*> -> {
                        ac = it.onBackPressedSupport()
                    }
                    is BaseMvvmFragment<*> -> {
                        ac = it.onBackPressedSupport()
                    }

                }
            }
        }
        return ac
    }

    protected open fun needCreate(): Boolean {
        return true
    }


    /**
     * 设置placeholder View
     *
     * @param view 对应的View 或者 子View 如果子View是一个View则会自动寻找V
     */
    fun setStatusView(view: View?): StatusViewHelper {
        mStatusViewHelper = StatusViewHelper(view!!)
        return mStatusViewHelper!!
    }


    public override fun onStart() {
        super.onStart()
        (lifecycle as? LifecycleRegistry)?.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }


    override fun onClick(v: View) {
        //点击动作发送
        click(v.id)
    }



    /**
     * 加载根Fragment, 即Activity内的第一个Fragment 或 Fragment内的第一个子Fragment
     *
     * @param containerId 容器id
     * @param toFragment  目标Fragment
     */
    fun loadRootFragment(containerId: Int, toFragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(containerId, toFragment)
            .commitAllowingStateLoss()
    }

    private var showPosition = 0

    /**
     * 加载多个同级根Fragment,类似Wechat, QQ主页的场景
     */
    fun loadMultipleRootFragment(
        containerId: Int,
        showPosition: Int,
        vararg fragments: Fragment
    ) {
        loadMultipleRootFragment(containerId, showPosition, fragments.toList())
    }

    fun loadMultipleRootFragment(
        containerId: Int,
        showPosition: Int,
        fragments: List<Fragment>
    ) {
        this.showPosition = showPosition
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()

        for (i in fragments.indices) {
            val to = fragments[i]
            val toName = to.javaClass.name
            if (!to.isAdded) {
                ft.add(containerId, to, toName)
            }
            if (i != showPosition) {
                ft.hide(to)
            }
        }
        ft.commitAllowingStateLoss()
    }

    open fun showHideFragment(
        showFragment: Fragment,
        hideFragment: Fragment? = null,
        fm: FragmentManager? = supportFragmentManager
    ) {
        if (showFragment !== hideFragment) {
            val ft = (fm ?: supportFragmentManager).beginTransaction().show(showFragment)
            if (hideFragment == null) {
                val fragmentList =
                    FragmentationMagician.getActiveFragments(fm)
                if (fragmentList != null) {
                    val var6: Iterator<*> = fragmentList.iterator()
                    while (var6.hasNext()) {
                        val fragment = var6.next() as Fragment?
                        if (fragment != null && fragment !== showFragment) {
                            ft.hide(fragment)
                        }
                    }
                }
            } else {
                ft.hide((hideFragment as? Fragment?)!!)
            }
            ft.commitAllowingStateLoss()
        }
    }

    open fun isOnAppBackground(isBackground: Boolean) {}

    /***
     * 区别在于此方法用于使用LayoutInflater加载的layout的控件对象获取
     */
    protected fun <T : View?> findViewById(rootView: View, @IdRes id: Int): T {
        return rootView.findViewById<View>(id) as T
    }

    protected fun setOnClickListener(@IdRes id: Int) {
        setOnClickListener(findViewById<View>(id))
    }

    protected fun setOnClickListener(view: View) {
        view.setOnClickListener(this)
        DownAlphaAnim(view).setOnTouchEvent(null)
    }

    protected fun setOnClickListener(@IdRes id: Int, idNormal: Int, idPress: Int) {
        setOnClickListener(
            findViewById(id),
            ResUtils.getDrawable(idNormal),
            ResUtils.getDrawable(idPress)
        )
    }

    protected fun setOnClickListener(view: View, normal: Drawable?, press: Drawable?) {
        view.setOnClickListener(this)
        if (view is ImageView) {
            ClickSelector.addSelectorFromDrawable(view, normal, press)
        } else {
            ClickSelector.addSelectorFromDrawable(view, normal, press)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.fragments.forEach {
            it.onActivityResult(requestCode, resultCode, data)
            it.childFragmentManager.fragments.forEach { child ->
                child.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    /**
     * desc: 退出app
     *
     * @param doubleClickTime 传0值则不会有双点击
     * @author Jeff created on 2018/10/22 19:09
     */
    open fun exitApp(doubleClickTime: Long) {
        try {
            if (doubleClickTime <= 0) {
                onExitApp()
                Process.killProcess(Process.myPid())
                System.gc()
                System.exit(0)
            } else if (System.currentTimeMillis() - mExitTime > doubleClickTime) {
                ToastUtil.show("再按一次退出程序")
                mExitTime = System.currentTimeMillis()
            } else {
                onExitApp()
                Process.killProcess(Process.myPid())
                System.gc()
                System.exit(0)
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }

    protected open fun onExitApp() {}
    protected abstract fun contentViewLayoutID(): Int
    protected abstract fun initViewsAndEvents(paramBundle: Bundle?)


    open fun exitAppCallBack() {}

    open fun finishNoAnim() {
        try {
            super.finish()
            overridePendingTransition(0, 0)
        } catch (e: Exception) {
        }

    }

}