package com.sports.commonn.mvvm.ui.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sports.commonn.R;


/**
 * Desc:
 * Created by Jeff on 2018/9/14
 **/
public class DefaultStatusAdapter extends StatusAdapter {
    private ProgressBar mProgressBar;
    private TextView mTvLoadding,mTvDesc,mTvRetry;
    private ImageView mIvIcon;
    @Override
    public View getView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_status,null);
        mProgressBar = view.findViewById(R.id.pb_status_loadding);
        mTvLoadding = view.findViewById(R.id.tv_status_loadding);
        mTvDesc = view.findViewById(R.id.tv_status_desc);
        mTvRetry = view.findViewById(R.id.tv_status_retry);
        mIvIcon = view.findViewById(R.id.iv_status_icon);
        setOnClickListener(mTvRetry);
        return view;
    }

    @Override
    public void showError(int type, String errorMsg) {
        showErrorUi();
        switch (type) {
            case ErrorType.EMPTY:
                mIvIcon.setImageResource(R.drawable.empty);
                break;
            case ErrorType.TIMEOUT:
                mIvIcon.setImageResource(R.drawable.timeout);
                break;
            case ErrorType.ERROR:
                mIvIcon.setImageResource(R.drawable.error);
                break;
        }
        mTvDesc.setText(errorMsg);
    }

    private void showErrorUi(){
        mProgressBar.setVisibility(View.GONE);
        mTvLoadding.setVisibility(View.GONE);
        mIvIcon.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.VISIBLE);
        mTvDesc.setVisibility(View.VISIBLE);
    }

    private void showLoaddingUi(){
        mProgressBar.setVisibility(View.VISIBLE);
        mTvLoadding.setVisibility(View.VISIBLE);
        mIvIcon.setVisibility(View.GONE);
        mTvRetry.setVisibility(View.GONE);
        mTvDesc.setVisibility(View.GONE);
    }

    @Override
    public void showLoadding(String loaddingMsg) {
        showLoaddingUi();
        mTvLoadding.setText(loaddingMsg);
    }
}
