package com.sports.commonn.mvvm.livedata

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

/**
 *create by Albert
 */
class DataLiveData<T> : MutableLiveData<T>() {

    private val notifyConfig = HashMap<Int, ObserverConfig>()

    /**
     * requestDiff 是否为相同的数据不再观察者 仅对基础数据类型有效
     * intervalTime 通知时间间隔内不再通知观察者
     */
    fun observe(owner: LifecycleOwner, requestDiff: Boolean = false, requestFirstValue: Boolean = true, intervalTime: Int = -1, observer: Observer<T>) {
        val observer = observer
        notifyConfig[observer.hashCode()] = ObserverConfig().apply {
            isDiff = requestDiff
            time = intervalTime
        }
        val customObserver = CustomObserver(observer, notifyConfig, observer.hashCode(), requestFirstValue)
        super.observe(owner, customObserver)
    }


    class CustomObserver<T>(var observer: Observer<T>, hashCodeMap: HashMap<Int, ObserverConfig>, weakHashCode: Int, var requestFirstValue: Boolean) : Observer<T> {
        var lastValue: T? = null
        var lastSendTime = 0L
        var config: ObserverConfig? = null
        var isInit = true

        init {
            config = hashCodeMap[weakHashCode]
        }

        override fun onChanged(t: T?) {
            if (isInit) {
                isInit = false
                if (!requestFirstValue) return
            }
            val currentTime = System.currentTimeMillis()
            if (config?.isDiff == true) {
                if (lastValue != t) {
                    if (config?.time ?: 0 > 0) {
                        if (currentTime - lastSendTime > (config?.time ?: 0) * 1000) {
                            observer.onChanged(t)
                            lastSendTime = currentTime
                        }

                    } else {
                        observer.onChanged(t)
                        lastSendTime = currentTime
                    }
                }
            } else {
                if (config?.time ?: 0 > 0) {
                    if (currentTime - lastSendTime > (config?.time ?: 0) * 1000) {
                        observer.onChanged(t)
                        lastSendTime = currentTime
                    }
                } else {
                    observer.onChanged(t)
                    lastSendTime = currentTime
                }
            }
            lastValue = t
        }
    }

    class ObserverConfig {
        var time = -1
        var isDiff = false
    }
}