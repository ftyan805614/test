package com.sports.commonn.mvvm.ui

import android.app.ActivityManager
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Bundle
import android.os.Process
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.mvvm.activityshare.ViewModelShareProvider
import com.sports.commonn.utils.EventBusClickUtil
import com.sports.commonn.utils.ToastUtil
import java.lang.ref.WeakReference
import kotlin.system.exitProcess


/**
 * Desc:
 * Created by Jeff on 2018/10/26
 */
abstract class BaseMvvmActivity<T : ViewDataBinding> : CommonActivity(), View.OnClickListener {

    private var isCleared = false
    private var mExitTime: Long = 0
    lateinit var bind: T


    override fun initViewsAndEvents(paramBundle: Bundle?) {
    }

    override fun needCreate(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
        } catch (e: java.lang.Exception) {
            LogUtils.e(e)
        }
        val binding = DataBindingUtil.setContentView<T>(this, getContentViewLayoutId())
        bind = binding
        bind.lifecycleOwner = this
        initViewModel(bind)
        initView()
        initObserve()
        initData()
    }

    override fun contentViewLayoutID(): Int = 0

    protected abstract fun getContentViewLayoutId(): Int

    protected abstract fun initViewModel(bind: T)

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            resDestroy()
            isCleared = true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isCleared) {
            resDestroy()
        }
    }


    /**
     * 描述：Android系统 6.0以下AudioManager内存泄漏问题  播放视频内存泄漏
     * 日期：2019-07-07
     * 参数：
     * 返回值：
     */
    override fun attachBaseContext(newBase: Context) {
        try {
            super.attachBaseContext(object : ContextWrapper(newBase) {
                override fun getSystemService(name: String): Any? {
                    // 解决 VideoView 中 AudioManager 造成的内存泄漏
                    return if (Context.AUDIO_SERVICE == name) {
                        applicationContext.getSystemService(name)
                    } else super.getSystemService(name)
                }
            })
        } catch (e: Exception) {
            super.attachBaseContext(newBase)
        }
    }


    open fun initView() {

    }

    open fun initObserve() {
    }

    open fun initData() {

    }


    fun <V : ViewModel?> ofScopeActivity(t: Class<V>): V {
        val viewModel = ViewModelProviders.of(this as FragmentActivity, ViewModelFactory(this)).get(t)
        if (viewModel is BaseViewModel) {
            lifecycle.addObserver(viewModel)
            viewModel.lifecycleOwner = WeakReference(this)
        }
        return viewModel
    }

    fun <V : BaseShareViewModel?> ofScopeShareActivity(t: Class<V>): V {
        return ViewModelShareProvider.ofGet(this as FragmentActivity, t)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.fragments.forEach {
            it.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onClick(v: View) {
        //点击动作发送
        EventBusClickUtil.click(v?.id ?: 0)
    }

    override fun exitApp(doubleClickTime: Long) {
        try {
            when {
                doubleClickTime <= 0L -> {
                    kill()
                }
                System.currentTimeMillis() - this.mExitTime > doubleClickTime -> {
                    ToastUtil.show("再按一次退出程序")
                    this.mExitTime = System.currentTimeMillis()
                }
                else -> {
                    kill()
                }
            }
        } catch (var4: java.lang.Exception) {
            LogUtils.e(var4)
        }
    }

    private fun kill() {
        this.onExitApp()
        val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        am.runningAppProcesses?.forEach {
            if (it.pid != Process.myPid()) {
                Process.killProcess(it.pid)
            }
        }
        Process.killProcess(Process.myPid())
        System.gc()
        exitProcess(0)
    }

    override fun onExitApp() {}
}
