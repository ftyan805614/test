package com.sports.commonn.mvvm.activityshare

import android.annotation.SuppressLint
import android.app.Activity
import androidx.annotation.MainThread
import androidx.lifecycle.*
import com.sports.commonn.mvvm.ui.BaseShareViewModel

/**
 *Create By Albert on 2019-12-10
 */
object ViewModelShareProvider {


    @SuppressLint("UseSparseArrays")
    private val lifecyclerMonitors = LinkedHashMap<Int, LifecycleMonitor>()

    private val viewModelStore = LinkedHashMap<Int, ArrayList<BaseShareViewModel>>()

    @MainThread
    fun <V : BaseShareViewModel?> ofGet(
        container: LifecycleOwner,
        viewModelClass: Class<V>,
        lifecycleOwner: LifecycleOwner = container
    ): V {
        val haseViewModel = viewModelStore.flatMap { it.value }.firstOrNull { it::class.java.name == viewModelClass.name }
        val viewModel: V = if (haseViewModel != null) {
            haseViewModel as V
        } else {
            val newInstance = viewModelClass.newInstance()
            var list = viewModelStore[container.hashCode()]
            if (list == null) {
                list = ArrayList()
                viewModelStore[container.hashCode()] = list
            }
            list.add(newInstance as BaseShareViewModel)
            newInstance
        }
        viewModel?.attouchLifecycleOwner(lifecycleOwner)
        val monitor = lifecyclerMonitors[lifecycleOwner.hashCode()]
        if (monitor == null) {
            val monitor = LifecycleMonitor(viewModelStore, lifecyclerMonitors)
            lifecyclerMonitors[lifecycleOwner.hashCode()] = monitor
            lifecycleOwner.lifecycle.addObserver(monitor)
        }
        checkClear(viewModel as BaseShareViewModel)
        return viewModel
    }

    fun getLifeMonitor(lifecycleOwner: LifecycleOwner): LifecycleMonitor? {
        return lifecyclerMonitors[lifecycleOwner.hashCode()]
    }

    private fun checkClear(viewModel: BaseShareViewModel) {
        viewModel.onattouchedLifecycleOwner.forEach {
            if (it.get()?.lifecycle?.currentState == Lifecycle.State.DESTROYED) {
                val lifecycleOwner = it.get()
                if (lifecycleOwner != null) {
                    lifecyclerMonitors[lifecycleOwner.hashCode()]?.onDestroy(lifecycleOwner)
                }
            }
        }
    }

    class LifecycleMonitor(
        var viewModels: LinkedHashMap<Int, ArrayList<BaseShareViewModel>>,
        var monitor: LinkedHashMap<Int, LifecycleMonitor>?
    ) : LifecycleObserver {

        var isDisposed = false


        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun onCreate(lifecycleOwner: LifecycleOwner) {
            viewModels.forEach {
                it.value.forEach { viewModel ->
                    if (viewModel.onattouchedLifecycleOwner.firstOrNull { lf -> lf.get() == lifecycleOwner } != null) {
                        viewModel.onCreate(lifecycleOwner)
                    }
                }
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onStart(lifecycleOwner: LifecycleOwner) {
            viewModels.forEach {
                it.value.forEach { viewModel ->
                    if (viewModel.onattouchedLifecycleOwner.firstOrNull { lf -> lf.get() == lifecycleOwner } != null) {
                        viewModel.onStart(lifecycleOwner)
                    }
                }
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume(lifecycleOwner: LifecycleOwner) {
            viewModels.forEach {
                it.value.forEach { viewModel ->
                    if (viewModel.onattouchedLifecycleOwner.firstOrNull { lf -> lf.get() == lifecycleOwner } != null) {
                        viewModel.attouchLifecycleOwner(lifecycleOwner)
                        viewModel.onResume(lifecycleOwner)
                    }
                }
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause(lifecycleOwner: LifecycleOwner) {
            viewModels.forEach {
                it.value.forEach { viewModel ->
                    if (viewModel.onattouchedLifecycleOwner.firstOrNull { lf -> lf.get() == lifecycleOwner } != null) {
                        viewModel.onPause(lifecycleOwner)
                    }
                }
            }
            if ((lifecycleOwner as? Activity)?.isFinishing == true) {
                dispose(lifecycleOwner)
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop(lifecycleOwner: LifecycleOwner) {
            viewModels.forEach {
                it.value.forEach { viewModel ->
                    if (viewModel.onattouchedLifecycleOwner.firstOrNull { lf -> lf.get() == lifecycleOwner } != null) {
                        viewModel.onStop(lifecycleOwner)
                    }
                }
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy(lifecycleOwner: LifecycleOwner) {
            viewModels.forEach {
                it.value.forEach { viewModel ->
                    if (viewModel.onattouchedLifecycleOwner.firstOrNull { lf -> lf.get() == lifecycleOwner } != null) {
                        viewModel.onDestroy(lifecycleOwner)
                    }
                }
            }
            if (!isDisposed) {
                dispose(lifecycleOwner)
            }

        }


        private fun dispose(lifecycleOwner: LifecycleOwner) {
            isDisposed = true
            monitor?.remove(lifecycleOwner.hashCode())
            checkClear(lifecycleOwner)
            lifecycleOwner.lifecycle.removeObserver(this)
        }


        private fun checkClear(lifecycleOwner: LifecycleOwner) {
            val viewModelsIterator = viewModels.iterator()
            viewModelsIterator.forEach { map ->
                val viewModelIterator = map.value.iterator()
                viewModelIterator.forEach {
                    checkNeedRemoveLifecycleOwner(it, lifecycleOwner)
                    checkCallCleared(it, viewModelIterator)
                    if (map.value.size == 0) {
                        viewModelsIterator.remove()
                    }
                }
            }
        }

        private fun checkNeedRemoveLifecycleOwner(viewModel: BaseShareViewModel, lifecycleOwner: LifecycleOwner) {
            val iterator = viewModel.onattouchedLifecycleOwner.iterator()
            iterator.forEach {
                if (it.get() == null || it.get() == lifecycleOwner || it.get()?.lifecycle?.currentState == Lifecycle.State.DESTROYED) {
                    iterator.remove()
                }
            }
            viewModel.lifecycleOwner = viewModel.onattouchedLifecycleOwner.lastOrNull()
        }

        private fun checkCallCleared(viewModel: BaseShareViewModel, viewModelIterator: MutableIterator<BaseShareViewModel>) {
            //如果该 viewModel 没有attouch 的 lifecyclerOwner 则全部清除，移除该viewmodel
            if (viewModel.onattouchedLifecycleOwner.size == 0) {
                viewModel.onCleared()
                viewModelIterator.remove()
            }
        }
    }

    interface LifecycleShareObserver {

        fun onCreate(lifecycleOwner: LifecycleOwner)

        fun onStart(lifecycleOwner: LifecycleOwner)

        fun onResume(lifecycleOwner: LifecycleOwner)

        fun onPause(lifecycleOwner: LifecycleOwner)

        fun onStop(lifecycleOwner: LifecycleOwner)

        fun onDestroy(lifecycleOwner: LifecycleOwner)
    }
}