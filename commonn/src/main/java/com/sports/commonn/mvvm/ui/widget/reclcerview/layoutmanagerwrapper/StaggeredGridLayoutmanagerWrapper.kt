package com.sports.commonn.mvvm.ui.widget.reclcerview.layoutmanagerwrapper

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import java.lang.ref.WeakReference

/**
 *Create By Albert on 2020/4/24
 */
class StaggeredGridLayoutmanagerWrapper : LayoutManagerWrapper {

    var spanCount = 1
    var orientation: Int = LinearLayoutManager.VERTICAL
    var reverseLayout: Boolean = false

    constructor(context: Context?, spanCount: Int) {
        this.context = WeakReference(context)
        this.spanCount = spanCount
        layoutManager = WeakReference(StaggeredGridLayoutManager(spanCount, orientation))
    }

    constructor(context: Context?, spanCount: Int, orientation: Int) {
        this.context = WeakReference(context)
        this.spanCount = spanCount
        this.orientation = orientation
        this.reverseLayout = reverseLayout
        layoutManager = WeakReference(StaggeredGridLayoutManager(spanCount, orientation))
    }


    override fun getLayoutManager(): RecyclerView.LayoutManager? {
        return layoutManager?.get()
    }

    override fun copyLayoutManager(context: Context?): RecyclerView.LayoutManager {
        this.context = WeakReference(context)
        return StaggeredGridLayoutManager(spanCount, orientation)
    }

    override fun setStackFromEnd(statck: Boolean) {

    }
}