package com.sports.commonn.memory

import com.sports.commonn.entity.UserInfo
import com.sports.commonn.mvvm.livedata.SafeMutableLiveData

/**
 *create by Albert
 */
object DataManager {

    @JvmStatic
    val netwokIsConnet = SafeMutableLiveData(true)


    //黑夜白天模式
    @JvmStatic
    val appIsNightTheme by lazy { SafeMutableLiveData(false) }

    @JvmStatic
    var token: String? = null

    var user: UserInfo? = null

    var imgServerHost = "http://admin.tybzbz.com/storage"

    fun loginOut() {
    }

    val downloadList = HashSet<Int>()

    val collectionList = HashSet<Int>()
}