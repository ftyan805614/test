package com.sports.commonn.utils

import android.Manifest.permission
import android.app.Activity
import android.app.Application
import android.app.KeyguardManager
import android.content.ComponentCallbacks
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.util.DisplayMetrics
import android.view.Surface
import android.view.WindowManager
import androidx.annotation.RequiresPermission
import com.sports.commonn.YaboLib
import com.sports.commonn.mvvm.TopLifecyclerOwnerManager

object ScreenUtil {

    var defaultStatusbarHeight = ResUtils.dp2px(25.0f);

    /**
     * 获取导航栏高度
     * @param context
     * @return
     */
    fun getNavigationHeight(): Int {
        val result = 0
        var resourceId = 0
        val rid =
            YaboLib.app?.resources?.getIdentifier("config_showNavigationBar", "bool", "android")
        return if (rid != 0) {
            resourceId =
                YaboLib.app.resources.getIdentifier(
                    "navigation_bar_height",
                    "dimen",
                    "android"
                )
            YaboLib.app.resources.getDimensionPixelSize(resourceId)
        } else 0
    }

    fun isNavigationBarShow(): Boolean {
        /**
         * 获取应用区域高度
         */
        val outRect1 = Rect()
        try {
            TopLifecyclerOwnerManager.instance.curActivity?.window?.decorView?.getWindowVisibleDisplayFrame(
                outRect1
            )
        } catch (e: ClassCastException) {
            e.printStackTrace()
            return false
        }
        val activityHeight = outRect1.height()

        /**
         * 屏幕物理高度 - 状态栏高度  导航栏高度 = 导航栏存在时的应用实际高度
         */
        val remainHeight = getRealHeight() - getStatusBarHeight() - getNavBarHeight()
        /**
         * 剩余高度跟应用区域高度相等 说明导航栏存在
         */
        return activityHeight == remainHeight
    }

    fun getNavBarHeight(): Int {
        val res = Resources.getSystem()
        val resourceId = res.getIdentifier("navigation_bar_height", "dimen", "android")
        return if (resourceId != 0) {
            res.getDimensionPixelSize(resourceId)
        } else {
            0
        }
    }

    fun getStatusBarHeight(): Int {
        val resources = Resources.getSystem()
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId == 0) return defaultStatusbarHeight
        return resources.getDimensionPixelSize(resourceId)
    }

    fun getRealHeight(): Int {
        val wm = YaboLib.app.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val point = Point()
        wm.defaultDisplay.getRealSize(point)
        return point.y
    }


    /**
     * 获取屏幕宽度
     * Return the width of screen, in pixel.
     *
     * @return the width of screen, in pixel
     */
    @JvmStatic
    val screenWidth: Int
        get() {
            val wm = YaboLib.app.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                ?: return YaboLib.app.resources.displayMetrics.widthPixels
            val point = Point()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                wm.defaultDisplay.getRealSize(point)
            } else {
                wm.defaultDisplay.getSize(point)
            }
            return point.x
        }

    /**
     * 获取屏幕高度
     *
     * @return the height of screen, in pixel
     */
    @JvmStatic
    val screenHeight: Int
        get() {
            val wm = YaboLib.app.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                ?: return YaboLib.app.resources.displayMetrics.heightPixels
            val point = Point()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                wm.defaultDisplay.getRealSize(point)
            } else {
                wm.defaultDisplay.getSize(point)
            }
            return point.y
        }

    /**
     * Return the density of screen.
     *
     * @return the density of screen
     */
    val screenDensity: Float
        get() = YaboLib.app.resources.displayMetrics.density

    /**
     * Return the screen density expressed as dots-per-inch.
     *
     * @return the screen density expressed as dots-per-inch
     */
    val screenDensityDpi: Int
        get() = YaboLib.app.resources.displayMetrics.densityDpi

    /**
     * 设置全屏
     *
     * @param activity The activity.
     */
    fun setFullScreen(activity: Activity) {
        activity.window.addFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN
                    or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    /**
     * 退出设置全屏
     *
     * @param activity The activity.
     */
    fun setNonFullScreen(activity: Activity) {
        activity.window.clearFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN
                    or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    /**
     * Toggle full screen.
     *
     * @param activity The activity.
     */
    fun toggleFullScreen(activity: Activity) {
        val fullScreenFlag = WindowManager.LayoutParams.FLAG_FULLSCREEN
        val window = activity.window
        if (window.attributes.flags and fullScreenFlag == fullScreenFlag) {
            window.clearFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN
                        or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        } else {
            window.addFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN
                        or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        }
    }

    /**
     * Return whether screen is full.
     *
     * @param activity The activity.
     * @return `true`: yes<br></br>`false`: no
     */
    fun isFullScreen(activity: Activity): Boolean {
        val fullScreenFlag = WindowManager.LayoutParams.FLAG_FULLSCREEN
        return activity.window.attributes.flags and fullScreenFlag == fullScreenFlag
    }

    /**
     * 设置横屏
     * Set the screen to landscape.
     *
     * @param activity The activity.
     */
    fun setLandscape(activity: Activity) {
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }

    /**
     * 设置竖屏
     * Set the screen to portrait.
     *
     * @param activity The activity.
     */
    fun setPortrait(activity: Activity) {
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    /**
     * 是否是横屏
     * Return whether screen is landscape.
     *
     * @return `true`: yes<br></br>`false`: no
     */
    val isLandscape: Boolean
        get() = (YaboLib.app.resources.configuration.orientation
                == Configuration.ORIENTATION_LANDSCAPE)

    /**
     * 是否是竖屏
     * Return whether screen is portrait.
     *
     * @return `true`: yes<br></br>`false`: no
     */
    val isPortrait: Boolean
        get() = (YaboLib.app.resources.configuration.orientation
                == Configuration.ORIENTATION_PORTRAIT)

    /**
     * 获取屏幕旋转角度
     * Return the rotation of screen.
     *
     * @param activity The activity.
     * @return the rotation of screen
     */
    fun getScreenRotation(activity: Activity): Int {
        return when (activity.windowManager.defaultDisplay.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> 0
        }
    }
    /**
     * 截取屏幕 并删除status bar
     * Return the bitmap of screen.
     *
     * @param activity          The activity.
     * @param isDeleteStatusBar True to delete status bar, false otherwise.
     * @return the bitmap of screen
     */
    /**
     * 截取屏幕
     * Return the bitmap of screen.
     *
     * @param activity The activity.
     * @return the bitmap of screen
     */
    fun screenShot(activity: Activity, isDeleteStatusBar: Boolean = false): Bitmap? {
        val decorView = activity.window.decorView
        decorView.isDrawingCacheEnabled = true
        decorView.setWillNotCacheDrawing(false)
        val bmp = decorView.drawingCache ?: return null
        val dm = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(dm)
        val ret: Bitmap
        ret = if (isDeleteStatusBar) {
            val resources = activity.resources
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            val statusBarHeight = resources.getDimensionPixelSize(resourceId)
            Bitmap.createBitmap(
                bmp,
                0,
                statusBarHeight,
                dm.widthPixels,
                dm.heightPixels - statusBarHeight
            )
        } else {
            Bitmap.createBitmap(bmp, 0, 0, dm.widthPixels, dm.heightPixels)
        }
        decorView.destroyDrawingCache()
        return ret
    }

    /**
     * 是否锁屏
     * Return whether screen is locked.
     *
     * @return `true`: yes<br></br>`false`: no
     */
    val isScreenLock: Boolean
        get() {
            val km = YaboLib.app.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            return km != null && km.inKeyguardRestrictedInputMode()
        }

    /**
     * 获取进入锁屏的时间
     * Return the duration of sleep.
     *
     * @return the duration of sleep.
     */
    /**
     * 设置进入锁屏时间
     * Set the duration of sleep.
     *
     * Must hold `<uses-permission android:name="android.permission.WRITE_SETTINGS" />`
     *
     * @param duration The duration.
     */
    @set:RequiresPermission(permission.WRITE_SETTINGS)
    var sleepDuration: Int
        get() = try {
            Settings.System.getInt(
                YaboLib.app.contentResolver,
                Settings.System.SCREEN_OFF_TIMEOUT
            )
        } catch (e: SettingNotFoundException) {
            e.printStackTrace()
            -123
        }
        set(duration) {
            Settings.System.putInt(
                YaboLib.app.contentResolver,
                Settings.System.SCREEN_OFF_TIMEOUT,
                duration
            )
        }

    /**
     * 是否是平板
     * Return whether device is tablet.
     *
     * @return `true`: yes<br></br>`false`: no
     */
    val isTablet: Boolean
        get() = ((YaboLib.app.resources.configuration.screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE)

    /**
     * 垂直布局UI适配
     * Adapt the screen for vertical slide.
     *
     * @param activity        The activity.
     * @param designWidthIndp UI宽度的dp值
     */
    @JvmStatic
    fun adaptScreen4VerticalSlide(
        activity: Activity?,
        designWidthIndp: Int
    ) {
        adaptScreen4VerticalSlide(activity, designWidthIndp, true)
    }

    /**
     * 横屏布局ui适配
     * Adapt the screen for horizontal slide.
     *
     * @param activity         The activity.
     * @param designHeightIndp UI高度的dp值
     */
    fun adaptScreen4HorizontalSlide(
        activity: Activity?, designHeightIndp: Int
    ): DisplayMetrics? {
        return adaptScreen4HorizontalSlide(activity, designHeightIndp, false)
    }

    /**
     * 垂直布局UI适配
     * Adapt the screen for vertical slide.
     *
     * @param activity        The activity.
     * @param designWidthIndp UI宽度的dp值
     * @param isOnlyActivity 是否只适配当前activity
     */
    fun adaptScreen4VerticalSlide(
        activity: Activity?,
        designWidthIndp: Int,
        isOnlyActivity: Boolean
    ) {
        val componentCallbacks = object : ComponentCallbacks {
            override fun onLowMemory() {

            }

            override fun onConfigurationChanged(newConfig: Configuration) {
                adaptScreen(activity, designWidthIndp.toFloat(), true, isOnlyActivity)
            }
        }
        activity?.application?.registerComponentCallbacks(componentCallbacks)
        val aclf = object : Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity) {
            }

            override fun onActivityStarted(activity: Activity) {
            }

            override fun onActivityDestroyed(mactivity: Activity) {
                if (activity == mactivity) {
                    activity.application.unregisterActivityLifecycleCallbacks(this)
                    activity.application.unregisterComponentCallbacks(componentCallbacks)
                }
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
            }

            override fun onActivityStopped(activity: Activity) {
            }

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            }

            override fun onActivityResumed(activity: Activity) {
                adaptScreen(activity, designWidthIndp.toFloat(), true, isOnlyActivity)
            }

        }
        activity?.application?.registerActivityLifecycleCallbacks(aclf)
        adaptScreen(activity, designWidthIndp.toFloat(), true, isOnlyActivity)
    }

    /**
     * 横屏布局ui适配
     * Adapt the screen for horizontal slide.
     *
     * @param activity         The activity.
     * @param designHeightIndp UI高度的dp值
     * @param isOnlyActivity 是否只适配当前activity
     */
    fun adaptScreen4HorizontalSlide(
        activity: Activity?,
        designHeightIndp: Int,
        isOnlyActivity: Boolean
    ): DisplayMetrics? {
        val componentCallbacks = object : ComponentCallbacks {
            override fun onLowMemory() {

            }

            override fun onConfigurationChanged(newConfig: Configuration) {
                adaptScreen(activity, designHeightIndp.toFloat(), false, isOnlyActivity)
            }
        }
        activity?.application?.registerComponentCallbacks(componentCallbacks)
        val aclf = object : Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(mactivity: Activity) {
                if (activity == mactivity && mactivity.isFinishing) {
                    activity.application.unregisterActivityLifecycleCallbacks(this)
                    activity.application.unregisterComponentCallbacks(componentCallbacks)
                }
            }

            override fun onActivityStarted(activity: Activity) {
            }

            override fun onActivityDestroyed(mactivity: Activity) {
                if (activity == mactivity) {
                    activity.application.unregisterActivityLifecycleCallbacks(this)
                    activity.application.unregisterComponentCallbacks(componentCallbacks)
                }
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
            }

            override fun onActivityStopped(activity: Activity) {
            }

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            }

            override fun onActivityResumed(activity: Activity) {
                adaptScreen(activity, designHeightIndp.toFloat(), false, isOnlyActivity)
            }

        }
        activity?.application?.registerActivityLifecycleCallbacks(aclf)
        return adaptScreen(activity, designHeightIndp.toFloat(), false, isOnlyActivity)
    }

    /**
     * Reference from: https://mp.weixin.qq.com/s/d9QCoBP6kV9VSWvVldVVwA
     */
    private fun adaptScreen(
        activity: Activity?,
        sizeInDp: Float,
        isVerticalSlide: Boolean,
        isOnlyActivity: Boolean
    ): DisplayMetrics? {
        return adpterDisplay(sizeInDp, isVerticalSlide, activity?.resources, isOnlyActivity)
    }

    fun adpterDisplay(
        sizeInDp: Float,
        isVerticalSlide: Boolean,
        resources: Resources?,
        isOnlyActivity: Boolean = true
    ): DisplayMetrics? {
        val systemDm = Resources.getSystem().displayMetrics
        val appDm = YaboLib.app.resources.displayMetrics
        val targetDensity: Float
        targetDensity = if (isVerticalSlide) {
            appDm.widthPixels / sizeInDp
        } else {
            appDm.heightPixels / sizeInDp
        }
        val scaledDensity = targetDensity * (systemDm.scaledDensity / systemDm.density)
        val densityDpi = (160 * targetDensity).toInt()
        if (!isOnlyActivity) {
            appDm.density = targetDensity
            appDm.scaledDensity = scaledDensity
            appDm.densityDpi = densityDpi
        }
        val activityDm = resources?.displayMetrics
        activityDm?.density = targetDensity
        activityDm?.scaledDensity = scaledDensity
        activityDm?.densityDpi = densityDpi
        val configuration = Configuration()
        configuration.setToDefaults()
        resources?.updateConfiguration(configuration, activityDm)
        return activityDm
    }

    /**
     * 取消适配
     * Cancel adapt the screen.
     *
     * @param activity The activity.
     */
    fun cancelAdaptScreen(activity: Activity) {
        val systemDm = Resources.getSystem().displayMetrics
        val appDm = YaboLib.app.resources.displayMetrics
        val activityDm = activity.resources.displayMetrics
        activityDm.density = systemDm.density
        activityDm.scaledDensity = systemDm.scaledDensity
        activityDm.densityDpi = systemDm.densityDpi
        appDm.density = systemDm.density
        appDm.scaledDensity = systemDm.scaledDensity
        appDm.densityDpi = systemDm.densityDpi
    }

    /**
     * 是否已经做了ui适配的设置
     * Return whether adapt screen.
     *
     * @return `true`: yes<br></br>`false`: no
     */
    fun isAdaptScreen(): Boolean {
        val systemDm = Resources.getSystem().displayMetrics
        val appDm = YaboLib.app.resources.displayMetrics
        return systemDm.density != appDm.density
    }



    fun replaceData(data:String):String{
        var newData:String=data
        if (data.contains("\"data\":[]")) {
            newData = newData.replace("\"data\":[]", "\"data\":null")
        } else if (data.contains("\"data\":\"\"")) {
            newData = newData.replace("\"data\":\"\"", "\"data\":null")
        }else if (data.contains("\"data\":{}")) {
            newData = newData.replace("\"data\":{}", "\"data\":null")
        }
        return newData
    }

}