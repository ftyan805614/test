package com.sports.commonn.utils

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 *Create By Albert on 2020/9/6
 */
object RecyclerViewUtils {


    fun isVisCompleteBottom(recyclerView: RecyclerView): Boolean {
        val layoutManager: LinearLayoutManager? =
            recyclerView.layoutManager as? LinearLayoutManager?
        //屏幕中最后一个可见子项的position
        val lastVisibleItemPosition: Int = layoutManager?.findLastCompletelyVisibleItemPosition() ?: 0
        //当前屏幕所看到的子项个数
        val visibleItemCount: Int = layoutManager?.childCount ?: 0
        //当前RecyclerView的所有子项个数
        val totalItemCount: Int = layoutManager?.itemCount ?: 0
        //RecyclerView的滑动状态
        val state = recyclerView.scrollState
        return visibleItemCount > 0 && lastVisibleItemPosition == totalItemCount - 1 && state == RecyclerView.SCROLL_STATE_IDLE
    }

    fun isVisCompleteTop(recyclerView: RecyclerView): Boolean {
        val layoutManager: LinearLayoutManager? =
            recyclerView.layoutManager as? LinearLayoutManager?
        //屏幕中最后一个可见子项的position
        val firstVisibleItemPosition: Int = layoutManager?.findFirstCompletelyVisibleItemPosition() ?: 0
        //当前屏幕所看到的子项个数
        val visibleItemCount: Int = layoutManager?.childCount ?: 0
        //RecyclerView的滑动状态
        val state = recyclerView.scrollState
        return visibleItemCount > 0 && firstVisibleItemPosition == 0 && state == RecyclerView.SCROLL_STATE_IDLE
    }

    fun isVisBottom(recyclerView: RecyclerView): Boolean {
        val layoutManager: LinearLayoutManager? =
            recyclerView.layoutManager as? LinearLayoutManager?
        //屏幕中最后一个可见子项的position
        val lastVisibleItemPosition: Int = layoutManager?.findFirstVisibleItemPosition() ?: 0
        //当前屏幕所看到的子项个数
        val visibleItemCount: Int = layoutManager?.childCount ?: 0
        //当前RecyclerView的所有子项个数
        val totalItemCount: Int = layoutManager?.itemCount ?: 0
        //RecyclerView的滑动状态
        val state = recyclerView.scrollState
        return visibleItemCount > 0 && lastVisibleItemPosition == totalItemCount - 1 && state == RecyclerView.SCROLL_STATE_IDLE
    }

    fun isVisTop(recyclerView: RecyclerView): Boolean {
        val layoutManager: LinearLayoutManager? =
            recyclerView.layoutManager as? LinearLayoutManager?
        val firstVisibleItemPosition = if (layoutManager is GridLayoutManager) {
            layoutManager.findFirstVisibleItemPosition()
        } else {
            layoutManager?.findLastVisibleItemPosition() ?: 0
        }
        //当前屏幕所看到的子项个数
        val visibleItemCount: Int = layoutManager?.childCount ?: 0
        //RecyclerView的滑动状态
        val state = recyclerView.scrollState
        return visibleItemCount > 0 && firstVisibleItemPosition == 0 && state == RecyclerView.SCROLL_STATE_IDLE
    }

}