package com.sports.commonn.utils;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.sports.commonn.mvvm.ui.widget.Config;
import com.sports.commonn.mvvm.ui.widget.IStatusViewHelper;
import com.sports.commonn.mvvm.ui.widget.OnStatusClickListener;
import com.sports.commonn.mvvm.ui.widget.StatusAdapter;

/**
 * Desc:
 * Created by Jeff on 2018/9/14
 **/
public class StatusViewHelper implements IStatusViewHelper {
    private ViewGroup mContainerView;
    private View statusView;
    private StatusAdapter mAdapter;
    private OnStatusClickListener mClickListener;
    private Factory sFactory;

    public StatusViewHelper(@NonNull View view) {
        mContainerView = findSuitableParent(view);
        this.sFactory = Config.getStatusFactory();
    }

    private void initStatusView(){
        assertParentView();
        if (statusView == null){
            if (sFactory == null) {
                sFactory = Config.getStatusFactory();
            }
            mAdapter =  sFactory.adapter();
            mAdapter.setOnChildClickListener(mClickListener);
            statusView = mAdapter.getView(mContainerView.getContext());
            statusView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            statusView.setVisibility(View.GONE);
            mContainerView.addView(statusView);
        }
    }

    private void assertParentView(){
        if (mContainerView == null){
            throw new IllegalArgumentException("创建StatusViewHelper对象需要的view,请勿使用动态创建的view");
        }
    }

    @Override
    public void showError(int type,String message){
        initStatusView();
        statusView.setVisibility(View.VISIBLE);
        mAdapter.showError(type,message);
    }

    @Override
    public void showLoadding(String message){
        initStatusView();
        statusView.setVisibility(View.VISIBLE);
        mAdapter.showLoadding(message);
    }

    @Override
    public void success(){
        initStatusView();
        mAdapter.success();
        statusView.setVisibility(View.GONE);
    }

    @Override
    public void setOnChildClickListener(OnStatusClickListener listener){
       this.mClickListener = listener;
       if (mAdapter != null){
           mAdapter.setOnChildClickListener(listener);
       }
    }

    private ViewGroup findSuitableParent(View view) {
        ViewGroup fallback = null;
        do {
            if (isFreeView(view)) {
                return (ViewGroup) view;
            }else if (view instanceof LinearLayout
                    || view instanceof ViewGroup){
                int childIndex = 0;
                final ViewGroup contentParent = (ViewGroup) view.getParent();
                if (contentParent == null) return (ViewGroup) view;
                if (contentParent.getId() == android.R.id.content){
                    return contentParent;
                }

                int childCount = contentParent.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    if (contentParent.getChildAt(i) == view) {
                        childIndex = i;
                        break;
                    }
                }

                if (isFreeView(contentParent)){
                    fallback = genParentView(view,false);
                }else {
                    contentParent.removeView(view);
                    fallback = genParentView(view,true);
                }

                contentParent.addView(fallback,childIndex);
                return fallback;
            }

            if (view != null) {
                // Else, we will loop and crawl up the view hierarchy and try to find a parent
                final ViewParent parent = view.getParent();
                view = parent instanceof View ? (View) parent : null;
                fallback = (ViewGroup) view;
            }
        } while (view != null);

        // If we reach here then we didn't find a CoL or a suitable content view so we'll fallback
        return fallback;
    }

    /**
     * desc: 是否是自由父控件
     *@author Jeff created on 2018/10/28 14:21
     */
    private boolean isFreeView(View contentParent){
        return contentParent instanceof ConstraintLayout ||
                contentParent instanceof RelativeLayout ||
                contentParent instanceof FrameLayout ||
                contentParent instanceof CoordinatorLayout;
    }

    /**
     * desc: 获取StatusView的容器
     *@author Jeff created on 2018/10/28 14:14
     * @param view 目标容器
     * @param isAddChildView 是否要把目标容器添加到StatusView容器
     */
    private ViewGroup genParentView(final View view, boolean isAddChildView){
        final ViewGroup.LayoutParams params = view.getLayoutParams();
        final FrameLayout statusParentView = new FrameLayout(view.getContext());

        if (isAddChildView){
            //因为父布局改变,所以子布局的LayoutParams 需要重新定义
            FrameLayout.LayoutParams childParams =
                    new FrameLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                    );
            view.setLayoutParams(childParams);
            statusParentView.addView(view);
        }else if (params instanceof ConstraintLayout.LayoutParams){
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    params.height = view.getHeight();
                    statusParentView.setLayoutParams(params);
                }
            });
        }else if (params instanceof FrameLayout.LayoutParams){
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        statusParentView.setLayoutParams(params);
        return statusParentView;
    }

    @Override
    public void setFactory(Factory factory){
        if (factory == null){
            throw new NullPointerException("Status factory is null ! you can use YaboLib.init().setFactory or use StatusViewHelper.setFactory() set this object!");
        }
        sFactory = factory;
    }

    public interface Factory {
        StatusAdapter adapter();
    }
}
