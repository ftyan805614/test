package com.sports.commonn.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.apkfuns.logutils.LogUtils;
import com.sports.commonn.YaboLib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.UUID;


public class AppUtil {

    /**
     * 获取版本名字
     */
    public synchronized static String getVersionName() {
        String version = "0";
        // 获取packagemanager的实例
        PackageManager packageManager = YaboLib.getApp().getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(YaboLib.getApp().getPackageName(), PackageManager.GET_CONFIGURATIONS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null != packInfo) {
            version = packInfo.versionName;
        }
        return version;
    }

    /**
     * 获取版本好
     */
    public synchronized static int getVersionCode() {
        if (YaboLib.getConfig() != null && YaboLib.getConfig().getCurrentVerCode() != null) {
            return YaboLib.getConfig().getCurrentVerCode();
        }
        int versionCode = 0;
        // 获取packagemanager的实例
        PackageManager packageManager = YaboLib.getApp().getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(YaboLib.getApp().getPackageName(), PackageManager.GET_CONFIGURATIONS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null != packInfo) {
            versionCode = packInfo.versionCode;
        }
        return versionCode;
    }

    /**
     * 得到软件包名
     */
    public static String getPackgeName() {
        return PackgeName();
    }

    /**
     * 得到软件包名
     */
    public synchronized static String PackgeName() {
        String packgename = "unknow";
        try {
            packgename = YaboLib.getApp().getPackageManager().getPackageInfo(
                    YaboLib.getApp().getPackageName(), PackageManager.GET_CONFIGURATIONS).packageName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return packgename;
    }

    /**
     * 获取当前进程的名字
     */
    public static String getProcessName(int pid) {
        ActivityManager am = (ActivityManager) YaboLib.getApp().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
            if (procInfo.pid == pid) {
                return procInfo.processName;
            }
        }
        return null;
    }

    /**
     * 判断程序是否在前台运行
     *
     * @return true在前台，false在后台
     */
    public synchronized static boolean isAppForeground() {
        boolean isForground = false;
        try {
            ActivityManager am = (ActivityManager) YaboLib.getApp().getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                if (runningProcesses != null) {
                    for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                        //前台程序
                        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                            for (String pkgName : processInfo.pkgList) {
                                if (pkgName.equals(YaboLib.getApp().getPackageName())) {
                                    isForground = true;
                                }
                            }
                        }
                    }
                }
            } else {
                //@deprecated As of {@link android.os.Build.VERSION_CODES#LOLLIPOP}, 从Android5.0开始不能使用getRunningTask函数
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(YaboLib.getApp().getPackageName())) {
                    isForground = true;
                }
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }

        return isForground;
    }


    /**
     * 打印签名信息
     */
    public synchronized static void printSignature() {
        try {
            PackageManager manager = YaboLib.getApp().getPackageManager();
            PackageInfo packageinfo = manager.getPackageInfo(getPackgeName(), PackageManager.GET_SIGNATURES);
            LogUtils.e("签名 : %s", packageinfo.signatures[0].toCharsString());
        } catch (Exception e) {
            LogUtils.e(e);
        }
    }


    private static String sID = null;

    private static final String INSTALLATION = "INSTALLATION";

    // 保存文件的路径
    private static final String CACHE_IMAGE_DIR = "aray/devices";
    // 保存的文件 采用隐藏文件的形式进行保存
    private static final String DEVICES_FILE_NAME = ".DEVICES";


    /**
     * 获取UUID
     *
     * @return uuid字符串
     */
    public synchronized static String getUUID() {
        if (sID != null) {
            return sID;
        }
        File installation = new File(YaboLib.getApp().getFilesDir(), INSTALLATION);
        try {
            if (!installation.exists()) {
                String uuid = getAndroidId(YaboLib.getApp());
                if (TextUtils.isEmpty(uuid)) {
                    uuid = UUID.randomUUID().toString();
                }
                sID = writeInstallationFile(installation, uuid);
            } else {
                sID = readInstallationFile(installation);
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return sID;
    }

    /**
     * 获得设备的AndroidId
     *
     * @param context 上下文
     * @return 设备的AndroidId
     */
    private static String getAndroidId(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }


    /**
     * 获取UUID
     *
     * @return uuid字符串
     */
    public synchronized static String getUUID2() {
        if (!TextUtils.isEmpty(sID)) {
            return sID;
        }

        // 首先冲app应用缓存文件里面获取
        File installation = new File(YaboLib.getApp().getFilesDir(), INSTALLATION);
        try {
            if (!installation.exists()) {
                // 检查外部存储在不在？
                String deviceId = getDeviceID();
                if (!TextUtils.isEmpty(deviceId)) {
                    // 外部存在，就更新内部存儲
                    sID = deviceId;
                    writeInstallationFile(installation, sID);
                } else {
                    // 都不存在，重新生成
                    String uuid = UUID.randomUUID().toString();
                    sID = writeInstallationFile(installation, uuid);
                    saveDeviceID(sID);
                }
            } else {
                // 内部存在
                sID = readInstallationFile(installation);
                // 同时检查一下外部存储文件在不在
                File file = getExternalDir();
                if (file == null && !file.exists()) {
                    // 同步数据到外部存储里面
                    saveDeviceID(sID);
                }
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }

        LogUtils.d("+++++sID=" + sID);

        return sID;
    }

    private static String readInstallationFile(File installation) throws IOException {

        RandomAccessFile f = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[(int) f.length()];
        f.readFully(bytes);
        f.close();

        return new String(bytes);

    }

    private static String writeInstallationFile(File installation, String id) throws IOException {
        FileOutputStream out = new FileOutputStream(installation);
        out.write(id.getBytes());
        out.close();
        return id;
    }

    private static String getDeviceID() throws IOException {
        File file = getExternalDir();
        if (file != null && file.exists()) {
            StringBuffer buffer = new StringBuffer();
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            Reader in = new BufferedReader(isr);
            int i;
            while ((i = in.read()) > -1) {
                buffer.append((char) i);
            }
            in.close();
            return buffer.toString();
        }
        return "";
    }


    /**
     * 保存 内容到 SD卡中,  这里保存的就是 设备唯一标识符
     *
     * @param str
     */
    private static void saveDeviceID(String str) throws IOException {
        File file = getExternalDir();
        try {
            if (file != null && file.exists()) {
                FileOutputStream fos = new FileOutputStream(file);
                Writer out = new OutputStreamWriter(fos, "UTF-8");
                out.write(str);
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 统一处理设备唯一标识 保存的文件的地址
     *
     * @return
     */
    private static File getExternalDir() throws IOException {
        File mCropFile = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File cropdir = new File(Environment.getExternalStorageDirectory(), CACHE_IMAGE_DIR);
            if (!cropdir.exists()) {
                if (!cropdir.getParentFile().exists()) {
                    cropdir.getParentFile().mkdirs();
                }
                cropdir.mkdirs();
            }
            // 用当前时间给取得的图片命名
            mCropFile = new File(cropdir, DEVICES_FILE_NAME);
            if (!mCropFile.exists()) {
                mCropFile.createNewFile();
            }
        }
        return mCropFile;
    }
}
