package com.sports.commonn.utils

import androidx.lifecycle.Observer
import com.jeremyliao.liveeventbus.LiveEventBus

/**
 * Click发送事件统一处理
 */
object EventBusClickUtil {
    private var isOpenClickEvent = false
    private var eventbus_key_click = "eventbus_key_click"

    fun click(clickId: Int?){
        click(clickId, eventbus_key_click)
    }

    fun click(clickId: Int?,eventBusKey:String){
        if(isOpenClickEvent){
            LiveEventBus.get(eventBusKey).post(clickId)
        }
    }

    fun register(observer: Observer<Int>){
        register(eventbus_key_click,observer)
    }


    fun register(eventBusKey:String,observer: Observer<Int>){
        LiveEventBus.get(eventBusKey,Int::class.java).observeForever(observer)
    }
    
    fun openClickListener(isOpenClickEvent:Boolean){
        EventBusClickUtil.isOpenClickEvent = isOpenClickEvent
    }
    
    fun unRegister(eventBusKey:String,observer: Observer<Int>){
        LiveEventBus.get(eventBusKey,Int::class.java).removeObserver(observer)
    }

    fun unRegister(observer: Observer<Int>?){
        if(null != observer){
            unRegister(eventbus_key_click,observer)
        }
    }
}