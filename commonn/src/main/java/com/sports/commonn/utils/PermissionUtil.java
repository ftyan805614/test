package com.sports.commonn.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * App权限申请
 * Created by Jane on 2018/10/11.
 */

public class PermissionUtil {
    public static String[] PERMISSION_PHONE_STATE = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static String[] PERMISSION_CAMERA_WRITE = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static String[] ACCESS_NETWORK_STATE = {Manifest.permission.ACCESS_NETWORK_STATE};
    public static final int permissions_request_code = 0x1;

    public static boolean isLacksOfPermission(Activity activity,String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ContextCompat.checkSelfPermission(
                    activity.getApplication().getApplicationContext(), permission) != PackageManager.PERMISSION_DENIED;
        }
        return true;
    }

    /**
     * 单个权限检查
     * @param activity
     * @param permission
     * @return
     */
    public static boolean isPermissionsApply(Activity activity,String permission){
        boolean result = true;
        if(!isLacksOfPermission(activity,permission)){
            ActivityCompat.requestPermissions(activity,new String[]{permission} , permissions_request_code);
            result = false;
        }
        return result;
    }

    /**
     * 多个权限检查
     * @param activity
     * @param permission
     * @return
     */
    public static boolean isPermissionsApply(Activity activity,String[] permission){
        boolean result = true;
        List<String> mPermissionList = new ArrayList<>();
        for (int i = 0; i < permission.length; i++) {
            if (!isLacksOfPermission(activity,permission[i])) {
                mPermissionList.add(permission[i]);
            }
        }
        if(!mPermissionList.isEmpty()){
            ActivityCompat.requestPermissions(activity,mPermissionList.toArray(new String[mPermissionList.size()]), permissions_request_code);
            result = false;
        }

        return result;
    }


    public static boolean onRequestPermissionsResult(Activity activity,int requestCode, String[] permissions, int[] grantResults) {
        return onRequestPermissionsResult(activity,requestCode,permissions,grantResults,true);
    }

    /**
     * 权限申请结果
     * @param requestCode
     * @param permissions
     * @param grantResults
     * @return
     */
    public static boolean onRequestPermissionsResult(Activity activity,int requestCode, String[] permissions, int[] grantResults,boolean showToast) {
        boolean isApplyResult = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                isApplyResult = false;
                //判断是否勾选禁止后不再询问
                boolean showRequestPermission = ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[i]);
                if (showRequestPermission) {
                    if(showToast){
                        ToastUtil.show("为了正常使用，请允许权限");
                    }
                    break;
                }
                break;
            }
        }
        return isApplyResult;
    }


    /**
     * 检查是否拥有指定的所有权限
     */
    public static boolean checkPermissionAllGranted(Activity activity, String[] permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                // 只要有一个权限没有被授予, 则直接返回 false
                return false;
            }
        }
        return true;
    }


}
