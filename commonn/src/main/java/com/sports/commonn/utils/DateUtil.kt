package com.sports.commonn.utils

import android.text.TextUtils
import com.apkfuns.logutils.LogUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * author:Averson
 * createTime:2021/3/31
 * descriptions:
 *
 **/
object DateUtil {

    const val FormatPatter1 = "yyyy-MM-dd'T'HH:mm:ss"

    const val FormatPatter2 = "MM月dd日"

    fun getFormatDate(
        dateStr: String?,
        pattern: String,
        toFormatPattern: String
    ): String {
        if (TextUtils.isEmpty(dateStr)) return ""
        val formatter = SimpleDateFormat(pattern)
        var date: Date? = null
        try {
            date = formatter.parse(dateStr)
            val sdf = SimpleDateFormat(toFormatPattern)
            val sDate = sdf.format(date)
            val parse = sdf.parse(sDate)
            val lt = parse.time + 12 * 3600 * 1000
            return sdf.format(lt)
        } catch (e: ParseException) {
            LogUtils.e(e)
        }
        return ""
    }
}