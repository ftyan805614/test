package com.sports.commonn.utils;

import android.os.Build;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OSUtils {
    private static final String ROM_MIUI = "MIUI";
    private static final String ROM_EMUI = "EMUI";
    private static final String ROM_FLYME = "FLYME";
    private static final String ROM_OPPO = "OPPO";
    private static final String ROM_SMARTISAN = "SMARTISAN";
    private static final String ROM_VIVO = "VIVO";
    private static final String ROM_QIKU = "QIKU";

    private static final String KEY_VERSION_MIUI = "ro.miui.ui.version.name";
    private static final String KEY_VERSION_EMUI = "ro.build.version.emui";
    private static final String KEY_VERSION_OPPO = "ro.build.version.opporom";
    private static final String KEY_VERSION_SMARTISAN = "ro.smartisan.version";
    private static final String KEY_VERSION_VIVO = "ro.vivo.os.version";

    private static String sName;
    private static String sVersion;
    
    private static Boolean isEmui;
    private static Boolean isVivo;
    private static Boolean isOppo;
    private static Boolean isFlyme;
    private static Boolean is360;
    private static Boolean isSmartisan;
    private static Boolean isMiui;


    public static void init() {
        isEmui();
        isVivo();
        isOppo();
        isFlyme();
        is360();
        isSmartisan();
        isMiui();
    }

    public static boolean isEmui() {
        if (isEmui == null) {
            isEmui = check(ROM_EMUI);
        }
        return isEmui;
    }

    public static boolean isMiui() {
        if (isMiui == null) {
            isMiui = check(ROM_MIUI);
        }
        return isMiui;
    }

    public static boolean isVivo() {
        if (isVivo == null) {
            isVivo = check(ROM_VIVO);
        }
        return isVivo;
    }

    public static boolean isOppo() {
        if (isOppo == null) {
            isOppo = check(ROM_OPPO);
        }
        return isOppo;
    }

    public static boolean isFlyme() {
        if (isFlyme==null) {
            isFlyme = check(ROM_FLYME);
        }
        return isFlyme;
    }

    public static boolean is360() {
        if (is360 == null) {
            is360 = check(ROM_QIKU) || check("360");
        }
        return is360;
    }

    public static boolean isSmartisan() {
        if (isSmartisan == null) {
            isSmartisan = check(ROM_SMARTISAN);
        }
        return isSmartisan;
    }

    public static String getName() {
        if (sName == null) {
            check("");
        }
        return sName;
    }

    public static String getVersion() {
        if (sVersion == null) {
            check("");
        }
        return sVersion;
    }

    private static boolean check(String rom) {
        if (sName != null) {
            return sName.equals(rom);
        }

        if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_MIUI))) {
            sName = ROM_MIUI;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_EMUI))) {
            sName = ROM_EMUI;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_OPPO))) {
            sName = ROM_OPPO;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_VIVO))) {
            sName = ROM_VIVO;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_SMARTISAN))) {
            sName = ROM_SMARTISAN;
        } else {
            sVersion = Build.DISPLAY;
            if (sVersion.toUpperCase().contains(ROM_FLYME)) {
                sName = ROM_FLYME;
            } else {
                sVersion = Build.UNKNOWN;
                sName = Build.MANUFACTURER.toUpperCase();
            }
        }
        return sName.equals(rom);
    }

    private static String getProp(String name) {
        String line;
        BufferedReader input = null;
        try {
            Process p = Runtime.getRuntime().exec("getprop " + name);
            input = new BufferedReader(new InputStreamReader(p.getInputStream()), 1024);
            line = input.readLine();
            input.close();
        } catch (IOException ex) {
            return null;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return line;
    }
}

