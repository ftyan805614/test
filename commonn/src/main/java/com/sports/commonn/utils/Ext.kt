package com.sports.commonn.utils

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import com.apkfuns.logutils.LogUtils
import com.google.gson.Gson
import java.math.BigDecimal
import java.math.RoundingMode
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DecimalFormat
import java.util.*


/**
 *created by Albert
 */
val gson = Gson()

/**
 *Create By Albert on 2020/11/7
 */
private val stringBuilder = java.lang.StringBuilder()
private val stringBuffer = StringBuffer()


object ExtUtlils {

    @JvmStatic
    fun keepTwo(float: Number): String {
        return float.toDouble().keepTwo()
    }
}


fun buildStringe(block: java.lang.StringBuilder.() -> Unit): String {
    stringBuilder.setLength(0)
    block.invoke(stringBuilder)
    return stringBuilder.toString()
}


fun bufferStringe(block: StringBuffer.() -> Unit): String {
    stringBuffer.setLength(0)
    block.invoke(stringBuffer)
    return stringBuffer.toString()
}

fun MutableLiveData<String>?.toDouble(): Double {
    return try {
        java.lang.Double.parseDouble(this?.value?.toString())
    } catch (e: Exception) {
        0.00
    }
}


fun <T> List<T>?.toSelectMaps(block: (T) -> Array<String>): ArrayList<LinkedHashMap<String, T>>? {
    if (this?.size ?: 0 > 0) {
        val list = ArrayList<LinkedHashMap<String, T>>()
        this?.forEach { t ->
            val arrys = block(t)
            arrys.withIndex().forEach { key ->
                var map = list.getSF(key.index)
                if (map == null) {
                    map = LinkedHashMap()
                    list.add(map)
                }
                map[key.value] = t
            }
        }
        return list
    }
    return null
}

fun <T, V> List<T>?.select(block: (T) -> V): ArrayList<V> {
    val select = ArrayList<V>()
    this?.forEach {
        select.add(block(it))
    }
    return select
}

fun <T> List<T>?.containValue(block: (T) -> Boolean): Boolean {
    if (this?.size ?: 0 > 0) {
        this?.forEach {
            return block(it)
        }
    }
    return false
}


fun <T, V> ArrayList<T>?.foreachs(block: (T) -> List<V>?, block2: (T, V) -> Unit) {
    if (this != null) {
        val iterator1 = this.iterator()
        iterator1.forEach { t1 ->
            block(t1)?.forEach {
                block2(t1, it)
            }
        }
    }
}

fun <T, V, Z> ArrayList<T>?.foreachs(
    block: (T) -> List<V>?,
    block2: (T, V) -> List<Z>?,
    block3: (T, V, Z) -> Unit
) {
    if (this != null) {
        val iterator1 = this.iterator()
        iterator1.forEach { t ->
            block(t)?.forEach { v ->
                block2(t, v)?.forEach { z ->
                    block3(t, v, z)
                }
            }
        }
    }
}

fun <T, V, Z> ArrayList<T>?.foreachs(
    block: (T) -> ArrayList<V>,
    block2: (V) -> ArrayList<Z>,
    block3: (Z) -> Unit
) {
    if (this != null) {
        val iterator1 = this.iterator()
        iterator1.forEach { t1 ->
            block(t1).forEach {
                block2(it).forEach {
                    block3(it)
                }
            }
        }
    }
}


fun <T, V, Z, M> ArrayList<T>?.foreachs(
    block: (T) -> List<V>?,
    block2: (T, V) -> List<Z>?,
    block3: (T, V, Z) -> List<M>,
    block4: (T, V, Z, M) -> Unit
) {
    if (this != null) {
        val iterator1 = this.iterator()
        iterator1.forEach { t ->
            block(t)?.forEach { v ->
                block2(t, v)?.forEach { z ->
                    block3(t, v, z).forEach { m ->
                        block4(t, v, z, m)
                    }
                }
            }
        }
    }
}


fun <T, V, Z, M, N> ArrayList<T>?.foreachs(
    block: (T) -> List<V>?, block2: (T, V) -> List<Z>?, block3: (T, V, Z) -> List<M>,
    block4: (T, V, Z, M) -> List<N>, block5: (T, V, Z, M, N) -> Unit
) {
    if (this != null) {
        val iterator1 = this.iterator()
        iterator1.forEach { t ->
            block(t)?.forEach { v ->
                block2(t, v)?.forEach { z ->
                    block3(t, v, z).forEach { m ->
                        block4(t, v, z, m).forEach { n ->
                            block5(t, v, z, m, n)
                        }
                    }
                }
            }
        }
    }
}

fun <T, V, Z, X> ArrayList<T>?.foreachs(
    block: (T) -> List<V>,
    block2: (V) -> List<Z>,
    block3: (Z) -> List<X>,
    block4: (X) -> Unit
) {
    if (this != null) {
        val iterator1 = this.iterator()
        iterator1.forEach { t1 ->
            block(t1).forEach {
                block2(it).forEach {
                    block3(it).forEach {
                        block4(it)
                    }
                }
            }
        }
    }
}

fun <T, V> Iterable<T>?.topMapValueList(block: (T) -> V): LinkedHashMap<V, ArrayList<T>>? {
    if (this?.count() ?: 0 > 0) {
        val map = LinkedHashMap<V, ArrayList<T>>()
        this?.forEach {
            var array: ArrayList<T>? = null
            if (map[block(it)] == null) {
                map[block(it)] = ArrayList()
            }
            array = map[block(it)]
            array?.add(it)
        }
        return map
    }
    return null
}

fun <T> Iterable<T>?.topMapValueListeKeys(block: (T) -> Keys): LinkedHashMap<Keys, ArrayList<T>>? {
    if (this?.count() ?: 0 > 0) {
        val map = LinkedHashMap<Keys, ArrayList<T>>()
        val addedKeys = HashMap<String, Keys>()
        this?.forEach { t ->
            val array: java.util.ArrayList<T>?
            var keyBean = block(t)
            val key = keyBean.toString()
            if (addedKeys[key] == null) {
                addedKeys[key] = keyBean
                map[keyBean] = ArrayList()
            } else {
                addedKeys[key]?.let {
                    keyBean = it
                }
            }
            array = map[keyBean]
            array?.add(t)
        }
        return map
    }
    val s = Keys("s")

    return null
}

fun <T> Iterable<T>?.toMapValueList(block: (T) -> Keys): LinkedHashMap<Keys, ArrayList<T>>? {
    if (this?.count() ?: 0 > 0) {
        val map = LinkedHashMap<Keys, ArrayList<T>>()
        val contonsKeys = HashMap<String, ArrayList<T>>()
        this?.forEach {
            val key = block(it).toString()
            if (!contonsKeys.contains(key)) {
                contonsKeys[key] = ArrayList()
                map[block(it)] = contonsKeys[key]!!
            }
            contonsKeys[key]?.add(it)
        }
        return map
    }
    return null
}

fun <T> Iterable<T>?.toMapValueListVs(block: (T) -> KeyVs): LinkedHashMap<KeyVs, ArrayList<T>>? {
    if (this?.count() ?: 0 > 0) {
        val map = LinkedHashMap<KeyVs, ArrayList<T>>()
        val contonsKeys = HashMap<String, ArrayList<T>>()
        this?.forEach {
            val key = block(it).toString()
            if (!contonsKeys.contains(key)) {
                contonsKeys[key] = ArrayList()
                map[block(it)] = contonsKeys[key]!!
            }
            contonsKeys[key]?.add(it)
        }
        return map
    }
    return null
}

data class KeyVs(var key: String, var name: String)
class Keys(vararg params: String) {
    var value = ArrayList<String>()

    init {
        params.forEach {
            value.add(it)
        }
    }

    override fun toString(): String {
        val builder = StringBuilder()
        value.forEach {
            builder.append(it)
        }
        return builder.toString()
    }
}


fun <T> List<T>?.toFirst(): T? {
    return this?.getSF(0)
}


fun <T> ArrayList<T>?.isNotNullAndNoEmpty(): Boolean {
    if (this == null) return false
    if (this.isEmpty()) return false
    return true
}


fun <T, V> List<T>?.isNotNullAndNoEmpty(v: V): V? {
    if (this == null) return null
    if (this.isEmpty()) return null
    return v
}


fun <K, V> Map<K, V>?.toValueMap(): ArrayList<V> {
    val list = ArrayList<V>()
    this?.forEach {
        list.add(it.value)
    }
    return list
}

infix fun Any?.nullBool(block: () -> Unit) {
    if (this == null) {
        block()
    }
}

fun BigDecimal?.toCompareToZERO(): Int {
    return try {
        val i = this!!.compareTo(BigDecimal.ZERO)
        if (i == 1) {
            1
        } else { //num大于0
            -1
        }
    } catch (e: Exception) {
        -1
    }
}

fun <K, V, T> Map<K, V>?.toValueList(block: (K, V) -> T): ArrayList<T>? {
    if (this != null) {
        val container = ArrayList<T>()
        this.forEach {
            container.add(block(it.key, it.value))
        }
        return container
    }
    return null
}


fun <K, V> Map<K, V>?.toValueList(): ArrayList<V>? {
    if (this != null) {
        val container = ArrayList<V>()
        this.forEach {
            if (it.value is Collection<*>) {
                container.addAll(it.value as Collection<V>)
            } else {
                container.add(it.value)
            }
        }
        return container
    }
    return null
}


fun <K, V> Map<K, V>?.toKeyList(): ArrayList<K>? {
    if (this != null) {
        val container = ArrayList<K>()
        this.forEach {
            if (it.key is Collection<*>) {
                container.addAll(it.value as Collection<K>)
            } else {
                container.add(it.key)
            }
        }
        return container
    }
    return null
}

fun Double.keepTwo(): String {
    return try {
        val df = DecimalFormat("0.00")
        df.roundingMode = RoundingMode.FLOOR
        df.format(this)
    } catch (e: Exception) {
        LogUtils.e(e)
        String.format("%.2f", this)
    }
}

fun Double.keepTwoAdd(): String {
    return try {
        val df = DecimalFormat("0.00")
        df.roundingMode = RoundingMode.FLOOR
        if (this > 0.0) {
            "+" + df.format(this)
        } else {
            df.format(this)
        }
    } catch (e: Exception) {
        LogUtils.e(e)
        if (this > 0.0) {
            "+" + String.format("%.2f", this)
        } else {
            String.format("%.2f", this)
        }

    }
}

infix fun String?.eq(param: String?): Boolean {
    return TextUtils.equals(this, param)
}

infix fun Boolean.go(block: () -> Unit): Boolean {
    if (this)
        block()
    return this
}


infix fun Boolean.other(block: () -> Unit): Boolean {
    if (!this)
        block()
    return this
}


infix fun Boolean.and(block: () -> Boolean): Boolean {
    return (this && block())
}

infix fun Boolean.or(block: () -> Boolean): Boolean {
    return (this && block())
}

infix fun String?.uneq(param: String?): Boolean {
    return !TextUtils.equals(this, param)
}

fun String?.subStart(position: Int): String {
    return try {
        val resourceId = this?.substring(0, position) ?: ""
        if (resourceId.startsWith("0")) {
            return resourceId.substring(1)
        }
        return resourceId
    } catch (e: Exception) {
        LogUtils.e(e)
        this ?: ""
    }
}

fun String?.subEnd(position: Int): String {
    return try {
        val resourceId = this?.substring(this.length - position, this.length) ?: ""
        if (resourceId.startsWith("0")) {
            return resourceId.substring(1)
        }
        return resourceId
    } catch (e: Exception) {
        LogUtils.e(e)
        this ?: ""
    }
}


infix fun String?.contained(param: List<String>): Boolean {
    return param.contains(this)
}

fun String?.containses(vararg param: String): Boolean {
    return param.firstOrNull { this?.contains(it) == true } != null
}

fun <T> List<T>?.getSF(index: Int): T? {
    if (this == null) return null
    if (this.size <= index || index < 0) {
        return null
    }
    return this[index]
}

fun <T> ArrayList<T>.removeAtSF(index: Int) {
    try {
        if (this.size > index) {
            this.removeAt(index)
        }
    } catch (e: Exception) {
        LogUtils.e(e)
    }
}

fun <T, V> List<T>?.topMap(block: (T) -> V): LinkedHashMap<V, T>? {
    if (this?.size ?: 0 > 0) {
        val map = LinkedHashMap<V, T>()
        this?.forEach {
            map[block(it)] = it
        }
        return map
    }
    return null
}

fun <T, V> ArrayList<T>?.doubleForeach(
    list2: ArrayList<V>?,
    block: (list1: MutableIterator<T>, list2: MutableIterator<V>, T, V) -> Unit
) {
    if (this != null && list2 != null) {
        val iterator1 = this.iterator()
        val iterator2 = list2.iterator()
        iterator1.forEach { t1 ->
            iterator2.forEach { t2 ->
                block(iterator1, iterator2, t1, t2)
            }
        }
    }
}


fun <T, V> List<T>?.topMapValueList(block: (T) -> V): LinkedHashMap<V, ArrayList<T>>? {
    if (this?.size ?: 0 > 0) {
        val map = LinkedHashMap<V, ArrayList<T>>()
        this?.forEach {
            var array: ArrayList<T>? = map[block(it)]
            if (array == null) {
                map[block(it)] = ArrayList()
            }
            array = map[block(it)]
            array?.add(it)
        }
        return map
    }
    return null
}

fun <K, V> Map<K, V>?.containsKeyOb(any: K): V? {
    val or = md5(gson.toJson(any))
    this?.forEach { map ->
        if (md5(gson.toJson(map.key)) == or) {
            return map.value
        }
    }
    return null
}

fun <T, V> List<T>.toFilterList(block: (T) -> V): List<V>? {
    if (this.isNotEmpty()) {
        val filterList = ArrayList<V>()
        this.forEach {
            filterList.add(block(it))
        }
        return filterList
    }
    return null
}

fun <T, V> List<T>?.toSelectMap(block: (T) -> V): java.util.LinkedHashMap<V, T>? {
    if (this?.size ?: 0 > 0) {
        val map = java.util.LinkedHashMap<V, T>()
        this?.forEach {
            map[block(it)] = it
        }
        return map
    }
    return null
}

fun md5(password: String): String {
    try {
        val instance: MessageDigest = MessageDigest.getInstance("MD5")
        val digest: ByteArray = instance.digest(password.toByteArray())
        val sb = StringBuffer()
        for (b in digest) {
            val i: Int = b.toInt() and 0xff
            var hexString = Integer.toHexString(i)
            if (hexString.length < 2) {
                hexString = "0$hexString"
            }
            sb.append(hexString)
        }
        return sb.toString()

    } catch (e: NoSuchAlgorithmException) {
        LogUtils.e(e)
    }
    return ""
}


fun <K, V> Map<K, List<V>>?.toValueLists(): ArrayList<V> {
    val list = ArrayList<V>()
    this?.forEach {
        list.addAll(it.value)
    }
    return list
}


fun <T> List<T>.split(perSplitSize: Int): ArrayList<ArrayList<T>> {
    val wrapList = ArrayList<ArrayList<T>>()
    var count = 0
    while (count < this.size) {
        wrapList.add(
            ArrayList(
                this.subList(
                    count,
                    if (count + perSplitSize > this.size) this.size else count + perSplitSize
                )
            )
        )
        count += perSplitSize
    }
    return wrapList
}

fun <T> List<T>.isLast(t: T): Boolean {
    return this.getSF(this.size - 1) == this.indexOf(t)
}


fun Double.keepTwoUP(): String {
    return try {
        Locale.setDefault(Locale.CHINA)
        val df = DecimalFormat("0.00")
        df.roundingMode = RoundingMode.HALF_UP
        df.format(this)
    } catch (e: Exception) {
        LogUtils.e(e)
        String.format("%.2f", this)
    }
}


