package com.sports.wallpaper.utils

import android.app.Activity
import android.view.Gravity
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.sports.commonn.mvvm.ui.XmKProgressHUDPop


//loading框
private var mKProgressHUD: XmKProgressHUDPop? = null

/**
 * 打开等待框
 */
fun AppCompatActivity.showLoadingExt(message: String = "请求网络中") {
    if (!this.isFinishing) {
        if (mKProgressHUD?.isShowing == true) {
            return
        }
        if (mKProgressHUD == null || mKProgressHUD!!.context != this) {
            mKProgressHUD = XmKProgressHUDPop(this)
            mKProgressHUD!!.setAlignBackgroundGravity(Gravity.CENTER)
        }
        mKProgressHUD!!.showPopupWindow()
    }
}

/**
 * 打开等待框
 */
fun Fragment.showLoadingExt(message: String = "请求网络中") {
    this.let {
        if (!it.isHidden) {
            if (mKProgressHUD?.isShowing == true) {
                return
            }
            mKProgressHUD = XmKProgressHUDPop(it)
            mKProgressHUD!!.setAlignBackgroundGravity(Gravity.CENTER)
            mKProgressHUD!!.showPopupWindow()
        }
    }
}

/**
 * 关闭等待框
 */
fun Activity.dismissLoadingExt() {
    try {
        mKProgressHUD?.dismiss()
    } catch (e: Exception) {
    }
}

/**
 * 关闭等待框
 */
fun Fragment.dismissLoadingExt() {
    try {
        mKProgressHUD?.dismiss()
    } catch (e: Exception) {
    }
}
