package com.sports.commonn.utils

import android.content.Context
import android.content.MutableContextWrapper
import android.view.ViewGroup
import com.sports.commonn.mvvm.ui.CacheSupportAble

/**
 *Create By Albert on 2020/12/29
 */
object CacheSupportUtils {


    @JvmStatic
    fun changeContextChild(viewG: ViewGroup, isAttuch: Boolean, context: Context) {
        if (viewG is CacheSupportAble) {
            if (viewG.context is MutableContextWrapper) {
                if (isAttuch) {
                    (viewG.context as MutableContextWrapper).baseContext = (context)
                } else {
                    (viewG.context as MutableContextWrapper).baseContext = (context)
                }
            }
        }
        for (i in 0 until viewG.childCount) {
            val childAt = viewG.getChildAt(i)
            if (childAt is ViewGroup) {
                if (childAt is CacheSupportAble) {
                    if (isAttuch) {
                        childAt.attachContext(context)
                    } else {
                        childAt.disAttachContext(context)
                    }
                }
            }
            if (childAt.context is MutableContextWrapper) {
                if (isAttuch) {
                    (childAt.context as MutableContextWrapper).baseContext = (context)
                } else {
                    (childAt.context as MutableContextWrapper).baseContext = (context)
                }
            }
        }
    }
}