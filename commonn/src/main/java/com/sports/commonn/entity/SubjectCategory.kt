package com.sports.commonn.entity

import com.sports.commonn.memory.DataManager

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:专题分类
 *
 **/
class SubjectCategory {
    /**
     * {
    "id": 1,
    "category_name": "实况动图",
    "is_type": 0,
    "sort": 0,
    "created_at": "2021-03-22T03:40:24.000000Z",
    "updated_at": "2021-03-22T03:40:24.000000Z",
    "img_url": null
    }
     */
    var id: Int? = null
    var category_name: String? = null
    var img_url: String? = null
        get() {
            if (field?.isNotEmpty() == true) {
                return DataManager.imgServerHost + field
            }
            return field
        }
}