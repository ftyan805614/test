package com.sports.commonn.entity

import android.os.Parcelable
import com.sports.commonn.memory.DataManager
import kotlinx.android.parcel.Parcelize

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
@Parcelize
class ImgInfo : Parcelable {
    /**
     *  {
    "id": 94,
    "subject_id": 3,
    "url": "/upload/08322b3478e132fe75ed9b498b698fea.jpeg",
    "sort": 1,
    "created_at": "2021-03-24T02:27:43.000000Z",
    "updated_at": "2021-03-24T02:27:43.000000Z"
    }
     */

    var id: Int? = null

    var img_id: Int? = null

    var subject_id: Int? = null

    var url: String? = null

    var fullUrl: String? = null
        get() {
            if (url?.isNotEmpty() == true) {
                return DataManager.imgServerHost + url
            }
            return field
        }

    var sort: Int? = null

    var created_at: String? = null
}