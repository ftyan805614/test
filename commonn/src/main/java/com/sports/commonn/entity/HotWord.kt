package com.sports.commonn.entity

/**
 * author:Averson
 * createTime:2021/3/31
 * descriptions:
 *
 **/
class HotWord {
    /**
     * {
    "id": 4,
    "created_at": "2021-03-24T04:00:04.000000Z",
    "updated_at": "2021-03-29T15:00:24.000000Z",
    "search": "足球",
    "amount": 51
    }
     */
    var id: Int? = null
    var search: String? = null
    var amount: Int? = null
}