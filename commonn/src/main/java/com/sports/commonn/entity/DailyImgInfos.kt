package com.sports.commonn.entity

import android.text.TextUtils
import com.sports.commonn.utils.DateUtil

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class DailyImgInfos {
    var data: ArrayList<CategoryWallPaperInfo>? = null

    class CategoryWallPaperInfo {
        /**
         * {
        "id":3,
        "subject_category_id":1,
        "subject_name":"足球2",
        "remark":"地方",
        "user_name":"2021-03-24",
        "issue":344,
        "created_at":"2021-03-24T02:00:59.000000Z",
        "updated_at":"2021-03-24T02:00:59.000000Z",
        "img":[
        {
        "id":94,
        "subject_id":3,
        "url":"/upload/08322b3478e132fe75ed9b498b698fea.jpeg",
        "sort":1,
        "created_at":"2021-03-24T02:27:43.000000Z",
        "updated_at":"2021-03-24T02:27:43.000000Z"
        }
        ]
        }
         */
        var id: Int? = null
        var subject_category_id: Int? = null
        var subject_name: String? = null
        var remark: String? = null
        var user_name: String? = null
        var issue: String? = null
        var created_at: String? = null
            get() {
                if (!TextUtils.isEmpty(field)) {
                    return DateUtil.getFormatDate(
                        field,
                        DateUtil.FormatPatter1,
                        DateUtil.FormatPatter2
                    )
                }
                return field
            }
        var updated_at: String? = null
        var img: ArrayList<ImgInfo>? = null
        var data: ArrayList<ImgInfo>? = null
    }
}