package com.sports.commonn.entity

import com.sports.commonn.Constant

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class BaseEntity<T> {
    var data: T? = null

    var message: String? = null

    var code: Int? = null

    var success: Boolean? = null
        get() {
            if (field == null) {
                return false
            }
            return field
        }

    fun isSuc(): Boolean {
        return code == Constant.SUCCESS_CODE && (success ?: false)
    }
}