package com.sports.commonn.entity

import org.litepal.annotation.Column
import org.litepal.crud.LitePalSupport

/**
 * author:Averson
 * createTime:2021/3/31
 * descriptions:
 *
 **/
class DownloadRecord : LitePalSupport() {

    val url: String? = null

    var createTiem: Long? = null

    var updateTime: Long? = null

    @Column(unique = true, defaultValue = "0")
    var imgId: Int? = null

    @Column(defaultValue = "0")
    var status: Int = 0 // 0下载中 1 下载完成

    var localPath: String? = null
}