package com.sports.commonn.entity.request

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class RegisterReq {
    var mobile: String? = null
    var password: String? = null
    var user_name: String? = null
}