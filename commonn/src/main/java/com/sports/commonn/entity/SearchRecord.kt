package com.sports.commonn.entity

import org.litepal.annotation.Column
import org.litepal.crud.LitePalSupport


/**
 * author:Averson
 * createTime:2021/3/31
 * descriptions:
 *
 **/
class SearchRecord : LitePalSupport() {

    @Column(unique = true, defaultValue = "unknown")
    var searchName: String? = null

    var createTime: Long? = null

    var updateTime: Long? = null

    var times: Int = 0
}
