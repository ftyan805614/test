package com.sports.commonn.entity

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class UserInfo {
    var id: Int? = null

    var token: String? = null

    var username: String? = null

    var avatar: String? = null

    var phone: String? = null
}