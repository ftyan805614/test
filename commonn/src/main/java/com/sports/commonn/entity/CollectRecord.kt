package com.sports.commonn.entity

import org.litepal.annotation.Column
import org.litepal.crud.LitePalSupport

/**
 * author:Averson
 * createTime:2021/3/31
 * descriptions:
 *
 **/
class CollectRecord : LitePalSupport() {

    var url: String? = null

    var createTiem: Long? = null

    var updateTime: Long? = null

    @Column(unique = true, defaultValue = "0")
    var imgId: Int? = null

    @Column(defaultValue = "0")
    var status: Int = 0 //0未收藏 1已收藏 2收藏中
}