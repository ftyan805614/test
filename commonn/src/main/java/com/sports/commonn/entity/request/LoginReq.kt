package com.sports.commonn.entity.request

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
class LoginReq {
    var mobile: String? = null
    var password: String? = null
}