package com.sports.commonn.api

import com.sports.commonn.entity.*
import com.sports.commonn.entity.request.LoginReq
import com.sports.commonn.entity.request.RegisterReq
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.http.*

/**
 * author:Averson
 * createTime:2021/3/30
 * descriptions:
 *
 **/
interface Api {

    /**
     * 登录
     *
     * @return
     */
    @POST("/api/login")
    fun login(
        @Body params: LoginReq
    ): Observable<BaseEntity<Any>>

    /**
     * 注册
     *
     * @return
     */
    @POST("/api/register")
    fun register(
        @Body params: RegisterReq
    ): Observable<BaseEntity<Any>>

    /**
     * 按天专题列表
     *
     */
    @POST("/api/subjectdaylist")
    fun getDailyWallPaper(): Observable<BaseEntity<DailyImgInfos>>

    /**
     * 专题列表
     *
     */
    @FormUrlEncoded
    @POST("/api/subjectcategorylist")
    fun getSubjectCategoryList(@Field("is_type") isType: String): Observable<BaseEntity<List<SubjectCategory>>>

    /**
     * 按天专题列表
     *
     */
    @FormUrlEncoded
    @POST("/api/subjectimglist")
    fun getCategoryWallPaperList(@Field("subject_category_id") categoryId: Int): Observable<BaseEntity<DailyImgInfos.CategoryWallPaperInfo>>

    /**
     * 热门搜索
     *
     */
    @POST("/api/hotsearch")
    fun getHotSearchs(): Observable<BaseEntity<HotSearchs>>

    /**
     * 按天专题列表
     *
     */
    @FormUrlEncoded
    @POST("/api/subjectsearch")
    fun getSearchResultByWord(@Field("search") search: String): Observable<BaseEntity<DailyImgInfos.CategoryWallPaperInfo>>


    /**
     * 下载
     *
     * @return
     */
    @GET("/api/down")
    fun canDownImage(
        @Query("img_id") img_id: Int
    ): Observable<BaseEntity<Any>>

    /**
     * 下载列表
     *
     */
    @POST("/api/downlist")
    fun getDownlist(): Observable<BaseEntity<DailyImgInfos.CategoryWallPaperInfo>>

    /**
     * 收藏
     *
     * @return
     */
    @POST("/api/collectionstatus")
    fun doCollect(
        @Query("img_id") img_id: Int
    ): Observable<BaseEntity<Any>>

    /**
     * 收藏列表
     *
     */
    @POST("/api/collectionlist")
    fun getCollectionlist(): Observable<BaseEntity<DailyImgInfos.CategoryWallPaperInfo>>

    /**
     * 发送验证码
     *
     * @return
     */
    @POST("/api/sendsms")
    fun sendSms(
        @Query("mobile") mobile: String?
    ): Observable<BaseEntity<Any>>

    /**
     * 验证码核对
     *
     * @return
     */
    @POST("/api/checksms")
    fun checkSms(
        @Query("mobile") mobile: String?, @Query("code") code: String?
    ): Observable<BaseEntity<Any>>

    /**
     * 修改密码
     *
     * @return
     */
    @POST("/api/forgetpassword")
    fun resetPassword(
        @Query("mobile") mobile: String?, @Query("password") password: String?
    ): Observable<BaseEntity<Any>>

    /**
     * 退出登录
     *
     * @return
     */
    @POST("/api/logout")
    fun logout(): Observable<BaseEntity<Any>>

    /**
     * 初始化
     *
     * @return
     */
    @POST("/api/init")
    fun init(): Observable<BaseEntity<ImgServerInfo>>

    /**
     * 更新用户名
     *
     * @return
     */
    @POST("/api/updateusername")
    fun updateUsername(
        @Query("user_name") user_name: String?
    ): Observable<BaseEntity<Any>>

    @Multipart
    @POST("/api/uploadimg")
    fun uploadAvatar(@Part imgs: MultipartBody.Part?): Observable<AvatarInfo>
}