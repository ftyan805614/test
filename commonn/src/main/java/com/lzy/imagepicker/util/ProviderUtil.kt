package com.lzy.imagepicker.util

import com.sports.commonn.YaboLib

/**
 * 用于解决provider冲突的util
 *
 *
 * Author: nanchen
 * Email: liushilin520@foxmail.com
 * Date: 2017-03-22  18:55
 */
object ProviderUtil {
    @JvmStatic
    val fileProviderName: String
        get() {
            return YaboLib.realPackageName + ".fileProvier"
        }
}