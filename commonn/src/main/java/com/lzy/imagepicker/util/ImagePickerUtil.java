package com.lzy.imagepicker.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.loader.GlideImageLoader;
import com.lzy.imagepicker.loader.ImageLoader;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.lzy.imagepicker.view.CropImageView;

import java.util.ArrayList;

/**
 * 图片选择工具类
 * Created by jane on 2018/12/11.
 */
public class ImagePickerUtil {

    public static void tackPhoto(@Nullable Context context, ArrayList<ImageItem> images) {
        Intent intent = new Intent(context, ImageGridActivity.class);
        intent.putExtra(ImageGridActivity.EXTRAS_IMAGES, images);
        //ImagePicker.getInstance().setSelectedImages(images);
        if (context instanceof Activity) {
            ((Activity) context).startActivityForResult(intent, 0x1000);
        }

    }

    public static void initPhoto(int maxSize) {
        ImagePicker imagePicker = ImagePicker.getInstance();
        imagePicker.setImageLoader((ImageLoader) new GlideImageLoader());   //设置图片加载器
        imagePicker.setShowCamera(true);  //显示拍照按钮
        imagePicker.setMultiMode(false);
        imagePicker.setCrop(true);        //允许裁剪（单选才有效）
        imagePicker.setSaveRectangle(true); //是否按矩形区域保存
        imagePicker.setStyle(CropImageView.Style.CIRCLE);  //裁剪框的形状
        imagePicker.setFocusWidth(maxSize);   //裁剪框的宽度。单位像素（圆形自动取宽高最小值）
        imagePicker.setFocusHeight(maxSize);  //裁剪框的高度。单位像素（圆形自动取宽高最小值）
        imagePicker.setOutPutX(maxSize);//保存文件的宽度。单位像素
        imagePicker.setOutPutY(maxSize);//保存文件的高度。单位像素
    }

    /**
     * 接收图片
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Nullable
    public static ArrayList<ImageItem> onActivityResult(@Nullable Context context, int requestCode, int resultCode, Intent data) {
        ArrayList<ImageItem> imageItems = null;
        if (onActivityResultPickerPhoto(resultCode)) {
            if (requestCode == 0x1000) {
                if (data != null) {
                    try {
                        imageItems = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "选择图片失败", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "选择图片失败", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return imageItems;
    }

    public static boolean onActivityResultPickerPhoto(int resultCode) {
        return resultCode == ImagePicker.RESULT_CODE_ITEMS;
    }
}
