package androidx.fragment.app

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.sports.commonn.mvvm.ui.BaseMvvmFragment
import java.lang.reflect.Field

/**
 *Create By Albert on 2020/8/13
 */
abstract class BaseAsynInflaterFragment<T : ViewDataBinding> : BaseMvvmFragment<T>() {

    open val delay = 0L

    private val hanlder = Handler()

    override fun callOnCreateView(
            paramLayoutInflater: LayoutInflater,
            paramViewGroup: ViewGroup?,
            paramBundle: Bundle?
    ): View? {
        if (paramBundle == null) {
            AsyncLayoutInflater(requireContext()).inflate(contentViewLayoutId(), paramViewGroup) { view, resid, parent ->
                if (delay <= 0) {
                    createView(view, paramViewGroup, paramBundle)
                } else {
                    hanlder.postDelayed({
                        createView(view, paramViewGroup, paramBundle)
                        applySkinTheme()
                    }, delay)
                }

            }
            return null
        } else {
            return super.callOnCreateView(paramLayoutInflater, paramViewGroup, paramBundle)
        }

    }

    private fun createView(view: View, paramViewGroup: ViewGroup?, paramBundle: Bundle?) {
        if (!isAdded) return
        if (activity == null) return
        if (activity?.isFinishing == true) return
        if (activity?.isDestroyed == true) return
        if (view.parent is ViewGroup) {
            (view.parent as ViewGroup).removeView(view)
        }
        paramViewGroup?.addView(view)
        getFragmentContetentViewField()?.set(this, view)
        val owner = FragmentViewLifecycleOwner()
        owner.initialize()
        getViewLifecycleOwnerField()?.set(this, owner)
        (getLifecycleOwnerLiveDataField()?.get(this) as? MutableLiveData<LifecycleOwner>)?.value = owner
        bindRootView(view)
        onViewCreated(view, paramBundle)
    }

    companion object {

        private var fragmentViewField: Field? = null
        private var mViewLifecycleOwnerField: Field? = null
        private var mViewLifecycleOwnerLiveDataField: Field? = null

        private fun getFragmentContetentViewField(): Field? {
            if (fragmentViewField == null) {
                try {
                    fragmentViewField = Fragment::class.java.getDeclaredField("mView")
                    fragmentViewField?.isAccessible = true
                } catch (e: Exception) {

                }
            }
            return fragmentViewField
        }

        private fun getViewLifecycleOwnerField(): Field? {
            if (mViewLifecycleOwnerField == null) {
                try {
                    mViewLifecycleOwnerField = Fragment::class.java.getDeclaredField("mViewLifecycleOwner")
                    mViewLifecycleOwnerField?.isAccessible = true
                } catch (e: Exception) {

                }
            }
            return mViewLifecycleOwnerField
        }

        private fun getLifecycleOwnerLiveDataField(): Field? {
            if (mViewLifecycleOwnerLiveDataField == null) {
                try {
                    mViewLifecycleOwnerLiveDataField = Fragment::class.java.getDeclaredField("mViewLifecycleOwnerLiveData")
                    mViewLifecycleOwnerLiveDataField?.isAccessible = true
                } catch (e: Exception) {

                }
            }
            return mViewLifecycleOwnerLiveDataField
        }

    }


}