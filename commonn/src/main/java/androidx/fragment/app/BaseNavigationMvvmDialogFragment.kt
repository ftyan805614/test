package androidx.fragment.app

import androidx.databinding.ViewDataBinding
import androidx.navigation.fragment.findNavController
import com.sports.commonn.mvvm.popBackStackDialog


/**
 *created by Albert
 */
abstract class BaseNavigationMvvmDialogFragment<T : ViewDataBinding> : BaseMvvmDialogFragment<T>() {


    /**
     * 特别注意 如果要使用
     * @see com.bob.mvvm.NavigationExtKt.popBackStackDialog 结束弹窗，
     * 那么打开此弹窗就一定要使用
     * @see androidx.navigation.NavController.navigate 方式
     * */
    @Deprecated("", ReplaceWith("findNavController().popBackStack()"))
    override fun dismiss() {
        super.dismiss()
    }

    /**
     * 特别注意 如果要使用
     * @see androidx.navigation.NavController.navigate 打开弹窗，
     * 那么关闭此弹窗就一定要使用
     * @see com.bob.mvvm.NavigationExtKt.popBackStackDialog 方式
     * */
    @Deprecated("", ReplaceWith("findNavController().navigateOnlyDialog()"))
    override fun show(manager: FragmentManager, tag: String?) {
        super.show(manager, tag)
    }

    /**
     * 特别注意 如果要使用
     * @see androidx.navigation.NavController.navigate 打开弹窗，
     * 那么关闭此弹窗就一定要使用
     * @see com.bob.mvvm.NavigationExtKt.popBackStackDialog 方式
     * */
    @Deprecated("", ReplaceWith("findNavController().navigateOnlyDialog()"))
    override fun showSF(host: FragmentActivity?) {
        super.showSF(host)
    }

    override fun onTouchOutDismiss() {
        findNavController().popBackStackDialog()
    }
}
