package androidx.fragment.app

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.MutableContextWrapper
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.apkfuns.logutils.LogUtils
import com.sports.commonn.R
import com.sports.commonn.mvvm.activityshare.ViewModelShareProvider
import com.sports.commonn.mvvm.ui.BaseShareViewModel
import com.sports.commonn.mvvm.ui.BaseViewModel
import com.sports.commonn.mvvm.ui.ViewModelFactory
import com.sports.commonn.utils.ResUtils
import com.sports.commonn.utils.ScreenUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import java.lang.ref.WeakReference


/**
 *created by Albert
 */
abstract class BaseMvvmDialogFragment<T : ViewDataBinding> : DialogFragment(), CoroutineScope by MainScope() {

    lateinit var bind: T
    private var isInitView: Boolean = false
    private var backgroundColor = Color.parseColor("#80000000")
    private var compositeDisposable: CompositeDisposable? = null
    private var dismissListener: DialogInterface.OnDismissListener? = null
    private val dismissListeners = ArrayList<DialogInterface.OnDismissListener>()
    private var isDestroyed = false
    var rootView: View? = null

    //点击对话框关闭
    open val touchOutCloaseable = false

    open val drawInDisplayCutoutMode = true

    open val drawInStatusBar = true

    private var handlder: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.BobDialog)
        compositeDisposable = CompositeDisposable()
        try {
            val field = DialogFragment::class.java.getDeclaredField("mHandler")
            field.isAccessible = true
            handlder = field.get(this) as? Handler
        } catch (e: Exception) {

        }

    }

    override fun onCreateView(
        paramLayoutInflater: LayoutInflater,
        paramViewGroup: ViewGroup?,
        paramBundle: Bundle?
    ): View? {
        rootView = LayoutInflater.from(MutableContextWrapper(context)).inflate(contentViewLayoutId(), paramViewGroup, false)
        if (rootView != null) {
            try {
                val bindT = DataBindingUtil.bind<T>(rootView!!)
                if (bindT != null) {
                    bind = bindT
                    bind.lifecycleOwner = this
                }
            } catch (e: Exception) {

            }
        }
        return rootView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (this::bind.isInitialized) {
            initViewModel(bind)
        }
        initView()
        isInitView = true
        initObserve()
        initData()
    }

    protected abstract fun contentViewLayoutId(): Int

    protected abstract fun initViewModel(bind: T)

    open fun initView() {

    }

    open fun initObserve() {

    }

    open fun initData() {

    }


    fun <V : ViewModel?> ofScopeFragment(t: Class<V>): V {
        return ofScope(this, this, t)
    }


    fun <V : ViewModel?> ofScopeActivity(t: Class<V>): V {
        return ofScope(requireActivity(), requireActivity(), t)
    }

    fun <V : ViewModel?> ofScope(container: Any, lifecycleOwner: LifecycleOwner, t: Class<V>): V {
        val v = when (container) {
            is FragmentActivity -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t) as V
            }
            is Fragment -> {
                ViewModelProviders.of(container, ViewModelFactory(lifecycleOwner)).get(t)
            }
            else -> {
                ViewModelProviders.of(this, ViewModelFactory(lifecycleOwner)).get(t)
            }
        }
        if (v is BaseViewModel) {
            lifecycle.addObserver(v as BaseViewModel)
            (v as BaseViewModel).attouchLifecycleOwner(this)
        }
        return v
    }


    fun <V : BaseShareViewModel?> ofScopeShareActivity(t: Class<V>): V {
        return ofScopeShare(activity as FragmentActivity, activity as FragmentActivity, t)
    }

    fun <V : BaseShareViewModel?> ofScopeShareFragment(t: Class<V>): V {
        return ofScopeShare(this, this, t)
    }

    fun <V : BaseShareViewModel?> ofScopeShare(container: LifecycleOwner, lifecycleOwner: LifecycleOwner, t: Class<V>): V {
        return ViewModelShareProvider.ofGet(container, t, lifecycleOwner)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return WeakDialog(requireContext(), theme)
    }

    @SuppressLint("ObsoleteSdkInt")
    override fun onStart() {
        super.onStart()
        val dialogWindow = dialog?.window
        dialogWindow?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        dialog?.setOnDismissListener {
            dismiss()
        }
        if (null != dialogWindow && crossStart()) {
            val lp = dialogWindow.attributes
            lp.width = ScreenUtil.screenWidth
            lp.height = ScreenUtil.screenHeight
            rootView?.background = ResUtils.getDrawable(backgroundColor)
            lp.dimAmount = 0f
            dialogWindow.attributes = lp
            dialogWindow.setBackgroundDrawableResource(android.R.color.transparent)
        }
        if (touchOutCloaseable) {
            rootView?.setOnClickListener { onTouchOutDismiss() }
        }
        if (drawInStatusBar){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                dialog?.window?.statusBarColor = Color.TRANSPARENT
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                dialogWindow?.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            }
        }
        if (Build.VERSION.SDK_INT >= 28 && drawInDisplayCutoutMode) {
            val lp = dialog?.window?.attributes
            lp?.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
            dialog?.window?.attributes = lp
        }
    }

    open fun onTouchOutDismiss() {
        dismiss()
    }

    open fun crossStart(): Boolean = false


    fun setBackgroundColor(backgroundColor: Int) {
        this.backgroundColor = backgroundColor
    }

    open fun showSF(host: FragmentActivity?) {
        try {
            val manager = host?.supportFragmentManager ?: return
            if (!host.isDestroyed) show(manager, "")
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }

    open fun showSF(host: FragmentActivity?, tag: String? = "") {
        try {
            val manager = host?.supportFragmentManager ?: return
            if (!host.isDestroyed) show(manager, tag)
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }

    open fun showSF(
        host: FragmentActivity?,
        manager: FragmentManager? = host?.supportFragmentManager
    ) {
        try {
            if (manager == null) return
            if (host != null && !host.isDestroyed) show(manager, "")
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }

    open fun showSF(
        host: FragmentActivity?,
        manager: FragmentManager? = host?.supportFragmentManager,
        tag: String? = ""
    ) {
        try {
            if (manager == null) return
            if (host != null && !host.isDestroyed) show(manager, tag)
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }


    override fun show(manager: FragmentManager, tag: String?) {
        try {
            if (dialog?.isShowing == true) return
            if (isAdded) return
            //在每个add事务前增加一个remove事务，防止连续的add
            manager.beginTransaction().remove(this).commitAllowingStateLoss()
            super.show(manager, tag)
        } catch (e: Exception) {
            //同一实例使用不同的tag会异常,这里捕获一下
            e.printStackTrace()
        }

    }

    fun addDisposable(diaspoable: Disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable?.add(diaspoable)
    }

    private fun clearDisposable() {
        compositeDisposable?.clear()
    }


    override fun dismiss() {
        if (fragmentManager != null) {
            super.dismissAllowingStateLoss()
            //region 防止dialog 没有及时释放
            //如有疑惑，见： https://www.jianshu.com/p/0d59f9f4946c
            //https://stackoverflow.com/questions/54759425/bottomsheetdialogfragment-memory-leak-use-leakcanary
            dismissListeners.forEach { listener ->
                listener.onDismiss(this.dialog)
            }
            dismissListeners.clear()
            handlder?.post { }
            handlder?.sendEmptyMessage(0)
            handlder?.removeCallbacksAndMessages(null)
            //endregion
        }
    }

    fun addDismiessListener(dismissListener: DialogInterface.OnDismissListener) {
        dismissListeners.add(dismissListener)
    }

    override fun onPause() {
        super.onPause()
        if (activity?.isFinishing == true) {
            resDestroy()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isDestroyed) {
            resDestroy()
        }

    }

    open fun resDestroy() {
        isDestroyed = true
        cancel()
        clearDisposable()
        if (this::bind.isInitialized) {
            bind.unbind()
        }
        rootView = null
    }

    class WeakDialog : Dialog {

        constructor(@NonNull context: Context?) : super(context!!) {}

        constructor(@NonNull context: Context?, themeResId: Int) : super(context!!, themeResId) {}

        protected constructor(@NonNull context: Context?, cancelable: Boolean, @Nullable cancelListener: DialogInterface.OnCancelListener?) : super(context!!, cancelable, cancelListener) {}

        override fun setOnCancelListener(listener: DialogInterface.OnCancelListener?) {
            super.setOnCancelListener(WeakOnCancelListener(listener))
        }

        override fun setOnDismissListener(listener: DialogInterface.OnDismissListener?) {
            super.setOnDismissListener(WeakOnDismissListener(listener))
        }

        override fun setOnShowListener(listener: DialogInterface.OnShowListener?) {
            super.setOnShowListener(WeakOnShowListener(listener))
        }


        class WeakOnCancelListener(real: DialogInterface.OnCancelListener?) : DialogInterface.OnCancelListener {

            private var mRef: WeakReference<DialogInterface.OnCancelListener?> = WeakReference(real)

            override fun onCancel(dialog: DialogInterface) {
                mRef.get()?.onCancel(dialog)
            }

        }

        class WeakOnDismissListener(real: DialogInterface.OnDismissListener?) : DialogInterface.OnDismissListener {

            private var mRef: WeakReference<DialogInterface.OnDismissListener?> = WeakReference(real)
            override fun onDismiss(dialog: DialogInterface?) {
                mRef.get()?.onDismiss(dialog)
            }


        }

        class WeakOnShowListener(real: DialogInterface.OnShowListener?) : DialogInterface.OnShowListener {

            private var mRef: WeakReference<DialogInterface.OnShowListener?> = WeakReference(real)

            override fun onShow(dialog: DialogInterface?) {
                mRef.get()?.onShow(dialog)
            }


        }


    }

}
